<?php
/**
 * 3x 4x Oney Module version 1.1.21 for PrestaShop 1.6 and PrestaShop 1.7. Support contact : prestashop@itroom.fr
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/afl-3.0.php
 *
 * @author    ITroom (http://itroom.fr/)
 * @copyright ITroom
 * @license   https://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * @category  payment
 * @package   oney
 */

require_once($_SERVER['DOCUMENT_ROOT'] . '/config/config.inc.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/init.php');

if (Tools::getValue('token', 0) != '5C95E717753C656DB7C76C9747EBF') {
    die();
}

/** @var Oney $facilypay */
$facilypay = Module::getInstanceByName('oney');
$priceSimulation = Tools::getValue('price', 1500);
$businessTransactionCode = Tools::getValue('code', 'DV001');
$context = Context::getContext();

$bDomTom = Oney::getConfigByCountry('dom_tom', $facilypay->sIsoCodeCountry);

if ($bDomTom) {
    $identifiantFacilyPay = Configuration::get('FACILYPAY_MERCHANT_GUID_' .
    Oney::getConfigByCountry('country_reference', $facilypay->sIsoCodeCountry));
} else {
    $identifiantFacilyPay = Configuration::get('FACILYPAY_MERCHANT_GUID_' . $facilypay->sIsoCodeCountry);
}

if (!$identifiantFacilyPay) {
    return;
}

if (Tools::getValue('updeCodeOpc', false) && !Tools::getValue('inProduct', true)) {
    if ($bDomTom) {
        $getListOpc = $facilypay->opc->getListOpc(
            Oney::getConfigByCountry(
                'country_reference',
                $facilypay->sIsoCodeCountry
            ),
            1,
            $priceSimulation
        );
    } else {
        $getListOpc = $facilypay->opc->getListOpc($facilypay->sIsoCodeCountry, 1, $priceSimulation);
    }

    // $bHasFreeOpc = false;
    // foreach ($getListOpc as $opc) {
    //     if ($opc['free_business_transaction'] == 1) {
    //         $bHasFreeOpc = true;
    //     }
    // }
    $getInfoListOpc = array();
    $businnessCode = [];
    foreach ($getListOpc as $opc) {
        // if ($bHasFreeOpc && $opc['free_business_transaction'] == 1) {
        //     $businnessCode[$opc['minimum_number_of_instalments']] = $opc['business_transaction_code'];
        // } elseif (!$bHasFreeOpc && $opc['free_business_transaction'] == 0) {
            $businnessCode[$opc['minimum_number_of_instalments']] = $opc['business_transaction_code'];
        // }
    }
    $getInfoListOpc = $facilypay->getInfoListOpc();
    foreach ($getInfoListOpc['listOpc'] as $key => $opc) {
        if (!in_array($opc['business_transaction_code'], $businnessCode)) {
            // unset($getInfoListOpc['listOpc'][$key]);
        }
    }

    echo json_encode($businnessCode);
    die();
}

$facilypay->simulation = '';

/* Paramètre et simulation ( ou cache de la simulation ) */
$paramsSimulation = array(
    'business_transaction_code' => $businessTransactionCode,
    'pricesimulation' => $priceSimulation
);

if ($bDomTom) {
    $simulation = $facilypay->getSimulationFinancement(
        $paramsSimulation,
        Oney::getConfigByCountry('country_reference', $facilypay->sIsoCodeCountry)
    );
} else {
    $simulation = $facilypay->getSimulationFinancement($paramsSimulation, $facilypay->sIsoCodeCountry);
}
if (!$simulation) {
    return;
}

$instalmentAmount = 0;
foreach ($simulation->instalments as $instalment) {
    if ($instalmentAmount < $instalment->instalment_amount) {
        $instalmentAmount = $instalment->instalment_amount;
    }
}

/* Assignation de la simulation / liste / mentions légales / price / urlSimulation */
$context->smarty->assign(
    array(
        'module_dir' => '/modules/oney/',
        'simulation' => $simulation,
        'listOPC' => $facilypay->getListOpc($priceSimulation),
        // 'legalNotice' => $facilypay->getMentionLegale('P', $facilypay->sIsoCodeCountry),
        'priceProduct' => $priceSimulation,
        'priceInstalments' => $instalmentAmount,
        'inProduct' => (bool)Tools::getValue('inProduct', false),
        'base_uri' => __PS_BASE_URI__
    )
);

if ($facilypay->sIsoCodeCountry != 'FR' && !$bDomTom) {
    $file = _PS_THEME_DIR_ . 'modules/' . $facilypay->name .
        '/views/templates/hook/international/simulation-product-ajax.tpl';
    if (file_exists($file)) {
        $simulation_info = $context->smarty->fetch($file);
    } else {
        $file = _PS_MODULE_DIR_ . $facilypay->name .
            '/views/templates/hook/international/simulation-product-ajax.tpl';
        $simulation_info = $context->smarty->fetch($file);
    }
    $file = _PS_THEME_DIR_ . 'modules/' . $facilypay->name .
        '/views/templates/hook/international/simulation-cart-ajax.tpl';
    if (file_exists($file)) {
        $shopping_info = $context->smarty->fetch($file);
    } else {
        $file = _PS_MODULE_DIR_ . $facilypay->name .
            '/views/templates/hook/international/simulation-cart-ajax.tpl';
        $shopping_info = $context->smarty->fetch($file);
    }
} else {
    $file = _PS_THEME_DIR_ . 'modules/' . $facilypay->name .
        '/views/templates/hook/france/simulation-product-ajax.tpl';
    if (file_exists($file)) {
        $simulation_info = $context->smarty->fetch($file);
    } else {
        $file = _PS_MODULE_DIR_ . $facilypay->name . '/views/templates/hook/france/simulation-product-ajax.tpl';
        $simulation_info = $context->smarty->fetch($file);
    }
    $file = _PS_THEME_DIR_ . 'modules/' . $facilypay->name . '/views/templates/hook/france/simulation-cart-ajax.tpl';
    if (file_exists($file)) {
        $shopping_info = $context->smarty->fetch($file);
    } else {
        $file = _PS_MODULE_DIR_ . $facilypay->name . '/views/templates/hook/france/simulation-cart-ajax.tpl';
        $shopping_info = $context->smarty->fetch($file);
    }
}

echo json_encode(
    array(
        'mensualite' => count($simulation->instalments) + 1,
        'simulation_info' => $simulation_info,
        'shopping_info' => $shopping_info,
    )
);
die();
