<?php
/**
 * 3x 4x Oney Module version 1.1.21 for PrestaShop 1.6 and PrestaShop 1.7. Support contact : prestashop@itroom.fr
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/afl-3.0.php
 *
 * @author    ITroom (http://itroom.fr/)
 * @copyright ITroom
 * @license   https://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * @category  payment
 * @package   oney
 */

class PepsWebService
{

    private $pepsErrorHttpCodes = array(210, 400, 401, 403, 422, 404, 500);
    private $url;

    public $correspondance_encode = array(
        '"' => '#',
        '.' => '-'
    );

    public function __construct($production = 1)
    {
        $this->url = $production ? 'https://api.oney.io' : 'https://oney-staging.azure-api.net/staging';
    }

    /**
     * Fonction chargée de requêter le webservice PEPS Oney afin de recupéré la
     * liste des offres de financement disponibles.
     *
     * @param array $parametres
     * @param $sIsoCodeCountry
     * @return \stdClass Le json décodé renvoyé par le web service PEPS.
     */
    public function listerOperationsCommerciales(array $parametres, $sIsoCodeCountry)
    {
        $url = $this->url . '/product_catalog/v1/business_transaction?' . http_build_query($parametres);

        if (!$reponse = $this->getCache($url)) {
            try {
                $reponse = $this->providePepsResponse(
                    $url,
                    Configuration::get('FACILIPAY_API_KEY_MARKETING_' . $sIsoCodeCountry),
                    $sIsoCodeCountry
                );
                $reponse = $reponse['body'];
            } catch (Exception $e) {
                if (_PS_MODE_DEV_) {
                    echo $e->getMessage();
                }
            }
        }

        if (!$reponse) {
            return;
        }

        return json_decode($reponse);
    }

    /**
     * Fonction chargée de requêter le webservice PEPS Oney afin de recupéré une
     * simulation de financement.
     *
     * @param array $parametres
     * @return \stdClass   Le json décodé renvoyé par le web service PEPS.
     */
    public function simulerFinancement(array $parametres, $sIsoCodeCountry)
    {
        $url = $this->url . '/sale_support_tools/v1/simulation?' . http_build_query($parametres);
        $reponse = array();
        if (!$reponse = $this->getCache($url)) {
            try {
                $reponse = $this->providePepsResponse(
                    $url,
                    Configuration::get('FACILIPAY_API_KEY_MARKETING_' . $sIsoCodeCountry),
                    $sIsoCodeCountry
                );
                $reponse = json_decode($reponse['body']);
            } catch (Exception $e) {
                if (_PS_MODE_DEV_) {
                    echo $e->getMessage();
                }
            }
        }

        if (!$reponse) {
            return;
        }

        if (!is_object($reponse)) {
            $reponse = json_decode($reponse);
        }
        
        if (isset($reponse->instalments) && is_array($reponse->instalments)) {
            // Récupération de la mensualité la plus haute.
            $fInstalmentAmount = 0;
            foreach ($reponse->instalments as $instalment) {
                if ($fInstalmentAmount < $instalment->instalment_amount) {
                    $fInstalmentAmount = $instalment->instalment_amount;
                }
            }
            $reponse->price_instalments = $fInstalmentAmount;
        }

        return $reponse;
    }

    /**
     * @param $order_reference
     * @param $id_country
     * @return array|mixed
     */
    public function getOrderState($order_reference, $sIsoCodeCountry)
    {
        $url = $this->url . '/payments/v1/purchase/';

        $sPSPGuid = Configuration::get('FACILYPAY_PSP_GUID_' . $sIsoCodeCountry);
        if (!empty($sPSPGuid) || $sPSPGuid != 0) {
            $url .= 'psp_guid/' . $sPSPGuid . '/';
        }
        $url .= 'merchant_guid/' . Configuration::get('FACILYPAY_MERCHANT_GUID_' . $sIsoCodeCountry) .
            '/reference/CMDE|' . $order_reference . '?language_code=' . $sIsoCodeCountry;
        $reponse = false;
        try {
            $reponse = $this->providePepsResponse(
                $url,
                Configuration::get('FACILYPAY_API_KEY_PAYMENT_' . $sIsoCodeCountry),
                $sIsoCodeCountry
            );
            $reponse = json_decode($reponse['body']);
        } catch (Exception $e) {
            if (_PS_MODE_DEV_) {
                echo $e->getMessage();
            }
        }

        return $reponse;
    }

    /**
     * Fonction chargée de requêter le webservice PEPS Oney au moment de la transaction.
     *
     * @param array $parametres
     * @return \stdClass   Le json décodé renvoyé par le web service PEPS.
     */
    public function paiement(array $parametres, $sIsoCodeCountry)
    {
        $requestBody = (json_encode($parametres));
        $url = $this->url . '/payments/v1/purchase/facilypay_url';
        $reponse = array();
        try {
            $reponse = $this->providePepsResponse(
                $url,
                Configuration::get('FACILYPAY_API_KEY_PAYMENT_' . $sIsoCodeCountry),
                $sIsoCodeCountry,
                $requestBody
            );
            $reponse = json_decode($reponse['body']);
        } catch (Exception $e) {
            if (_PS_MODE_DEV_) {
                echo $e->getMessage();
            }
        }

        return $reponse;
    }

    /**
     * @param $order_reference
     * @param array $parameters
     * @param int $id_lang
     * @return array|mixed
     */
    public function actionConfirm($order_reference, array $parameters, $sIsoCodeCountry)
    {
        $requestBody = (json_encode($parameters));

        $url = $this->url . '/payments/v1/purchase/';

        $sPSPGuid = Configuration::get('FACILYPAY_PSP_GUID_' . $sIsoCodeCountry);
        if (!empty($sPSPGuid) || $sPSPGuid != 0) {
            $url .= 'psp_guid/' . $sPSPGuid . '/';
        }

        $url .= 'merchant_guid/' .
            Configuration::get('FACILYPAY_MERCHANT_GUID_' . $sIsoCodeCountry) .
            '/reference/CMDE|' . $order_reference . '/action/confirm';
        $reponse = array();
        try {
            $reponse = $this->providePepsResponse(
                $url,
                Configuration::get('FACILYPAY_API_KEY_PAYMENT_' . $sIsoCodeCountry),
                $sIsoCodeCountry,
                $requestBody
            );
            $reponse = json_decode($reponse['body']);
        } catch (Exception $e) {
            if (_PS_MODE_DEV_) {
                echo $e->getMessage();
            }
        }

        return $reponse;
    }

    private function b64e($str)
    {
        $b64e = 'base64_encode';
        return $b64e($str);
    }

    /**
     * @param $str
     * @return mixed
     */
    private function b64d($str)
    {
        $b64d = 'base64_decode';
        return $b64d($str);
    }

    /**
     * @param $parametres
     * @param $key
     * @return string
     */
    public function encryptBody($parametres, $key)
    {
        $key = $this->b64d($key);

        return $this->b64e(openssl_encrypt($parametres, 'aes-256-ecb', $key, OPENSSL_RAW_DATA, ''));
    }

    /**
     * @param $cryptMessage
     * @param $sIsoCodeCountry
     * @return mixed
     */
    public function decryptBody($cryptMessage, $sIsoCodeCountry)
    {
        $key = $this->b64d(Configuration::get('FACILIPAY_ONEY_SECRET_' . $sIsoCodeCountry));
        $message = $this->b64d($cryptMessage);

        return json_decode(urldecode(openssl_decrypt($message, 'aes-256-ecb', $key, OPENSSL_RAW_DATA, '')));
    }

    /**
     * @param $str
     * @return array
     */
    public function paramsEncrypt($str)
    {
        $str = json_encode($str);
        $sEchaped = '';
        if (Tools::strlen($str) <= 100) {
            $sEchaped = str_replace(
                array_keys($this->correspondance_encode),
                $this->correspondance_encode,
                $str
            );
        } else {
            /* @TODO Prévoir le cas ou les chaines de caractère sont longues. */
            throw new \Exception('Error IT1 : ' . $this->module->l('Veuillez contacter le service technique'));
        }

        if (Tools::strlen($sEchaped) % 2 == 0) {
            return str_split($sEchaped, Tools::strlen($sEchaped) / 2);
        } else {
            $aReturn = array();
            $aSplit = str_split($sEchaped, Tools::strlen($sEchaped) / 2);
            $aReturn[] = $aSplit[0];
            $aReturn[] = $aSplit[1] . $aSplit[2];
            return $aReturn;
        }
    }

    /**
     * @param $str
     * @return mixed
     */
    public function paramsDecrypt($str)
    {
        $sUnshaped = str_replace($this->correspondance_encode, array_keys($this->correspondance_encode), $str);
        return json_decode($sUnshaped);
    }

    /**
     * Fonction chargée de requêter le webservice PEPS Oney afin de recupéré les mentions légales
     *
     * @param array $parametres
     * @return \stdClass   Le json décodé renvoyé par le web service PEPS.
     * @throws Exception
     */
    public function getMentionlegale(array $parametres, $sIsoCodeCountry)
    {
        if (Oney::getConfigByCountry("mention_legale", $sIsoCodeCountry)) {
            $encodedParams = http_build_query($parametres);
            $url = $this->url . '/product_catalog/v1/legal_notice?' . $encodedParams;
            $reponse = array();

            if (!$reponse = $this->getCache($url)) {
                try {
                    $reponse = $this->providePepsResponse(
                        $url,
                        Configuration::get('FACILIPAY_API_KEY_MARKETING_' . $sIsoCodeCountry),
                        $sIsoCodeCountry
                    );
                    $reponse = $reponse['body'];
                } catch (Exception $e) {
                    if (_PS_MODE_DEV_ && !Tools::getValue('ajax')) {
                        echo $e->getMessage();
                    }
                }
            }

            if (!$reponse) {
                return;
            }

            return json_decode($reponse);
        } else {
            return;
        }
    }

    /**
     * @param String $sReference reference of the order to cancel
     * @param $aParameters
     * @return array
     */
    public function cancelPayment($sReference, $aParameters, $sIsoCodeCountry)
    {
        $url = $this->url . '/payments/v1/purchase/';

        $sPSPGuid = Configuration::get('FACILYPAY_PSP_GUID_' . $sIsoCodeCountry);
        if (!empty($sPSPGuid) || $sPSPGuid != 0) {
            $url .= 'psp_guid/' . $sPSPGuid . '/';
        }

        $url .= 'merchant_guid/' . Configuration::get('FACILYPAY_MERCHANT_GUID_' . $sIsoCodeCountry) .
            '/reference/CMDE|' . $sReference .
            '/action/cancel';
        try {
            $response = $this->providePepsResponse(
                $url,
                Configuration::get('FACILYPAY_API_KEY_PAYMENT_' . $sIsoCodeCountry),
                $sIsoCodeCountry,
                json_encode($aParameters)
            );
        } catch (Exception $e) {
            return $e->getMessage();
        }

        return json_decode($response['body']);
    }

    /**
     * Fonction chargée de requêter l'url donnée en fournissant l'apiKey donnée
     * dans l'en-tête de la requête.
     *
     * @param   string $url L'url à appeler.
     * @param   string $apiKey L'api key à fournir dans l'en-tête de la requête.
     * @param   string $requestBody Le corps de la requête (à utiliser dans le cas de requêtes post)
     * @param int $id_lang
     * @return  array   Réponse HTTP parsée.
     * @throws Exception Lors que le web service PEPS renvoi un code d'erreur.
     */
    public function providePepsResponse($url, $apiKey, $sIsoCodeCountry, $requestBody = null)
    {
        $curl = curl_init();

        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HEADER, true);

        $sIsoCode = Oney::getConfigByCountry('iso_code', $sIsoCodeCountry);

        $headers = array(
            'X-Oney-Authorization: ' . $apiKey,
            'X-Oney-Partner-Country-Code: ' . (($sIsoCode) ? $sIsoCode : Tools::strtoupper($sIsoCodeCountry)),
            'Content-Type: application/json; charset=utf-8',
        );

        // Permet de savoir si le body est encrypté
        $encrypt = false;
        if (empty($requestBody)) {
            curl_setopt($curl, CURLOPT_HTTPGET, true);
            if ($encrypt) {
                $headers[] = 'X-Oney-Secret: Method-body';
            } else {
                $headers[] = 'X-Oney-Secret: None';
            }
        } else {
            if ($encrypt) {
                $headers[] = 'X-Oney-Secret: Method-body';
                $isJson = (is_string($requestBody) && is_array(json_decode($requestBody, true)));
                if ($isJson) {
                    $key = (Configuration::get('FACILIPAY_ONEY_SECRET_' . $sIsoCodeCountry));
                    $requestBody = json_encode(
                        array(
                            'merchant_guid' => Configuration::get('FACILYPAY_MERCHANT_GUID_' . $sIsoCodeCountry),
                            'encrypted_message' => $this->encryptBody($requestBody, $key)
                        )
                    );

                    $sPSPGuid = Configuration::get('FACILYPAY_PSP_GUID_' . $sIsoCodeCountry);
                    if (!empty($sPSPGuid) || $sPSPGuid != 0) {
                        $requestBody['psp_guid'] = $sPSPGuid;
                    }
                } else {
                    // Si la procedure en cours est une demande de clé encrypté
                    $headers[] = 'X-Oney-Secret:' . $requestBody;
                    $requestBody = json_encode(array());
                }
            } else {
                $headers[] = 'X-Oney-Secret: None';
            }

            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $requestBody);
            // methode HTTP => GET.
            // $headers[] = 'Content-Length: ' . Tools::strlen($requestBody);
        }

        // en-tête HTTP de la requête.
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

        $httpResponse = curl_exec($curl);

        if (!$httpResponse) {
            throw new \Exception(curl_error($curl), curl_errno($curl));
        }
        $headerSize = curl_getinfo($curl, CURLINFO_HEADER_SIZE);

        curl_close($curl);
        $reponse = $this->parseHttpResponse($httpResponse, $headerSize);

        $bDebugTest = true;
        $bTestPaiement = true;
        if (($bTestPaiement && $requestBody != '') || $bDebugTest) {
            $sLogPath = _PS_MODULE_DIR_ . 'oney/logs/';
            $fileName = date('Y-m-d') . '_debug'._PS_VERSION_.'.txt';
            file_put_contents($sLogPath . $fileName, date('d-m-Y h:i:s') . "\n", FILE_APPEND);
            file_put_contents($sLogPath . $fileName, print_r(array('oney_url' => $url), true) . "\n", FILE_APPEND);
            file_put_contents(
                $sLogPath . $fileName,
                print_r(array('oney_headers' => $headers), true) . "\n",
                FILE_APPEND
            );
            file_put_contents(
                $sLogPath . $fileName,
                print_r(array('oney_request_url', $requestBody), true) . "\n",
                FILE_APPEND
            );
            file_put_contents(
                $sLogPath . $fileName,
                print_r(array('oney_response' => $reponse), true) . "\n\n",
                FILE_APPEND
            );

            // Delete log file on serveur 10 jours
            $aFiles = glob($sLogPath . '*.txt');
            foreach ($aFiles as $file) {
                $filename = @end(explode('/', $file));
                $sDateFile = @reset(explode('_', $filename));
                $datetime1 = new DateTime($sDateFile);
                $datetime2 = new DateTime(date('Y-m-d'));
                $interval = $datetime1->diff($datetime2);
                if ($interval->days > 10) {
                    unlink($file);
                }
            }
        }

        if (in_array((int)$reponse['responseCode'], $this->pepsErrorHttpCodes)) {
            $json = json_decode($reponse['body']);

            $message = "Erreur renvoyé par le web service Oney : <br/>";
            if (isset($json->code)) {
                $message .= "&nbsp;&nbsp;&nbsp;&nbsp; Code '" . $json->code . "'";
            } elseif (isset($json->errorCode)) {
                $message .= "&nbsp;&nbsp;&nbsp;&nbsp; Code '" . $json->errorCode . "'";
            }
            if (isset($json->message)) {
                $message .= " Message '" . $json->message . "'<br/>";
            } elseif (isset($json->errorMessage)) {
                $message .= " Message '" . $json->errorMessage . "'<br/>";
            } elseif (isset($json->error)) {
                $message .= " Message '" . $json->error . "'<br/>";
            }
            if (isset($json->fields) && !empty($json->fields)) {
                $message .= " Champs en erreur : ";
                foreach ($json->fields as $field => $value) {
                    $message .= " '" . $field . "' ";
                }
            }

            if (isset($json->error_list) && !empty($json->error_list) && _PS_MODE_DEV_) {
                $message .= " Liste d'erreurs : <ul>";
                foreach ($json->error_list as $field => $value) {
                    if (isset($value->error)) {
                        $value = $value->error;
                    }
                    $message .= '<li> code erreur : ' . $value->error_code . '
                     | label : ' . $value->error_label .
                        ((isset($value->field)) ? ' ' . $value->field : '') . '</li>';
                }
                $message .= "</ul>";
            }
            throw new \Exception($message);
        }

        // Si le request body est vide, c'est que l'appel est marketing et non paiement.
        // Nous ne stockons pas les paiements en cache
        if (empty($requestBody) && $reponse['responseCode'] == 200) {
            $this->setCache($url, $reponse['body']);
        }
        
        return $reponse;
    }

    /**
     * Fonction chargée de parser le retour HTTP issu de cURL.
     *
     * @param   string $reponse
     * @param   int $headerSize
     * @return  array       La réponse HTTP parsé avec comme index :
     *                      _ 'protocole' : version du protocole
     *                      _ 'responseCode' : code de retour HTTP
     *                      _ 'responseCodeReason' : raison du code de retour HTTP
     *                      _ 'headers' : tableau des en-têtes HTTP
     *                      _ 'body' : le corps de la requête HTTP
     * @throws  \Exception  Lorsque la chaine de caractère ne correspond pas à
     *                      un retour HTTP.
     */
    private function parseHttpResponse($reponse, $headerSize)
    {
        if (strpos($reponse, "HTTP") === false) {
            throw new \Exception('Ceci n\'est pas un retour HTTP valide.');
        }

        $headers = Tools::substr($reponse, 0, $headerSize);

        $parsedHttp = $this->parseHttpHeader($headers);
        $parsedHttp['body'] = Tools::substr($reponse, $headerSize);

        return $parsedHttp;
    }

    /**
     * Fonction chargée de parser l'en-tête du retour HTTP issu de cURL.
     *
     * @param   string $httpHeaders
     * @return  array       Le contenu de l'en-tête HTTP parsé avec comme index :
     *                      _ 'protocole' : version du protocole
     *                      _ 'responseCode' : code de retour HTTP
     *                      _ 'responseCodeReason' : raison du code de retour HTTP
     *                      _ 'headers' : tableau des en-têtes HTTP
     */
    private function parseHttpHeader($httpHeaders)
    {
        $i = 0;
        $explode = explode(PHP_EOL, $httpHeaders);
        $protocole = null;
        $reponseCode = null;
        $reponseCodeReason = null;
        $headers = array();

        while ($i !== count($explode) - 1) {
            if ($explode[$i] !== '') {
                if (strrpos($explode[$i], 'HTTP') !== false) {
                    $explodeBis = explode(' ', $explode[$i]);
                    $protocole = $explodeBis[0];
                    $reponseCode = $explodeBis[1];
                    $reponseCodeReason = Tools::substr(
                        $explode[$i],
                        Tools::strlen($explodeBis[0]) + Tools::strlen($explodeBis[1]) + 2
                    );
                    if ($reponseCode === '100') {
                        $protocole = null;
                        $reponseCode = null;
                        $reponseCodeReason = null;
                    }
                } else {
                    $explodeBis = explode(': ', $explode[$i]);
                    if (isset($explodeBis[1]) && !empty($explodeBis[1])) {
                        $headers[$explodeBis[0]] = $explodeBis[1];
                    }
                }
            }
            $i++;
        }

        unset($explode, $explodeBis);

        return array(
            'protocole' => $protocole,
            'responseCode' => $reponseCode,
            'responseCodeReason' => $reponseCodeReason,
            'headers' => $headers,
        );
    }

    public function getCache($sURL)
    {
        $sSQL = 'SELECT reponse FROM ' . _DB_PREFIX_ . 'oney_cache '.
            'WHERE id = "' . md5($sURL) . '" ' .
            'AND date_upd = "' . date('Y-m-d') . '"';

        return Db::getInstance()->getValue($sSQL);
    }

    public function setCache($sURL, $sResponse)
    {
        $sSQL = 'INSERT INTO ' . _DB_PREFIX_ . 'oney_cache (id, reponse, date_upd) VALUES ' .
            '("' . md5($sURL) . '", \'' . pSQL($sResponse) . '\', "' . date('Y-m-d') . '") ' .
            'ON DUPLICATE KEY UPDATE reponse = VALUES(reponse), date_upd = VALUES(date_upd)';

        Db::getInstance()->execute($sSQL);
    }
}
