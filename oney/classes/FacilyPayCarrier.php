<?php
/**
 * 3x 4x Oney Module version 1.1.21 for PrestaShop 1.6 and PrestaShop 1.7. Support contact : prestashop@itroom.fr
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/afl-3.0.php
 *
 * @author    ITroom (http://itroom.fr/)
 * @copyright ITroom
 * @license   https://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * @category  payment
 * @package   oney
 */

class FacilyPayCarrier extends ObjectModel
{
    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table' => 'facilypay_carriers',
        'primary' => 'id_carrier',
        'fields' => array(
            'id_carrier' => array('type' => self::TYPE_INT),
            'delivery_mode' => array('type' => self::TYPE_INT),
            'priority' => array('type' => self::TYPE_INT),
            'priority_delivery_code' => array('type' => self::TYPE_INT),
            'address_type' => array('type' => self::TYPE_INT),
            'id_store' => array('type' => self::TYPE_INT),
        )
    );

    public function setAssociation($datas)
    {
        $sSQL = 'INSERT INTO ' . _DB_PREFIX_ . pSQL(self::$definition['table']) . '(' .
            'id_carrier, delivery_mode, priority, priority_delivery_code, address_type, id_store) VALUES (' .
            implode(',', $datas) . ') ' .
            'ON DUPLICATE KEY UPDATE delivery_mode = VALUES(delivery_mode), priority = VALUES(priority), ' .
            'priority_delivery_code = VALUES(priority_delivery_code), address_type = VALUES(address_type), ' .
            'id_store = VALUES(id_store)';

        Db::getInstance()->execute($sSQL);
    }

    /**
     * @return array
     * @throws PrestaShopDatabaseException
     */
    public function getAssociation()
    {
        $sql = "SELECT " . pSQL(implode(',', array_keys(self::$definition['fields']))) . " 
            FROM " . _DB_PREFIX_ . pSQL(self::$definition['table']);
        $results = Db::getInstance()->executeS($sql);

        $return_array = array();
        if ($results) {
            foreach ($results as $result) {
                $return_array[$result['id_carrier']] = $result;
            }
        }

        return $return_array;
    }

    public function getInfoByCarrier($id_carrier)
    {
        $sql = "SELECT " . pSQL(implode(',', array_keys(self::$definition['fields']))) . " 
            FROM " . _DB_PREFIX_ . pSQL(self::$definition['table']) . " WHERE id_carrier = " . (int)$id_carrier;

        return Db::getInstance()->getRow($sql);
    }
}
