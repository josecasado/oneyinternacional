<?php
/**
 * 3x 4x Oney Module version 1.1.21 for PrestaShop 1.6 and PrestaShop 1.7. Support contact : prestashop@itroom.fr
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/afl-3.0.php
 *
 * @author    ITroom (http://itroom.fr/)
 * @copyright ITroom
 * @license   https://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * @category  payment
 * @package   oney
 */

class FacilyPayCountry extends ObjectModel
{

    public $id_country;
    public $iso2;
    public $iso3;

    public static $definition = array(
        'table'   => 'facilypay_country',
        'primary' => 'iso2',
        'fields'  => array(
            'iso2' => array('type' => self::TYPE_STRING),
            'iso3' => array('type' => self::TYPE_STRING)
        )
    );

    public static function getIsoAlpha3($isoAlpha2)
    {
        $isoAlpha2 = (Oney::getConfigByCountry('dom_tom', $isoAlpha2)) ?
        Oney::getConfigByCountry('country_reference', $isoAlpha2) :
        $isoAlpha2;
        
        $sql = 'SELECT iso3 FROM ' . _DB_PREFIX_ . pSQL(self::$definition['table']) . ' 
                WHERE ' . pSQL(self::$definition['primary']) . ' = \'' . pSQL($isoAlpha2) . '\'';

        return Db::getInstance()->getValue($sql);
    }
}
