<?php
/**
 * 3x 4x Oney Module version 1.1.21 for PrestaShop 1.6 and PrestaShop 1.7. Support contact : prestashop@itroom.fr
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/afl-3.0.php
 *
 * @author    ITroom (http://itroom.fr/)
 * @copyright ITroom
 * @license   https://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * @category  payment
 * @package   oney
 */

class FacilyPayLock extends ObjectModel
{
    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table'   => 'facilypay_lock',
        'primary' => 'id_facilypay_lock',
        'fields'  => array(
            'reference' => array('type' => self::TYPE_STRING),
        )
    );

    public function exist($reference)
    {
        $sql = 'SELECT '. pSQL(self::$definition['primary']) . '
            FROM ' . _DB_PREFIX_ . pSQL(self::$definition['table']) . '
            WHERE reference = "' . pSQL($reference) . '"';

        if (!Db::getInstance()->getValue($sql)) {
            $this->insertReference($reference);
            return false;
        }

        return true;
    }

    public function insertReference($reference)
    {
        $sql = 'INSERT INTO ' . _DB_PREFIX_ . pSQL(self::$definition['table']) . ' 
            VALUES (default,\'' . pSQL($reference) . '\')';
        Db::getInstance()->execute($sql);
    }

    public function remove($reference)
    {
        $sql = 'DELETE FROM ' . _DB_PREFIX_ . pSQL(self::$definition['table']) . ' 
            WHERE reference = \'' . pSQL($reference) . '\'';
        Db::getInstance()->execute($sql);
    }
}
