<?php
/**
 * 3x 4x Oney Module version 1.1.21 for PrestaShop 1.6 and PrestaShop 1.7. Support contact : prestashop@itroom.fr
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/afl-3.0.php
 *
 * @author    ITroom (http://itroom.fr/)
 * @copyright ITroom
 * @license   https://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * @category  payment
 * @package   oney
 */

class FacilyPayOrder extends ObjectModel
{

    public $reference;
    public $transaction_id;

    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table'   => 'facilypay_orders',
        'primary' => 'id_facilypay_order',
        'fields'  => array(
            'reference' => array('type' => self::TYPE_STRING),
            'transaction_id' => array('type' => self::TYPE_STRING),
        )
    );

    /**
     * @param $sReference
     * @return false|string|null
     */
    public function getTransactionId($sReference)
    {
        $sSQL = 'SELECT transaction_id FROM ' . _DB_PREFIX_ . self::$definition['table'] .
            ' WHERE reference = \'' . pSQL($sReference) . '\'';

        return Db::getInstance()->getValue($sSQL);
    }
}
