<?php
/**
 * 3x 4x Oney Module version 1.1.21 for PrestaShop 1.6 and PrestaShop 1.7. Support contact : prestashop@itroom.fr
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/afl-3.0.php
 *
 * @author    ITroom (http://itroom.fr/)
 * @copyright ITroom
 * @license   https://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * @category  payment
 * @package   oney
 */

class OneyPhoneNumber
{
    /**
     * @param $sNumber
     * @param $sIsoCountry
     * @param int $iMobile
     * @return bool|string
     */
    public static function isValidPhoneNumber($sNumber, $sIsoCountry, $iMobile)
    {
        if (empty($sNumber)) {
            return false;
        }

        $sSQL = 'SELECT prefix, size FROM ' . _DB_PREFIX_ . 'oney_phone ' .
            'WHERE iso_code = "' . pSQL($sIsoCountry) .'" ' .
            'AND (mobile_or_fixe = 3 OR mobile_or_fixe = ' . (int)$iMobile . ')';

        $aValidPhone = Db::getInstance()->getRow($sSQL);
        
        if (!empty($aValidPhone)) {
            // Vérifie si l'indicatif est déjà présent.
            if ($sNumber[0] === '+') {
                // Nettoyage de la chaine
                $sNumber = str_replace(array('.', '-', ' ', '/' ), '', $sNumber);
                $sIndicatif = Tools::substr($sNumber, 0, Tools::strlen($aValidPhone['prefix']));
                $sNumber = Tools::substr($sNumber, Tools::strlen($aValidPhone['prefix']));
                if ($sIndicatif === $aValidPhone['prefix'] && (int)$aValidPhone['size'] === Tools::strlen($sNumber)) {
                    return $aValidPhone['prefix'] . $sNumber;
                }

                return false;
            }

            // Suppression du zéro devant si présent.
            if ($sNumber[0] === '0') {
                $sNumber = Tools::substr($sNumber, 1);
            }

            // Suppression des caractères inutile ('.', '-', ' ', '/' )
            $sNumber = str_replace(array('.', '-', ' ', '/' ), '', $sNumber);

            // Analyse de la taille de la chaine avec la taille récupéré en base
            if ((int)$aValidPhone['size'] === Tools::strlen($sNumber)) {
                return $aValidPhone['prefix'] . $sNumber;
            }
        }

        return false;
    }
}
