<?php
/**
 * 3x 4x Oney Module version 1.1.21 for PrestaShop 1.6 and PrestaShop 1.7. Support contact : prestashop@itroom.fr
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/afl-3.0.php
 *
 * @author    ITroom (http://itroom.fr/)
 * @copyright ITroom
 * @license   https://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * @category  payment
 * @package   oney
 */

class Opc extends ObjectModel
{
    public $business_transaction_code;
    public $business_transaction_version;
    public $business_transaction_type;
    public $short_label;
    public $long_label;
    public $customer_label;
    public $minimum_number_of_instalments;
    public $maximum_number_of_instalments;
    public $minimum_selling_price;
    public $maximum_selling_price;
    public $discount_rate;
    public $choosable_number_of_instalments;
    public $free_business_transaction;
    public $example;
    public $validity_start_date;
    public $validity_end_date;
    public $postponement_duration;
    public $display_order;
    public $selected;
    public $iso_code;
    public $id_shop;

    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table'   => 'facilypay_opc',
        'primary' => 'id_opc',
        'fields'  => array(
            'business_transaction_code'       => array('type' => self::TYPE_STRING),
            'business_transaction_version'    => array('type' => self::TYPE_INT),
            'business_transaction_type'       => array('type' => self::TYPE_STRING),
            'short_label'                     => array('type' => self::TYPE_STRING),
            'long_label'                      => array('type' => self::TYPE_STRING),
            'customer_label'                  => array('type' => self::TYPE_STRING),
            'minimum_number_of_instalments'   => array('type' => self::TYPE_INT),
            'maximum_number_of_instalments'   => array('type' => self::TYPE_INT),
            'choosable_number_of_instalments' => array('type' => self::TYPE_INT),
            'minimum_selling_price'           => array('type' => self::TYPE_INT),
            'maximum_selling_price'           => array('type' => self::TYPE_INT),
            'discount_rate'                   => array('type' => self::TYPE_INT),
            'free_business_transaction'       => array('type' => self::TYPE_INT),
            'example'                         => array('type' => self::TYPE_STRING),
            'validity_start_date'             => array('type' => self::TYPE_STRING),
            'validity_end_date'               => array('type' => self::TYPE_STRING),
            'postponement_duration'           => array('type' => self::TYPE_INT),
            'display_order'                   => array('type' => self::TYPE_INT),
            'selected'                        => array('type' => self::TYPE_INT),
            'iso_code'                         => array('type' => self::TYPE_STRING),
            'id_shop'                         => array('type' => self::TYPE_INT)
        )
    );

    public function getOpc($sCodeOpc, $iLangCountry)
    {
        $sSQL = 'SELECT * FROM ' . _DB_PREFIX_ . pSQL(self::$definition['table']) . ' 
                WHERE business_transaction_code = "'. $sCodeOpc .'" AND iso_code = \'' .  pSQL($iLangCountry) . '\'';
        $sSQL .= ' AND id_shop = ' . Context::getContext()->shop->id;

        return Db::getInstance()->executeS($sSQL);
    }

    public function cleanSelected($sIsoCoCountry, $iShopId)
    {
        return Db::getInstance()->update(
            'facilypay_opc',
            array('selected' => 0),
            'selected = 1 and iso_code = \'' . pSQL($sIsoCoCountry) . '\'' .
            (($iShopId) ? 'AND id_shop = ' . (int)$iShopId : '')
        );
    }

    public function getListOpc($iLangCountry, $selected = null, $price = null)
    {

        if ($price) {
            $sCondPrice = ' minimum_selling_price <= ' . (float)$price .
            ' AND '. (float)$price .' <= maximum_selling_price ';
        }

        $iShopId = ShopCore::getContextShopID();
        $sSQL = '';
        if ($iLangCountry === 0) {
            $sSQL = 'SELECT * FROM ' . _DB_PREFIX_ . pSQL(self::$definition['table']) . ' ';

            if ($iShopId) {
                $sSQL .= ' WHERE id_shop = ' . $iShopId;
                if (isset($sCondPrice)) {
                    $sSQL .= ' AND ' . $sCondPrice;
                }
            }

            $sSQL .= ' GROUP BY business_transaction_code';
        }

        if ($selected) {
            $sSQL = 'SELECT * FROM ' . _DB_PREFIX_ . pSQL(self::$definition['table']) . ' 
                WHERE selected = 1 AND iso_code = \'' .  pSQL($iLangCountry) . '\'';

            if ($iShopId) {
                $sSQL .= ' AND id_shop = ' . $iShopId;
            }
            if (isset($sCondPrice)) {
                $sSQL .= ' AND ' . $sCondPrice;
            }

            $sSQL .= ' GROUP BY business_transaction_code';
        }

        if (empty($sSQL)) {
            $sSQL = 'SELECT * FROM ' . _DB_PREFIX_ . pSQL(self::$definition['table']) . '
                WHERE iso_code = \'' . pSQL($iLangCountry) . '\' ';

            if ($iShopId) {
                $sSQL .= (strpos($sSQL, 'WHERE') !== false) ?
                    ' AND id_shop = ' . $iShopId : ' AND id_shop = ' . $iShopId;
            }

            if (isset($sCondPrice)) {
                $sSQL .= ' AND ' . $sCondPrice;
            }

            $sSQL .= ' GROUP BY business_transaction_code';
        }
        $sSQL .= ' ORDER BY minimum_number_of_instalments';

        return Db::getInstance()->executeS($sSQL);
    }

    public function getListOpcWithPrice($price, $sIsoCode)
    {
        $iShopId = ShopCore::getContextShopID();
        $sSQL = 'SELECT * FROM ' . _DB_PREFIX_ . pSQL(self::$definition['table']) . ' ' .
            'WHERE selected = 1 '.
            'AND iso_code = \'' . pSQL($sIsoCode) . '\' '.
            'AND minimum_selling_price <= ' . (float)$price . ' '.
            'AND maximum_selling_price >= ' . (float)$price . ' ' .
            'AND id_shop = ' . $iShopId;

        return Db::getInstance()->executeS($sSQL);
    }

    public function getSingleOpcSimulationWithFilterPrice($price, $sIsoCodeCountry)
    {
        $iShopId = ShopCore::getContextShopID();
        $sSQL = 'SELECT business_transaction_code FROM ' . _DB_PREFIX_ . pSQL(self::$definition['table']) . ' '.
            'WHERE minimum_selling_price <= "' . (float)$price . '" '.
            'AND maximum_selling_price >= "' . (float)$price . '" '.
            'AND selected = "1" '.
            'AND iso_code = \'' . pSQL($sIsoCodeCountry) . '\' ' .
            'AND id_shop = ' . $iShopId;

        return Db::getInstance()->getValue($sSQL);
    }

    public function getOpcSimulationWithFilterPrice($price, $sIsoCodeCountry)
    {
        $iShopId = ShopCore::getContextShopID();
        $sSQL = 'SELECT business_transaction_code, customer_label, example, '.
            'minimum_selling_price, maximum_selling_price, free_business_transaction ' .
            'FROM ' . _DB_PREFIX_ . pSQL(self::$definition['table']) . ' ' .
            'WHERE minimum_selling_price <= "' . (float)$price .
            '" AND maximum_selling_price >= "' . (float)$price . '" AND selected = "1" 
            AND iso_code = \'' . pSQL($sIsoCodeCountry) . '\' ' .
            'AND id_shop = ' . $iShopId;

        return Db::getInstance()->executeS($sSQL);
    }

    public static function getIdWithTransactionCode($businessTransactionCode, $sIsoCodeCountry, $iShopId = null)
    {
        $sSQL = 'SELECT id_opc FROM ' . _DB_PREFIX_ . pSQL(self::$definition['table']) . ' '.
            'WHERE business_transaction_code = "' . pSQL($businessTransactionCode) . '" ' .
            'AND iso_code = \'' . pSQL($sIsoCodeCountry) . '\' ';

        if ($iShopId) {
            $sSQL .= 'AND id_shop = ' . $iShopId;

            return Db::getInstance()->getValue($sSQL);
        }

        return Db::getInstance()->executeS($sSQL);
    }
}
