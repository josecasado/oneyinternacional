<?php
/**
 * 3x 4x Oney Module version 1.1.21 for PrestaShop 1.6 and PrestaShop 1.7. Support contact : prestashop@itroom.fr
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/afl-3.0.php
 *
 * @author    ITroom (http://itroom.fr/)
 * @copyright ITroom
 * @license   https://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * @category  payment
 * @package   oney
 */

class OneyConfirmationModuleFrontController extends ModuleFrontController
{
    /**
     * @throws PrestaShopDatabaseException
     * @throws PrestaShopException
     */
    public function postProcess()
    {
        $this->display_column_left = false;
        $this->display_column_right = false;

        /** @var Order $order */
        $order = Order::getByReference(Tools::getValue('reference'))->getFirst();

        if ($order && $this->context->customer->secure_key === $order->getCustomer()->secure_key) {
            $orderState = new OrderState($order->current_state);

            $this->context->smarty->assign(
                array(
                    'reference' => $order->reference,
                    'total'     => $order->total_paid,
                    'status'    => 'ok',
                    'statut'    => $orderState->name[$this->context->language->id],

                    'shop_name' => array(
                        Configuration::get('PS_SHOP_NAME', $this->context->language->id)
                    )
                )
            );
            $this->setTemplate('confirmation.tpl');
        } else {
            Tools::redirect('/');
        }
    }
}
