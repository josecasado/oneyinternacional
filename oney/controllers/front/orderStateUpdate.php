<?php
/**
 * 3x 4x Oney Module version 1.1.21 for PrestaShop 1.6 and PrestaShop 1.7. Support contact : prestashop@itroom.fr
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/afl-3.0.php
 *
 * @author    ITroom (http://itroom.fr/)
 * @copyright ITroom
 * @license   https://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * @category  payment
 * @package   oney
 */

class OneyOrderStateUpdateModuleFrontController extends ModuleFrontController
{
    public function postProcess()
    {
        /** @var Oney $module */
        $module = $this->module;

        // If the module is not active anymore, no need to process anything.
        if ($module->active == false ||
            Tools::getValue('token') != Configuration::get('FACILYPAY_TOKEN')
        ) {
            Tools::redirect('/');
        }

        echo 'Mise a jour des statuts commande <br/>';

        $sql = 'SELECT id_order FROM ' . _DB_PREFIX_ . 'orders 
        WHERE module = "oney" AND current_state in (' .
            // PS order status
//            (int)(Configuration::get('PS_OS_OUTOFSTOCK')) . ', ' .
//            (int)(Configuration::get('PS_OS_WS_PAYMENT')) . ', ' .

            // Oney order status
            (int)(Configuration::get('FACILYPAY_OS_PENDING')) . ', ' .
            (int)(Configuration::get('FACILYPAY_OS_FAVORABLE')) . ', ' .
            (int)(Configuration::get('FACILYPAY_OS_CAPTURE')) . ')';

        $result = Db::getInstance()->executeS($sql);
        foreach ($result as $value) {
            $order = new Order($value['id_order']);
            $reference = $order->reference;
            $aAddressInvoice = Address::getCountryAndState($order->id_address_invoice);
            $sIsoCodeCountryByAddress = Country::getIsoById($aAddressInvoice['id_country']);
            $sIsoCodeCountry = (Oney::getConfigByCountry('dom_tom', $sIsoCodeCountryByAddress)) ?
                Oney::getConfigByCountry('country_reference', $sIsoCodeCountryByAddress) : $sIsoCodeCountryByAddress;

            $response = $module->pepsWebService->getOrderState(
                $reference,
                $sIsoCodeCountry
            );

            if (isset($response->encrypted_message)) {
                $response = $module->pepsWebService->decryptBody(
                    $response->encrypted_message,
                    $sIsoCodeCountry
                );
            }

            if ($response) {
                $status_code = $response->purchase->status_code;
                switch ($status_code) {
                    case 'FUNDED':
                        $payment_status = Configuration::get('PS_OS_PAYMENT');
                        break;
                    case 'FAVORABLE':
                        $payment_status = Configuration::get('FACILYPAY_OS_FAVORABLE');

                        if (!Configuration::get('FACILYPAY_ACTION_CONFIRM') &&
                            $order->current_state == Configuration::get('FACILYPAY_OS_CAPTURE')
                        ) {
                            $payment_status = Configuration::get('FACILYPAY_OS_CAPTURE');
                        }

                        if (Configuration::get('FACILYPAY_ACTION_CONFIRM') ||
                            $order->current_state == Configuration::get('FACILYPAY_OS_CAPTURE')) {
                            $paramsActionConfirm = array(
                                'language_code'       => $sIsoCodeCountry,
                                'merchant_request_id' => date('YmdHis') . '-' . $order->id_cart,
                                'payment'             => array(
                                    'payment_amount' => round($order->getOrdersTotalPaid(), 2)
                                )
                            );
                            $result = $module->pepsWebService->actionConfirm(
                                $order->reference,
                                $paramsActionConfirm,
                                $sIsoCodeCountry
                            );

                            if (isset($result->encrypted_message)) {
                                $result = $module->pepsWebService->decryptBody(
                                    $result->encrypted_message,
                                    $sIsoCodeCountry
                                );
                            }
                            $status_code = $result->purchase->status_code;

                            if ($status_code == 'FUNDED') {
                                $payment_status = (int)Configuration::get('PS_OS_PAYMENT');
                            }
                        }
                        break;
                    case 'PENDING':
                    case 'TO_BE_FUNDED':
                        $payment_status = Configuration::get('FACILYPAY_OS_PENDING');
                        break;
                    case 'ABORTED':
                    case 'CANCELLED':
                    case 'REFUSED':
                        $payment_status = Configuration::get('PS_OS_CANCELED');
                        break;
                    default:
                        continue;
                }
            } else {
                $payment_status = $order->current_state;
            }

            if ($order->current_state != $payment_status) {
                $order->setCurrentState($payment_status);
            }
        }
        echo 'Fin de mise à jour des status commande<br/>';
        die();
    }
}
