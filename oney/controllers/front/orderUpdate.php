<?php
/**
 * 3x 4x Oney Module version 1.1.21 for PrestaShop 1.6 and PrestaShop 1.7. Support contact : prestashop@itroom.fr
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/afl-3.0.php
 *
 * @author    ITroom (http://itroom.fr/)
 * @copyright ITroom
 * @license   https://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * @category  payment
 * @package   oney
 */

class OneyOrderUpdateModuleFrontController extends ModuleFrontController
{
    public function init()
    {
        set_time_limit(-1);
        $postData = Tools::file_get_contents("php://input");
        $postObj = json_decode($postData);
        /** @var Oney $module */
        $module = $this->module;

        $bLog = true;
        if ($bLog) {
            $sLogPath = _PS_MODULE_DIR_ . 'oney/logs/';
            $fileName = date('Y-m-d') . '_callback'._PS_VERSION_.'.txt';
            file_put_contents($sLogPath . $fileName, date('d-m-Y h:i:s') . "\n", FILE_APPEND);
            file_put_contents(
                $sLogPath . $fileName,
                print_r(array('oney_obj_receive' => $postObj), true) . "\n",
                FILE_APPEND
            );
        }
        
        if (is_object($postObj) && ($sIsoCodeCountry = Tools::getValue('iso_code', false))) {
            if (isset($postObj->encrypted_message)) {
                $encryptedMessage = $postObj->encrypted_message;
                $response = $module->pepsWebService->decryptBody($encryptedMessage, $sIsoCodeCountry);
            } else {
                $response = $postObj;
            }

            $params = $module->pepsWebService->paramsDecrypt(
                $response->merchant_context . $response->psp_context
            );
            
            /** @var Cart $cart */
            $cart = new Cart($params->id_cart);
            $this->context->cart = $cart;

            $module->reference = $response->purchase->external_reference;
            $message = '';

            $bCreateOrder = true;
            $iOrderId = Order::getOrderByCartId($params->id_cart);
            if ($iOrderId) {
                $oOrder = new Order($iOrderId);
            }
            switch ($response->purchase->status_code) {
                case 'FUNDED':
                    $payment_status = Configuration::get('PS_OS_PAYMENT');
                    break;
                case 'PENDING':
                    $payment_status = Configuration::get('FACILYPAY_OS_PENDING');
                    break;
                case 'FAVORABLE':
                case 'TO_BE_FUNDED':
                    $payment_status = Configuration::get('FACILYPAY_OS_FAVORABLE');

                    $bExist = false;
                    if (isset($oOrder) && $oOrder->current_state == Configuration::get('FACILYPAY_OS_CAPTURE')) {
                        $bExist = true;
                        $payment_status = $oOrder->current_state;
                    }

                    if (Configuration::get('FACILYPAY_ACTION_CONFIRM') || $bExist) {
                        $paramsActionConfirm = array(
                            'language_code'       => $sIsoCodeCountry,
                            'merchant_request_id' => date('YmdHis') . '-' . $cart->id,
                            'payment'             => array(
                                'payment_amount' => number_format((float)$params->amount, 2, '.', '')
                            )
                        );
                        $result = $module->pepsWebService->actionConfirm(
                            $module->reference,
                            $paramsActionConfirm,
                            $sIsoCodeCountry
                        );

                        $status_code = $result->purchase->status_code;
                        if ($status_code == 'FUNDED') {
                            $payment_status = (int)Configuration::get('PS_OS_PAYMENT');
                        }
                    }
                    break;
                default:
                    PrestaShopLogger::addLog(
                        print_r($response->purchase->external_reference, true) . '',
                        4,
                        1001,
                        'Payment',
                        null,
                        true
                    );
                    $payment_status = Configuration::get('PS_OS_ERROR');
                    $bCreateOrder = false;
            }
            
            if (!$module->facilypay_lock->exist($module->reference) &&
                isset($params->id_cart) && !empty($params->id_cart) &&
                Order::getOrderByCartId($params->id_cart) == false
            ) {
                if ($bCreateOrder && $iOrderId == false) {
                    try {
                        $module->facilypay_lock->remove($module->reference);
                        $module->facilypay_orders->reference = $module->reference;
                        $module->facilypay_orders->transaction_id = $params->transaction_id;
                        $module->facilypay_orders->save();
                        $module->validateOrder(
                            $params->id_cart,
                            $payment_status,
                            (float)round($params->amount, 2),
                            $module->name,
                            $message,
                            array(),
                            $cart->id_currency,
                            false
                        );
                        header('Content-type: Application/json', true, 204);
                        echo json_encode(array());
                        die(204);
                    } catch (Exception $exception) {
                        if (_PS_MODE_DEV_) {
                            PrestaShopLogger::addLog(
                                pSQL($exception->getMessage()),
                                4,
                                0,
                                'Paiement Oney',
                                null,
                                true
                            );
                        }
                        $module->facilypay_lock->remove($module->reference);
                        header('Content-type: Application/json', true, 500);
                        die(500);
                    }
                }
            } else {
                $aStateToUpdate = array(
                    (int)(Configuration::get('FACILYPAY_OS_PENDING')),
                    (int)(Configuration::get('FACILYPAY_OS_FAVORABLE')),
                    (int)(Configuration::get('FACILYPAY_OS_CAPTURE'))
                );
                if (isset($oOrder) && $oOrder->id) {
                    if ($oOrder->current_state != $payment_status &&
                        in_array($oOrder->current_state, $aStateToUpdate)
                    ) {
                        $oOrder->setCurrentState($payment_status);
                    }
                }
                header('Content-type: Application/json', true, 204);
                echo json_encode(array());
                die(204);
            }
        }
        $module->facilypay_lock->remove($module->reference);
        header('Content-type: Application/json', true, 500);
        die(500);
    }
}
