<?php
/**
 * 3x 4x Oney Module version 1.1.21 for PrestaShop 1.6 and PrestaShop 1.7. Support contact : prestashop@itroom.fr
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/afl-3.0.php
 *
 * @author    ITroom (http://itroom.fr/)
 * @copyright ITroom
 * @license   https://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * @category  payment
 * @package   oney
 */

class OneyPaymenturlModuleFrontController extends ModuleFrontController
{
    public function postProcess()
    {
        $sSlectedOpc = Tools::getValue('opc', false);
        $shortLabel = Tools::getValue('short_label', false);
        $cart = Context::getContext()->cart;
        $ctx = Context::getContext();
        /** @var Oney $module */
        $module = $this->module;
        $customer = new Customer($cart->id_customer);
        $addressShipping = new Address($cart->id_address_delivery);
        $addressInvoice = new Address($cart->id_address_invoice);

        if (!$cart->id || !$sSlectedOpc) {
            Tools::redirect('/');
        }

        // Gestion des DOM
        $merchant_guid = Configuration::get('FACILYPAY_MERCHANT_GUID_' .
            ((Oney::getConfigByCountry('dom_tom', $module->sIsoCodeCountry)) ?
                Oney::getConfigByCountry('country_reference', $module->sIsoCodeCountry) :
                $module->sIsoCodeCountry));
        $psp_guid = Configuration::get('FACILYPAY_PSP_GUID_' .
            ((Oney::getConfigByCountry('dom_tom', $module->sIsoCodeCountry)) ?
                Oney::getConfigByCountry('country_reference', $module->sIsoCodeCountry) :
                $module->sIsoCodeCountry));

        do {
            $reference = Order::generateReference();
            $orderByRef = Order::getByReference($reference);
        } while ($orderByRef->count() > 0);

        if (!class_exists('Chronopost') &&
            file_exists(_PS_MODULE_DIR_ . '/chronopost/chronopost.php')) {
            include_once(_PS_MODULE_DIR_ . '/chronopost/chronopost.php');
        }

        $products = $cart->getProducts();
        $maint_item = array('price' => 0, 'reference' => 0);
        $iNumberOfItems = 0;
        $item_list = array();
        foreach ($products as $product) {
            $arrayTmp = array();
            $price = (isset($product['price_with_reduction']) ?
                $product['price_with_reduction'] : $product['price_wt']);
            if ($price > $maint_item['price']) {
                $maint_item['reference'] = sizeof($item_list);
                $maint_item['price'] = $price;
            }
            $arrayTmp['category_code'] = Configuration::get('FACILYPAY_CATEGORY');
            $arrayTmp['label'] = utf8_encode($product['name']);
            $bRefAuto = Oney::getConfigByCountry(
                'ref_auto',
                (Oney::getConfigByCountry('dom_tom', $module->sIsoCodeCountry)) ?
                Oney::getConfigByCountry('country_reference', $module->sIsoCodeCountry) :
                    $module->sIsoCodeCountry
            );
            $arrayTmp['item_external_code'] =
                (!empty($product['reference']) || !$bRefAuto) ? $product['reference'] : 'ONS_' . date('Y_m_d_G_i_s');
            $arrayTmp['quantity'] = $product['quantity'];
            $iNumberOfItems += $product['quantity'];
            $arrayTmp['price'] = number_format((float)$price, 2, '.', '');
            $arrayTmp['is_main_item'] = 0;
            if (Configuration::get('FACILYPAY_CATEGORY') == 14) {
                $arrayTmp['travel']['journey'] = array();
            }
            $item_list[] = $arrayTmp;
        }

        $item_list[$maint_item['reference']]['is_main_item'] = 1;

        // Add carrier cost in the ilst item products if the carrier cost is not free
        $oCarrier = new Carrier($cart->id_carrier, $cart->id_lang);
        $arrayTmp = array();
        $arrayTmp['category_code'] = Configuration::get('FACILYPAY_CATEGORY');
        $arrayTmp['label'] = $oCarrier->name;
        $arrayTmp['item_external_code'] = 'TRANSPORT';
        $arrayTmp['quantity'] = 1;
        $iNumberOfItems += $arrayTmp['quantity'];
        $arrayTmp['price'] = number_format((float)$cart->getTotalShippingCost(), 2, '.', '');
        $arrayTmp['is_main_item'] = 0;
        $item_list[] = $arrayTmp;

        $aParamsEncode = [];
        // CREATION DES PARAMETRES SUPPLEMENTAIRES POUR LA RECUPERATION NECESSAIRE AU MODULE
        try {
            $aParamsEncode = $module->pepsWebService->paramsEncrypt(
                array(
                    'id_cart' => $cart->id,
                    'amount' => number_format((float)$cart->getOrderTotal(), 2, '.', ''),
                    'transaction_id' => $shortLabel
                )
            );
        } catch (Exception $e) {
            Context::getContext()->cookie->oneyerror = 'Encoding error';
            Tools::redirect($this->context->link->getPageLink('order.php', true));
        }

        /** @var CountryCore $countryInvoice */
        if (!$addressInvoice->id_country) {
            $addressInvoice->id_country = CountryCore::getIdByName($cart->id_lang, $addressInvoice->country);
        }

        $countryInvoice = new Country($addressInvoice->id_country);

        // ADDRESS shipping
        $carrier_selected = $module->facilypay_carrier->getInfoByCarrier($cart->id_carrier);
        if ($carrier_selected['delivery_mode'] == 1) {
            // DELIVERY IN STORE
            if ($carrier_selected['id_store'] != 0) {
                // STORE EXIST
                $oStore = new Store($carrier_selected['id_store'], $customer->id_lang);
                $addressShipping = $oStore;
                $aShippingAddress = $this->formatAddr($oStore->address1 . ' ' . $oStore->address2);

                /** @var CountryCore $countryShipping */
                $countryShipping = new Country((int)$oStore->id_country);

                // PHONE NUMBERS
                $deliveryLandPhone = (isset($addressShipping->phone) && !empty($addressShipping->phone)) ?
                    OneyPhoneNumber::isValidPhoneNumber(
                        $addressShipping->phone,
                        $countryShipping->iso_code,
                        1
                    ) : OneyPhoneNumber::isValidPhoneNumber(
                        $addressShipping->phone_mobile,
                        $countryShipping->iso_code,
                        2
                    );

                $deliveryMobilePhone = (
                    isset($addressShipping->phone_mobile) && !empty($addressShipping->phone_mobile)
                ) ? OneyPhoneNumber::isValidPhoneNumber(
                    $addressShipping->phone_mobile,
                    $countryShipping->iso_code,
                    1
                ) : OneyPhoneNumber::isValidPhoneNumber(
                    $addressShipping->phone,
                    $countryShipping->iso_code,
                    2
                );
            } else {
                Context::getContext()->cookie->oneyerror = 'No store adress parameters';
                Tools::redirect($this->context->link->getPageLink('order.php', true));
            }
        } elseif ($carrier_selected['delivery_mode'] == 2 &&
            $oCarrier->external_module_name === 'chronopost' &&
            Module::isEnabled('chronopost') &&
            class_exists('Chronopost') &&
            Chronopost::isRelais($ctx->cart->id_carrier)
        ) {
            $account = Chronopost::getAccountInformationByAccountNumber(
                Configuration::get('CHRONOPOST_CHRONO10_ACCOUNT')
            );
            $relais = Db::getInstance()->getValue('SELECT id_pr FROM `' . _DB_PREFIX_ . 'chrono_cart_relais` ' .
                'WHERE id_cart = ' . (int)$ctx->cart->id);
            if ($relais && $account) {
                include_once _PS_MODULE_DIR_ . '/chronopost/libraries/PointRelaisServiceWSService.php';
                $ws = new PointRelaisServiceWSService();
                $paramsw = new rechercheDetailPointChronopost();
                $paramsw->accountNumber = $account['account'];
                $paramsw->password = $account['password'];
                $paramsw->identifiant = $relais;
                $bt = $ws->rechercheDetailPointChronopost($paramsw)->return->listePointRelais;
                if ($bt) {
                    $aDeliveryAddressFormat = $this->formatAddr($bt->nom . ' ' . $bt->adresse1 . ' ' . $bt->adresse2);
                    $countryShipping = new Country(Country::getByIso($bt->codePays));
                    $delivery_address = array(
                        'line1' => $aDeliveryAddressFormat[0],
                        'line2' => isset($aDeliveryAddressFormat[1]) ? $aDeliveryAddressFormat[1] : '',
                        'line3' => isset($aDeliveryAddressFormat[2]) ? $aDeliveryAddressFormat[2] : '',
                        'line4' => isset($aDeliveryAddressFormat[3]) ? $aDeliveryAddressFormat[3] : '',
                        'postal_code' => $bt->codePostal,
                        'municipality' => $bt->localite,
                        'country_code' => FacilyPayCountry::getIsoAlpha3($bt->codePays),
                        'country_label' => $countryShipping->name[(int)$customer->id_lang]
                    );
                    // PHONE NUMBERS
                    $deliveryLandPhone = (isset($addressInvoice->phone) && !empty($addressInvoice->phone)) ?
                        OneyPhoneNumber::isValidPhoneNumber(
                            $addressInvoice->phone,
                            $countryInvoice->iso_code,
                            1
                        ) : OneyPhoneNumber::isValidPhoneNumber(
                            $addressInvoice->phone_mobile,
                            $countryInvoice->iso_code,
                            2
                        );

                    $deliveryMobilePhone = (
                        isset($addressShipping->phone_mobile) && !empty($addressShipping->phone_mobile)
                    ) ? OneyPhoneNumber::isValidPhoneNumber(
                        $addressShipping->phone_mobile,
                        $countryShipping->iso_code,
                        1
                    ) : OneyPhoneNumber::isValidPhoneNumber(
                        $addressShipping->phone,
                        $countryShipping->iso_code,
                        2
                    );
                }
            }
        } elseif (($oCarrier->external_module_name === 'soliberte' ||
                $oCarrier->external_module_name === 'colissimo') &&
            $carrier_selected['delivery_mode'] == 2 &&
            ($aInfoRelaypoint = $this->isColissimoCarrier($cart->id, $oCarrier->external_module_name))
        ) {
            // Colissimo
            $aDeliveryAddressFormat = $this->formatAddr($aInfoRelaypoint['address']);
            $countryShipping = new Country(Country::getByIso($aInfoRelaypoint['country']));
            $delivery_address = array(
                'line1' => $aDeliveryAddressFormat[0],
                'line2' => isset($aDeliveryAddressFormat[1]) ? $aDeliveryAddressFormat[1] : '',
                'line3' => isset($aDeliveryAddressFormat[2])? $aDeliveryAddressFormat[2] : '',
                'line4' => isset($aDeliveryAddressFormat[3]) ? $aDeliveryAddressFormat[3] : '',
                'postal_code' => $aInfoRelaypoint['zipcode'],
                'municipality' => $aInfoRelaypoint['city'],
                'country_code' => FacilyPayCountry::getIsoAlpha3($aInfoRelaypoint['country']),
                'country_label' => $countryShipping->name[(int)$customer->id_lang]
            );
            // PHONE NUMBERS
            $deliveryLandPhone = (isset($addressInvoice->phone) && !empty($addressInvoice->phone)) ?
                OneyPhoneNumber::isValidPhoneNumber(
                    $addressInvoice->phone,
                    $countryInvoice->iso_code,
                    1
                ) : OneyPhoneNumber::isValidPhoneNumber(
                    $addressInvoice->phone_mobile,
                    $countryInvoice->iso_code,
                    2
                );

            $deliveryMobilePhone = (
                isset($addressShipping->phone_mobile) && !empty($addressShipping->phone_mobile)
            ) ? OneyPhoneNumber::isValidPhoneNumber(
                $addressShipping->phone_mobile,
                $countryShipping->iso_code,
                1
            ) : OneyPhoneNumber::isValidPhoneNumber(
                $addressShipping->phone,
                $countryShipping->iso_code,
                2
            );
        } else {
            // OTHER
            $aShippingAddress = $this->formatAddr($addressShipping->address1 . ' ' . $addressShipping->address2);
            /** @var CountryCore $countryShipping */
            if (!$addressShipping->id_country) {
                $addressShipping->id_country = CountryCore::getIdByName($cart->id_lang, $addressShipping->country);
            }

            $countryShipping = new Country($addressShipping->id_country);

            // PHONE NUMBERS
            $deliveryLandPhone = (isset($addressShipping->phone) && !empty($addressShipping->phone)) ?
                OneyPhoneNumber::isValidPhoneNumber(
                    $addressShipping->phone,
                    $countryShipping->iso_code,
                    1
                ) : OneyPhoneNumber::isValidPhoneNumber(
                    $addressShipping->phone_mobile,
                    $countryShipping->iso_code,
                    2
                );

            $deliveryMobilePhone = (
                isset($addressShipping->phone_mobile) && !empty($addressShipping->phone_mobile)
            ) ? OneyPhoneNumber::isValidPhoneNumber(
                $addressShipping->phone_mobile,
                $countryShipping->iso_code,
                1
            ) : OneyPhoneNumber::isValidPhoneNumber(
                $addressShipping->phone,
                $countryShipping->iso_code,
                2
            );
        }

        // PHONE NUMBERS
        $landPhone = (isset($addressInvoice->phone) && !empty($addressInvoice->phone)) ?
            OneyPhoneNumber::isValidPhoneNumber(
                $addressInvoice->phone,
                $countryInvoice->iso_code,
                1
            ) : OneyPhoneNumber::isValidPhoneNumber(
                $addressInvoice->phone_mobile,
                $countryInvoice->iso_code,
                2
            );

        $mobilePhone = (
            isset($addressInvoice->phone_mobile) && !empty($addressInvoice->phone_mobile)
        ) ? OneyPhoneNumber::isValidPhoneNumber(
            $addressInvoice->phone_mobile,
            $countryInvoice->iso_code,
            1
        ) : OneyPhoneNumber::isValidPhoneNumber(
            $addressInvoice->phone,
            $countryInvoice->iso_code,
            2
        );

        $aInvoiceAddress = $this->formatAddr($addressInvoice->address1 . ' ' . $addressInvoice->address2);
        $oGender = new Gender($customer->id_gender);

        if (!isset($delivery_address)) {
            $delivery_address = array(
                'line1' => $aShippingAddress[0],
                'line2' => isset($aShippingAddress[1]) ? $aShippingAddress[1] : '',
                'line3' => isset($aShippingAddress[2]) ? $aShippingAddress[2] : '',
                'line4' => isset($aShippingAddress[3]) ? $aShippingAddress[3] : '',
                'postal_code' => $addressShipping->postcode,
                'municipality' => $addressShipping->city,
                'country_code' => FacilyPayCountry::getIsoAlpha3($countryShipping->iso_code),
                'country_label' => $countryShipping->name[$customer->id_lang]
            );
        }

        $parameters = array(
            'language_code' => (Oney::getConfigByCountry('dom_tom', $module->sIsoCodeCountry)) ?
                Oney::getConfigByCountry('country_reference', $module->sIsoCodeCountry) :
                $module->sIsoCodeCountry,
            'merchant_guid' => $merchant_guid,
            'merchant_request_id' => date('YmdHis') . '-' . $cart->id,
            'purchase' => array(
                'external_reference_type' => 'CMDE',
                'external_reference' => (string)($reference),
                'purchase_amount' => number_format((float)$cart->getOrderTotal(), 2, '.', ''),
                'currency_code' => $this->context->currency->iso_code,

                'delivery' => array(
                    'delivery_date' => date('Y-m-d'),
                    'delivery_mode_code' => (!empty($carrier_selected['delivery_mode'])) ?
                        $carrier_selected['delivery_mode'] : 1,
                    'delivery_option' => (!empty($carrier_selected['priority'])) ?
                        $carrier_selected['priority'] : 1,
                    'priority_delivery_code' => $carrier_selected['priority_delivery_code'],
                    'address_type' => (!empty($carrier_selected['address_type'])) ?
                        $carrier_selected['address_type'] : 1,
                    'recipient' => array(
                        'recipient_honorific_code' => (int)$oGender->type + 1,
                        'surname' => $customer->lastname,
                        'first_name' => $customer->firstname,
                        'phone_number' => (!$deliveryLandPhone) ?
                            (!$deliveryMobilePhone ? "" : $deliveryMobilePhone)
                            : $deliveryLandPhone
                    ),
                    'delivery_address' => $delivery_address,
                ),
                "number_of_items" => (int)$iNumberOfItems,
                'item_list' => $item_list,
            ),
            'customer' => array(
                'customer_external_code' => (string)($customer->id),
                'language_code' => $countryInvoice->iso_code,
                'identity' => array(
                    'person_type' => (isset($customer->siret) && !empty($customer->siret)) ? '1' : '2',
                    'honorific_code' => (int)$oGender->type + 1,
                    'birth_name' => $customer->lastname,
                    'first_name' => $customer->firstname,
                ),
                'contact_details' => array(
                    'landline_number' => (!$landPhone) ? "" : $landPhone,
                    'mobile_phone_number' => (!$mobilePhone) ? "" : $mobilePhone,
                    'email_address' => Tools::strtolower($customer->email),
                ),
                'customer_address' => array(
                    'line1' => $aInvoiceAddress[0],
                    'line2' => isset($aInvoiceAddress[1]) ? $aInvoiceAddress[1] : '',
                    'line3' => isset($aInvoiceAddress[2]) ? $aInvoiceAddress[2] : '',
                    'line4' => isset($aInvoiceAddress[3]) ? $aInvoiceAddress[3] : '',
                    'postal_code' => $addressInvoice->postcode,
                    'municipality' => $addressInvoice->city,
                    'country_code' => FacilyPayCountry::getIsoAlpha3($countryInvoice->iso_code),
                    'country_label' => $countryInvoice->name[$customer->id_lang],
                ),
            ),
            'payment' => array(
                'merchant_guid' => $merchant_guid,
                'payment_amount' => number_format((float)$cart->getOrderTotal(), 2, '.', ''),
                'currency_code' => $this->context->currency->iso_code,
                'business_transaction' => array(
                    'code' => $sSlectedOpc
                ),
            ),
            'navigation' => array(
                'success_url' => $this->context->link->getModuleLink(
                    $module->name,
                    'validation',
                    array(
                        'reference' => $reference,
                        'cid' => $cart->id,
                        'amt' => number_format((float)$cart->getOrderTotal(), 2, '.', ''),
                        'tid' => $shortLabel
                    ),
                    true
                ),
                'fail_url' => $this->context->link->getPageLink('order.php', true),
                'server_response_url' => Context::getContext()->link->getModuleLink(
                    $module->name,
                    'orderUpdate',
                    array('iso_code' => $module->sIsoCodeCountry),
                    true
                )
            ),
            'merchant_context' => $aParamsEncode[0],
            'psp_context' => $aParamsEncode[1]
        );

        if ($psp_guid) {
            if (!empty($psp_guid) || $psp_guid != 0) {
                $parameters['psp_guid'] = $psp_guid;
            }
        }

        // Gestion des DOM
        $result = $module->pepsWebService->paiement(
            $parameters,
            (Oney::getConfigByCountry('dom_tom', $module->sIsoCodeCountry)) ?
                Oney::getConfigByCountry('country_reference', $module->sIsoCodeCountry) : $module->sIsoCodeCountry
        );

        $returnUrl = false;
        if (is_object($result)) {
            if (isset($result->encrypted_message) && !empty($result->encrypted_message)) {
                // Message encrypter : décrypter l'url
                $objResult = $this->pepsWebService->decryptBody(
                    $result->encrypted_message,
                    $module->sIsoCodeCountry
                );
                if (isset($objResult->returned_url)) {
                    $returnUrl = $objResult->returned_url;
                }
            } else {
                if (isset($result->returned_url)) {
                    $returnUrl = $result->returned_url;
                }
            }
        } else {
            Tools::redirect($this->context->link->getPageLink('order', true));
        }
        Tools::redirect($returnUrl);
    }

    public function formatAddr($aAddr)
    {
        $aReturn = array();
        if (Tools::strlen($aAddr) > 35) {
            $aReturn[] = Tools::substr($aAddr, 0, strrpos(Tools::substr($aAddr, 0, 35), ' '));
            $aReturn[] = Tools::substr($aAddr, strrpos(Tools::substr($aAddr, 0, 35), ' '));

            foreach ($aReturn as $key => $value) {
                if (Tools::strlen($value) > 35) {
                    $aReturn[$key] = Tools::substr(
                        $value,
                        0,
                        (strrpos(Tools::substr($value, 0, 35), ' ') - 1)
                    );
                    $aReturn[] = Tools::substr($value, strrpos(Tools::substr($value, 0, 35), ' ') - 1);
                }
            }
        } else {
            $aReturn[] = $aAddr;
        }

        return array_map('trim', $aReturn);
    }
    
    /**
     * Return true if the selected carrier is SoColissimo Liberty in relay point
     * @return array|bool = false
     */
    public function iscolissimoCarrier($iCartID, $sModule)
    {
        switch ($sModule) {
            case 'soliberte':
                $aFields = array(
                    'CONCAT(prname, " ", pradress1, " ", pradress2, " ", pradress3, " ", pradress4) as address',
                    'przipcode as zipcode',
                    'prtown as city',
                    'cecountry as country',
                );

                $sSQL = 'SELECT ' . implode(',', $aFields) . ' FROM ' . _DB_PREFIX_ . 'socolissimo_delivery_info ' .
                    'WHERE id_cart = ' . (int)$iCartID . ' ' .
                    'AND id_customer = ' . (int)Context::getContext()->customer->id;
                break;
            case 'colissimo':
                $aFields = array(
                    'CONCAT(company_name, " ", address1, " ", address2, " ", address3) as address',
                    'zipcode',
                    'city',
                    'iso_country as country',
                );

                $sSQL = 'SELECT ' . implode(',', $aFields) . ' FROM ' . _DB_PREFIX_ . 'colissimo_pickup_point pickup ' .
                    'JOIN ' . _DB_PREFIX_ . 'colissimo_cart_pickup_point pickup_cart ON '.
                    'pickup_cart.id_colissimo_pickup_point = pickup.id_colissimo_pickup_point ' .
                    'JOIN ' . _DB_PREFIX_ . 'cart cart ON cart.id_cart = pickup_cart.id_cart ' .
                    'WHERE cart.id_cart = ' . (int)$iCartID . ' ' .
                    'AND id_customer = ' . (int)Context::getContext()->customer->id;
                break;
        }

        return Db::getInstance()->getRow($sSQL);
    }
}
