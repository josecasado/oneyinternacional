<?php
/**
 * 3x 4x Oney Module version 1.1.21 for PrestaShop 1.6 and PrestaShop 1.7. Support contact : prestashop@itroom.fr
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/afl-3.0.php
 *
 * @author    ITroom (http://itroom.fr/)
 * @copyright ITroom
 * @license   https://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * @category  payment
 * @package   oney
 */

class OneyValidationModuleFrontController extends ModuleFrontController
{
    /**
     * This class should be use by your Instant Payment
     * Notification system to validate the order remotely
     */
    public function postProcess()
    {
        /** @var Oney $module */
        $module = $this->module;
        $ctx = Context::getContext();
        $iCartID = Tools::getValue('cid', 0);
        $iTransactionId = Tools::getValue('tid', 0);
        $fAmount = Tools::getValue('amt', 0);
        $sReference = Tools::getValue('reference', 0);

        if (Order::getOrderByCartId($iCartID) === false) {
            // Création de la commande si elle n'existe pas encore (Cause : callback beaucoup trop long)
            $module->facilypay_orders->reference = $sReference;
            $module->facilypay_orders->transaction_id = $iTransactionId;
            $module->facilypay_orders->save();
            $module->reference = $sReference;
            $module->validateOrder(
                $iCartID,
                Configuration::get('FACILYPAY_OS_PENDING'),
                (float)round($fAmount, 2),
                $module->name,
                '',
                array(),
                $ctx->cart->id_currency,
                false
            );
        }

        // Prestashop 1.6
        if (version_compare(_PS_VERSION_, '1.7', '<') === true) {
            $params = array(
                'reference'   => $sReference,
                'id_module' => $module->id
            );
            Tools::redirect($this->context->link->getModuleLink($module->name, 'confirmation', $params));
        }

        // Prestashop 1.7
        /** @var Order $order */
        $oOrder = Order::getByReference($sReference)->getFirst();

        if ($oOrder !== false && $this->context->customer->secure_key === $oOrder->getCustomer()->secure_key) {
            Tools::redirect(
                'index.php?controller=order-confirmation&id_cart=' . $oOrder->id_cart .
                '&id_module=' . $this->module->id .
                '&id_order=' . $oOrder->id .
                '&key=' . $oOrder->getCustomer()->secure_key
            );
        }
        // Si aucune action n'est trouvé, redirection vers l'accueil
        Tools::redirect('/');
    }
}
