<?php
/**
 * 3x 4x Oney Module version 1.1.21 for PrestaShop 1.6 and PrestaShop 1.7. Support contact : prestashop@itroom.fr
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/afl-3.0.php
 *
 * @author    ITroom (http://itroom.fr/)
 * @copyright ITroom
 * @license   https://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * @category  payment
 * @package   oney
 */

if (!defined('_PS_VERSION_')) {
    exit;
}

require_once 'api/PepsWebService.php';
require_once 'classes/Opc.php';
require_once 'classes/FacilyPayCarrier.php';
require_once 'classes/FacilyPayCountry.php';
require_once 'classes/FacilyPayLock.php';
require_once 'classes/FacilyPayOrder.php';
require_once 'classes/OneyPhoneNumber.php';

require_once('backward_compatibility/OneyTools.php');

class Oney extends PaymentModule
{
    public $pepsWebService;
    public $simulation;
    public $idLang;
    public $sIsoCodeCountry;
    public $reference;
    public $facilypay_lock;
    public $facilypay_orders;
    public $opc;
    public $facilypay_carrier;

    public $formConfig;
    public $formConfigUpload;
    public $formConfigUpload_allow_extension = array(
        'pdf'
    );
    private $oConfigCountry;

    public function __construct()
    {
        $this->name = 'oney';
        $this->tab = 'payments_gateways';
        $this->version = '1.1.21';
        $this->author = 'ITROOM';
        $this->need_instance = 0;
        $this->module_key = 'bfbd1f920327d7e2ea99ff318efb0aae';
        /**
         * Set $this->bootstrap to true if your module is compliant with bootstrap (PrestaShop 1.6)
         */
        $this->bootstrap = true;

        parent::__construct();
        $this->displayName = $this->l('3x 4x Oney');
        $this->description = $this->l('The first payment solution in 3 or 4 times by credit card');

        $this->confirmUninstall = $this->l('Are you sure you want to uninstall Oney 3x 4x ?');
        $this->ps_versions_compliancy = array('min' => '1.6.0', 'max' => _PS_VERSION_);

        $this->idLang = $this->context->language->id;
        $this->sIsoCodeCountry = 0;
        $record = null;
        // If not in BO
        if (strpos(Tools::getValue('controller', false), 'admin') !== 0 &&
            strpos(Tools::getValue('controller', false), 'Admin') !== 0) {
            // Prestashop 1.7
            try {
                if (class_exists('GeoIp2\Database\Reader') &&
                    (file_exists(_PS_GEOIP_DIR_ . _PS_GEOIP_CITY_FILE_))
                ) {
                    $reader = new GeoIp2\Database\Reader(_PS_GEOIP_DIR_ . _PS_GEOIP_CITY_FILE_);
                    $record = $reader->city(Tools::getRemoteAddr());
                    if (is_object($record)) {
                        $this->sIsoCodeCountry = $record->raw['country']['iso_code'];
                    }
                } elseif (file_exists(_PS_GEOIP_DIR_.'GeoLiteCity.dat')) {
                    include_once(_PS_GEOIP_DIR_.'geoipcity.inc');

                    $gi = geoip_open(realpath(_PS_GEOIP_DIR_.'GeoLiteCity.dat'), GEOIP_STANDARD);
                    $record = geoip_record_by_addr($gi, Tools::getRemoteAddr());

                    if (is_object($record)) {
                        $this->sIsoCodeCountry = $record->country_code;
                    }
                }
            } catch (\GeoIp2\Exception\AddressNotFoundException $e) {
                $record = null;
            }

            if (isset(Context::getContext()->cart->id)) {
                if (($iInvoiceId = Context::getContext()->cart->id_address_invoice) != 0) {
                    $aInvoiceAddress = Address::getCountryAndState($iInvoiceId);
                    $this->sIsoCodeCountry = Country::getIsoById($aInvoiceAddress['id_country']);
                } elseif ($record==null) {
                    $this->sIsoCodeCountry = Country::getIsoById(Configuration::get('PS_COUNTRY_DEFAULT'));
                }
            } elseif (isset(Context::getContext()->cookie->id_cart)) {
                $oCart = new Cart(Context::getContext()->cookie->id_cart);
                if (($iInvoiceId = $oCart->id_address_invoice) != 0) {
                    $aInvoiceAddress = Address::getCountryAndState($iInvoiceId);
                    $this->sIsoCodeCountry = Country::getIsoById($aInvoiceAddress['id_country']);
                } elseif ($record==null) {
                    $this->sIsoCodeCountry = Country::getIsoById(Configuration::get('PS_COUNTRY_DEFAULT'));
                }
            } elseif ($record==null) {
                $this->sIsoCodeCountry = Country::getIsoById(Configuration::get('PS_COUNTRY_DEFAULT'));
            }
        } else {
            $this->context->smarty->assign(array(
                'adminUrl' => Context::getContext()->link->getAdminLink('adminModules', false) . '&token=' . Tools::getValue('token') . '&configure=oney&submitOneyConnect=true'
            ));
        }

        $this->formConfig = array(
            'FACILYPAY_CATEGORY',
            'FACILYPAY_MODE',
            'FACILYPAY_COLOR',
            'FACILYPAY_MERCHANT_GUID',
            'FACILYPAY_PSP_GUID',
            'FACILYPAY_API_KEY_PAYMENT',
            'FACILIPAY_API_KEY_MARKETING',
            'FACILIPAY_ONEY_SECRET',
            'FACILYPAY_OS_PENDING',
            'FACILYPAY_ACTION_CONFIRM',
            'FACILIPAY_INTERMEDIAIRE_CREDIT',
            'FACILIPAY_INTERMEDIATION_TYPE',
            'FACILIPAY_MERCHANT_NAME'
        );

        $this->formConfigUpload = array(
            'FACILIPAY_LEGAL_DOCUMENT'
        );

        $this->opc = new Opc();
        $this->facilypay_carrier = new FacilyPayCarrier();
        $this->facilypay_lock = new FacilyPayLock();
        $this->pepsWebService = new PepsWebService(Configuration::get('FACILYPAY_MODE'));
        $this->facilypay_orders = new FacilyPayOrder();

        $this->context->smarty->assign(array(
            'iso_code_oney' =>  $this->sIsoCodeCountry
        ));

        if (version_compare(_PS_VERSION_, '1.7', '<')
            && isset($this->context->controller) &&
            !empty($this->context->controller)
        ) {
            if (strpos(Tools::getValue('controller'), 'Admin') === 0) {
                $this->context->controller->addCSS($this->_path . '/views/css/back.css');
                $this->context->controller->addJS($this->_path . '/views/js/back.js');
            } else {
                $this->context->controller->addCSS($this->_path . '/views/css/front.css');
                if (Configuration::get('FACILYPAY_COLOR') == 'gray') {
                    $this->context->controller->addCSS($this->_path . '/views/css/front_grey.css');
                }
            }
        }
    }

    public function install()
    {
        if (extension_loaded('curl') == false) {
            $this->_errors[] = $this->l('You have to enable the cURL extension on your server to install this module');
            return false;
        }

        Configuration::set('FACILYPAY_TOKEN', Tools::encrypt(Tools::getShopDomainSsl() . time()));

        include('sql/install.php');

        $this->installHookCustom();

        return parent::install()  && ((version_compare(_PS_VERSION_, '1.7', '<')) ? $this->registerHook('displayTop') : $this->registerHook('displayWrapperBottom')) &&
            $this->installOrderState() &&
            $this->registerHook('header') &&
            // Action
            $this->registerHook('actionAdminControllerSetMedia') &&
            $this->registerHook('actionPaymentCCAdd') &&
            $this->registerHook('actionObjectOrderSlipAddAfter') &&
            $this->registerHook('actionOrderHistoryAddAfter') &&
            // Display
            $this->registerHook('displayHome') &&
            $this->registerHook('displayLeftColumn') &&
            $this->registerHook('displayProductPriceBlock') &&
            $this->registerHook('displayProductButtons') &&
            $this->registerHook('displayRightColumn') &&
            $this->registerHook('displayShoppingCartFooter') &&
            $this->registerHook('displayCartPaiement') &&
            // Payment
            $this->registerHook('displayPaymentTop') &&
            $this->registerHook('displayPayment') &&
            $this->registerHook('paymentOptions') &&
            $this->registerHook('paymentReturn');
    }

    public function uninstall()
    {
        include('sql/uninstall.php');

        return parent::uninstall() && $this->uninstallOrderState();
    }

    /**
     * @return bool
     * @throws PrestaShopDatabaseException
     * @throws PrestaShopException
     */
    public function installOrderState()
    {
        if (Configuration::get('PS_LANG_DEFAULT') == false) {
            Configuration::updateValue('PS_LANG_DEFAULT', Context::getContext()->language->id);
        }

        $orderStates = array(
            'FACILYPAY_OS_PENDING'   => array(
                'fr' => 'En attente de paiement 3x 4x Oney',
                'it' => 'In attesa di pagamento 3x 4x Oney',
                'pt' => 'Aguarda decisão 3x 4x Oney',
                'en' => 'Awaiting payment 3x 4x Oney',
                'es' => 'En espera de confirmación de pago 3x 4x Oney'
        ),
            'FACILYPAY_OS_CAPTURE'   => array(
                'fr' => 'Prêt à envoyer 3x 4x Oney',
                'it' => 'Pronto a inviare 3x 4x Oney',
                'pt' => 'Pronto para expedir 3x 4x Oney',
                'en' => 'Ready to send 3x 4x Oney',
                'es' => 'Listo para envío 3x 4x Oney'
            ),
            'FACILYPAY_OS_FAVORABLE' => array(
                'fr' => 'Avis favorable 3x 4x Oney',
                'it' => 'Parere favorevole 3x 4x Oney',
                'pt' => 'Estado favorável 3x 4x Oney',
                'en' => 'Favourable opinion 3x 4x Oney',
                'es' => 'Pedido confirmado favorable 3x 4x Oney'
            )
        );

        foreach ($orderStates as $key => $os) {
            $name = array();
            foreach (Language::getLanguages(1, Context::getContext()->shop->id) as $idLang) {
                switch ($idLang['iso_code']) {
                    case 'it':
                        $name[$idLang['id_lang']] = $os['it'];
                        break;
                    case 'en':
                        $name[$idLang['id_lang']] = $os['en'];
                        break;
                    case 'pt':
                        $name[$idLang['id_lang']] = $os['pt'];
                        break;
                    default:
                        $name[$idLang['id_lang']] = $os['fr'];
                }
            }
            $orderState = new OrderState();
            $orderState->module_name = $this->name;
            $orderState->color = '#3399ff';
            $orderState->name = $name;

            if (!$orderState->add()) {
                return false;
            } else {
                Configuration::updateValue($key, $orderState->id);
            }
        }

        return true;
    }

    /**
     * @return bool
     * @throws PrestaShopDatabaseException
     * @throws PrestaShopException
     */
    public function uninstallOrderState()
    {
        $idState = Db::getInstance()->executeS(
            'SELECT id_order_state FROM ' . _DB_PREFIX_ . 'order_state WHERE module_name = \'' . $this->name . '\''
        );

        foreach ($idState as $idos) {
            $orderState = new OrderState((int)$idos['id_order_state']);
            if (!$orderState->delete()) {
                return false;
            }
        }

        return true;
    }

    public function installHookCustom()
    {
        $aFileToAddHook = array(
            'cart-detailed-totals.tpl' => _PS_THEME_DIR_ . 'templates/checkout/_partials/cart-detailed-totals.tpl',
            'cart-summary.tpl' => _PS_THEME_DIR_ . 'templates/checkout/_partials/cart-summary.tpl'
        );

        foreach ($aFileToAddHook as $key => $sPathTemplate) {
            if (file_exists($sPathTemplate)) {
                $sFileContent = Tools::file_get_contents($sPathTemplate);
                $iPosLastBlock = strrpos($sFileContent, '{/block}', -0);
                if ($iPosLastBlock !== false && strpos($sFileContent, '{hook h="displayCartPaiement"}') === false) {
                    $sFileContent = substr_replace($sFileContent, '{hook h="displayCartPaiement"}', $iPosLastBlock, -0);
                    rename($sPathTemplate, str_replace($key, 'oneyReplace_' . $key, $sPathTemplate));
                    file_put_contents($sPathTemplate, $sFileContent);
                }
            }
        }
    }
        
    public static function getConfigByCountry($sKeyName, $sIsoCodeCountry)
    {
        $sConfigPath = dirname(__FILE__) . '/configCountry.json';
        // Check if configCountryOverride.json exist
        if (file_exists(str_replace('.json', 'Override.json', $sConfigPath))) {
            $sConfigPath = str_replace('.json', 'Override.json', $sConfigPath);
        }
        if (file_exists($sConfigPath)) {
            $sContentConfig = json_decode(Tools::file_get_contents($sConfigPath));
            if (isset($sContentConfig->$sIsoCodeCountry)) {
                if (isset($sContentConfig->$sIsoCodeCountry->$sKeyName)) {
                    return $sContentConfig->$sIsoCodeCountry->$sKeyName;
                } else {
                    return $sContentConfig->DEFAULT->$sKeyName;
                }
            } else {
                if (isset($sContentConfig->DEFAULT->$sKeyName)) {
                    return $sContentConfig->DEFAULT->$sKeyName;
                }
            }
        } else {
            if (_PS_MODE_DEV_) {
                throw new Exception("File 'configCountry.json' not found in Oney module");
            }
        }

        return "";
    }

    public function onboarding()
    {
        if (Tools::isSubmit('submitOneyOnboarding')) {
            $sendMail = true;

            if ($sendMail) {
                $data = array();
                foreach (Tools::getValue('fiche') as $key => $value) {
                    $data['{' . $key . '}'] = Tools::nl2br($value);
                }
                unset($value);

                $to = self::getConfigByCountry("mail_onboarding", Tools::strtoupper(Context::getContext()->language->iso_code));
                
                $mailSend = Mail::Send(
                    $this->idLang,
                    'new_partner',
                    $this->l('New Partner'),
                    $data,
                    $to,
                    $this->name,
                    null,
                    null,
                    null,
                    null,
                    _PS_MODULE_DIR_ . $this->name . '/mails/'
                );
                if (!$mailSend) {
                    $this->context->smarty->assign('error', $this->l('An error was occured while sending mail'));
                } else {
                    Configuration::updateValue('FACILYPAY_ONBOARDING_SEND', 1);
                    Tools::redirectAdmin(
                        $this->context->link->getAdminLink('AdminModules') . '&configure=' . Tools::safeOutput(
                            $this->name
                        ) . '&sendMail=1'
                    );
                }
            }
        }

        return $this->context->smarty->fetch($this->local_path . 'views/templates/admin/onboarding.tpl');
    }

    /**
     * Load the configuration form
     */
    public function getContent()
    {
        $this->uploadLegalFile();
        if ($sIsoCodeFileToDelete = Tools::getValue('deleteUploadFile', false)) {
            $this->deleteUploadFile($sIsoCodeFileToDelete);
        }
        $this->uploadLegalFile();

        $sSQL = 'SELECT * FROM ' . _DB_PREFIX_ . 'carrier c WHERE c.deleted = 0 AND active = 1 GROUP BY id_reference';
        $carriers = Db::getInstance()->executeS($sSQL);

        $aStores = array(0 => array('id_store' => 0, 'name' => ''));
        if (version_compare(_PS_VERSION_, '1.7', '>=')) {
            $sSQL = 'SELECT s.id_store, `name` FROM ' . _DB_PREFIX_ . 'store_lang sl '.
                'JOIN ' . _DB_PREFIX_ . 'store s ON s.id_store = sl.id_store ' .
                'WHERE s.active = 1 ' .
                'AND sl.id_lang = ' . (int)Context::getContext()->employee->id_lang;
        } else {
            $sSQL = 'SELECT id_store, `name` FROM ' . _DB_PREFIX_ . 'store s WHERE s.active = 1 ';
        }
        $aStores = array_merge($aStores, Db::getInstance()->executeS($sSQL));
        $association_carriers = array(
            1 => $this->l('Collection of the goods in the merchant store'),
            2 => $this->l('Collection in a third party point (like ups, alveol, etc.)'),
            3 => $this->l('Collection in an airport, train station or travel agency'),
            4 => $this->l('Carrier (La Poste, Colissimo, UPS, DHL...or any private carrier)'),
            5 => $this->l('Electronic ticket issuance, download, etc')
        );

        $priority = array(
            1 => $this->l('Express'),
            2 => $this->l('Standard'),
            3 => $this->l('Priority'),
        );

        $priorities_code = array(
            1 => $this->l('Less than or equal to 1 hour'),
            2 => $this->l('Greater than 1 hour'),
            3 => $this->l('Immediate'),
            4 => $this->l('24/24 7/7')
        );

        $addresses_type = array(
            1 => $this->l('Merchant'),
            2 => $this->l('Third party relay point'),
            3 => $this->l('Airport, train station, travel agency'),
            4 => $this->l('Billing address'),
            5 => $this->l('Delivery address'),
            6 => $this->l('Electronic way (ticket, download)')
        );

        $categories = array(
            1  => $this->l('Food and drinks'),
            2  => $this->l('Auto and motorcycle'),
            3  => $this->l('Culture and entertainment'),
            4  => $this->l('Home and garden'),
            5  => $this->l('Home appliances'),
            6  => $this->l('Bidding and multi purchasing'),
            7  => $this->l('Flowers and gifts'),
            8  => $this->l('Computers and software'),
            9  => $this->l('Health and beauty'),
            10 => $this->l('Personal services'),
            11 => $this->l('Professional services '),
            12 => $this->l('Sport'),
            13 => $this->l('Clothing and accessories'),
            14 => $this->l('Travel and tourism'),
            15 => $this->l('Hifi, photo and video'),
            16 => $this->l('Telephone and communication')
        );

        /**
         * If values have been submitted in the form, process.
         */
        if ($id_lang = Tools::getValue('submitFacilypayModuleIdentification', false)) {
            $this->postProcessIdentification();
        }
        if ($id_lang = Tools::getValue('submitFacilypayModuleSaveOPC', false)) {
            $this->postProcessOPC($id_lang);
        }
        if ($id_lang = Tools::getValue('submitFacilypayModuleGetOPC', false)) {
            $this->postProcessGetOpc($id_lang);
        }
        if ($sIsoCodeCountry = Tools::getValue('submitFacilypayModuleTest', false)) {
            $this->postProcessIdentification();
            $this->postProcessTest($sIsoCodeCountry);
        }
        if (!Configuration::get('FACILYPAY_TOKEN')) {
            Configuration::updateValue('FACILYPAY_TOKEN', Tools::encrypt(Tools::getShopDomainSsl() . time()));
        }

        $psCountries = Country::getCountries(Context::getContext()->employee->id_lang, 1);
        $oneyCountries=[];
        $sConfigPath = dirname(__FILE__) . '/configCountry.json';
        if (file_exists($sConfigPath)) {
            $oneyCountries = json_decode(Tools::file_get_contents($sConfigPath));
        } else {
            if (_PS_MODE_DEV_) {
                throw new Exception("File 'configCountry.json' not found in Oney module");
            }
        }

        $countries=[];

        foreach ($psCountries as $country) {
            foreach ($oneyCountries as $key2 => $value2) {
                // Gestion des DOM
                if ($country['iso_code']==$key2 && (!isset($value2->dom_tom) || !$value2->dom_tom)) {
                    $countries[]=$country;
                }
            }
        }

        $this->context->smarty->assign(
            array(
                'countries'                => $countries,
                'stores'                   => $aStores,
                'module_dir'               => $this->_path,
                'shop_url'                 => Tools::getShopDomainSsl(true, true),
                'form_uri'                 => $_SERVER['REQUEST_URI'],
                'FACILYPAY_MODE'           => Configuration::get('FACILYPAY_MODE'),
                'FACILYPAY_COLOR'          => Configuration::get('FACILYPAY_COLOR'),
                'FACILYPAY_ACTION_CONFIRM' => Configuration::get('FACILYPAY_ACTION_CONFIRM'),
                'carriers'                 => $carriers,
                'association_carriers'     => $association_carriers,
                'priorities'               => $priority,
                'priorities_code'          => $priorities_code,
                'addresses_type'           => $addresses_type,
                'categories'               => $categories,
                'selected_categorie'       => Configuration::get('FACILYPAY_CATEGORY'),
                'selected_association'     => $this->facilypay_carrier->getAssociation(),
                'token'                    => Configuration::get('FACILYPAY_TOKEN'),
            )
        );

        foreach ($countries as $country) {
            $this->context->smarty->assign(
                array(
                    'FACILYPAY_MERCHANT_GUID_' . $country['iso_code']     => Configuration::get(
                        'FACILYPAY_MERCHANT_GUID_' . $country['iso_code']
                    ),
                    'FACILYPAY_ACTION_CONFIRM' . $country['iso_code']     => Configuration::get(
                        'FACILYPAY_ACTION_CONFIRM_' . $country['iso_code']
                    ),
                    'FACILYPAY_PSP_GUID_' . $country['iso_code']          => Configuration::get(
                        'FACILYPAY_PSP_GUID_' . $country['iso_code']
                    ),
                    'FACILYPAY_API_KEY_PAYMENT_' . $country['iso_code']   => Configuration::get(
                        'FACILYPAY_API_KEY_PAYMENT_' . $country['iso_code']
                    ),
                    'FACILIPAY_API_KEY_MARKETING_' . $country['iso_code'] => Configuration::get(
                        'FACILIPAY_API_KEY_MARKETING_' . $country['iso_code']
                    ),
                    'FACILIPAY_ONEY_SECRET_' . $country['iso_code']       => Configuration::get(
                        'FACILIPAY_ONEY_SECRET_' . $country['iso_code']
                    ),
                    'FACILIPAY_INTERMEDIAIRE_CREDIT_' . $country['iso_code']       => Configuration::get(
                        'FACILIPAY_INTERMEDIAIRE_CREDIT_' . $country['iso_code'],
                        null,
                        null,
                        null,
                        0
                    ),
                    'FACILIPAY_INTERMEDIATION_TYPE_' . $country['iso_code']       => Configuration::get(
                        'FACILIPAY_INTERMEDIATION_TYPE_' . $country['iso_code'],
                        null,
                        null,
                        null,
                        0
                    ),
                    'FACILIPAY_MERCHANT_NAME_' . $country['iso_code']          => Configuration::get(
                        'FACILIPAY_MERCHANT_NAME_' . $country['iso_code']
                    ),
                    'FACILIPAY_LISTE_OPC_' . $country['iso_code']         => $this->opc->getListOpc(
                        $country['iso_code']
                    ),
                )
            );
        }

        $aOPC = $this->opc->getListOpc(0);
        if (empty($aOPC) || Tools::getValue('debugOnboarding')) {
            $output = $this->onboarding();
        }

        if ((Tools::isSubmit('submitOneyConnect') ||
            !isset($output) ||
            Configuration::get('FACILYPAY_ONBOARDING_SEND', false) ||
            Tools::isSubmit('submitFacilypayModuleIdentification') ||
            Tools::isSubmit('submitFacilypayModuleTest')) && !Tools::getValue('debugOnboarding')
        ) {
            $output = $this->context->smarty->fetch($this->local_path . 'views/templates/admin/admin.tpl');
        }

        return $output;
    }

    public function uploadLegalFile()
    {
        foreach ($this->formConfigUpload as $itemToUpload) {
            if (isset($_FILES[$itemToUpload])) {
                $aFileInfo = $_FILES[$itemToUpload];
                foreach ($aFileInfo['tmp_name'] as $sIsoCode => $file) {
                    if (!empty($_FILES[$itemToUpload]['tmp_name'][$sIsoCode])) {
                        if (Oney::getConfigByCountry('upload_legal_file', $sIsoCode)) {
                            if (($sExtension = $this->checkExtension($aFileInfo['name'][$sIsoCode]))) {
                                $bMoveFile = move_uploaded_file(
                                    $aFileInfo['tmp_name'][$sIsoCode],
                                    _PS_MODULE_DIR_ . $this->name . '/' . 'legals_files/' . $sIsoCode . '_legalFile.' . $sExtension
                                );
                            } else {
                                // wrong file
                                $this->context->smarty->assign('error', $this->l('Failure - The file extension is not valid'));
                            }
                        }
                    }
                }
            }
        }
    }

    public static function getUploadedFile($sIsoCode)
    {
        $sPath = '/modules/oney/legals_files/' . $sIsoCode . '_legalFile.pdf';
        if (file_exists(_PS_MODULE_DIR_ . '/oney/legals_files/' . $sIsoCode . '_legalFile.pdf')) {
            return $sPath ;
        }

        return false;
    }

    public function checkExtension($aFileInfo)
    {
        if (in_array(
            ($sExtension = pathinfo($aFileInfo, PATHINFO_EXTENSION)),
            $this->formConfigUpload_allow_extension
        )) {
            return $sExtension;
        }

        return false;
    }

    public function deleteUploadFile($sIsoCode)
    {
        unlink(_PS_MODULE_DIR_ . $this->name . '/' . 'legals_files/' . $sIsoCode . '_legalFile.pdf');
        OneyTools::redirectAdmin(
            $this->context->link->getAdminLink('AdminModules', 1) .
            '&configure=' . Tools::safeOutput($this->name) . ''
        );
    }

    /**
     * Sauvegarde des informations
     */
    protected function postProcessIdentification()
    {
        // Insertion de l'assocation des transporteurs
        $carriers = Tools::getValue('oney_carrier');
        if (!empty($carriers)) {
            foreach ($carriers as $key => $carrier) {
                $data = array(
                    'id_carrier'             => $key,
                    'delivery_mode'          => $carrier['carrier'],
                    'priority'               => $carrier['priority'],
                    'priority_delivery_code' => $carrier['priority_code'],
                    'address_type'           => $carrier['address_type'],
                    'id_store'               => $carrier['store']
                );
                $this->facilypay_carrier->setAssociation($data);
            }
        }

        foreach (OneyTools::getAllValues() as $key => $value) {
            if (in_array($key, $this->formConfig)) {
                $getValue = Tools::getValue($key);
                if (is_array($getValue)) {
                    foreach ($getValue as $k => $v) {
                        if (!empty($v)) {
                            Configuration::updateValue($key . '_' . $k, $v);
                        } else {
                            ConfigurationCore::deleteByName($key . '_' . $k);
                        }
                    }
                } else {
                    Configuration::updateValue($key, $getValue);
                }
            }
        }

        unset($value);
    }

    /* Sauvegarde des OPC choisies */
    protected function postProcessOPC($sIsoCoCountry)
    {
        $data = Tools::getValue('FACILYPAY_OPC_' . $sIsoCoCountry);

        $iShopId = ShopCore::getContextShopID();
        if ($this->opc->cleanSelected($sIsoCoCountry, $iShopId)) {
            if (!empty($data)) {
                foreach ($data as $row) {
                    if ($iShopId) {
                        $opc = new Opc(OPC::getIdWithTransactionCode($row, $sIsoCoCountry, $iShopId));
                        $opc->selected = 1;
                        $opc->iso_code = $sIsoCoCountry;
                        $opc->save();
                    } else {
                        foreach (ShopCore::getShops(1) as $shop) {
                            $opc = new Opc(OPC::getIdWithTransactionCode($row, $sIsoCoCountry, $shop['id_shop']));
                            $opc->selected = 1;
                            $opc->iso_code = $sIsoCoCountry;
                            $opc->save();
                        }
                    }
                }
            }
        }
    }

    /* Appel à la liste OPC pour tester le merchand_guid */
    protected function postProcessTest($sIsoCodeCountry)
    {
        $params = array();
        $params['merchant_guid'] = Configuration::get('FACILYPAY_MERCHANT_GUID_' . $sIsoCodeCountry);
        $sPSPGuid = Configuration::get('FACILYPAY_PSP_GUID_' . $sIsoCodeCountry);
        if (!empty($sPSPGuid) || $sPSPGuid != 0) {
            $params['psp_guid'] = $sPSPGuid;
        }

        if ($this->pepsWebService->listerOperationsCommerciales($params, $sIsoCodeCountry)) {
            $this->context->smarty->assign('success', $this->l('Success - the connection is functional'));
        } else {
            $this->context->smarty->assign('error', $this->l('Failure - Connection failed'));
        }
    }

    /* Récupération de la liste OPC pour la sauvegarder */
    protected function postProcessGetOpc($sIsoCodeCountry)
    {
        $sMerchantGUID = Configuration::get('FACILYPAY_MERCHANT_GUID_' . $sIsoCodeCountry);

        if (!empty($sMerchantGUID)) {
            $params = array(
                'merchant_guid' => $sMerchantGUID,
                'business_transaction_type' => 'PNFCB'
            );

            $sPSPGuid = Configuration::get('FACILYPAY_PSP_GUID_' . $sIsoCodeCountry);
            if (!empty($sPSPGuid) || $sPSPGuid != 0) {
                $params['psp_guid'] = $sPSPGuid;
            }

            try {
                $data = $this->pepsWebService->listerOperationsCommerciales($params, $sIsoCodeCountry);

                $iShopId = ShopCore::getContextShopID();
                foreach ($data as $row) {
                    if ($iShopId) {
                        $opc = new Opc(Opc::getIdWithTransactionCode(
                            $row->business_transaction_code,
                            $sIsoCodeCountry,
                            $iShopId
                        ));
                        $opc->business_transaction_code = $row->business_transaction_code;
                        $opc->business_transaction_version = $row->business_transaction_version;
                        $opc->business_transaction_type = $row->business_transaction_type;
                        $opc->short_label = $row->short_label;
                        $opc->long_label = $row->long_label;
                        $opc->customer_label = $row->customer_label;
                        $opc->minimum_number_of_instalments = $row->minimum_number_of_instalments;
                        $opc->maximum_number_of_instalments = $row->maximum_number_of_instalments;
                        $opc->choosable_number_of_instalments = $row->choosable_number_of_instalments;
                        $opc->minimum_selling_price = $row->minimum_selling_price;
                        $opc->maximum_selling_price = $row->maximum_selling_price;
                        $opc->discount_rate = $row->discount_rate;
                        $opc->free_business_transaction = $row->free_business_transaction;
                        $opc->example = $row->example;
                        $opc->validity_start_date = $row->validity_start_date;
                        $opc->validity_end_date = $row->validity_end_date;
                        $opc->postponement_duration = $row->postponement_duration;
                        $opc->display_order = $row->display_order;
                        $opc->iso_code = $sIsoCodeCountry;
                        $opc->id_shop = ShopCore::getContextShopID();
                        $opc->save();
                    } else {
                        foreach (ShopCore::getShops(1) as $shop) {
                            $opc = new Opc(Opc::getIdWithTransactionCode(
                                $row->business_transaction_code,
                                $sIsoCodeCountry,
                                $shop['id_shop']
                            ));
                            $opc->business_transaction_code = $row->business_transaction_code;
                            $opc->business_transaction_version = $row->business_transaction_version;
                            $opc->business_transaction_type = $row->business_transaction_type;
                            $opc->short_label = $row->short_label;
                            $opc->long_label = $row->long_label;
                            $opc->customer_label = $row->customer_label;
                            $opc->minimum_number_of_instalments = $row->minimum_number_of_instalments;
                            $opc->maximum_number_of_instalments = $row->maximum_number_of_instalments;
                            $opc->choosable_number_of_instalments = $row->choosable_number_of_instalments;
                            $opc->minimum_selling_price = $row->minimum_selling_price;
                            $opc->maximum_selling_price = $row->maximum_selling_price;
                            $opc->discount_rate = $row->discount_rate;
                            $opc->free_business_transaction = $row->free_business_transaction;
                            $opc->example = $row->example;
                            $opc->validity_start_date = $row->validity_start_date;
                            $opc->validity_end_date = $row->validity_end_date;
                            $opc->postponement_duration = $row->postponement_duration;
                            $opc->display_order = $row->display_order;
                            $opc->iso_code = $sIsoCodeCountry;
                            $opc->id_shop = $shop['id_shop'];
                            $opc->save();
                        }
                    }
                }

                $this->context->smarty->assign(
                    array(
                        'success'    => $this->l('Success - list of imported UCIs'),
                        'tab_active' => 2
                    )
                );
            } catch (Exception $e) {
                $this->context->smarty->assign('error', $this->l('Failure - ') . $e->getMessage());
                return false;
            }
        } else {
            $this->context->smarty->assign('error', $this->l('No ID entered'));
            return false;
        }
    }

    /**
     * Add the CSS & JavaScript files you want to be loaded in the BO.
     */
    public function hookActionAdminControllerSetMedia()
    {
        if (Tools::getValue('configure') == $this->name) {
            $this->context->controller->addCSS($this->_path . 'views/css/back.css');
            $this->context->controller->addJS($this->_path . 'views/js/back.js');
        }
    }

    /**
     * Ajout du code de transaction pour savoir si 3x ou 4x
     * @param $params
     */
    public function hookActionPaymentCCAdd($params)
    {
        $params['paymentCC']->transaction_id = $this
            ->facilypay_orders
            ->getTransactionId(
                $params['paymentCC']->order_reference
            );
        $params['paymentCC']->save();
    }

    /**
     * @param $params
     * @throws PrestaShopDatabaseException
     * @throws PrestaShopException
     */
    public function hookActionObjectOrderSlipAddAfter($params)
    {
        /** @var OrderSlipCore $oOrderSlip */
        $oOrderSlip = $params['object'];

        /** @var OrderCore $oOrder */
        $oOrder = new Order($oOrderSlip->id_order);
        /** @var AddressCore $oAddress */
        $oAddress = new Address($oOrder->id_address_invoice);

        if (Tools::strtoupper(CountryCore::getIsoById($oAddress->id_country)) === 'FR') {
            return;
        }

        if ($oOrder->payment == $this->name) {
            $aParameters = array(
                'language_code' => Tools::strtoupper(Country::getIsoById($oAddress->id_country)),
                'merchant_request_id' => date('YmdHis') . '-' . $oOrder->id,
                'purchase' => array(
                    'cancellation_reason_code' => 0,
                    'cancellation_amount' => number_format(
                        (float)($oOrderSlip->amount + $oOrderSlip->total_shipping_tax_incl),
                        2,
                        '.',
                        ''
                    )
                )
            );

            $this->pepsWebService->cancelPayment(
                $oOrder->reference,
                $aParameters,
                Tools::strtoupper(Country::getIsoById($oAddress->id_country))
            );
        }
    }

    /**
     * Add the CSS & JavaScript files you want to be added on the FO.
     */
    public function hookHeader()
    {
        $this->context->controller->addJS($this->_path . '/views/js/front.js');
        $this->context->controller->addCSS($this->_path . '/views/css/front.css');
    }

    public function hookDisplayTop()
    {
        if (Configuration::get('PS_CATALOG_MODE')) {
            return;
        }

        $bDomTom = Oney::getConfigByCountry('dom_tom', $this->sIsoCodeCountry);

        if ($bDomTom) {
            if (empty($this->opc->getListOpc(Oney::getConfigByCountry('country_reference', $this->sIsoCodeCountry), 1))) {
                return '';
            }
        } else {
            if (empty($this->opc->getListOpc($this->sIsoCodeCountry, 1))) {
                return '';
            }
        }


        $getInfoListOpc = $this->getInfoListOpc();
        $rangeOpc = array();
        // Gestion des DOM
        if ($bDomTom) {
            /* Affichage des OPC choisi par le client */
            $opcSelected = $this->opc->getListOpc(Oney::getConfigByCountry('country_reference', $this->sIsoCodeCountry), 1);
        } else {
            /* Affichage des OPC choisi par le client */
            $opcSelected = $this->opc->getListOpc($this->sIsoCodeCountry, 1);
        }

        foreach ($opcSelected as $listopc) {
            $nxOney = $listopc['minimum_number_of_instalments'];
            if (!isset($rangeOpc[$nxOney]['minimum']) || $rangeOpc[$nxOney]['minimum'] > $listopc['minimum_selling_price']) {
                $rangeOpc[$nxOney]['minimum'] = $listopc['minimum_selling_price'];
            }

            if (!isset($rangeOpc[$nxOney]['maximum']) || $rangeOpc[$nxOney]['maximum'] < $listopc['maximum_selling_price']) {
                $rangeOpc[$nxOney]['maximum'] = $listopc['maximum_selling_price'];
            }
        }

        $this->context->smarty->assign(
            array(
                'listopc' => $rangeOpc,
                'minimum_selling_price' => $getInfoListOpc['minimum'],
                'maximum_selling_price' => $getInfoListOpc['maximum'],
                'free_transaction'      => $getInfoListOpc['free_transaction'],
                'legalNotice'           => $this->getMentionLegale('P', $this->sIsoCodeCountry),
                'base_uri'              => __PS_BASE_URI__
            )
        );

        if ($this->sIsoCodeCountry != 'FR' && !$bDomTom) {
            return $this->display(__FILE__, 'views/templates/hook/international/pedagogique-oney.tpl');
        } else {
            return $this->display(__FILE__, 'views/templates/hook/france/pedagogique-oney.tpl');
        }
    }

    public function hookDisplayWrapperBottom()
    {
        return $this->hookDisplayTop();
    }

    public function hookDisplayProductPriceBlock($params)
    {
        $bDomTom = Oney::getConfigByCountry('dom_tom', $this->sIsoCodeCountry);

        if ($bDomTom) {
            if (empty($this->opc->getListOpc(Oney::getConfigByCountry('country_reference', $this->sIsoCodeCountry), 1))) {
                return '';
            }
        } else {
            if (empty($this->opc->getListOpc($this->sIsoCodeCountry, 1))) {
                return '';
            }
        }

        switch ($params['type']) {
            case 'price':
            case 'after_price':
                // ignore le cas si prestashop suppérieur à 1.6.0
                if (version_compare(_PS_VERSION_, '1.6.1', '>=') && $params['type'] === 'price') {
                    break;
                }
                if (is_object($params['product'])) {
                    if (isset($params['product']->product) || ($params['product'] instanceof Product)) {
                        $price = $params['product']->getPrice();
                    } else {
                        // Prestashop 1.7.5
                        $price = Product::getPriceStatic($params['product']->getId());
                        // @TODO déclinaison.
                    }
                } else {
                    if (isset($params['product']['id_product_attribute']) && version_compare(_PS_VERSION_, '1.7', '>=')) {
                        $price = Product::getPriceStatic(
                            $params['product']['id_product'],
                            true,
                            $params['product']['id_product_attribute']
                        );
                    }
                }
                if (isset($price)) {
                    $this->context->smarty->assign(
                        array(
                            'hookName' => $params['type']
                        )
                    );

                    if ($bDomTom) {
                        $opcs = $this->opc->getListOpcWithPrice($price, Oney::getConfigByCountry('country_reference', $this->sIsoCodeCountry));
                    } else {
                        $opcs = $this->opc->getListOpcWithPrice($price, $this->sIsoCodeCountry);
                    }
                    
                    $getInfoListOpc = $this->getInfoListOpc();

                    $this->context->smarty->assign(
                        array(
                            'opcs'                  => $opcs,
                            'free_transaction'      => $getInfoListOpc['free_transaction'],
                            'priceProduct'          => $price,
                            'base_uri'              => __PS_BASE_URI__
                        )
                    );
                    // Détection si listing ou non
                    if (version_compare(_PS_VERSION_, '1.7', '<')) {
                        $this->context->smarty->assign('inProduct', is_object($params['product']));
                    } else {
                        $this->context->smarty->assign('inProduct', $params['type'] == 'after_price');
                    }

                    if ($this->sIsoCodeCountry != 'FR' && !$bDomTom) {
                        return $this->display(__FILE__, 'views/templates/hook/international/after-price.tpl');
                    } else {
                        return $this->display(__FILE__, 'views/templates/hook/france/after-price.tpl');
                    }
                }

                return;
            default:
                return;
        }
    }

    public function hookDisplayPaymentTop()
    {
        if (Configuration::get('PS_CATALOG_MODE')) {
            return '';
        }

        if ($message = Context::getContext()->cookie->oneyerror) {
            unset(Context::getContext()->cookie->oneyerror);
            echo '<div class="alert alert-danger">'. $message . '</div>';
        }

        $bDomTom = Oney::getConfigByCountry('dom_tom', $this->sIsoCodeCountry);

        if ($bDomTom) {
            if (empty($this->opc->getListOpc(Oney::getConfigByCountry('country_reference', $this->sIsoCodeCountry), 1))) {
                return '';
            }
        } else {
            if (empty($this->opc->getListOpc($this->sIsoCodeCountry, 1))) {
                return '';
            }
        }

        $getInfoListOpc = $this->getInfoListOpc();

        $this->context->smarty->assign(
            array(
                'minimum_selling_price' => $getInfoListOpc['minimum'],
                'maximum_selling_price' => $getInfoListOpc['maximum'],
                'free_transaction'      => $getInfoListOpc['free_transaction'],
                'payment_pedagogic'     => true,
                'base_uri'              => __PS_BASE_URI__
            )
        );

        return '';
    }

    public function hookDisplayHome()
    {
        if (Configuration::get('PS_CATALOG_MODE')) {
            return;
        }

        $bDomTom = Oney::getConfigByCountry('dom_tom', $this->sIsoCodeCountry);

        if ($bDomTom) {
            if (empty($this->opc->getListOpc(Oney::getConfigByCountry('country_reference', $this->sIsoCodeCountry), 1))) {
                return '';
            }
        } else {
            if (empty($this->opc->getListOpc($this->sIsoCodeCountry, 1))) {
                return '';
            }
        }

        $getInfoListOpc = $this->getInfoListOpc();

        $this->context->smarty->assign(
            array(
                'minimum_selling_price' => $getInfoListOpc['minimum'],
                'maximum_selling_price' => $getInfoListOpc['maximum'],
                'free_transaction'      => $getInfoListOpc['free_transaction'],
                'base_uri'              => __PS_BASE_URI__
            )
        );

        if ($this->sIsoCodeCountry != 'FR' && !$bDomTom) {
            return $this->display(__FILE__, 'views/templates/hook/international/banner-home.tpl');
        } else {
            return $this->display(__FILE__, 'views/templates/hook/france/banner-home.tpl');
        }
    }

    public function hookDisplayLeftColumn()
    {
        return $this->hookDisplayRightColumn();
    }

    public function hookDisplayRightColumn()
    {
        if (Configuration::get('PS_CATALOG_MODE')) {
            return;
        }

        $bDomTom = Oney::getConfigByCountry('dom_tom', $this->sIsoCodeCountry);

        if ($bDomTom) {
            if (empty($this->opc->getListOpc(Oney::getConfigByCountry('country_reference', $this->sIsoCodeCountry), 1))) {
                return '';
            }
        } else {
            if (empty($this->opc->getListOpc($this->sIsoCodeCountry, 1))) {
                return '';
            }
        }

        $getInfoListOpc = $this->getInfoListOpc();

        $this->context->smarty->assign(
            array(
                'minimum_selling_price' => $getInfoListOpc['minimum'],
                'maximum_selling_price' => $getInfoListOpc['maximum'],
                'free_transaction'      => $getInfoListOpc['free_transaction'],
                'base_uri'              => __PS_BASE_URI__
            )
        );

        if ($this->sIsoCodeCountry != 'FR' && !$bDomTom) {
            return $this->display(__FILE__, 'views/templates/hook/international/banner-col.tpl');
        } else {
            return $this->display(__FILE__, 'views/templates/hook/france/banner-col.tpl');
        }
    }

    public function hookDisplayCartPaiement($params)
    {
        /* Ne pas afficher en cas de boutique en mode catalogue */
        if (Configuration::get('PS_CATALOG_MODE')) {
            return;
        }

        if (version_compare(_PS_VERSION_, '1.7', '<')) {
            return;
        }
        
        $bDomTom = Oney::getConfigByCountry('dom_tom', $this->sIsoCodeCountry);

        if ($bDomTom) {
            if (empty($this->opc->getListOpc(Oney::getConfigByCountry('country_reference', $this->sIsoCodeCountry), 1))) {
                return '';
            }
        } else {
            if (empty($this->opc->getListOpc($this->sIsoCodeCountry, 1))) {
                return '';
            }
        }

        $bHasFreeOpc = false;

        // Gestion des DOM
        if ($bDomTom) {
            /* Affichage des OPC choisi par le client */
            $getListOpc = $this->opc->getListOpc(Oney::getConfigByCountry('country_reference', $this->sIsoCodeCountry), 1, $params['cart']->getTotalCart(Context::getContext()->cart->id));
        } else {
            /* Affichage des OPC choisi par le client */
            $getListOpc = $this->opc->getListOpc($this->sIsoCodeCountry, 1, $params['cart']->getTotalCart(Context::getContext()->cart->id));
        }

        foreach ($getListOpc as $opc) {
            if ($opc['free_business_transaction'] == 1) {
                $bHasFreeOpc = true;
            }
        }
        $getInfoListOpc = array();
        $businnessCode = [];
        foreach ($getListOpc as $opc) {
            if ($bHasFreeOpc && $opc['free_business_transaction'] == 1) {
                $businnessCode[$opc['minimum_number_of_instalments']] = $opc['business_transaction_code'];
            } elseif (!$bHasFreeOpc && $opc['free_business_transaction'] == 0) {
                $businnessCode[$opc['minimum_number_of_instalments']] = $opc['business_transaction_code'];
            }
        }
        
        $getInfoListOpc = $this->getInfoListOpc($params);

        foreach ($getInfoListOpc['listOpc'] as $key => $opc) {
            if (!in_array($opc['business_transaction_code'], $businnessCode)) {
                unset($getInfoListOpc['listOpc'][$key]);
            }
        }

        /** @var Cart $cart */
        $cart = $params['cart'];
        $pricesimulation = $cart->getOrderTotal();

        /* Assignation de la simulation / liste / mentions légales / price / urlSimulation */
        $this->context->smarty->assign(
            array(
                'module_dir'               => $this->_path,
                'simulation'            => $getInfoListOpc['simulation'],
                'listOPC'               => $getInfoListOpc['listOpc'],
                'legalNotice'           => $this->getMentionLegale('P', $this->sIsoCodeCountry),
                'priceProduct'          => $pricesimulation,
                'minimum_selling_price' => $getInfoListOpc['minimum'],
                'maximum_selling_price' => $getInfoListOpc['maximum'],
                'base_uri'              => __PS_BASE_URI__
            )
        );

        if ($this->sIsoCodeCountry != 'FR' && !$bDomTom) {
            return $this->display(__FILE__, 'views/templates/hook/international/simulation-cart.tpl');
        } else {
            return $this->display(__FILE__, 'views/templates/hook/france/simulation-cart.tpl');
        }
    }

    public function hookDisplayShoppingCartFooter($params)
    {
        /* Ne pas afficher en cas de boutique en mode catalogue */
        if (Configuration::get('PS_CATALOG_MODE')) {
            return;
        }

        if (version_compare(_PS_VERSION_, '1.7', '>=')) {
            return;
        }

        $bDomTom = Oney::getConfigByCountry('dom_tom', $this->sIsoCodeCountry);

        if ($bDomTom) {
            if (empty($this->opc->getListOpc(Oney::getConfigByCountry('country_reference', $this->sIsoCodeCountry), 1))) {
                return '';
            }
        } else {
            if (empty($this->opc->getListOpc($this->sIsoCodeCountry, 1))) {
                return '';
            }
        }

        if (Oney::getConfigByCountry('dom_tom', $this->sIsoCodeCountry)) {
            /* Affichage des OPC choisi par le client */
            $getInfoListOpc = $this->opc->getListOpc(Oney::getConfigByCountry('country_reference', $this->sIsoCodeCountry), 1);
        } else {
            /* Affichage des OPC choisi par le client */
            $getInfoListOpc = $this->opc->getListOpc($this->sIsoCodeCountry, 1);
        }
        //$getInfoListOpc = $this->getInfoListOpc($params);

        /** @var Cart $cart */
        $cart = $params['cart'];
        $pricesimulation = $cart->getOrderTotal();

        /* Assignation de la simulation / liste / mentions légales / price / urlSimulation */
        $this->context->smarty->assign(
            array(
                'simulation'            => (isset($getInfoListOpc['simulation']))?$getInfoListOpc['simulation']:null,
                'listOPC'               => (isset($getInfoListOpc['listOpc']))?$getInfoListOpc['listOpc']:null,
                'legalNotice'           => $this->getMentionLegale('P', $this->sIsoCodeCountry),
                'priceProduct'          => $pricesimulation,
                'minimum_selling_price' => (isset($getInfoListOpc['minimum']))?$getInfoListOpc['minimum']:null,
                'maximum_selling_price' => (isset($getInfoListOpc['maximum']))?$getInfoListOpc['maximum']:null,
                'base_uri'              => __PS_BASE_URI__
            )
        );

        if ($this->sIsoCodeCountry != 'FR' && !$bDomTom) {
            return $this->display(__FILE__, 'views/templates/hook/international/simulation-cart.tpl');
        } else {
            return $this->display(__FILE__, 'views/templates/hook/france/simulation-cart.tpl');
        }
    }

    public function hookActionOrderHistoryAddAfter($params)
    {
        $oOrder = new Order($params['order_history']->id_order);
        if ($this->name == $oOrder->module) {
            if ($params['order_history']->id_order_state == Configuration::get('FACILYPAY_OS_CAPTURE')) {
                $aAddressInvoice = Address::getCountryAndState($oOrder->id_address_invoice);
                $sIsoCodeCountryByAddress = Country::getIsoById($aAddressInvoice['id_country']);

                $sIsoCodeCountry = (Oney::getConfigByCountry('dom_tom', $sIsoCodeCountryByAddress)) ?
                    Oney::getConfigByCountry('country_reference', $sIsoCodeCountryByAddress) : $sIsoCodeCountryByAddress;

                $paramsActionConfirm = array(
                    'language_code'       => $sIsoCodeCountry,
                    'merchant_request_id' => date('YmdHis') . '-' . (int)$oOrder->id_cart,
                    'payment'             => array(
                        'payment_amount' => number_format((float)$oOrder->getOrdersTotalPaid(), 2, '.', '')
                    )
                );

                $result = $this->pepsWebService->actionConfirm(
                    $oOrder->reference,
                    $paramsActionConfirm,
                    $sIsoCodeCountry
                );

                $bLog = true;
                if ($bLog) {
                    $sLogPath = _PS_MODULE_DIR_ . 'oney/logs/';
                    $fileName = date('Y-m-d') . '_callback_test'._PS_VERSION_.'.txt';
                    file_put_contents($sLogPath . $fileName, date('d-m-Y h:i:s') . "\n", FILE_APPEND);
                    file_put_contents(
                        $sLogPath . $fileName,
                        print_r(array('oney_result' => $result), true) . "\n",
                        FILE_APPEND
                    );
                }

                if ($result->purchase->status_code == 'FUNDED') {
                    return $oOrder->setCurrentState((int)Configuration::get('PS_OS_PAYMENT'));
                }
            }

            if ($oOrder->current_state == Configuration::get('PS_OS_OUTOFSTOCK') ||
                $oOrder->current_state == Configuration::get('PS_OS_OUTOFSTOCK_PAID') ||
                $oOrder->current_state == Configuration::get('PS_OS_OUTOFSTOCK_UNPAID')) {
                $sSQL = 'SELECT id_order_state FROM ' . _DB_PREFIX_ . 'order_history ' .
                    'WHERE id_order = ' . (int)$oOrder->id . ' ' .
                    'AND id_order_state IN ('.
                    'SELECT id_order_state FROM ' . _DB_PREFIX_ . '_order_state ' .
                    'WHERE module_name = "' . pSQL($this->name) . '") ' .
                    'ORDER BY id_order_history DESC';

                $iOrderStateId = Db::getInstance()->getValue($sSQL);
                if ($iOrderStateId) {
                    return $oOrder->setCurrentState((int)$iOrderStateId);
                }
            }

            if ($oOrder->current_state == Configuration::get('PS_OS_CANCELED')) {
                /** @var AddressCore $oAddress */
                $oAddress = new Address($oOrder->id_address_invoice);

                if (Tools::strtoupper(CountryCore::getIsoById($oAddress->id_country)) === 'FR') {
                    return;
                }

                if ($oOrder->module == $this->name) {
                    $aAddressInvoice = Address::getCountryAndState($oOrder->id_address_invoice);
                    $aParameters = array(
                        'language_code' => Tools::strtoupper(Country::getIsoById($aAddressInvoice['id_country'])),
                        'merchant_request_id' => date('YmdHis') . '-' . $oOrder->id,
                        'purchase' => array(
                            'cancellation_reason_code' => 0,
                            'cancellation_amount' => number_format((float)$oOrder->getOrdersTotalPaid(), 2, '.', '')
                        )
                    );
                    $aResponse = $this->pepsWebService->cancelPayment(
                        $oOrder->reference,
                        $aParameters,
                        Tools::strtoupper(Country::getIsoById($aAddressInvoice['id_country']))
                    );
                    if (!isset($aResponse->purchase) || !$aResponse->purchase->status_code == 'CANCELLED') {
                        $this->context->controller->errors[] = $aResponse;
                    }
                }
            }
        }
    }

    /**
     * Affichage d'information sous le bouton d'ajout au panier
     * @param type $params
     * @return template
     */
    public function hookDisplayProductButtons($params)
    {
        /* Ne pas afficher en cas de boutique en mode catalogue */
        if (Configuration::get('PS_CATALOG_MODE')) {
            return;
        }

        $bDomTom = Oney::getConfigByCountry('dom_tom', $this->sIsoCodeCountry);

        if ($bDomTom) {
            if (empty($this->opc->getListOpc(Oney::getConfigByCountry('country_reference', $this->sIsoCodeCountry), 1))) {
                return '';
            }
        } else {
            if (empty($this->opc->getListOpc($this->sIsoCodeCountry, 1))) {
                return '';
            }
        }


        /** @var Product $oProduct */
        $oProduct = $params['product'];

        if (is_object($params['product'])) {
            if (isset($params['product']->product) || ($params['product'] instanceof  Product)) {
                $pricesimulation = (float)Tools::convertPriceFull(
                    $oProduct->getPrice(true),
                    null,
                    Context::getContext()->currency
                );
            } else {
                $pricesimulation = (float)Tools::convertPriceFull(
                    Product::getPriceStatic($params['product']->getId()),
                    null,
                    Context::getContext()->currency
                );
            }
        } else {
            $pricesimulation = Product::getPriceStatic(
                $params['product']['id'],
                true,
                $params['product']['id_product_attribute']
            );
        }

        if ($this->sIsoCodeCountry != 'FR' && !$bDomTom) {
            $templateFile = 'views/templates/hook/international/simulation-product.tpl';
        } else {
            $templateFile = 'views/templates/hook/france/simulation-product.tpl';
        }

        $getInfoListOpc = $this->getInfoListOpc($params, $pricesimulation);

        /* Assignation de la simulation / liste / mentions légales / price / urlSimulation */
        $this->context->smarty->assign(
            array(
                'simulation'            => $getInfoListOpc['simulation'],
                'listOPC'               => $getInfoListOpc['listOpc'],
                'legalNotice'           => $this->getMentionLegale('P', $this->sIsoCodeCountry),
                'priceProduct'          => $pricesimulation,
                'minimum_selling_price' => $getInfoListOpc['minimum'],
                'maximum_selling_price' => $getInfoListOpc['maximum'],
                'free_transaction'      => $getInfoListOpc['free_transaction'],
                'base_uri'              => __PS_BASE_URI__
            )
        );

        return $this->display(__FILE__, $templateFile);
    }

    public function getListOpc($pricesimulation)
    {
        try {
            /* Liste des OPCs disponibles */
            $bDomTom = Oney::getConfigByCountry('dom_tom', $this->sIsoCodeCountry);
            if ($bDomTom) {
                $opcSimulationWithFilterPrice = $this->opc->getOpcSimulationWithFilterPrice($pricesimulation, Oney::getConfigByCountry('country_reference', $this->sIsoCodeCountry));
            } else {
                $opcSimulationWithFilterPrice = $this->opc->getOpcSimulationWithFilterPrice($pricesimulation, $this->sIsoCodeCountry);
            }
            return $opcSimulationWithFilterPrice;
        } catch (Exception $e) {
            PrestaShopLogger::addLog('getListOpc : ' . $e->getMessage(), 4);
        }
    }

    public function getInfoListOpc($params = null, $pricesimulation = null)
    {
        if (isset($params)) {
            $cart = $params['cart'];
        } else {
            $cart = $this->context->cart;
        }

        if (!isset($pricesimulation)) {
            $pricesimulation = $cart->getOrderTotal();
        }

        $paramsSimulation = array(
            'pricesimulation' => $pricesimulation
        );

        $bDomTom = Oney::getConfigByCountry('dom_tom', $this->sIsoCodeCountry);
        if ($bDomTom) {
            $simulation = $this->getSimulationFinancement($paramsSimulation, Oney::getConfigByCountry('country_reference', $this->sIsoCodeCountry));
        } else {
            $simulation = $this->getSimulationFinancement($paramsSimulation, $this->sIsoCodeCountry);
        }

        //dump($bDomTom, $simulation);
        if (!$simulation) {
            // Gestion des DOM
            if ($bDomTom) {
                /* Affichage des OPC choisi par le client */
                $listOpc = $this->opc->getListOpc(Oney::getConfigByCountry('country_reference', $this->sIsoCodeCountry), 1);
            } else {
                /* Affichage des OPC choisi par le client */
                $listOpc = $this->opc->getListOpc($this->sIsoCodeCountry, 1);
            }
        } else {
            $listOpc = $this->getListOpc($pricesimulation);
        }

        $minimum = null;
        $maximum = 0;
        $free_transaction = 1;
        foreach ($listOpc as $opc) {
            if ($maximum < $opc['maximum_selling_price']) {
                $maximum = $opc['maximum_selling_price'];
            }
            if ($maximum != 0 && empty($minimum)) {
                $minimum = $maximum;
            }
            if ($minimum > $opc['minimum_selling_price']) {
                $minimum = $opc['minimum_selling_price'];
            }
            if ($free_transaction > $opc['free_business_transaction']) {
                $free_transaction = $opc['free_business_transaction'];
            }
        }
//dump($listOpc);
        return array(
            'listOpc'          => $listOpc,
            'minimum'          => $minimum,
            'maximum'          => $maximum,
            'simulation'       => $simulation,
            'free_transaction' => $free_transaction
        );
    }

    public function getMentionLegale($legal_notice_type, $sIsoCodeCountry)
    {
        if (!$identifiantFacilyPay = Configuration::get('FACILYPAY_MERCHANT_GUID_' . $sIsoCodeCountry)) {
            return;
        }

        try {
            /* Paramètre et Mention légale */
            $paramsLegalNotice = array(
                'merchant_guid'     => $identifiantFacilyPay,

                'legal_notice_type' => $legal_notice_type,
                'language_code'     => Tools::strtoupper($sIsoCodeCountry)
            );

            $sPSPGuid = Configuration::get('FACILYPAY_PSP_GUID_' . $sIsoCodeCountry);
            if (!empty($sPSPGuid) || $sPSPGuid != 0) {
                $paramsLegalNotice['psp_guid'] = $sPSPGuid;
            }

            return $this->pepsWebService->getMentionlegale($paramsLegalNotice, $sIsoCodeCountry);
        } catch (Exception $e) {
            PrestaShopLogger::addLog('getMentionLegale : ' . $e->getMessage(), 4);
        }

        return '';
    }

    public function getSimulationFinancement($data, $sIsoCodeCountry)
    {
        /* Stockage des informations pour l'affichage en haut et bas de page */
        try {
            $businessTransactionCode = $this->getTransactionCodeForPrice($data['pricesimulation']);
            $identifiantFacilyPay = Configuration::get('FACILYPAY_MERCHANT_GUID_' . $sIsoCodeCountry);
            if (!$businessTransactionCode || !$identifiantFacilyPay) {
                return;
            }

            /* Financement */
            $paramsSimulation = array(
                'merchant_guid' => $identifiantFacilyPay,
                'business_transaction_code' => isset($data['business_transaction_code']) ?
                    $data['business_transaction_code'] : $businessTransactionCode,
                'payment_amount' => $data['pricesimulation']
            );

            $sPSPGuid = Configuration::get('FACILYPAY_PSP_GUID_' . $sIsoCodeCountry);
            if (!empty($sPSPGuid) || $sPSPGuid != 0) {
                $paramsSimulation['psp_guid'] = $sPSPGuid;
            }

            $this->simulation = $this->pepsWebService->simulerFinancement($paramsSimulation, $sIsoCodeCountry);
        } catch (Exception $e) {
            PrestaShopLogger::addLog('getSimulationFinancement : ' . $e->getMessage(), 4);
        }

        return $this->simulation;
    }

    private function getTransactionCodeForPrice($price)
    {
        $bDomTom = Oney::getConfigByCountry('dom_tom', $this->sIsoCodeCountry);
        if ($bDomTom) {
            $transactionCodeForPrice = $this->opc->getSingleOpcSimulationWithFilterPrice($price, Oney::getConfigByCountry('country_reference', $this->sIsoCodeCountry));
        } else {
            $transactionCodeForPrice = $this->opc->getSingleOpcSimulationWithFilterPrice($price, $this->sIsoCodeCountry);
        }

        /* Récupération de la première OPC selectionnée */
        return $transactionCodeForPrice;
    }

    public function hookPaymentOptions($params)
    {
        if (!$this->active) {
            return;
        }

        /** @var Cart $cart */
        $cart = $params['cart'];

        /** @var AddressCore $addressInvoice */
        $addressInvoice = new Address($cart->id_address_invoice);

        if (!(OneyPhoneNumber::isValidPhoneNumber($addressInvoice->phone, $this->sIsoCodeCountry, 1) ||
            OneyPhoneNumber::isValidPhoneNumber($addressInvoice->phone_mobile, $this->sIsoCodeCountry, 2))
        ) {
            echo '<span class="alert alert-info oney-alert" style="display: block">' .
                $this->l('Si vous souhaitez payer par Oney, vous devez entrer au moins un numéro de téléphone') .
                '</span>';
            return false;
        }

        $bDomTom = Oney::getConfigByCountry('dom_tom', $this->sIsoCodeCountry);

        // Gestion des DOM
        if ($bDomTom) {
            /* Affichage des OPC choisi par le client */
            $opcSelected = $this->opc->getListOpc(Oney::getConfigByCountry('country_reference', $this->sIsoCodeCountry), 1);
        } else {
            /* Affichage des OPC choisi par le client */
            $opcSelected = $this->opc->getListOpc($this->sIsoCodeCountry, 1);
        }

        /* Test si les modes de paiements selectionné font partie des opc disponible par l'API */
        $newOption = array();
        $orderTotal = $cart->getOrderTotal();

        foreach ($opcSelected as $key => $opcap) {
            if ($opcap['minimum_selling_price'] > $orderTotal || $opcap['maximum_selling_price'] < $orderTotal) {
                unset($opcSelected[$key]);
                continue;
            }

            $params = array(
                'business_transaction_code' => $opcap['business_transaction_code'],
                'pricesimulation' => $orderTotal,
            );

            if ($bDomTom) {
                /* Affichage des OPC choisi par le client */
                $opcSelected[$key]['simulation'] = $this->getSimulationFinancement($params, Oney::getConfigByCountry('country_reference', $this->sIsoCodeCountry));
            } else {
                $opcSelected[$key]['simulation'] = $this->getSimulationFinancement($params, $this->sIsoCodeCountry);
            }


            $this->context->smarty->assign(
                array(
                    'module_dir'    => $this->_path,
                    'methodPayment' => $opcSelected,
                    'key'           => $key
                )
            );

            if ($this->sIsoCodeCountry != 'FR' && !$bDomTom) {
                $templateAdditionalInfomation = $this->fetch('module:' . $this->name .
                    '/views/templates/hook/international/17/payment_intro.tpl');
            } else {
                $templateAdditionalInfomation = $this->fetch('module:' .
                    $this->name . '/views/templates/hook/france/17/payment_intro.tpl');
            }

            $newOption[$key] = new PrestaShop\PrestaShop\Core\Payment\PaymentOption();
            $newOption[$key]->setModuleName($this->name)
                            ->setCallToActionText($opcap['customer_label'])
                            ->setAction(
                                $this->context->link->getModuleLink($this->name, 'paymenturl')
                            )
                            ->setInputs(array(
                                array(
                                    'type' => 'hidden',
                                    'name' => 'opc',
                                    'value' => $opcap['business_transaction_code']
                                ),
                                array(
                                    'type' => 'hidden',
                                    'name' => 'short_label',
                                    'value' => $opcap['business_transaction_code']
                                )
                            ))
                            ->setAdditionalInformation($templateAdditionalInfomation);
        }

        return $newOption;
    }

    /**
     * This method is used to render the payment button,
     * Take care if the button should be displayed or not.
     * @param $params
     * @return string
     * @throws PrestaShopDatabaseException
     * @throws PrestaShopException
     */
    public function hookDisplayPayment($params)
    {
        if (!$this->active) {
            return;
        }

        /** @var Cart $cart */
        $cart = $params['cart'];
        /** @var AddressCore $addressInvoice */
        $addressInvoice = new Address($cart->id_address_invoice);

        if (!(OneyPhoneNumber::isValidPhoneNumber($addressInvoice->phone, $this->sIsoCodeCountry, 1) ||
                OneyPhoneNumber::isValidPhoneNumber($addressInvoice->phone_mobile, $this->sIsoCodeCountry, 2))) {
            return false;
        }
        $bDomTom = Oney::getConfigByCountry('dom_tom', $this->sIsoCodeCountry);

        // Gestion des DOM
        if ($bDomTom) {
            /* Affichage des OPC choisi par le client */
            $opcSelected = $this->opc->getListOpc(Oney::getConfigByCountry('country_reference', $this->sIsoCodeCountry), 1);
        } else {
            /* Affichage des OPC choisi par le client */
            $opcSelected = $this->opc->getListOpc($this->sIsoCodeCountry, 1);
        }

        $orderTotal = $cart->getOrderTotal();

        foreach ($opcSelected as $key => $opcap) {
            if ($opcap['minimum_selling_price'] > $orderTotal || $opcap['maximum_selling_price'] < $orderTotal) {
                unset($opcSelected[$key]);
                continue;
            }

            $params = array(
                'business_transaction_code' => $opcap['business_transaction_code'],
                'pricesimulation' => $orderTotal,
            );

            if ($bDomTom) {
                /* Affichage des OPC choisi par le client */
                $opcSelected[$key]['simulation'] = $this->getSimulationFinancement($params, Oney::getConfigByCountry('country_reference', $this->sIsoCodeCountry));
            } else {
                $opcSelected[$key]['simulation'] = $this->getSimulationFinancement($params, $this->sIsoCodeCountry);
            }

            $opcSelected[$key]['url'] = $this->context->link->getModuleLink($this->name, 'paymenturl') .
                '?opc=' . $opcap['business_transaction_code'] .
                '&short_label=' . $opcap['business_transaction_code'];
            $this->context->smarty->assign(
                array(
                    'key' => $key
                )
            );
        }
        $this->context->smarty->assign(
            array(
                'methodPayment' => $opcSelected,
            )
        );

        if ($this->sIsoCodeCountry != 'FR' && !$bDomTom) {
            return $this->display(__FILE__, 'views/templates/hook/international/payment.tpl');
        } else {
            return $this->display(__FILE__, 'views/templates/hook/france/payment.tpl');
        }
    }

    public function hookPaymentReturn($params)
    {
        /* Place your code here. */
        if ($this->active == false) {
            return;
        }

        $bDomTom = Oney::getConfigByCountry('dom_tom', $this->sIsoCodeCountry);

        /** @var Order $order */
        $order = $params['order'];

        if ($order->getCurrentOrderState()->id != Configuration::get('PS_OS_ERROR')) {
            $this->context->smarty->assign('status', 'ok');
        }
        $oOrderState = new OrderState($order->current_state);

        $this->context->smarty->assign(
            array(
                'id_order'  => $order->id,
                'statut'    => $oOrderState->name[$this->context->language->id],
                'reference' => $order->reference,
                'params'    => $params,
                'total'     => Tools::displayPrice(
                    $order->total_paid,
                    Currency::getCurrency($order->id_currency),
                    false
                ),
                'shop_name' => array($this->context->shop->name),
            )
        );

        if ($this->sIsoCodeCountry != 'FR' && !$bDomTom) {
            return $this->display(__FILE__, 'views/templates/hook/international/17/confirmation.tpl');
        } else {
            return $this->display(__FILE__, 'views/templates/hook/france/17/confirmation.tpl');
        }
    }

    /**
     * Validate an order in database
     * Function called from a payment module
     *
     * @param int $id_cart
     * @param int $id_order_state
     * @param float $amount_paid Amount really paid by customer (in the default currency)
     * @param string $payment_method Payment method (eg. 'Credit card')
     * @param null $message Message to attach to order
     * @param array $extra_vars
     * @param null $currency_special
     * @param bool $dont_touch_amount
     * @param bool $secure_key
     * @param Shop $shop
     *
     * @return bool
     * @throws PrestaShopException
     */
    public function validateOrder(
        $id_cart,
        $id_order_state,
        $amount_paid,
        $payment_method = 'Unknown',
        $message = null,
        $extra_vars = array(),
        $currency_special = null,
        $dont_touch_amount = false,
        $secure_key = false,
        Shop $shop = null
    ) {
        if (version_compare(_PS_VERSION_, '1.7', '<')) {
            if (version_compare(_PS_VERSION_, '1.6.1', '<')) {
                $validateOrder = $this->validateOrder160(
                    $id_cart,
                    $id_order_state,
                    $amount_paid,
                    $payment_method,
                    $message,
                    $extra_vars,
                    $currency_special,
                    $dont_touch_amount,
                    $secure_key,
                    $shop
                );
            } else {
                $validateOrder = $this->validateOrder16(
                    $id_cart,
                    $id_order_state,
                    $amount_paid,
                    $payment_method,
                    $message,
                    $extra_vars,
                    $currency_special,
                    $dont_touch_amount,
                    $secure_key,
                    $shop
                );
            }
        } else {
            $validateOrder = $this->validateOrder17(
                $id_cart,
                $id_order_state,
                $amount_paid,
                $payment_method,
                $message,
                $extra_vars,
                $currency_special,
                $dont_touch_amount,
                $secure_key,
                $shop
            );
        }

        return $validateOrder;
    }

    public function validateOrder160(
        $id_cart,
        $id_order_state,
        $amount_paid,
        $payment_method = 'Unknown',
        $message = null,
        $extra_vars = array(),
        $currency_special = null,
        $dont_touch_amount = false,
        $secure_key = false,
        Shop $shop = null
    ) {
        if (self::DEBUG_MODE) {
            PrestaShopLogger::addLog(
                'PaymentModule::validateOrder - Function called',
                1,
                null,
                'Cart',
                (int)$id_cart,
                true
            );
        }

        $this->context->cart = new Cart($id_cart);
        $this->context->customer = new Customer($this->context->cart->id_customer);
        $this->context->language = new Language($this->context->cart->id_lang);
        $this->context->shop = ($shop ? $shop : new Shop($this->context->cart->id_shop));
        ShopUrl::resetMainDomainCache();

        $id_currency = $currency_special ? (int)$currency_special : (int)$this->context->cart->id_currency;
        $this->context->currency = new Currency($id_currency, null, $this->context->shop->id);
        if (Configuration::get('PS_TAX_ADDRESS_TYPE') == 'id_address_delivery') {
            $context_country = $this->context->country;
        }

        $order_status = new OrderState((int)$id_order_state, (int)$this->context->language->id);
        if (!Validate::isLoadedObject($order_status)) {
            PrestaShopLogger::addLog(
                'PaymentModule::validateOrder - Order Status cannot be loaded',
                3,
                null,
                'Cart',
                (int)$id_cart,
                true
            );
            throw new PrestaShopException('Can\'t load Order status');
        }

        if (!$this->active) {
            PrestaShopLogger::addLog(
                'PaymentModule::validateOrder - Module is not active',
                3,
                null,
                'Cart',
                (int)$id_cart,
                true
            );
            die(Tools::displayError());
        }

        // Does order already exists ?
        if (Validate::isLoadedObject($this->context->cart) && $this->context->cart->OrderExists() == false) {
            if ($secure_key !== false && $secure_key != $this->context->cart->secure_key) {
                PrestaShopLogger::addLog(
                    'PaymentModule::validateOrder - Secure key does not match',
                    3,
                    null,
                    'Cart',
                    (int)$id_cart,
                    true
                );
                die(Tools::displayError());
            }

            // For each package, generate an order
            $delivery_option_list = $this->context->cart->getDeliveryOptionList();
            $package_list = $this->context->cart->getPackageList();
            $cart_delivery_option = $this->context->cart->getDeliveryOption();

            // If some delivery options are not defined, or not valid, use the first valid option
            foreach ($delivery_option_list as $id_address => $package) {
                if (!isset($cart_delivery_option[$id_address]) ||
                    !array_key_exists($cart_delivery_option[$id_address], $package)) {
                    foreach ($package as $key => $val) {
                        $cart_delivery_option[$id_address] = $key;
                        break;
                    }
                }
            }

            $order_list = array();
            $order_detail_list = array();

            $this->currentOrderReference = $this->reference;

            $order_creation_failed = false;
            $cart_total_paid = (float)Tools::ps_round((float)$this->context->cart->getOrderTotal(true, Cart::BOTH), 2);

            foreach ($cart_delivery_option as $id_address => $key_carriers) {
                foreach ($delivery_option_list[$id_address][$key_carriers]['carrier_list'] as $id_carrier => $data) {
                    foreach ($data['package_list'] as $id_package) {
                        // Rewrite the id_warehouse
                        $package_list[$id_address][$id_package]['id_warehouse'] =
                            (int)$this->context->cart->getPackageIdWarehouse(
                                $package_list[$id_address][$id_package],
                                (int)$id_carrier
                            );
                        $package_list[$id_address][$id_package]['id_carrier'] = $id_carrier;
                    }
                }
            }
            // Make sure CarRule caches are empty
            CartRule::cleanCache();
            $cart_rules = $this->context->cart->getCartRules();
            foreach ($cart_rules as $cart_rule) {
                if (($rule = new CartRule((int)$cart_rule['obj']->id)) && Validate::isLoadedObject($rule)) {
                    if ($error = $rule->checkValidity($this->context, true, true)) {
                        $this->context->cart->removeCartRule((int)$rule->id);
                        if (isset($this->context->cookie) &&
                            isset($this->context->cookie->id_customer) &&
                            $this->context->cookie->id_customer && !empty($rule->code)) {
                            if (Configuration::get('PS_ORDER_PROCESS_TYPE') == 1) {
                                Tools::redirect(
                                    'index.php?controller=order-opc&submitAddDiscount=1&discount_name=' .
                                    urlencode($rule->code)
                                );
                            }
                            Tools::redirect('index.php?controller=order&submitAddDiscount=1&discount_name=' .
                                urlencode($rule->code));
                        } else {
                            $rule_name = isset($rule->name[(int)$this->context->cart->id_lang]) ?
                                $rule->name[(int)$this->context->cart->id_lang] : $rule->code;
                            $error = Tools::displayError(sprintf('CartRule ID %1s (%2s) used in this cart '.
                                'is not valid and has been withdrawn from cart', (int)$rule->id, $rule_name));
                            PrestaShopLogger::addLog($error, 3, '0000002', 'Cart', (int)$this->context->cart->id);
                        }
                    }
                }
            }

            foreach ($package_list as $id_address => $packageByAddress) {
                foreach ($packageByAddress as $id_package => $package) {
                    $order = new Order();
                    $order->product_list = $package['product_list'];
                    if (Configuration::get('PS_TAX_ADDRESS_TYPE') == 'id_address_delivery') {
                        $address = new Address($id_address);
                        $this->context->country = new Country($address->id_country, $this->context->cart->id_lang);
                        if (!$this->context->country->active) {
                            throw new PrestaShopException('The delivery address country is not active.');
                        }
                    }

                    $carrier = null;
                    if (!$this->context->cart->isVirtualCart() && isset($package['id_carrier'])) {
                        $carrier = new Carrier($package['id_carrier'], $this->context->cart->id_lang);
                        $order->id_carrier = (int)$carrier->id;
                        $id_carrier = (int)$carrier->id;
                    } else {
                        $order->id_carrier = 0;
                        $id_carrier = 0;
                    }

                    $order->id_customer = (int)$this->context->cart->id_customer;
                    $order->id_address_invoice = (int)$this->context->cart->id_address_invoice;
                    $order->id_address_delivery = (int)$id_address;
                    $order->id_currency = $this->context->currency->id;
                    $order->id_lang = (int)$this->context->cart->id_lang;
                    $order->id_cart = (int)$this->context->cart->id;
                    $order->reference = $this->reference;
                    $order->id_shop = (int)$this->context->shop->id;
                    $order->id_shop_group = (int)$this->context->shop->id_shop_group;
                    $order->secure_key = ($secure_key ?
                        pSQL($secure_key) :
                        pSQL($this->context->customer->secure_key));
                    $order->payment = $payment_method;
                    if (isset($this->name)) {
                        $order->module = $this->name;
                    }
                    $order->recyclable = $this->context->cart->recyclable;
                    $order->gift = (int)$this->context->cart->gift;
                    $order->gift_message = $this->context->cart->gift_message;
                    $order->mobile_theme = $this->context->cart->mobile_theme;
                    $order->conversion_rate = $this->context->currency->conversion_rate;
                    $amount_paid = !$dont_touch_amount ? Tools::ps_round((float)$amount_paid, 2) : $amount_paid;
                    $order->total_paid_real = 0;

                    $order->total_products = (float)$this->context->cart->getOrderTotal(
                        false,
                        Cart::ONLY_PRODUCTS,
                        $order->product_list,
                        $id_carrier
                    );
                    $order->total_products_wt = (float)$this->context->cart->getOrderTotal(
                        true,
                        Cart::ONLY_PRODUCTS,
                        $order->product_list,
                        $id_carrier
                    );

                    $order->total_discounts_tax_excl = (float)abs($this->context->cart->getOrderTotal(
                        false,
                        Cart::ONLY_DISCOUNTS,
                        $order->product_list,
                        $id_carrier
                    ));
                    $order->total_discounts_tax_incl = (float)abs($this->context->cart->getOrderTotal(
                        true,
                        Cart::ONLY_DISCOUNTS,
                        $order->product_list,
                        $id_carrier
                    ));
                    $order->total_discounts = $order->total_discounts_tax_incl;

                    $order->total_shipping_tax_excl = (float)$this->context->cart->getPackageShippingCost(
                        (int)$id_carrier,
                        false,
                        null,
                        $order->product_list
                    );
                    $order->total_shipping_tax_incl = (float)$this->context->cart->getPackageShippingCost(
                        (int)$id_carrier,
                        true,
                        null,
                        $order->product_list
                    );
                    $order->total_shipping = $order->total_shipping_tax_incl;

                    if (!is_null($carrier) && Validate::isLoadedObject($carrier)) {
                        $order->carrier_tax_rate = $carrier->getTaxesRate(
                            new Address($this->context->cart->{Configuration::get('PS_TAX_ADDRESS_TYPE')})
                        );
                    }
                    $order->total_wrapping_tax_excl = (float)abs($this->context->cart->getOrderTotal(
                        false,
                        Cart::ONLY_WRAPPING,
                        $order->product_list,
                        $id_carrier
                    ));
                    $order->total_wrapping_tax_incl = (float)abs($this->context->cart->getOrderTotal(
                        true,
                        Cart::ONLY_WRAPPING,
                        $order->product_list,
                        $id_carrier
                    ));
                    $order->total_wrapping = $order->total_wrapping_tax_incl;

                    $order->total_paid_tax_excl = (float)Tools::ps_round((float)$this->context->cart->getOrderTotal(
                        false,
                        Cart::BOTH,
                        $order->product_list,
                        $id_carrier
                    ), 2);
                    $order->total_paid_tax_incl = (float)Tools::ps_round((float)$this->context->cart->getOrderTotal(
                        true,
                        Cart::BOTH,
                        $order->product_list,
                        $id_carrier
                    ), 2);
                    $order->total_paid = $order->total_paid_tax_incl;

                    $order->invoice_date = '0000-00-00 00:00:00';
                    $order->delivery_date = '0000-00-00 00:00:00';

                    if (self::DEBUG_MODE) {
                        PrestaShopLogger::addLog(
                            'PaymentModule::validateOrder - Order is about to be added',
                            1,
                            null,
                            'Cart',
                            (int)$id_cart,
                            true
                        );
                    }
                    // Creating order
                    $result = $order->add();

                    if (!$result) {
                        PrestaShopLogger::addLog(
                            'PaymentModule::validateOrder - Order cannot be created',
                            3,
                            null,
                            'Cart',
                            (int)$id_cart,
                            true
                        );
                        throw new PrestaShopException('Can\'t save Order');
                    }

                    // Amount paid by customer is not the right one -> Status = payment error
                    // We don't use the following condition to avoid the fl
                    //oat precision issues : http://www.php.net/manual/en/language.types.float.php
                    // if ($order->total_paid != $order->total_paid_real)
                    // We use number_format in order to compare two string
                    if ($order_status->logable && number_format($cart_total_paid, 2) !=
                        number_format($amount_paid, 2)) {
                        $id_order_state = Configuration::get('PS_OS_ERROR');
                    }

                    $order_list[] = $order;

                    if (self::DEBUG_MODE) {
                        PrestaShopLogger::addLog(
                            'PaymentModule::validateOrder - OrderDetail is about to be added',
                            1,
                            null,
                            'Cart',
                            (int)$id_cart,
                            true
                        );
                    }
                    // Insert new Order detail list using cart for the current order
                    $order_detail = new OrderDetail(null, null, $this->context);
                    $order_detail->createList(
                        $order,
                        $this->context->cart,
                        $id_order_state,
                        $order->product_list,
                        0,
                        true,
                        $package_list[$id_address][$id_package]['id_warehouse']
                    );
                    $order_detail_list[] = $order_detail;

                    if (self::DEBUG_MODE) {
                        PrestaShopLogger::addLog(
                            'PaymentModule::validateOrder - OrderCarrier is about to be added',
                            1,
                            null,
                            'Cart',
                            (int)$id_cart,
                            true
                        );
                    }
                    // Adding an entry in order_carrier table
                    if (!is_null($carrier)) {
                        $order_carrier = new OrderCarrier();
                        $order_carrier->id_order = (int)$order->id;
                        $order_carrier->id_carrier = (int)$id_carrier;
                        $order_carrier->weight = (float)$order->getTotalWeight();
                        $order_carrier->shipping_cost_tax_excl = (float)$order->total_shipping_tax_excl;
                        $order_carrier->shipping_cost_tax_incl = (float)$order->total_shipping_tax_incl;
                        $order_carrier->add();
                    }
                }
            }
            // The country can only change if the address used for the calculation is
            // the delivery address, and if multi-shipping is activated
            if (Configuration::get('PS_TAX_ADDRESS_TYPE') == 'id_address_delivery') {
                $this->context->country = $context_country;
            }

            if (!$this->context->country->active) {
                PrestaShopLogger::addLog(
                    'PaymentModule::validateOrder - Country is not active',
                    3,
                    null,
                    'Cart',
                    (int)$id_cart,
                    true
                );
                throw new PrestaShopException('The order address country is not active.');
            }

            if (self::DEBUG_MODE) {
                PrestaShopLogger::addLog(
                    'PaymentModule::validateOrder - Payment is about to be added',
                    1,
                    null,
                    'Cart',
                    (int)$id_cart,
                    true
                );
            }
            // Register Payment only if the order status validate the order
            if ($order_status->logable) {
                // $order is the last order loop in the foreach
                // The method addOrderPayment of the class Order make a create a paymentOrder
                //     linked to the order reference and not to the order id
                if (isset($extra_vars['transaction_id'])) {
                    $transaction_id = $extra_vars['transaction_id'];
                } else {
                    $transaction_id = null;
                }
                if (!$order->addOrderPayment($amount_paid, null, $transaction_id)) {
                    PrestaShopLogger::addLog(
                        'PaymentModule::validateOrder - Cannot save Order Payment',
                        3,
                        null,
                        'Cart',
                        (int)$id_cart,
                        true
                    );
                    throw new PrestaShopException('Can\'t save Order Payment');
                }
            }

            // Next !
            $only_one_gift = false;
            $cart_rule_used = array();
            $products = $this->context->cart->getProducts();

            // Make sure CarRule caches are empty
            CartRule::cleanCache();
            foreach ($order_detail_list as $key => $order_detail) {
                $order = $order_list[$key];
                if (!$order_creation_failed && isset($order->id)) {
                    if (!$secure_key) {
                        $message .= '<br />' .
                            Tools::displayError(
                                'Warning: the secure key is empty, check your payment account before validation'
                            );
                    }
                    // Optional message to attach to this order
                    if (isset($message) & !empty($message)) {
                        $msg = new Message();
                        $message = strip_tags($message, '<br>');
                        if (Validate::isCleanHtml($message)) {
                            if (self::DEBUG_MODE) {
                                PrestaShopLogger::addLog(
                                    'PaymentModule::validateOrder - Message is about to be added',
                                    1,
                                    null,
                                    'Cart',
                                    (int)$id_cart,
                                    true
                                );
                            }
                            $msg->message = $message;
                            $msg->id_order = intval($order->id);
                            $msg->private = 1;
                            $msg->add();
                        }
                    }

                    // Insert new Order detail list using cart for the current order
                    //$orderDetail = new OrderDetail(null, null, $this->context);
                    //$orderDetail->createList($order, $this->context->cart, $id_order_state);

                    // Construct order detail table for the email
                    $products_list = '';
                    $virtual_product = true;

                    $product_var_tpl_list = array();
                    foreach ($order->product_list as $product) {
                        $price = Product::getPriceStatic(
                            (int)$product['id_product'],
                            false,
                            ($product['id_product_attribute'] ? (int)$product['id_product_attribute'] : null),
                            6,
                            null,
                            false,
                            true,
                            $product['cart_quantity'],
                            false,
                            (int)$order->id_customer,
                            (int)$order->id_cart,
                            (int)$order->{Configuration::get('PS_TAX_ADDRESS_TYPE')}
                        );
                        $price_wt = Product::getPriceStatic(
                            (int)$product['id_product'],
                            true,
                            ($product['id_product_attribute'] ? (int)$product['id_product_attribute'] : null),
                            2,
                            null,
                            false,
                            true,
                            $product['cart_quantity'],
                            false,
                            (int)$order->id_customer,
                            (int)$order->id_cart,
                            (int)$order->{Configuration::get('PS_TAX_ADDRESS_TYPE')}
                        );

                        $product_price = Product::getTaxCalculationMethod() == PS_TAX_EXC ?
                            Tools::ps_round($price, 2) :
                            $price_wt;

                        $product_var_tpl = array(
                            'reference' => $product['reference'],
                            'name' => $product['name'] . (isset($product['attributes']) ?
                                    ' - '.$product['attributes'] :
                                    ''),
                            'unit_price' => Tools::displayPrice($product_price, $this->context->currency, false),
                            'price' => Tools::displayPrice(
                                $product_price * $product['quantity'],
                                $this->context->currency,
                                false
                            ),
                            'quantity' => $product['quantity'],
                            'customization' => array()
                        );

                        $customized_datas = Product::getAllCustomizedDatas((int)$order->id_cart);
                        if (isset($customized_datas[$product['id_product']][$product['id_product_attribute']])) {
                            $product_var_tpl['customization'] = array();
                            foreach ($customized_datas[$product['id_product']]
                                     [$product['id_product_attribute']][$order->id_address_delivery] as $customization) {
                                $customization_text = '';
                                if (isset($customization['datas'][Product::CUSTOMIZE_TEXTFIELD])) {
                                    foreach ($customization['datas'][Product::CUSTOMIZE_TEXTFIELD] as $text) {
                                        $customization_text .= $text['name'].': '.$text['value'].'<br />';
                                    }
                                }
                                if (isset($customization['datas'][Product::CUSTOMIZE_FILE])) {
                                    $customization_text .= sprintf(
                                        Tools::displayError('%d image(s)'),
                                        count($customization['datas'][Product::CUSTOMIZE_FILE])
                                    ) . '<br />';
                                }
                                $customization_quantity = (int)$product['customization_quantity'];

                                $product_var_tpl['customization'][] = array(
                                    'customization_text' => $customization_text,
                                    'customization_quantity' => $customization_quantity,
                                    'quantity' => Tools::displayPrice(
                                        $customization_quantity * $product_price,
                                        $this->context->currency,
                                        false
                                    )
                                );
                            }
                        }

                        $product_var_tpl_list[] = $product_var_tpl;
                        // Check if is not a virutal product for the displaying of shipping
                        if (!$product['is_virtual']) {
                            $virtual_product &= false;
                        }
                    } // end foreach ($products)

                    $product_list_txt = '';
                    $product_list_html = '';
                    if (count($product_var_tpl_list) > 0) {
                        $product_list_txt = $this->getEmailTemplateContent(
                            'order_conf_product_list.txt',
                            Mail::TYPE_TEXT,
                            $product_var_tpl_list
                        );
                        $product_list_html = $this->getEmailTemplateContent(
                            'order_conf_product_list.tpl',
                            Mail::TYPE_HTML,
                            $product_var_tpl_list
                        );
                    }

                    $cart_rules_list = array();
                    $total_reduction_value_ti = 0;
                    $total_reduction_value_tex = 0;
                    foreach ($cart_rules as $cart_rule) {
                        $package = array(
                            'id_carrier' => $order->id_carrier,
                            'id_address' => $order->id_address_delivery,
                            'products' => $order->product_list
                        );
                        $values = array(
                            'tax_incl' => $cart_rule['obj']->getContextualValue(
                                true,
                                $this->context,
                                CartRule::FILTER_ACTION_ALL_NOCAP,
                                $package
                            ),
                            'tax_excl' => $cart_rule['obj']->getContextualValue(
                                false,
                                $this->context,
                                CartRule::FILTER_ACTION_ALL_NOCAP,
                                $package
                            )
                        );

                        // If the reduction is not applicable to this order, then continue with the next one
                        if (!$values['tax_excl']) {
                            continue;
                        }

                        /* IF
                        ** - This is not multi-shipping
                        ** - The value of the voucher is greater than the total of the order
                        ** - Partial use is allowed
                        ** - This is an "amount" reduction, not a reduction in % or a gift
                        ** THEN
                        ** The voucher is cloned with a new value corresponding to the remainder
                        */

                        if (count($order_list) == 1 &&
                            $values['tax_incl'] > ($order->total_products_wt - $total_reduction_value_ti) &&
                            $cart_rule['obj']->partial_use == 1 && $cart_rule['obj']->reduction_amount > 0
                        ) {
                            // Create a new voucher from the original
                            $voucher = new CartRule($cart_rule['obj']->id);
                            // We need to instantiate the CartRule without lang parameter to allow saving it
                            unset($voucher->id);

                            // Set a new voucher code
                            $voucher->code = empty($voucher->code) ?
                                Tools::substr(md5($order->id.'-'.$order->id_customer.'-'.$cart_rule['obj']->id), 0, 16) :
                                $voucher->code.'-2';
                            if (preg_match(
                                '/\-([0-9]{1,2})\-([0-9]{1,2})$/',
                                $voucher->code,
                                $matches
                            ) && $matches[1] == $matches[2]) {
                                $voucher->code = preg_replace(
                                    '/' . $matches[0] . '$/',
                                    '-' . (intval($matches[1]) + 1),
                                    $voucher->code
                                );
                            }

                            // Set the new voucher value
                            if ($voucher->reduction_tax) {
                                $voucher->reduction_amount = $values['tax_incl'] -
                                    ($order->total_products_wt - $total_reduction_value_ti);

                                // Add total shipping amout only if reduction amount > total shipping
                                if ($voucher->free_shipping == 1 &&
                                    $voucher->reduction_amount >= $order->total_shipping_tax_incl) {
                                    $voucher->reduction_amount -= $order->total_shipping_tax_incl;
                                }
                            } else {
                                $voucher->reduction_amount = $values['tax_excl'] -
                                    ($order->total_products - $total_reduction_value_tex);
                                // Add total shipping amout only if reduction amount > total shipping
                                if ($voucher->free_shipping == 1 &&
                                    $voucher->reduction_amount >= $order->total_shipping_tax_excl) {
                                    $voucher->reduction_amount -= $order->total_shipping_tax_excl;
                                }
                            }

                            $voucher->id_customer = $order->id_customer;
                            $voucher->quantity = 1;
                            $voucher->quantity_per_user = 1;
                            $voucher->free_shipping = 0;
                            if ($voucher->add()) {
                                // If the voucher has conditions, they are now copied to the new voucher
                                CartRule::copyConditions($cart_rule['obj']->id, $voucher->id);

                                $params = array(
                                    '{voucher_amount}' => Tools::displayPrice(
                                        $voucher->reduction_amount,
                                        $this->context->currency,
                                        false
                                    ),
                                    '{voucher_num}' => $voucher->code,
                                    '{firstname}' => $this->context->customer->firstname,
                                    '{lastname}' => $this->context->customer->lastname,
                                    '{id_order}' => $order->reference,
                                    '{order_name}' => $order->getUniqReference()
                                );
                                Mail::Send(
                                    (int)$order->id_lang,
                                    'voucher',
                                    sprintf(
                                        Mail::l(
                                            'New voucher for your order %s',
                                            (int)$order->id_lang
                                        ),
                                        $order->reference
                                    ),
                                    $params,
                                    $this->context->customer->email,
                                    $this->context->customer->firstname.' '.$this->context->customer->lastname,
                                    null,
                                    null,
                                    null,
                                    null,
                                    _PS_MAIL_DIR_,
                                    false,
                                    (int)$order->id_shop
                                );
                            }

                            $values['tax_incl'] -= $values['tax_incl'] - $order->total_products_wt;
                            $values['tax_excl'] -= $values['tax_excl'] - $order->total_products;
                        }
                        $total_reduction_value_ti += $values['tax_incl'];
                        $total_reduction_value_tex += $values['tax_excl'];

                        $order->addCartRule(
                            $cart_rule['obj']->id,
                            $cart_rule['obj']->name,
                            $values,
                            0,
                            $cart_rule['obj']->free_shipping
                        );

                        if ($id_order_state != Configuration::get('PS_OS_ERROR') &&
                            $id_order_state != Configuration::get('PS_OS_CANCELED') &&
                            !in_array($cart_rule['obj']->id, $cart_rule_used)) {
                            $cart_rule_used[] = $cart_rule['obj']->id;

                            // Create a new instance of Cart Rule without id_lang, in order to update its quantity
                            $cart_rule_to_update = new CartRule($cart_rule['obj']->id);
                            $cart_rule_to_update->quantity = max(0, $cart_rule_to_update->quantity - 1);
                            $cart_rule_to_update->update();
                        }

                        $cart_rules_list[] = array(
                            'voucher_name' => $cart_rule['obj']->name,
                            'voucher_reduction' => ($values['tax_incl'] != 0.00 ? '-' : '') .
                                Tools::displayPrice($values['tax_incl'], $this->context->currency, false)
                        );
                    }

                    $cart_rules_list_txt = '';
                    $cart_rules_list_html = '';
                    if (count($cart_rules_list) > 0) {
                        $cart_rules_list_txt = $this->getEmailTemplateContent(
                            'order_conf_cart_rules.txt',
                            Mail::TYPE_TEXT,
                            $cart_rules_list
                        );
                        $cart_rules_list_html = $this->getEmailTemplateContent(
                            'order_conf_cart_rules.tpl',
                            Mail::TYPE_HTML,
                            $cart_rules_list
                        );
                    }

                    // Specify order id for message
                    $old_message = Message::getMessageByCartId((int)$this->context->cart->id);
                    if ($old_message) {
                        $update_message = new Message((int)$old_message['id_message']);
                        $update_message->id_order = (int)$order->id;
                        $update_message->update();

                        // Add this message in the customer thread
                        $customer_thread = new CustomerThread();
                        $customer_thread->id_contact = 0;
                        $customer_thread->id_customer = (int)$order->id_customer;
                        $customer_thread->id_shop = (int)$this->context->shop->id;
                        $customer_thread->id_order = (int)$order->id;
                        $customer_thread->id_lang = (int)$this->context->language->id;
                        $customer_thread->email = $this->context->customer->email;
                        $customer_thread->status = 'open';
                        $customer_thread->token = Tools::passwdGen(12);
                        $customer_thread->add();

                        $customer_message = new CustomerMessage();
                        $customer_message->id_customer_thread = $customer_thread->id;
                        $customer_message->id_employee = 0;
                        $customer_message->message = $update_message->message;
                        $customer_message->private = 0;

                        if (!$customer_message->add()) {
                            $this->errors[] = Tools::displayError('An error occurred while saving message');
                        }
                    }

                    if (self::DEBUG_MODE) {
                        PrestaShopLogger::addLog(
                            'PaymentModule::validateOrder - Hook validateOrder is about to be called',
                            1,
                            null,
                            'Cart',
                            (int)$id_cart,
                            true
                        );
                    }
                    // Hook validate order
                    Hook::exec('actionValidateOrder', array(
                        'cart' => $this->context->cart,
                        'order' => $order,
                        'customer' => $this->context->customer,
                        'currency' => $this->context->currency,
                        'orderStatus' => $order_status
                    ));

                    foreach ($this->context->cart->getProducts() as $product) {
                        if ($order_status->logable) {
                            ProductSale::addProductSale((int)$product['id_product'], (int)$product['cart_quantity']);
                        }
                    }
                    if (self::DEBUG_MODE) {
                        PrestaShopLogger::addLog(
                            'PaymentModule::validateOrder - Order Status is about to be added',
                            1,
                            null,
                            'Cart',
                            (int)$id_cart,
                            true
                        );
                    }
                    // Set the order status
                    $new_history = new OrderHistory();
                    $new_history->id_order = (int)$order->id;
                    $new_history->changeIdOrderState((int)$id_order_state, $order, true);
                    $new_history->addWithemail(true, $extra_vars);

                    // Switch to back order if needed
                    if (Configuration::get('PS_STOCK_MANAGEMENT') && $order_detail->getStockState()) {
                        $history = new OrderHistory();
                        $history->id_order = (int)$order->id;
                        $history->changeIdOrderState(Configuration::get('PS_OS_OUTOFSTOCK'), $order, true);
                        $history->addWithemail();
                    }

                    unset($order_detail);

                    // Order is reloaded because the status just changed
                    $order = new Order($order->id);

                    // Send an e-mail to customer (one order = one email)
                    if ($id_order_state != Configuration::get('PS_OS_ERROR') &&
                        $id_order_state != Configuration::get('PS_OS_CANCELED') &&
                        $this->context->customer->id) {
                        $invoice = new Address($order->id_address_invoice);
                        $delivery = new Address($order->id_address_delivery);
                        $delivery_state = $delivery->id_state ? new State($delivery->id_state) : false;
                        $invoice_state = $invoice->id_state ? new State($invoice->id_state) : false;

                        $data = array(
                            '{firstname}' => $this->context->customer->firstname,
                            '{lastname}' => $this->context->customer->lastname,
                            '{email}' => $this->context->customer->email,
                            '{delivery_block_txt}' => $this->_getFormatedAddress($delivery, "\n"),
                            '{invoice_block_txt}' => $this->_getFormatedAddress($invoice, "\n"),
                            '{delivery_block_html}' => $this->_getFormatedAddress($delivery, '<br />', array(
                                'firstname' => '<span style="font-weight:bold;">%s</span>',
                                'lastname' => '<span style="font-weight:bold;">%s</span>'
                            )),
                            '{invoice_block_html}' => $this->_getFormatedAddress($invoice, '<br />', array(
                                'firstname' => '<span style="font-weight:bold;">%s</span>',
                                'lastname' => '<span style="font-weight:bold;">%s</span>'
                            )),
                            '{delivery_company}' => $delivery->company,
                            '{delivery_firstname}' => $delivery->firstname,
                            '{delivery_lastname}' => $delivery->lastname,
                            '{delivery_address1}' => $delivery->address1,
                            '{delivery_address2}' => $delivery->address2,
                            '{delivery_city}' => $delivery->city,
                            '{delivery_postal_code}' => $delivery->postcode,
                            '{delivery_country}' => $delivery->country,
                            '{delivery_state}' => $delivery->id_state ? $delivery_state->name : '',
                            '{delivery_phone}' => ($delivery->phone) ? $delivery->phone : $delivery->phone_mobile,
                            '{delivery_other}' => $delivery->other,
                            '{invoice_company}' => $invoice->company,
                            '{invoice_vat_number}' => $invoice->vat_number,
                            '{invoice_firstname}' => $invoice->firstname,
                            '{invoice_lastname}' => $invoice->lastname,
                            '{invoice_address2}' => $invoice->address2,
                            '{invoice_address1}' => $invoice->address1,
                            '{invoice_city}' => $invoice->city,
                            '{invoice_postal_code}' => $invoice->postcode,
                            '{invoice_country}' => $invoice->country,
                            '{invoice_state}' => $invoice->id_state ? $invoice_state->name : '',
                            '{invoice_phone}' => ($invoice->phone) ? $invoice->phone : $invoice->phone_mobile,
                            '{invoice_other}' => $invoice->other,
                            '{order_name}' => $order->getUniqReference(),
                            '{date}' => Tools::displayDate(date('Y-m-d H:i:s'), null, 1),
                            '{carrier}' => ($virtual_product || !isset($carrier->name)) ?
                                Tools::displayError('No carrier') :
                                $carrier->name,
                            '{payment}' => Tools::substr($order->payment, 0, 32),
                            '{products}' => $product_list_html,
                            '{products_txt}' => $product_list_txt,
                            '{discounts}' => $cart_rules_list_html,
                            '{discounts_txt}' => $cart_rules_list_txt,
                            '{total_paid}' => Tools::displayPrice($order->total_paid, $this->context->currency, false),
                            '{total_products}' => Tools::displayPrice(
                                $order->total_paid - $order->total_shipping - $order->total_wrapping +
                                $order->total_discounts,
                                $this->context->currency,
                                false
                            ),
                            '{total_discounts}' => Tools::displayPrice(
                                $order->total_discounts,
                                $this->context->currency,
                                false
                            ),
                            '{total_shipping}' => Tools::displayPrice(
                                $order->total_shipping,
                                $this->context->currency,
                                false
                            ),
                            '{total_wrapping}' => Tools::displayPrice(
                                $order->total_wrapping,
                                $this->context->currency,
                                false
                            ),
                            '{total_tax_paid}' => Tools::displayPrice(
                                ($order->total_products_wt - $order->total_products) +
                                ($order->total_shipping_tax_incl - $order->total_shipping_tax_excl),
                                $this->context->currency,
                                false
                            ));

                        if (is_array($extra_vars)) {
                            $data = array_merge($data, $extra_vars);
                        }
                        // Join PDF invoice
                        if ((int)Configuration::get('PS_INVOICE') &&
                            $order_status->invoice && $order->invoice_number) {
                            $pdf = new PDF(
                                $order->getInvoicesCollection(),
                                PDF::TEMPLATE_INVOICE,
                                $this->context->smarty
                            );
                            $file_attachement=[];
                            $file_attachement['content'] = $pdf->render(false);
                            $file_attachement['name'] =
                                Configuration::get('PS_INVOICE_PREFIX', (int)$order->id_lang, null, $order->id_shop) .
                                sprintf('%06d', $order->invoice_number) . '.pdf';
                            $file_attachement['mime'] = 'application/pdf';
                        } else {
                            $file_attachement = null;
                        }

                        if (self::DEBUG_MODE) {
                            PrestaShopLogger::addLog(
                                'PaymentModule::validateOrder - Mail is about to be sent',
                                1,
                                null,
                                'Cart',
                                (int)$id_cart,
                                true
                            );
                        }

                        if (Validate::isEmail($this->context->customer->email)) {
                            Mail::Send(
                                (int)$order->id_lang,
                                'order_conf',
                                Mail::l('Order confirmation', (int)$order->id_lang),
                                $data,
                                $this->context->customer->email,
                                $this->context->customer->firstname . ' ' . $this->context->customer->lastname,
                                null,
                                null,
                                $file_attachement,
                                null,
                                _PS_MAIL_DIR_,
                                false,
                                (int)$order->id_shop
                            );
                        }
                    }

                    // updates stock in shops
                    if (Configuration::get('PS_ADVANCED_STOCK_MANAGEMENT')) {
                        $product_list = $order->getProducts();
                        foreach ($product_list as $product) {
                            // if the available quantities depends on the physical stock
                            if (StockAvailable::dependsOnStock($product['product_id'])) {
                                // synchronizes
                                StockAvailable::synchronize($product['product_id'], $order->id_shop);
                            }
                        }
                    }
                } else {
                    $error = Tools::displayError('Order creation failed');
                    PrestaShopLogger::addLog($error, 4, '0000002', 'Cart', intval($order->id_cart));
                    die($error);
                }
            } // End foreach $order_detail_list
            // Use the last order as currentOrder
            $this->currentOrder = (int)$order->id;

            if (self::DEBUG_MODE) {
                PrestaShopLogger::addLog(
                    'PaymentModule::validateOrder - End of validateOrder',
                    1,
                    null,
                    'Cart',
                    (int)$id_cart,
                    true
                );
            }

            return true;
        } else {
            $error = Tools::displayError('Cart cannot be loaded or an order has already been placed using this cart');
            PrestaShopLogger::addLog($error, 4, '0000001', 'Cart', intval($this->context->cart->id));
            die($error);
        }
    }

    public function validateOrder16(
        $id_cart,
        $id_order_state,
        $amount_paid,
        $payment_method = 'Unknown',
        $message = null,
        $extra_vars = array(),
        $currency_special = null,
        $dont_touch_amount = false,
        $secure_key = false,
        Shop $shop = null
    ) {
        if (self::DEBUG_MODE) {
            PrestaShopLogger::addLog(
                'PaymentModule::validateOrder - Function called',
                1,
                null,
                'Cart',
                (int)$id_cart,
                true
            );
        }

        if (!isset($this->context)) {
            $this->context = Context::getContext();
        }
        $this->context->cart = new Cart((int)$id_cart);
        $this->context->customer = new Customer((int)$this->context->cart->id_customer);
        // The tax cart is loaded before the customer so re-cache the tax calculation method
        $this->context->cart->setTaxCalculationMethod();

        $this->context->language = new Language((int)$this->context->cart->id_lang);
        $this->context->shop = ($shop ? $shop : new Shop((int)$this->context->cart->id_shop));
        ShopUrl::resetMainDomainCache();
        $id_currency = $currency_special ? (int)$currency_special : (int)$this->context->cart->id_currency;
        $this->context->currency = new Currency((int)$id_currency, null, (int)$this->context->shop->id);
        if (Configuration::get('PS_TAX_ADDRESS_TYPE') == 'id_address_delivery') {
            $context_country = $this->context->country;
        }
        $order_status = new OrderState((int)$id_order_state, (int)$this->context->language->id);
        if (!Validate::isLoadedObject($order_status)) {
            PrestaShopLogger::addLog(
                'PaymentModule::validateOrder - Order Status cannot be loaded',
                $id_order_state,
                null,
                'Cart',
                (int)$id_cart,
                true
            );
            throw new PrestaShopException('Can\'t load Order status');
        }

        if (!$this->active) {
            PrestaShopLogger::addLog(
                'PaymentModule::validateOrder - Module is not active',
                3,
                null,
                'Cart',
                (int)$id_cart,
                true
            );
            die(Tools::displayError());
        }
        // Does order already exists ?
        if (Validate::isLoadedObject($this->context->cart) && $this->context->cart->OrderExists() == false) {
            if ($secure_key !== false && $secure_key != $this->context->cart->secure_key) {
                PrestaShopLogger::addLog(
                    'PaymentModule::validateOrder - Secure key does not match',
                    3,
                    null,
                    'Cart',
                    (int)$id_cart,
                    true
                );
                die(Tools::displayError());
            }

            // For each package, generate an order
            $delivery_option_list = $this->context->cart->getDeliveryOptionList();
            $package_list = $this->context->cart->getPackageList();
            $cart_delivery_option = $this->context->cart->getDeliveryOption();

            // If some delivery options are not defined, or not valid, use the first valid option
            foreach ($delivery_option_list as $id_address => $package) {
                if (!isset($cart_delivery_option[$id_address]) || !array_key_exists(
                    $cart_delivery_option[$id_address],
                    $package
                )) {
                    $keys = array_keys($package);
                    foreach ($keys as $key) {
                        $cart_delivery_option[$id_address] = $key;
                        break;
                    }
                }
            }

            $order_list = array();
            $order_detail_list = array();

            $this->currentOrderReference = $this->reference;

            $order_creation_failed = false;
            $cart_total_paid = (float)Tools::ps_round((float)$this->context->cart->getOrderTotal(true, Cart::BOTH), 2);

            foreach ($cart_delivery_option as $id_address => $key_carriers) {
                foreach ($delivery_option_list[$id_address][$key_carriers]['carrier_list'] as $id_carrier => $data) {
                    foreach ($data['package_list'] as $id_package) {
                        // Rewrite the id_warehouse
                        $package_list[$id_address][$id_package]['id_warehouse'] =
                            (int)$this->context->cart->getPackageIdWarehouse(
                                $package_list[$id_address][$id_package],
                                (int)$id_carrier
                            );
                        $package_list[$id_address][$id_package]['id_carrier'] = $id_carrier;
                    }
                }
            }
            // Make sure CartRule caches are empty
            CartRule::cleanCache();
            $cart_rules = $this->context->cart->getCartRules();
            foreach ($cart_rules as $cart_rule) {
                if (($rule = new CartRule((int)$cart_rule['obj']->id)) && Validate::isLoadedObject($rule)) {
                    if ($error = $rule->checkValidity($this->context, true, true)) {
                        $this->context->cart->removeCartRule((int)$rule->id);
                        if (isset($this->context->cookie) && isset($this->context->cookie->id_customer) &&
                            $this->context->cookie->id_customer && !empty($rule->code)) {
                            if (Configuration::get('PS_ORDER_PROCESS_TYPE') == 1) {
                                Tools::redirect(
                                    'index.php?controller=order-opc&submitAddDiscount=1&discount_name=' . urlencode(
                                        $rule->code
                                    )
                                );
                            }
                            Tools::redirect(
                                'index.php?controller=order&submitAddDiscount=1&discount_name=' . urlencode($rule->code)
                            );
                        } else {
                            $rule_name = isset($rule->name[(int)$this->context->cart->id_lang]) ?
                                $rule->name[(int)$this->context->cart->id_lang] : $rule->code;
                            $error = sprintf(
                                Tools::displayError(
                                    'CartRule ID %1s (%2s) used in this cart is not valid and has been
                                    withdrawn from cart'
                                ),
                                (int)$rule->id,
                                $rule_name
                            );
                            PrestaShopLogger::addLog($error, 3, '0000002', 'Cart', (int)$this->context->cart->id);
                        }
                    }
                }
            }

            foreach ($package_list as $id_address => $packageByAddress) {
                foreach ($packageByAddress as $id_package => $package) {
                    /** @var Order $order */
                    $order = new Order();
                    $order->product_list = $package['product_list'];

                    if (Configuration::get('PS_TAX_ADDRESS_TYPE') == 'id_address_delivery') {
                        $address = new Address((int)$id_address);
                        $this->context->country = new Country(
                            (int)$address->id_country,
                            (int)$this->context->cart->id_lang
                        );
                        if (!$this->context->country->active) {
                            throw new PrestaShopException('The delivery address country is not active.');
                        }
                    }

                    $carrier = null;
                    if (!$this->context->cart->isVirtualCart() && isset($package['id_carrier'])) {
                        $carrier = new Carrier((int)$package['id_carrier'], (int)$this->context->cart->id_lang);
                        $order->id_carrier = (int)$carrier->id;
                        $id_carrier = (int)$carrier->id;
                    } else {
                        $order->id_carrier = 0;
                        $id_carrier = 0;
                    }

                    $order->id_customer = (int)$this->context->cart->id_customer;
                    $order->id_address_invoice = (int)$this->context->cart->id_address_invoice;
                    $order->id_address_delivery = (int)$id_address;
                    $order->id_currency = $this->context->currency->id;
                    $order->id_lang = (int)$this->context->cart->id_lang;
                    $order->id_cart = (int)$this->context->cart->id;
                    $order->reference = $this->reference;
                    $order->id_shop = (int)$this->context->shop->id;
                    $order->id_shop_group = (int)$this->context->shop->id_shop_group;

                    $order->secure_key = ($secure_key ? pSQL($secure_key) : pSQL($this->context->customer->secure_key));
                    $order->payment = $payment_method;
                    if (isset($this->name)) {
                        $order->module = $this->name;
                    }
                    $order->recyclable = $this->context->cart->recyclable;
                    $order->gift = (int)$this->context->cart->gift;
                    $order->gift_message = $this->context->cart->gift_message;
                    $order->mobile_theme = $this->context->cart->mobile_theme;
                    $order->conversion_rate = $this->context->currency->conversion_rate;
                    $amount_paid = !$dont_touch_amount ? Tools::ps_round((float)$amount_paid, 2) : $amount_paid;
                    $order->total_paid_real = 0;

                    $order->total_products = (float)$this->context->cart->getOrderTotal(
                        false,
                        Cart::ONLY_PRODUCTS,
                        $order->product_list,
                        $id_carrier
                    );
                    $order->total_products_wt = (float)$this->context->cart->getOrderTotal(
                        true,
                        Cart::ONLY_PRODUCTS,
                        $order->product_list,
                        $id_carrier
                    );
                    $order->total_discounts_tax_excl = (float)abs(
                        $this->context->cart->getOrderTotal(
                            false,
                            Cart::ONLY_DISCOUNTS,
                            $order->product_list,
                            $id_carrier
                        )
                    );
                    $order->total_discounts_tax_incl = (float)abs(
                        $this->context->cart->getOrderTotal(
                            true,
                            Cart::ONLY_DISCOUNTS,
                            $order->product_list,
                            $id_carrier
                        )
                    );
                    $order->total_discounts = $order->total_discounts_tax_incl;

                    $order->total_shipping_tax_excl = (float)$this->context->cart->getPackageShippingCost(
                        (int)$id_carrier,
                        false,
                        null,
                        $order->product_list
                    );
                    $order->total_shipping_tax_incl = (float)$this->context->cart->getPackageShippingCost(
                        (int)$id_carrier,
                        true,
                        null,
                        $order->product_list
                    );
                    $order->total_shipping = $order->total_shipping_tax_incl;

                    if (!is_null($carrier) && Validate::isLoadedObject($carrier)) {
                        $order->carrier_tax_rate = $carrier->getTaxesRate(
                            new Address((int)$this->context->cart->{Configuration::get('PS_TAX_ADDRESS_TYPE')})
                        );
                    }

                    $order->total_wrapping_tax_excl = (float)abs(
                        $this->context->cart->getOrderTotal(
                            false,
                            Cart::ONLY_WRAPPING,
                            $order->product_list,
                            $id_carrier
                        )
                    );
                    $order->total_wrapping_tax_incl = (float)abs(
                        $this->context->cart->getOrderTotal(
                            true,
                            Cart::ONLY_WRAPPING,
                            $order->product_list,
                            $id_carrier
                        )
                    );
                    $order->total_wrapping = $order->total_wrapping_tax_incl;

                    $order->total_paid_tax_excl = (float)Tools::ps_round(
                        (float)$this->context->cart->getOrderTotal(
                            false,
                            Cart::BOTH,
                            $order->product_list,
                            $id_carrier
                        ),
                        _PS_PRICE_COMPUTE_PRECISION_
                    );
                    $order->total_paid_tax_incl = (float)Tools::ps_round(
                        (float)$this->context->cart->getOrderTotal(
                            true,
                            Cart::BOTH,
                            $order->product_list,
                            $id_carrier
                        ),
                        _PS_PRICE_COMPUTE_PRECISION_
                    );
                    $order->total_paid = $order->total_paid_tax_incl;
                    $order->round_mode = Configuration::get('PS_PRICE_ROUND_MODE');
                    $order->round_type = Configuration::get('PS_ROUND_TYPE');

                    $order->invoice_date = '0000-00-00 00:00:00';
                    $order->delivery_date = '0000-00-00 00:00:00';

                    if (self::DEBUG_MODE) {
                        PrestaShopLogger::addLog(
                            'PaymentModule::validateOrder - Order is about to be added',
                            1,
                            null,
                            'Cart',
                            (int)$id_cart,
                            true
                        );
                    }

                    // Creating order
                    $result = $order->add();

                    if (!$result) {
                        PrestaShopLogger::addLog(
                            'PaymentModule::validateOrder - Order cannot be created',
                            3,
                            null,
                            'Cart',
                            (int)$id_cart,
                            true
                        );
                        throw new PrestaShopException('Can\'t save Order');
                    }

                    // Amount paid by customer is not the right one -> Status = payment error
                    // We don't use the following condition to avoid the float precision issues :
                    // http://www.php.net/manual/en/language.types.float.php
                    // if ($order->total_paid != $order->total_paid_real)
                    // We use number_format in order to compare two string
                    if ($order_status->logable && number_format($cart_total_paid, _PS_PRICE_COMPUTE_PRECISION_)
                        != number_format($amount_paid, _PS_PRICE_COMPUTE_PRECISION_)
                    ) {
                        $id_order_state = Configuration::get('PS_OS_ERROR');
                    }

                    $order_list[] = $order;

                    if (self::DEBUG_MODE) {
                        PrestaShopLogger::addLog(
                            'PaymentModule::validateOrder - OrderDetail is about to be added',
                            1,
                            null,
                            'Cart',
                            (int)$id_cart,
                            true
                        );
                    }

                    // Insert new Order detail list using cart for the current order
                    $order_detail = new OrderDetail(null, null, $this->context);
                    $order_detail->createList(
                        $order,
                        $this->context->cart,
                        $id_order_state,
                        $order->product_list,
                        0,
                        true,
                        $package_list[$id_address][$id_package]['id_warehouse']
                    );
                    $order_detail_list[] = $order_detail;

                    if (self::DEBUG_MODE) {
                        PrestaShopLogger::addLog(
                            'PaymentModule::validateOrder - OrderCarrier is about to be added',
                            1,
                            null,
                            'Cart',
                            (int)$id_cart,
                            true
                        );
                    }

                    // Adding an entry in order_carrier table
                    if (!is_null($carrier)) {
                        $order_carrier = new OrderCarrier();
                        $order_carrier->id_order = (int)$order->id;
                        $order_carrier->id_carrier = (int)$id_carrier;
                        $order_carrier->weight = (float)$order->getTotalWeight();
                        $order_carrier->shipping_cost_tax_excl = (float)$order->total_shipping_tax_excl;
                        $order_carrier->shipping_cost_tax_incl = (float)$order->total_shipping_tax_incl;
                        $order_carrier->add();
                    }
                }
            }

            // The country can only change if the address used for the calculation is the delivery address,
            // and if multi-shipping is activated
            if (Configuration::get('PS_TAX_ADDRESS_TYPE') == 'id_address_delivery') {
                $this->context->country = $context_country;
            }

            if (!$this->context->country->active) {
                PrestaShopLogger::addLog(
                    'PaymentModule::validateOrder - Country is not active',
                    3,
                    null,
                    'Cart',
                    (int)$id_cart,
                    true
                );
                throw new PrestaShopException('The order address country is not active.');
            }

            if (self::DEBUG_MODE) {
                PrestaShopLogger::addLog(
                    'PaymentModule::validateOrder - Payment is about to be added',
                    1,
                    null,
                    'Cart',
                    (int)$id_cart,
                    true
                );
            }

            // Register Payment only if the order status validate the order
            if ($order_status->logable) {
                // $order is the last order loop in the foreach
                // The method addOrderPayment of the class Order make a create a paymentOrder
                // linked to the order reference and not to the order id
                if (isset($extra_vars['transaction_id'])) {
                    $transaction_id = $extra_vars['transaction_id'];
                } else {
                    $transaction_id = null;
                }

                if (!isset($order) || !Validate::isLoadedObject($order) || !$order->addOrderPayment(
                    $amount_paid,
                    null,
                    $transaction_id
                )) {
                    PrestaShopLogger::addLog(
                        'PaymentModule::validateOrder - Cannot save Order Payment',
                        3,
                        null,
                        'Cart',
                        (int)$id_cart,
                        true
                    );
                    throw new PrestaShopException('Can\'t save Order Payment');
                }
            }

            // Next !
            $cart_rule_used = array();

            // Make sure CartRule caches are empty
            CartRule::cleanCache();
            foreach ($order_detail_list as $key => $order_detail) {
                /** @var OrderDetail $order_detail */

                $order = $order_list[$key];
                if (!$order_creation_failed && isset($order->id)) {
                    /*if (!$secure_key) {
                        $message .= '<br />' . Tools::displayError(
                            'Warning: the secure key is empty, check your payment account before validation'
                        );
                    }*/
                    // Optional message to attach to this order
                    if (isset($message) & !empty($message)) {
                        $msg = new Message();
                        $message = strip_tags($message, '<br>');
                        if (Validate::isCleanHtml($message)) {
                            if (self::DEBUG_MODE) {
                                PrestaShopLogger::addLog(
                                    'PaymentModule::validateOrder - Message is about to be added',
                                    1,
                                    null,
                                    'Cart',
                                    (int)$id_cart,
                                    true
                                );
                            }
                            $msg->message = $message;
                            $msg->id_cart = (int)$id_cart;
                            $msg->id_customer = (int)($order->id_customer);
                            $msg->id_order = (int)$order->id;
                            $msg->private = 1;
                            $msg->add();
                        }
                    }

                    // Insert new Order detail list using cart for the current order
                    //$orderDetail = new OrderDetail(null, null, $this->context);
                    //$orderDetail->createList($order, $this->context->cart, $id_order_state);

                    // Construct order detail table for the email
                    $virtual_product = true;

                    $product_var_tpl_list = array();
                    foreach ($order->product_list as $product) {
                        $price = Product::getPriceStatic(
                            (int)$product['id_product'],
                            false,
                            ($product['id_product_attribute'] ? (int)$product['id_product_attribute'] : null),
                            6,
                            null,
                            false,
                            true,
                            $product['cart_quantity'],
                            false,
                            (int)$order->id_customer,
                            (int)$order->id_cart,
                            (int)$order->{Configuration::get('PS_TAX_ADDRESS_TYPE')}
                        );
                        $price_wt = Product::getPriceStatic(
                            (int)$product['id_product'],
                            true,
                            ($product['id_product_attribute'] ? (int)$product['id_product_attribute'] : null),
                            2,
                            null,
                            false,
                            true,
                            $product['cart_quantity'],
                            false,
                            (int)$order->id_customer,
                            (int)$order->id_cart,
                            (int)$order->{Configuration::get('PS_TAX_ADDRESS_TYPE')}
                        );

                        $product_price = Product::getTaxCalculationMethod() == PS_TAX_EXC ? Tools::ps_round(
                            $price,
                            2
                        ) : $price_wt;

                        $product_var_tpl = array(
                            'reference'     => $product['reference'],
                            'name'          => $product['name'] . (isset($product['attributes']) ? ' - ' .
                                    $product['attributes'] : ''),
                            'unit_price'    => Tools::displayPrice($product_price, $this->context->currency, false),
                            'price'         => Tools::displayPrice(
                                $product_price * $product['quantity'],
                                $this->context->currency,
                                false
                            ),
                            'quantity'      => $product['quantity'],
                            'customization' => array()
                        );

                        $customized_datas = Product::getAllCustomizedDatas((int)$order->id_cart);
                        if (isset($customized_datas[$product['id_product']][$product['id_product_attribute']])) {
                            $product_var_tpl['customization'] = array();
                            $customizations = $customized_datas[$product['id_product']]
                                              [$product['id_product_attribute']][$order->id_address_delivery];
                            foreach ($customizations as $customization) {
                                $customization_text = '';
                                if (isset($customization['datas'][Product::CUSTOMIZE_TEXTFIELD])) {
                                    foreach ($customization['datas'][Product::CUSTOMIZE_TEXTFIELD] as $text) {
                                        $customization_text .= $text['name'] . ': ' . $text['value'] . '<br />';
                                    }
                                }

                                if (isset($customization['datas'][Product::CUSTOMIZE_FILE])) {
                                    $customization_text .= sprintf(
                                        Tools::displayError('%d image(s)'),
                                        count($customization['datas'][Product::CUSTOMIZE_FILE])
                                    ) . '<br />';
                                }

                                $customization_quantity = (int)$product['customization_quantity'];

                                $product_var_tpl['customization'][] = array(
                                    'customization_text'     => $customization_text,
                                    'customization_quantity' => $customization_quantity,
                                    'quantity'               => Tools::displayPrice(
                                        $customization_quantity * $product_price,
                                        $this->context->currency,
                                        false
                                    )
                                );
                            }
                        }

                        $product_var_tpl_list[] = $product_var_tpl;
                        // Check if is not a virutal product for the displaying of shipping
                        if (!$product['is_virtual']) {
                            $virtual_product &= false;
                        }
                    } // end foreach ($products)

                    $product_list_txt = '';
                    $product_list_html = '';
                    if (count($product_var_tpl_list) > 0) {
                        $product_list_txt = $this->getEmailTemplateContent(
                            'order_conf_product_list.txt',
                            Mail::TYPE_TEXT,
                            $product_var_tpl_list
                        );
                        $product_list_html = $this->getEmailTemplateContent(
                            'order_conf_product_list.tpl',
                            Mail::TYPE_HTML,
                            $product_var_tpl_list
                        );
                    }

                    $cart_rules_list = array();
                    $total_reduction_value_ti = 0;
                    $total_reduction_value_tex = 0;
                    foreach ($cart_rules as $cart_rule) {
                        $package = array(
                            'id_carrier' => $order->id_carrier,
                            'id_address' => $order->id_address_delivery,
                            'products'   => $order->product_list
                        );
                        $values = array(
                            'tax_incl' => $cart_rule['obj']->getContextualValue(
                                true,
                                $this->context,
                                CartRule::FILTER_ACTION_ALL_NOCAP,
                                $package
                            ),
                            'tax_excl' => $cart_rule['obj']->getContextualValue(
                                false,
                                $this->context,
                                CartRule::FILTER_ACTION_ALL_NOCAP,
                                $package
                            )
                        );

                        // If the reduction is not applicable to this order, then continue with the next one
                        if (!$values['tax_excl']) {
                            continue;
                        }

                        // IF
                        // This is not multi-shipping
                        // The value of the voucher is greater than the total of the order
                        // Partial use is allowed
                        // This is an "amount" reduction, not a reduction in % or a gift
                        // THEN
                        // The voucher is cloned with a new value corresponding to the remainder
                        if (count($order_list) == 1 &&
                            $values['tax_incl'] > ($order->total_products_wt - $total_reduction_value_ti) &&
                            $cart_rule['obj']->partial_use == 1 &&
                            $cart_rule['obj']->reduction_amount > 0
                        ) {
                            // Create a new voucher from the original
                            $voucher = new CartRule(
                                (int)$cart_rule['obj']->id
                            ); // We need to instantiate the CartRule without lang parameter to allow saving it
                            unset($voucher->id);

                            // Set a new voucher code
                            $voucher->code = empty($voucher->code) ? Tools::substr(
                                md5($order->id . '-' . $order->id_customer . '-' . $cart_rule['obj']->id),
                                0,
                                16
                            ) : $voucher->code . '-2';
                            if (preg_match(
                                '/\-([0-9]{1,2})\-([0-9]{1,2})$/',
                                $voucher->code,
                                $matches
                            ) && $matches[1] == $matches[2]
                            ) {
                                $voucher->code = preg_replace(
                                    '/' . $matches[0] . '$/',
                                    '-' . ((int)$matches[1] + 1),
                                    $voucher->code
                                );
                            }

                            // Set the new voucher value
                            if ($voucher->reduction_tax) {
                                $voucher->reduction_amount = ($total_reduction_value_ti + $values['tax_incl'])
                                    - $order->total_products_wt;

                                // Add total shipping amout only if reduction amount > total shipping
                                if ($voucher->free_shipping == 1 &&
                                    $voucher->reduction_amount >= $order->total_shipping_tax_incl) {
                                    $voucher->reduction_amount -= $order->total_shipping_tax_incl;
                                }
                            } else {
                                $voucher->reduction_amount = ($total_reduction_value_tex + $values['tax_excl'])
                                    - $order->total_products;

                                // Add total shipping amout only if reduction amount > total shipping
                                if ($voucher->free_shipping == 1 &&
                                    $voucher->reduction_amount >= $order->total_shipping_tax_excl) {
                                    $voucher->reduction_amount -= $order->total_shipping_tax_excl;
                                }
                            }
                            if ($voucher->reduction_amount <= 0) {
                                continue;
                            }

                            if ($this->context->customer->isGuest()) {
                                $voucher->id_customer = 0;
                            } else {
                                $voucher->id_customer = $order->id_customer;
                            }

                            $voucher->quantity = 1;
                            $voucher->reduction_currency = $order->id_currency;
                            $voucher->quantity_per_user = 1;
                            $voucher->free_shipping = 0;
                            if ($voucher->add()) {
                                // If the voucher has conditions, they are now copied to the new voucher
                                CartRule::copyConditions($cart_rule['obj']->id, $voucher->id);

                                $params = array(
                                    '{voucher_amount}' => Tools::displayPrice(
                                        $voucher->reduction_amount,
                                        $this->context->currency,
                                        false
                                    ),
                                    '{voucher_num}'    => $voucher->code,
                                    '{firstname}'      => $this->context->customer->firstname,
                                    '{lastname}'       => $this->context->customer->lastname,
                                    '{id_order}'       => $order->reference,
                                    '{order_name}'     => $order->getUniqReference()
                                );
                                Mail::Send(
                                    (int)$order->id_lang,
                                    'voucher',
                                    sprintf(
                                        Mail::l('New voucher for your order %s', (int)$order->id_lang),
                                        $order->reference
                                    ),
                                    $params,
                                    $this->context->customer->email,
                                    $this->context->customer->firstname . ' ' . $this->context->customer->lastname,
                                    null,
                                    null,
                                    null,
                                    null,
                                    _PS_MAIL_DIR_,
                                    false,
                                    (int)$order->id_shop
                                );
                            }

                            $values['tax_incl'] = $order->total_products_wt - $total_reduction_value_ti;
                            $values['tax_excl'] = $order->total_products - $total_reduction_value_tex;
                        }
                        $total_reduction_value_ti += $values['tax_incl'];
                        $total_reduction_value_tex += $values['tax_excl'];

                        $order->addCartRule(
                            $cart_rule['obj']->id,
                            $cart_rule['obj']->name,
                            $values,
                            0,
                            $cart_rule['obj']->free_shipping
                        );

                        if ($id_order_state != Configuration::get('PS_OS_ERROR') &&
                            $id_order_state != Configuration::get('PS_OS_CANCELED') &&
                            !in_array(
                                $cart_rule['obj']->id,
                                $cart_rule_used
                            )
                        ) {
                            $cart_rule_used[] = $cart_rule['obj']->id;

                            // Create a new instance of Cart Rule without id_lang, in order to update its quantity
                            $cart_rule_to_update = new CartRule((int)$cart_rule['obj']->id);
                            $cart_rule_to_update->quantity = max(0, $cart_rule_to_update->quantity - 1);
                            $cart_rule_to_update->update();
                        }

                        $cart_rules_list[] = array(
                            'voucher_name'      => $cart_rule['obj']->name,
                            'voucher_reduction' => ($values['tax_incl'] != 0.00 ? '-' : '') . Tools::displayPrice(
                                $values['tax_incl'],
                                $this->context->currency,
                                false
                            )
                        );
                    }

                    $cart_rules_list_txt = '';
                    $cart_rules_list_html = '';
                    if (count($cart_rules_list) > 0) {
                        $cart_rules_list_txt = $this->getEmailTemplateContent(
                            'order_conf_cart_rules.txt',
                            Mail::TYPE_TEXT,
                            $cart_rules_list
                        );
                        $cart_rules_list_html = $this->getEmailTemplateContent(
                            'order_conf_cart_rules.tpl',
                            Mail::TYPE_HTML,
                            $cart_rules_list
                        );
                    }

                    // Specify order id for message
                    $old_message = Message::getMessageByCartId((int)$this->context->cart->id);
                    if ($old_message && !$old_message['private']) {
                        $update_message = new Message((int)$old_message['id_message']);
                        $update_message->id_order = (int)$order->id;
                        $update_message->update();

                        // Add this message in the customer thread
                        $customer_thread = new CustomerThread();
                        $customer_thread->id_contact = 0;
                        $customer_thread->id_customer = (int)$order->id_customer;
                        $customer_thread->id_shop = (int)$this->context->shop->id;
                        $customer_thread->id_order = (int)$order->id;
                        $customer_thread->id_lang = (int)$this->context->language->id;
                        $customer_thread->email = $this->context->customer->email;
                        $customer_thread->status = 'open';
                        $customer_thread->token = Tools::passwdGen(12);
                        $customer_thread->add();

                        $customer_message = new CustomerMessage();
                        $customer_message->id_customer_thread = $customer_thread->id;
                        $customer_message->id_employee = 0;
                        $customer_message->message = $update_message->message;
                        $customer_message->private = 0;

                        if (!$customer_message->add()) {
                            $this->errors[] = Tools::displayError('An error occurred while saving message');
                        }
                    }

                    if (self::DEBUG_MODE) {
                        PrestaShopLogger::addLog(
                            'PaymentModule::validateOrder - Hook validateOrder is about to be called',
                            1,
                            null,
                            'Cart',
                            (int)$id_cart,
                            true
                        );
                    }

                    // Hook validate order
                    Hook::exec(
                        'actionValidateOrder',
                        array(
                            'cart'        => $this->context->cart,
                            'order'       => $order,
                            'customer'    => $this->context->customer,
                            'currency'    => $this->context->currency,
                            'orderStatus' => $order_status
                        )
                    );

                    foreach ($this->context->cart->getProducts() as $product) {
                        if ($order_status->logable) {
                            ProductSale::addProductSale((int)$product['id_product'], (int)$product['cart_quantity']);
                        }
                    }

                    if (self::DEBUG_MODE) {
                        PrestaShopLogger::addLog(
                            'PaymentModule::validateOrder - Order Status is about to be added',
                            1,
                            null,
                            'Cart',
                            (int)$id_cart,
                            true
                        );
                    }

                    // Set the order status
                    $new_history = new OrderHistory();
                    $new_history->id_order = (int)$order->id;
                    $new_history->changeIdOrderState((int)$id_order_state, $order, true);
                    $new_history->addWithemail(true, $extra_vars);

                    // Switch to back order if needed
                    if (Configuration::get('PS_STOCK_MANAGEMENT') &&
                        ($order_detail->getStockState() ||
                            $order_detail->product_quantity_in_stock <= 0)
                    ) {
                        $history = new OrderHistory();
                        $history->id_order = (int)$order->id;
                        $history->changeIdOrderState(
                            Configuration::get($order->valid ? 'PS_OS_OUTOFSTOCK_PAID' : 'PS_OS_OUTOFSTOCK_UNPAID'),
                            $order,
                            true
                        );
                        $history->addWithemail();
                    }

                    unset($order_detail);

                    // Order is reloaded because the status just changed
                    $order = new Order((int)$order->id);

                    // Send an e-mail to customer (one order = one email)
                    if ($id_order_state != Configuration::get('PS_OS_ERROR') &&
                        $id_order_state != Configuration::get('PS_OS_CANCELED') &&
                        $this->context->customer->id
                    ) {
                        $invoice = new Address((int)$order->id_address_invoice);
                        $delivery = new Address((int)$order->id_address_delivery);
                        $delivery_state = $delivery->id_state ? new State((int)$delivery->id_state) : false;
                        $invoice_state = $invoice->id_state ? new State((int)$invoice->id_state) : false;

                        $data = array(
                            '{firstname}'            => $this->context->customer->firstname,
                            '{lastname}'             => $this->context->customer->lastname,
                            '{email}'                => $this->context->customer->email,
                            '{delivery_block_txt}'   => $this->_getFormatedAddress($delivery, "\n"),
                            '{invoice_block_txt}'    => $this->_getFormatedAddress($invoice, "\n"),
                            '{delivery_block_html}'  => $this->_getFormatedAddress(
                                $delivery,
                                '<br />',
                                array(
                                    'firstname' => '<span style="font-weight:bold;">%s</span>',
                                    'lastname'  => '<span style="font-weight:bold;">%s</span>'
                                )
                            ),
                            '{invoice_block_html}'   => $this->_getFormatedAddress(
                                $invoice,
                                '<br />',
                                array(
                                    'firstname' => '<span style="font-weight:bold;">%s</span>',
                                    'lastname'  => '<span style="font-weight:bold;">%s</span>'
                                )
                            ),
                            '{delivery_company}'     => $delivery->company,
                            '{delivery_firstname}'   => $delivery->firstname,
                            '{delivery_lastname}'    => $delivery->lastname,
                            '{delivery_address1}'    => $delivery->address1,
                            '{delivery_address2}'    => $delivery->address2,
                            '{delivery_city}'        => $delivery->city,
                            '{delivery_postal_code}' => $delivery->postcode,
                            '{delivery_country}'     => $delivery->country,
                            '{delivery_state}'       => $delivery->id_state ? $delivery_state->name : '',
                            '{delivery_phone}'       => ($delivery->phone) ? $delivery->phone : $delivery->phone_mobile,
                            '{delivery_other}'       => $delivery->other,
                            '{invoice_company}'      => $invoice->company,
                            '{invoice_vat_number}'   => $invoice->vat_number,
                            '{invoice_firstname}'    => $invoice->firstname,
                            '{invoice_lastname}'     => $invoice->lastname,
                            '{invoice_address2}'     => $invoice->address2,
                            '{invoice_address1}'     => $invoice->address1,
                            '{invoice_city}'         => $invoice->city,
                            '{invoice_postal_code}'  => $invoice->postcode,
                            '{invoice_country}'      => $invoice->country,
                            '{invoice_state}'        => $invoice->id_state ? $invoice_state->name : '',
                            '{invoice_phone}'        => ($invoice->phone) ? $invoice->phone : $invoice->phone_mobile,
                            '{invoice_other}'        => $invoice->other,
                            '{order_name}'           => $order->getUniqReference(),
                            '{date}'                 => Tools::displayDate(date('Y-m-d H:i:s'), null, 1),
                            '{carrier}'              => ($virtual_product || !isset($carrier->name)) ?
                                Tools::displayError(
                                    'No carrier'
                                ) : $carrier->name,
                            '{payment}'              => Tools::substr($order->payment, 0, 32),
                            '{products}'             => $product_list_html,
                            '{products_txt}'         => $product_list_txt,
                            '{discounts}'            => $cart_rules_list_html,
                            '{discounts_txt}'        => $cart_rules_list_txt,
                            '{total_paid}'           => Tools::displayPrice(
                                $order->total_paid,
                                $this->context->currency,
                                false
                            ),
                            '{total_products}'       => Tools::displayPrice(
                                Product::getTaxCalculationMethod() == PS_TAX_EXC ?
                                    $order->total_products :
                                    $order->total_products_wt,
                                $this->context->currency,
                                false
                            ),
                            '{total_discounts}'      => Tools::displayPrice(
                                $order->total_discounts,
                                $this->context->currency,
                                false
                            ),
                            '{total_shipping}'       => Tools::displayPrice(
                                $order->total_shipping,
                                $this->context->currency,
                                false
                            ),
                            '{total_wrapping}'       => Tools::displayPrice(
                                $order->total_wrapping,
                                $this->context->currency,
                                false
                            ),
                            '{total_tax_paid}'       => Tools::displayPrice(
                                ($order->total_products_wt - $order->total_products) +
                                ($order->total_shipping_tax_incl - $order->total_shipping_tax_excl),
                                $this->context->currency,
                                false
                            )
                        );

                        if (is_array($extra_vars)) {
                            $data = array_merge($data, $extra_vars);
                        }
                        $file_attachement = array();
                        // Join PDF invoice
                        if ((int)Configuration::get('PS_INVOICE') && $order_status->invoice && $order->invoice_number) {
                            $order_invoice_list = $order->getInvoicesCollection();
                            Hook::exec('actionPDFInvoiceRender', array('order_invoice_list' => $order_invoice_list));
                            $pdf = new PDF($order_invoice_list, PDF::TEMPLATE_INVOICE, $this->context->smarty);
                            $file_attachement['content'] = $pdf->render(false);
                            $file_attachement['name'] = Configuration::get(
                                'PS_INVOICE_PREFIX',
                                (int)$order->id_lang,
                                null,
                                $order->id_shop
                            ) . sprintf('%06d', $order->invoice_number) . '.pdf';
                            $file_attachement['mime'] = 'application/pdf';
                        } else {
                            $file_attachement = null;
                        }

                        if (self::DEBUG_MODE) {
                            PrestaShopLogger::addLog(
                                'PaymentModule::validateOrder - Mail is about to be sent',
                                1,
                                null,
                                'Cart',
                                (int)$id_cart,
                                true
                            );
                        }

                        if (Validate::isEmail($this->context->customer->email)) {
                            Mail::Send(
                                (int)$order->id_lang,
                                'order_conf',
                                Mail::l('Order confirmation', (int)$order->id_lang),
                                $data,
                                $this->context->customer->email,
                                $this->context->customer->firstname . ' ' . $this->context->customer->lastname,
                                null,
                                null,
                                $file_attachement,
                                null,
                                _PS_MAIL_DIR_,
                                false,
                                (int)$order->id_shop
                            );
                        }
                    }

                    // updates stock in shops
                    if (Configuration::get('PS_ADVANCED_STOCK_MANAGEMENT')) {
                        $product_list = $order->getProducts();
                        foreach ($product_list as $product) {
                            // if the available quantities depends on the physical stock
                            if (StockAvailable::dependsOnStock($product['product_id'])) {
                                // synchronizes
                                StockAvailable::synchronize($product['product_id'], $order->id_shop);
                            }
                        }
                    }

                    $order->updateOrderDetailTax();
                } else {
                    $error = Tools::displayError('Order creation failed');
                    PrestaShopLogger::addLog($error, 4, '0000002', 'Cart', (int)$order->id_cart);
                    die($error);
                }
            } // End foreach $order_detail_list

            // Use the last order as currentOrder
            if (isset($order) && $order->id) {
                $this->currentOrder = (int)$order->id;
            }

            if (self::DEBUG_MODE) {
                PrestaShopLogger::addLog(
                    'PaymentModule::validateOrder - End of validateOrder',
                    1,
                    null,
                    'Cart',
                    (int)$id_cart,
                    true
                );
            }

            return true;
        } else {
            $error = Tools::displayError('Cart cannot be loaded or an order has already been placed using this cart');
            PrestaShopLogger::addLog($error, 4, '0000001', 'Cart', (int)$this->context->cart->id);
            die($error);
        }
    }

    public function validateOrder17(
        $id_cart,
        $id_order_state,
        $amount_paid,
        $payment_method = 'Unknown',
        $message = null,
        $extra_vars = array(),
        $currency_special = null,
        $dont_touch_amount = false,
        $secure_key = false,
        Shop $shop = null
    ) {
        if (self::DEBUG_MODE) {
            PrestaShopLogger::addLog(
                'PaymentModule::validateOrder - Function called',
                1,
                null,
                'Cart',
                (int)$id_cart,
                true
            );
        }

        if (!isset($this->context)) {
            $this->context = Context::getContext();
        }
        $this->context->cart = new Cart((int)$id_cart);
        $this->context->customer = new Customer((int)$this->context->cart->id_customer);
        // The tax cart is loaded before the customer so re-cache the tax calculation method
        $this->context->cart->setTaxCalculationMethod();

        $this->context->language = new Language((int)$this->context->cart->id_lang);
        $this->context->shop = ($shop ? $shop : new Shop((int)$this->context->cart->id_shop));
        ShopUrl::resetMainDomainCache();
        $id_currency = $currency_special ? (int)$currency_special : (int)$this->context->cart->id_currency;
        $this->context->currency = new Currency((int)$id_currency, null, (int)$this->context->shop->id);
        if (Configuration::get('PS_TAX_ADDRESS_TYPE') == 'id_address_delivery') {
            $context_country = $this->context->country;
        }

        $order_status = new OrderState((int)$id_order_state, (int)$this->context->language->id);
        if (!Validate::isLoadedObject($order_status)) {
            PrestaShopLogger::addLog(
                'PaymentModule::validateOrder - Order Status cannot be loaded',
                3,
                null,
                'Cart',
                (int)$id_cart,
                true
            );
            throw new PrestaShopException('Can\'t load Order status');
        }

        if (!$this->active) {
            PrestaShopLogger::addLog(
                'PaymentModule::validateOrder - Module is not active',
                3,
                null,
                'Cart',
                (int)$id_cart,
                true
            );
            die(Tools::displayError());
        }

        // Does order already exists ?
        if (Validate::isLoadedObject($this->context->cart) && $this->context->cart->OrderExists() == false) {
            if ($secure_key !== false && $secure_key != $this->context->cart->secure_key) {
                PrestaShopLogger::addLog(
                    'PaymentModule::validateOrder - Secure key does not match',
                    3,
                    null,
                    'Cart',
                    (int)$id_cart,
                    true
                );
                die(Tools::displayError());
            }

            // For each package, generate an order
            $delivery_option_list = $this->context->cart->getDeliveryOptionList();
            $package_list = $this->context->cart->getPackageList();
            $cart_delivery_option = $this->context->cart->getDeliveryOption();

            // If some delivery options are not defined, or not valid, use the first valid option
            foreach ($delivery_option_list as $id_address => $package) {
                if (!isset($cart_delivery_option[$id_address]) || !array_key_exists(
                    $cart_delivery_option[$id_address],
                    $package
                )) {
                    $keys = array_keys($package);
                    foreach ($keys as $key) {
                        $cart_delivery_option[$id_address] = $key;
                        break;
                    }
                }
            }

            $order_list = array();
            $order_detail_list = array();

            $this->currentOrderReference = $this->reference;

            $cart_total_paid = (float)Tools::ps_round((float)$this->context->cart->getOrderTotal(true, Cart::BOTH), 2);

            foreach ($cart_delivery_option as $id_address => $key_carriers) {
                foreach ($delivery_option_list[$id_address][$key_carriers]['carrier_list'] as $id_carrier => $data) {
                    foreach ($data['package_list'] as $id_package) {
                        // Rewrite the id_warehouse
                        $package_list[$id_address]
                        [$id_package]['id_warehouse'] = (int)$this->context->cart->getPackageIdWarehouse(
                            $package_list[$id_address][$id_package],
                            (int)$id_carrier
                        );
                        $package_list[$id_address][$id_package]['id_carrier'] = $id_carrier;
                    }
                }
            }
            // Make sure CartRule caches are empty
            CartRule::cleanCache();
            $cart_rules = $this->context->cart->getCartRules();
            foreach ($cart_rules as $cart_rule) {
                if (($rule = new CartRule((int)$cart_rule['obj']->id)) && Validate::isLoadedObject($rule)) {
                    if ($error = $rule->checkValidity($this->context, true, true)) {
                        $this->context->cart->removeCartRule((int)$rule->id);
                        if (isset($this->context->cookie) && isset($this->context->cookie->id_customer) &&
                            $this->context->cookie->id_customer && !empty($rule->code)) {
                            Tools::redirect(
                                'index.php?controller=order&submitAddDiscount=1&discount_name=' . urlencode($rule->code)
                            );
                        } else {
                            $rule_name = isset($rule->name[(int)$this->context->cart->id_lang]) ?
                                $rule->name[(int)$this->context->cart->id_lang] : $rule->code;
                            $error = $this->trans(
                                'The cart rule named "%1s" (ID %2s) used in this cart is not valid and has 
                                been withdrawn from cart',
                                array($rule_name, (int)$rule->id),
                                'Admin.Payment.Notification'
                            );
                            PrestaShopLogger::addLog($error, 3, '0000002', 'Cart', (int)$this->context->cart->id);
                        }
                    }
                }
            }

            foreach ($package_list as $id_address => $packageByAddress) {
                foreach ($packageByAddress as $id_package => $package) {
                    /** @var Order $order */
                    $order = new Order();
                    $order->product_list = $package['product_list'];

                    if (Configuration::get('PS_TAX_ADDRESS_TYPE') == 'id_address_delivery') {
                        $address = new Address((int)$id_address);
                        $this->context->country = new Country(
                            (int)$address->id_country,
                            (int)$this->context->cart->id_lang
                        );
                        if (!$this->context->country->active) {
                            throw new PrestaShopException('The delivery address country is not active.');
                        }
                    }

                    $carrier = null;
                    if (!$this->context->cart->isVirtualCart() && isset($package['id_carrier'])) {
                        $carrier = new Carrier((int)$package['id_carrier'], (int)$this->context->cart->id_lang);
                        $order->id_carrier = (int)$carrier->id;
                        $id_carrier = (int)$carrier->id;
                    } else {
                        $order->id_carrier = 0;
                        $id_carrier = 0;
                    }

                    $order->id_customer = (int)$this->context->cart->id_customer;
                    $order->id_address_invoice = (int)$this->context->cart->id_address_invoice;
                    $order->id_address_delivery = (int)$id_address;
                    $order->id_currency = $this->context->currency->id;
                    $order->id_lang = (int)$this->context->cart->id_lang;
                    $order->id_cart = (int)$this->context->cart->id;
                    $order->reference = $this->reference;
                    $order->id_shop = (int)$this->context->shop->id;
                    $order->id_shop_group = (int)$this->context->shop->id_shop_group;

                    $order->secure_key = ($secure_key ? pSQL($secure_key) : pSQL($this->context->customer->secure_key));
                    $order->payment = $payment_method;
                    if (isset($this->name)) {
                        $order->module = $this->name;
                    }
                    $order->recyclable = $this->context->cart->recyclable;
                    $order->gift = (int)$this->context->cart->gift;
                    $order->gift_message = $this->context->cart->gift_message;
                    $order->mobile_theme = $this->context->cart->mobile_theme;
                    $order->conversion_rate = $this->context->currency->conversion_rate;
                    $amount_paid = !$dont_touch_amount ? Tools::ps_round((float)$amount_paid, 2) : $amount_paid;
                    $order->total_paid_real = 0;

                    $order->total_products = (float)$this->context->cart->getOrderTotal(
                        false,
                        Cart::ONLY_PRODUCTS,
                        $order->product_list,
                        $id_carrier
                    );
                    $order->total_products_wt = (float)$this->context->cart->getOrderTotal(
                        true,
                        Cart::ONLY_PRODUCTS,
                        $order->product_list,
                        $id_carrier
                    );
                    $order->total_discounts_tax_excl = (float)abs(
                        $this->context->cart->getOrderTotal(
                            false,
                            Cart::ONLY_DISCOUNTS,
                            $order->product_list,
                            $id_carrier
                        )
                    );
                    $order->total_discounts_tax_incl = (float)abs(
                        $this->context->cart->getOrderTotal(
                            true,
                            Cart::ONLY_DISCOUNTS,
                            $order->product_list,
                            $id_carrier
                        )
                    );
                    $order->total_discounts = $order->total_discounts_tax_incl;

                    $order->total_shipping_tax_excl = (float)$this->context->cart->getPackageShippingCost(
                        (int)$id_carrier,
                        false,
                        null,
                        $order->product_list
                    );
                    $order->total_shipping_tax_incl = (float)$this->context->cart->getPackageShippingCost(
                        (int)$id_carrier,
                        true,
                        null,
                        $order->product_list
                    );
                    $order->total_shipping = $order->total_shipping_tax_incl;

                    if (!is_null($carrier) && Validate::isLoadedObject($carrier)) {
                        $order->carrier_tax_rate = $carrier->getTaxesRate(
                            new Address((int)$this->context->cart->{Configuration::get('PS_TAX_ADDRESS_TYPE')})
                        );
                    }

                    $order->total_wrapping_tax_excl = (float)abs(
                        $this->context->cart->getOrderTotal(
                            false,
                            Cart::ONLY_WRAPPING,
                            $order->product_list,
                            $id_carrier
                        )
                    );
                    $order->total_wrapping_tax_incl = (float)abs(
                        $this->context->cart->getOrderTotal(
                            true,
                            Cart::ONLY_WRAPPING,
                            $order->product_list,
                            $id_carrier
                        )
                    );
                    $order->total_wrapping = $order->total_wrapping_tax_incl;

                    $order->total_paid_tax_excl = (float)Tools::ps_round(
                        (float)$this->context->cart->getOrderTotal(
                            false,
                            Cart::BOTH,
                            $order->product_list,
                            $id_carrier
                        ),
                        _PS_PRICE_COMPUTE_PRECISION_
                    );
                    $order->total_paid_tax_incl = (float)Tools::ps_round(
                        (float)$this->context->cart->getOrderTotal(
                            true,
                            Cart::BOTH,
                            $order->product_list,
                            $id_carrier
                        ),
                        _PS_PRICE_COMPUTE_PRECISION_
                    );
                    $order->total_paid = $order->total_paid_tax_incl;
                    $order->round_mode = Configuration::get('PS_PRICE_ROUND_MODE');
                    $order->round_type = Configuration::get('PS_ROUND_TYPE');

                    $order->invoice_date = '0000-00-00 00:00:00';
                    $order->delivery_date = '0000-00-00 00:00:00';

                    if (self::DEBUG_MODE) {
                        PrestaShopLogger::addLog(
                            'PaymentModule::validateOrder - Order is about to be added',
                            1,
                            null,
                            'Cart',
                            (int)$id_cart,
                            true
                        );
                    }

                    // Creating order
                    $result = $order->add();
                    $result = 1;

                    if (!$result) {
                        PrestaShopLogger::addLog(
                            'PaymentModule::validateOrder - Order cannot be created',
                            3,
                            null,
                            'Cart',
                            (int)$id_cart,
                            true
                        );
                        throw new PrestaShopException('Can\'t save Order');
                    }

                    // Amount paid by customer is not the right one -> Status = payment error
                    // We don't use the following condition to avoid the float precision issues :
                    // http://www.php.net/manual/en/language.types.float.php
                    // if ($order->total_paid != $order->total_paid_real)
                    // We use number_format in order to compare two string
                    if ($order_status->logable &&
                        number_format($cart_total_paid, _PS_PRICE_COMPUTE_PRECISION_) !=
                        number_format($amount_paid, _PS_PRICE_COMPUTE_PRECISION_)
                    ) {
                        $id_order_state = Configuration::get('PS_OS_ERROR');
                    }

                    $order_list[] = $order;

                    if (self::DEBUG_MODE) {
                        PrestaShopLogger::addLog(
                            'PaymentModule::validateOrder - OrderDetail is about to be added',
                            1,
                            null,
                            'Cart',
                            (int)$id_cart,
                            true
                        );
                    }

                    // Insert new Order detail list using cart for the current order
                    $order_detail = new OrderDetail(null, null, $this->context);
                    $order_detail->createList(
                        $order,
                        $this->context->cart,
                        $id_order_state,
                        $order->product_list,
                        0,
                        true,
                        $package_list[$id_address][$id_package]['id_warehouse']
                    );
                    $order_detail_list[] = $order_detail;

                    if (self::DEBUG_MODE) {
                        PrestaShopLogger::addLog(
                            'PaymentModule::validateOrder - OrderCarrier is about to be added',
                            1,
                            null,
                            'Cart',
                            (int)$id_cart,
                            true
                        );
                    }

                    // Adding an entry in order_carrier table
                    if (!is_null($carrier)) {
                        $order_carrier = new OrderCarrier();
                        $order_carrier->id_order = (int)$order->id;
                        $order_carrier->id_carrier = (int)$id_carrier;
                        $order_carrier->weight = (float)$order->getTotalWeight();
                        $order_carrier->shipping_cost_tax_excl = (float)$order->total_shipping_tax_excl;
                        $order_carrier->shipping_cost_tax_incl = (float)$order->total_shipping_tax_incl;
                        $order_carrier->add();
                    }
                }
            }

            // The country can only change if the address used for the calculation is the delivery address,
            // and if multi-shipping is activated
            if (Configuration::get('PS_TAX_ADDRESS_TYPE') == 'id_address_delivery') {
                $this->context->country = $context_country;
            }

            if (!$this->context->country->active) {
                PrestaShopLogger::addLog(
                    'PaymentModule::validateOrder - Country is not active',
                    3,
                    null,
                    'Cart',
                    (int)$id_cart,
                    true
                );
                throw new PrestaShopException('The order address country is not active.');
            }

            if (self::DEBUG_MODE) {
                PrestaShopLogger::addLog(
                    'PaymentModule::validateOrder - Payment is about to be added',
                    1,
                    null,
                    'Cart',
                    (int)$id_cart,
                    true
                );
            }

            // Register Payment only if the order status validate the order
            if ($order_status->logable) {
                // $order is the last order loop in the foreach
                // The method addOrderPayment of the class Order make a create a paymentOrder
                // linked to the order reference and not to the order id
                if (isset($extra_vars['transaction_id'])) {
                    $transaction_id = $extra_vars['transaction_id'];
                } else {
                    $transaction_id = null;
                }

                if (!$order->addOrderPayment($amount_paid, null, $transaction_id)) {
                    PrestaShopLogger::addLog(
                        'PaymentModule::validateOrder - Cannot save Order Payment',
                        3,
                        null,
                        'Cart',
                        (int)$id_cart,
                        true
                    );
                    throw new PrestaShopException('Can\'t save Order Payment');
                }
            }

            // Next !
            $cart_rule_used = array();

            // Make sure CartRule caches are empty
            CartRule::cleanCache();
            foreach ($order_detail_list as $key => $order_detail) {
                /** @var OrderDetail $order_detail */

                $order = $order_list[$key];
                if (isset($order->id)) {
                    if (!$secure_key) {
                        $message .= '<br />' . $this->trans(
                            'Warning: the secure key is empty, check your payment account before validation',
                            array(),
                            'Admin.Payment.Notification'
                        );
                    }
                    // Optional message to attach to this order
                    if (isset($message) & !empty($message)) {
                        $msg = new Message();
                        $message = strip_tags($message, '<br>');
                        if (Validate::isCleanHtml($message)) {
                            if (self::DEBUG_MODE) {
                                PrestaShopLogger::addLog(
                                    'PaymentModule::validateOrder - Message is about to be added',
                                    1,
                                    null,
                                    'Cart',
                                    (int)$id_cart,
                                    true
                                );
                            }
                            $msg->message = $message;
                            $msg->id_cart = (int)$id_cart;
                            $msg->id_customer = (int)($order->id_customer);
                            $msg->id_order = (int)$order->id;
                            $msg->private = 1;
                            $msg->add();
                        }
                    }

                    // Insert new Order detail list using cart for the current order
                    //$orderDetail = new OrderDetail(null, null, $this->context);
                    //$orderDetail->createList($order, $this->context->cart, $id_order_state);

                    // Construct order detail table for the email
                    $virtual_product = true;

                    $product_var_tpl_list = array();
                    foreach ($order->product_list as $product) {
                        $price = Product::getPriceStatic(
                            (int)$product['id_product'],
                            false,
                            ($product['id_product_attribute'] ? (int)$product['id_product_attribute'] : null),
                            6,
                            null,
                            false,
                            true,
                            $product['cart_quantity'],
                            false,
                            (int)$order->id_customer,
                            (int)$order->id_cart,
                            (int)$order->{Configuration::get('PS_TAX_ADDRESS_TYPE')},
                            $specific_price,
                            true,
                            true,
                            null,
                            true,
                            $product['id_customization']
                        );
                        $price_wt = Product::getPriceStatic(
                            (int)$product['id_product'],
                            true,
                            ($product['id_product_attribute'] ? (int)$product['id_product_attribute'] : null),
                            2,
                            null,
                            false,
                            true,
                            $product['cart_quantity'],
                            false,
                            (int)$order->id_customer,
                            (int)$order->id_cart,
                            (int)$order->{Configuration::get('PS_TAX_ADDRESS_TYPE')},
                            $specific_price,
                            true,
                            true,
                            null,
                            true,
                            $product['id_customization']
                        );

                        $product_price = Product::getTaxCalculationMethod() == PS_TAX_EXC ? Tools::ps_round(
                            $price,
                            2
                        ) : $price_wt;

                        $product_var_tpl = array(
                            'id_product'    => $product['id_product'],
                            'reference'     => $product['reference'],
                            'name'          => $product['name'] . (isset($product['attributes']) ? ' - ' .
                                    $product['attributes'] : ''),
                            'price'         => Tools::displayPrice(
                                $product_price * $product['quantity'],
                                $this->context->currency,
                                false
                            ),
                            'quantity'      => $product['quantity'],
                            'customization' => array()
                        );

                        if (isset($product['price']) && $product['price']) {
                            $product_var_tpl['unit_price'] = Tools::displayPrice(
                                $product_price,
                                $this->context->currency,
                                false
                            );
                            $product_var_tpl['unit_price_full'] = Tools::displayPrice(
                                $product_price,
                                $this->context->currency,
                                false
                            ) . ' ' . $product['unity'];
                        } else {
                            $product_var_tpl['unit_price'] = $product_var_tpl['unit_price_full'] = '';
                        }

                        $customized_datas = Product::getAllCustomizedDatas(
                            (int)$order->id_cart,
                            null,
                            true,
                            null,
                            (int)$product['id_customization']
                        );
                        if (isset($customized_datas[$product['id_product']][$product['id_product_attribute']])) {
                            $product_var_tpl['customization'] = array();
                            foreach ($customized_datas[$product['id_product']][$product['id_product_attribute']]
                                     [$order->id_address_delivery] as $customization) {
                                $customization_text = '';
                                if (isset($customization['datas'][Product::CUSTOMIZE_TEXTFIELD])) {
                                    foreach ($customization['datas'][Product::CUSTOMIZE_TEXTFIELD] as $text) {
                                        $customization_text .= '<strong>' . $text['name'] . '</strong>: ' .
                                            $text['value'] . '<br />';
                                    }
                                }

                                if (isset($customization['datas'][Product::CUSTOMIZE_FILE])) {
                                    $customization_text .= $this->trans(
                                        '%d image(s)',
                                        array(count($customization['datas'][Product::CUSTOMIZE_FILE])),
                                        'Admin.Payment.Notification'
                                    ) . '<br />';
                                }

                                $customization_quantity = (int)$customization['quantity'];

                                $product_var_tpl['customization'][] = array(
                                    'customization_text'     => $customization_text,
                                    'customization_quantity' => $customization_quantity,
                                    'quantity'               => Tools::displayPrice(
                                        $customization_quantity * $product_price,
                                        $this->context->currency,
                                        false
                                    )
                                );
                            }
                        }

                        $product_var_tpl_list[] = $product_var_tpl;
                        // Check if is not a virutal product for the displaying of shipping
                        if (!$product['is_virtual']) {
                            $virtual_product &= false;
                        }
                    } // end foreach ($products)

                    $product_list_txt = '';
                    $product_list_html = '';
                    if (count($product_var_tpl_list) > 0) {
                        $product_list_txt = $this->getEmailTemplateContent(
                            'order_conf_product_list.txt',
                            Mail::TYPE_TEXT,
                            $product_var_tpl_list
                        );
                        $product_list_html = $this->getEmailTemplateContent(
                            'order_conf_product_list.tpl',
                            Mail::TYPE_HTML,
                            $product_var_tpl_list
                        );
                    }

                    $cart_rules_list = array();
                    $total_reduction_value_ti = 0;
                    $total_reduction_value_tex = 0;
                    foreach ($cart_rules as $cart_rule) {
                        $package = array(
                            'id_carrier' => $order->id_carrier,
                            'id_address' => $order->id_address_delivery,
                            'products'   => $order->product_list
                        );
                        $values = array(
                            'tax_incl' => $cart_rule['obj']->getContextualValue(
                                true,
                                $this->context,
                                CartRule::FILTER_ACTION_ALL_NOCAP,
                                $package
                            ),
                            'tax_excl' => $cart_rule['obj']->getContextualValue(
                                false,
                                $this->context,
                                CartRule::FILTER_ACTION_ALL_NOCAP,
                                $package
                            )
                        );

                        // If the reduction is not applicable to this order, then continue with the next one
                        if (!$values['tax_excl']) {
                            continue;
                        }

                        // IF
                        //  This is not multi-shipping
                        //  The value of the voucher is greater than the total of the order
                        //  Partial use is allowed
                        //  This is an "amount" reduction, not a reduction in % or a gift
                        // THEN
                        //  The voucher is cloned with a new value corresponding to the remainder
                        if (count($order_list) == 1 &&
                            $values['tax_incl'] > ($order->total_products_wt - $total_reduction_value_ti) &&
                            $cart_rule['obj']->partial_use == 1 &&
                            $cart_rule['obj']->reduction_amount > 0
                        ) {
                            // Create a new voucher from the original
                            $voucher = new CartRule(
                                (int)$cart_rule['obj']->id
                            ); // We need to instantiate the CartRule without lang parameter to allow saving it
                            unset($voucher->id);

                            // Set a new voucher code
                            $voucher->code = empty($voucher->code) ? Tools::substr(
                                md5($order->id . '-' . $order->id_customer . '-' . $cart_rule['obj']->id),
                                0,
                                16
                            ) : $voucher->code . '-2';
                            if (preg_match(
                                '/\-([0-9]{1,2})\-([0-9]{1,2})$/',
                                $voucher->code,
                                $matches
                            ) && $matches[1] == $matches[2]
                            ) {
                                $voucher->code = preg_replace(
                                    '/' . $matches[0] . '$/',
                                    '-' . ((int)($matches[1]) + 1),
                                    $voucher->code
                                );
                            }

                            // Set the new voucher value
                            if ($voucher->reduction_tax) {
                                $voucher->reduction_amount = ($total_reduction_value_ti + $values['tax_incl']) -
                                    $order->total_products_wt;

                                // Add total shipping amout only if reduction amount > total shipping
                                if ($voucher->free_shipping == 1 &&
                                    $voucher->reduction_amount >= $order->total_shipping_tax_incl) {
                                    $voucher->reduction_amount -= $order->total_shipping_tax_incl;
                                }
                            } else {
                                $voucher->reduction_amount = ($total_reduction_value_tex + $values['tax_excl']) -
                                    $order->total_products;

                                // Add total shipping amout only if reduction amount > total shipping
                                if ($voucher->free_shipping == 1 &&
                                    $voucher->reduction_amount >= $order->total_shipping_tax_excl) {
                                    $voucher->reduction_amount -= $order->total_shipping_tax_excl;
                                }
                            }
                            if ($voucher->reduction_amount <= 0) {
                                continue;
                            }

                            if ($this->context->customer->isGuest()) {
                                $voucher->id_customer = 0;
                            } else {
                                $voucher->id_customer = $order->id_customer;
                            }

                            $voucher->quantity = 1;
                            $voucher->reduction_currency = $order->id_currency;
                            $voucher->quantity_per_user = 1;
                            if ($voucher->add()) {
                                // If the voucher has conditions, they are now copied to the new voucher
                                CartRule::copyConditions($cart_rule['obj']->id, $voucher->id);
                                $orderLanguage = new Language((int)$order->id_lang);

                                $params = array(
                                    '{voucher_amount}' => Tools::displayPrice(
                                        $voucher->reduction_amount,
                                        $this->context->currency,
                                        false
                                    ),
                                    '{voucher_num}'    => $voucher->code,
                                    '{firstname}'      => $this->context->customer->firstname,
                                    '{lastname}'       => $this->context->customer->lastname,
                                    '{id_order}'       => $order->reference,
                                    '{order_name}'     => $order->getUniqReference()
                                );
                                Mail::Send(
                                    (int)$order->id_lang,
                                    'voucher',
                                    Context::getContext()->getTranslator()->trans(
                                        'New voucher for your order %s',
                                        array($order->reference),
                                        'Emails.Subject',
                                        $orderLanguage->locale
                                    ),
                                    $params,
                                    $this->context->customer->email,
                                    $this->context->customer->firstname . ' ' . $this->context->customer->lastname,
                                    null,
                                    null,
                                    null,
                                    null,
                                    _PS_MAIL_DIR_,
                                    false,
                                    (int)$order->id_shop
                                );
                            }

                            $values['tax_incl'] = $order->total_products_wt - $total_reduction_value_ti;
                            $values['tax_excl'] = $order->total_products - $total_reduction_value_tex;
                            if (1 == $voucher->free_shipping) {
                                $values['tax_incl'] += $order->total_shipping_tax_incl;
                                $values['tax_excl'] += $order->total_shipping_tax_excl;
                            }
                        }
                        $total_reduction_value_ti += $values['tax_incl'];
                        $total_reduction_value_tex += $values['tax_excl'];

                        $order->addCartRule(
                            $cart_rule['obj']->id,
                            $cart_rule['obj']->name,
                            $values,
                            0,
                            $cart_rule['obj']->free_shipping
                        );

                        if ($id_order_state != Configuration::get('PS_OS_ERROR') &&
                            $id_order_state != Configuration::get('PS_OS_CANCELED') &&
                            !in_array(
                                $cart_rule['obj']->id,
                                $cart_rule_used
                            )
                        ) {
                            $cart_rule_used[] = $cart_rule['obj']->id;

                            // Create a new instance of Cart Rule without id_lang, in order to update its quantity
                            $cart_rule_to_update = new CartRule((int)$cart_rule['obj']->id);
                            $cart_rule_to_update->quantity = max(0, $cart_rule_to_update->quantity - 1);
                            $cart_rule_to_update->update();
                        }

                        $cart_rules_list[] = array(
                            'voucher_name'      => $cart_rule['obj']->name,
                            'voucher_reduction' => ($values['tax_incl'] != 0.00 ? '-' : '') . Tools::displayPrice(
                                $values['tax_incl'],
                                $this->context->currency,
                                false
                            )
                        );
                    }

                    $cart_rules_list_txt = '';
                    $cart_rules_list_html = '';
                    if (count($cart_rules_list) > 0) {
                        $cart_rules_list_txt = $this->getEmailTemplateContent(
                            'order_conf_cart_rules.txt',
                            Mail::TYPE_TEXT,
                            $cart_rules_list
                        );
                        $cart_rules_list_html = $this->getEmailTemplateContent(
                            'order_conf_cart_rules.tpl',
                            Mail::TYPE_HTML,
                            $cart_rules_list
                        );
                    }

                    // Specify order id for message
                    $old_message = Message::getMessageByCartId((int)$this->context->cart->id);
                    if ($old_message && !$old_message['private']) {
                        $update_message = new Message((int)$old_message['id_message']);
                        $update_message->id_order = (int)$order->id;
                        $update_message->update();

                        // Add this message in the customer thread
                        $customer_thread = new CustomerThread();
                        $customer_thread->id_contact = 0;
                        $customer_thread->id_customer = (int)$order->id_customer;
                        $customer_thread->id_shop = (int)$this->context->shop->id;
                        $customer_thread->id_order = (int)$order->id;
                        $customer_thread->id_lang = (int)$this->context->language->id;
                        $customer_thread->email = $this->context->customer->email;
                        $customer_thread->status = 'open';
                        $customer_thread->token = Tools::passwdGen(12);
                        $customer_thread->add();

                        $customer_message = new CustomerMessage();
                        $customer_message->id_customer_thread = $customer_thread->id;
                        $customer_message->id_employee = 0;
                        $customer_message->message = $update_message->message;
                        $customer_message->private = 1;

                        if (!$customer_message->add()) {
                            $this->errors[] = $this->trans(
                                'An error occurred while saving message',
                                array(),
                                'Admin.Payment.Notification'
                            );
                        }
                    }

                    if (self::DEBUG_MODE) {
                        PrestaShopLogger::addLog(
                            'PaymentModule::validateOrder - Hook validateOrder is about to be called',
                            1,
                            null,
                            'Cart',
                            (int)$id_cart,
                            true
                        );
                    }

                    // Hook validate order
                    Hook::exec(
                        'actionValidateOrder',
                        array(
                            'cart'        => $this->context->cart,
                            'order'       => $order,
                            'customer'    => $this->context->customer,
                            'currency'    => $this->context->currency,
                            'orderStatus' => $order_status
                        )
                    );

                    foreach ($this->context->cart->getProducts() as $product) {
                        if ($order_status->logable) {
                            ProductSale::addProductSale((int)$product['id_product'], (int)$product['cart_quantity']);
                        }
                    }

                    if (self::DEBUG_MODE) {
                        PrestaShopLogger::addLog(
                            'PaymentModule::validateOrder - Order Status is about to be added',
                            1,
                            null,
                            'Cart',
                            (int)$id_cart,
                            true
                        );
                    }

                    // Set the order status
                    $new_history = new OrderHistory();
                    $new_history->id_order = (int)$order->id;
                    $new_history->changeIdOrderState((int)$id_order_state, $order, true);
                    $new_history->addWithemail(true, $extra_vars);

                    // Switch to back order if needed
                    if (Configuration::get('PS_STOCK_MANAGEMENT') &&
                        ($order_detail->getStockState() || $order_detail->product_quantity_in_stock <= 0)
                    ) {
                        $history = new OrderHistory();
                        $history->id_order = (int)$order->id;
                        $history->changeIdOrderState(
                            Configuration::get($order->valid ? 'PS_OS_OUTOFSTOCK_PAID' : 'PS_OS_OUTOFSTOCK_UNPAID'),
                            $order,
                            true
                        );
                        $history->addWithemail();
                    }

                    unset($order_detail);

                    // Order is reloaded because the status just changed
                    $order = new Order((int)$order->id);

                    // Send an e-mail to customer (one order = one email)
                    if ($id_order_state != Configuration::get('PS_OS_ERROR') &&
                        $id_order_state != Configuration::get('PS_OS_CANCELED') &&
                        $this->context->customer->id
                    ) {
                        $invoice = new Address((int)$order->id_address_invoice);
                        $delivery = new Address((int)$order->id_address_delivery);
                        $delivery_state = $delivery->id_state ? new State((int)$delivery->id_state) : false;
                        $invoice_state = $invoice->id_state ? new State((int)$invoice->id_state) : false;

                        $data = array(
                            '{firstname}'            => $this->context->customer->firstname,
                            '{lastname}'             => $this->context->customer->lastname,
                            '{email}'                => $this->context->customer->email,
                            '{delivery_block_txt}'   => $this->_getFormatedAddress($delivery, "\n"),
                            '{invoice_block_txt}'    => $this->_getFormatedAddress($invoice, "\n"),
                            '{delivery_block_html}'  => $this->_getFormatedAddress(
                                $delivery,
                                '<br />',
                                array(
                                    'firstname' => '<span style="font-weight:bold;">%s</span>',
                                    'lastname'  => '<span style="font-weight:bold;">%s</span>'
                                )
                            ),
                            '{invoice_block_html}'   => $this->_getFormatedAddress(
                                $invoice,
                                '<br />',
                                array(
                                    'firstname' => '<span style="font-weight:bold;">%s</span>',
                                    'lastname'  => '<span style="font-weight:bold;">%s</span>'
                                )
                            ),
                            '{delivery_company}'     => $delivery->company,
                            '{delivery_firstname}'   => $delivery->firstname,
                            '{delivery_lastname}'    => $delivery->lastname,
                            '{delivery_address1}'    => $delivery->address1,
                            '{delivery_address2}'    => $delivery->address2,
                            '{delivery_city}'        => $delivery->city,
                            '{delivery_postal_code}' => $delivery->postcode,
                            '{delivery_country}'     => $delivery->country,
                            '{delivery_state}'       => $delivery->id_state ? $delivery_state->name : '',
                            '{delivery_phone}'       => ($delivery->phone) ? $delivery->phone : $delivery->phone_mobile,
                            '{delivery_other}'       => $delivery->other,
                            '{invoice_company}'      => $invoice->company,
                            '{invoice_vat_number}'   => $invoice->vat_number,
                            '{invoice_firstname}'    => $invoice->firstname,
                            '{invoice_lastname}'     => $invoice->lastname,
                            '{invoice_address2}'     => $invoice->address2,
                            '{invoice_address1}'     => $invoice->address1,
                            '{invoice_city}'         => $invoice->city,
                            '{invoice_postal_code}'  => $invoice->postcode,
                            '{invoice_country}'      => $invoice->country,
                            '{invoice_state}'        => $invoice->id_state ? $invoice_state->name : '',
                            '{invoice_phone}'        => ($invoice->phone) ? $invoice->phone : $invoice->phone_mobile,
                            '{invoice_other}'        => $invoice->other,
                            '{order_name}'           => $order->getUniqReference(),
                            '{date}'                 => Tools::displayDate(date('Y-m-d H:i:s'), null, 1),
                            '{carrier}'              => ($virtual_product || !isset($carrier->name)) ? $this->trans(
                                'No carrier',
                                array(),
                                'Admin.Payment.Notification'
                            ) : $carrier->name,
                            '{payment}'              => Tools::substr($order->payment, 0, 255),
                            '{products}'             => $product_list_html,
                            '{products_txt}'         => $product_list_txt,
                            '{discounts}'            => $cart_rules_list_html,
                            '{discounts_txt}'        => $cart_rules_list_txt,
                            '{total_paid}'           => Tools::displayPrice(
                                $order->total_paid,
                                $this->context->currency,
                                false
                            ),
                            '{total_products}'       => Tools::displayPrice(
                                Product::getTaxCalculationMethod() == PS_TAX_EXC ? $order->total_products :
                                    $order->total_products_wt,
                                $this->context->currency,
                                false
                            ),
                            '{total_discounts}'      => Tools::displayPrice(
                                $order->total_discounts,
                                $this->context->currency,
                                false
                            ),
                            '{total_shipping}'       => Tools::displayPrice(
                                $order->total_shipping,
                                $this->context->currency,
                                false
                            ),
                            '{total_wrapping}'       => Tools::displayPrice(
                                $order->total_wrapping,
                                $this->context->currency,
                                false
                            ),
                            '{total_tax_paid}'       => Tools::displayPrice(
                                ($order->total_products_wt - $order->total_products) +
                                ($order->total_shipping_tax_incl - $order->total_shipping_tax_excl),
                                $this->context->currency,
                                false
                            )
                        );

                        if (is_array($extra_vars)) {
                            $data = array_merge($data, $extra_vars);
                        }
                        $file_attachement = array();
                        // Join PDF invoice
                        if ((int)Configuration::get('PS_INVOICE') && $order_status->invoice && $order->invoice_number) {
                            $order_invoice_list = $order->getInvoicesCollection();
                            Hook::exec('actionPDFInvoiceRender', array('order_invoice_list' => $order_invoice_list));
                            $pdf = new PDF($order_invoice_list, PDF::TEMPLATE_INVOICE, $this->context->smarty);
                            $file_attachement['content'] = $pdf->render(false);
                            $file_attachement['name'] = Configuration::get(
                                'PS_INVOICE_PREFIX',
                                (int)$order->id_lang,
                                null,
                                $order->id_shop
                            ) . sprintf('%06d', $order->invoice_number) . '.pdf';
                            $file_attachement['mime'] = 'application/pdf';
                        } else {
                            $file_attachement = null;
                        }

                        if (self::DEBUG_MODE) {
                            PrestaShopLogger::addLog(
                                'PaymentModule::validateOrder - Mail is about to be sent',
                                1,
                                null,
                                'Cart',
                                (int)$id_cart,
                                true
                            );
                        }

                        $orderLanguage = new Language((int)$order->id_lang);

                        if (Validate::isEmail($this->context->customer->email)) {
                            Mail::Send(
                                (int)$order->id_lang,
                                'order_conf',
                                Context::getContext()->getTranslator()->trans(
                                    'Order confirmation',
                                    array(),
                                    'Emails.Subject',
                                    $orderLanguage->locale
                                ),
                                $data,
                                $this->context->customer->email,
                                $this->context->customer->firstname . ' ' . $this->context->customer->lastname,
                                null,
                                null,
                                $file_attachement,
                                null,
                                _PS_MAIL_DIR_,
                                false,
                                (int)$order->id_shop
                            );
                        }
                    }

                    // updates stock in shops
                    if (Configuration::get('PS_ADVANCED_STOCK_MANAGEMENT')) {
                        $product_list = $order->getProducts();
                        foreach ($product_list as $product) {
                            // if the available quantities depends on the physical stock
                            if (StockAvailable::dependsOnStock($product['product_id'])) {
                                // synchronizes
                                StockAvailable::synchronize($product['product_id'], $order->id_shop);
                            }
                        }
                    }

                    $order->updateOrderDetailTax();

                    // sync all stock
                    (new PrestaShop\PrestaShop\Adapter\StockManager())->updatePhysicalProductQuantity(
                        (int)$order->id_shop,
                        (int)Configuration::get('PS_OS_ERROR'),
                        (int)Configuration::get('PS_OS_CANCELED'),
                        null,
                        (int)$order->id
                    );
                } else {
                    $error = $this->trans('Order creation failed', array(), 'Admin.Payment.Notification');
                    PrestaShopLogger::addLog($error, 4, '0000002', 'Cart', (int)($order->id_cart));
                    die($error);
                }
            } // End foreach $order_detail_list

            // Use the last order as currentOrder
            if (isset($order) && $order->id) {
                $this->currentOrder = (int)$order->id;
            }

            if (self::DEBUG_MODE) {
                PrestaShopLogger::addLog(
                    'PaymentModule::validateOrder - End of validateOrder',
                    1,
                    null,
                    'Cart',
                    (int)$id_cart,
                    true
                );
            }

            return true;
        } else {
            $error = $this->trans(
                'Cart cannot be loaded or an order has already been placed using this cart',
                array(),
                'Admin.Payment.Notification'
            );
            PrestaShopLogger::addLog($error, 4, '0000001', 'Cart', (int)($this->context->cart->id));
            die($error);
        }
    }
}
