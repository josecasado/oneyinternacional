<?php
/**
 * 3x 4x Oney Module version 1.1.21 for PrestaShop 1.6 and PrestaShop 1.7. Support contact : prestashop@itroom.fr
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/afl-3.0.php
 *
 * @author    ITroom (http://itroom.fr/)
 * @copyright ITroom
 * @license   https://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * @category  payment
 * @package   oney
 */

$sql = array();

$sql[] = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'facilypay_lock` (
    `id_facilypay_lock` int(11) unsigned NOT NULL AUTO_INCREMENT,
    `reference` VARCHAR(50) NOT NULL,
    PRIMARY KEY (`id_facilypay_lock`)
)';

$sql[] = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'facilypay_orders` (
    `id_facilypay_order` int(11) unsigned NOT NULL AUTO_INCREMENT,
    `reference` VARCHAR(50) NOT NULL,
    `transaction_id` VARCHAR(50) NOT NULL,
    PRIMARY KEY (`id_facilypay_order`)
)';

$sql[] = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'facilypay_opc` (
  `id_opc` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `business_transaction_code` varbinary(50) NOT NULL,
  `business_transaction_version` int(11) NOT NULL,
  `business_transaction_type` varchar(20) CHARACTER SET latin1 NOT NULL,
  `short_label` varchar(200) CHARACTER SET latin1 NOT NULL,
  `long_label` varchar(200) CHARACTER SET latin1 NOT NULL,
  `customer_label` varchar(200) CHARACTER SET latin1 NOT NULL,
  `minimum_number_of_instalments` int(11) NOT NULL,
  `maximum_number_of_instalments` int(11) NOT NULL,
  `choosable_number_of_instalments` tinyint(1) NOT NULL,
  `minimum_selling_price` int(11) NOT NULL,
  `maximum_selling_price` int(11) NOT NULL,
  `discount_rate` int(11) NOT NULL,
  `free_business_transaction` tinyint(1) NOT NULL,
  `example` text CHARACTER SET latin1 NOT NULL,
  `validity_start_date` date NOT NULL,
  `validity_end_date` date NOT NULL,
  `postponement_duration` int(11) NOT NULL,
  `display_order` int(11) NOT NULL,
  `selected` tinyint(1) DEFAULT "0",
  `iso_code` varchar(2) DEFAULT NULL,
  `id_shop` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`id_opc`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;';

$sql[] = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'facilypay_carriers`(
            `id_carrier` int(11),
            `delivery_mode` int(11),
            `priority` int(11),
            `priority_delivery_code` int(11),
            `address_type` int(11),
            `id_store` int(11),
            PRIMARY KEY (`id_carrier`)
        )';

$sql[] = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'facilypay_country`(
            `iso2` varchar(2),
            `iso3` varchar(3),
            PRIMARY KEY (`iso2`, `iso3`)
        )';

$sql[] = 'INSERT IGNORE INTO `' . _DB_PREFIX_ . 'facilypay_country` VALUES ("AF","AFG"), ("ZA","ZAF"), ("AX","ALA"),' .
    '("AL","ALB"), ("DZ","DZA"), ("DE","DEU"), ("AD","AND"), ("AO","AGO"), ("AI","AIA"), ("AQ","ATA"), ("AG","ATG"),' .
    '("AN","ANT"), ("SA","SAU"), ("AR","ARG"), ("AM","ARM"), ("AW","ABW"), ("AU","AUS"), ("AT","AUT"), ("AZ","AZE"),' .
    '("BS","BHS"), ("BH","BHR"), ("BD","BGD"), ("BB","BRB"), ("BY","BLR"), ("BE","BEL"), ("BZ","BLZ"), ("BJ","BEN"),' .
    '("BM","BMU"), ("BT","BTN"), ("BO","BOL"), ("BA","BIH"), ("BW","BWA"), ("BV","BVT"), ("BR","BRA"), ("BN","BRN"),' .
    '("BG","BGR"), ("BF","BFA"), ("MM","MMR"), ("BI","BDI"), ("KY","CYM"), ("KH","KHM"), ("CM","CMR"), ("CA","CAN"),' .
    '("CV","CPV"), ("CF","CAF"), ("CL","CHL"), ("CN","CHN"), ("CX","CXR"), ("CY","CYP"), ("CC","CCK"), ("CO","COL"),' .
    '("KM","COM"), ("CG","COG"), ("CD","COD"), ("CK","COK"), ("KR","KOR"), ("KP","PRK"), ("CR","CRI"), ("CI","CIV"),' .
    '("HR","HRV"), ("CU","CUB"), ("DK","DNK"), ("DJ","DJI"), ("DM","DMA"), ("EG","EGY"), ("SV","SLV"), ("AE","ARE"),' .
    '("EC","ECU"), ("ER","ERI"), ("ES","ESP"), ("EE","EST"), ("US","USA"), ("ET","ETH"), ("FK","FLK"), ("FO","FRO"),' .
    '("FJ","FJI"), ("FI","FIN"), ("FR","FRA"), ("GA","GAB"), ("GM","GMB"), ("GE","GEO"), ("GS","SGS"), ("GH","GHA"),' .
    '("GI","GIB"), ("GR","GRC"), ("GD","GRD"), ("GL","GRL"), ("GP","GLP"), ("GU","GUM"), ("GT","GTM"), ("GG","GGY"),' .
    '("GN","GIN"), ("GQ","GNQ"), ("GW","GNB"), ("GY","GUY"), ("GF","GUF"), ("HT","HTI"), ("HM","HMD"), ("HN","HND"),' .
    '("HK","HKG"), ("HU","HUN"), ("MU","MUS"), ("VG","VGB"), ("VI","VIR"), ("IN","IND"), ("ID","IDN"), ("IR","IRN"),' .
    '("IQ","IRQ"), ("IE","IRL"), ("IS","ISL"), ("IL","ISR"), ("IT","ITA"), ("JM","JAM"), ("JP","JPN"), ("JE","JEY"),' .
    '("JO","JOR"), ("KZ","KAZ"), ("KE","KEN"), ("KG","KGZ"), ("KI","KIR"), ("KW","KWT"), ("LA","LAO"), ("LS","LSO"),' .
    '("LV","LVA"), ("LB","LBN"), ("LR","LBR"), ("LY","LBY"), ("LI","LIE"), ("LT","LTU"), ("LU","LUX"), ("MO","MAC"),' .
    '("MK","MKD"), ("MG","MDG"), ("MY","MYS"), ("MW","MWI"), ("MV","MDV"), ("ML","MLI"), ("MT","MLT"), ("IM","IMN"),' .
    '("MP","MNP"), ("MA","MAR"), ("MH","MHL"), ("MQ","MTQ"), ("MR","MRT"), ("YT","MYT"), ("MX","MEX"), ("FM","FSM"),' .
    '("MD","MDA"), ("MC","MCO"), ("MN","MNG"), ("ME","MNE"), ("MS","MSR"), ("MZ","MOZ"), ("NA","NAM"), ("NR","NRU"),' .
    '("NP","NPL"), ("NI","NIC"), ("NE","NER"), ("NG","NGA"), ("NU","NIU"), ("NF","NFK"), ("NO","NOR"), ("NC","NCL"),' .
    '("NZ","NZL"), ("IO","IOT"), ("OM","OMN"), ("UG","UGA"), ("UZ","UZB"), ("PK","PAK"), ("PW","PLW"), ("PS","PSE"),' .
    '("PA","PAN"), ("PG","PNG"), ("PY","PRY"), ("NL","NLD"), ("PE","PER"), ("PH","PHL"), ("PN","PCN"), ("PL","POL"),' .
    '("PF","PYF"), ("PR","PRI"), ("PT","PRT"), ("QA","QAT"), ("DO","DOM"), ("CZ","CZE"), ("RE","REU"), ("RO","ROU"),' .
    '("GB","GBR"), ("RU","RUS"), ("RW","RWA"), ("EH","ESH"), ("BL","BLM"), ("KN","KNA"), ("SM","SMR"), ("MF","MAF"),' .
    '("PM","SPM"), ("VA","VAT"), ("VC","VCT"), ("LC","LCA"), ("SB","SLB"), ("WS","WSM"), ("AS","ASM"), ("ST","STP"),' .
    '("SN","SEN"), ("RS","SRB"), ("SC","SYC"), ("SL","SLE"), ("SG","SGP"), ("SK","SVK"), ("SI","SVN"), ("SO","SOM"),' .
    '("SD","SDN"), ("LK","LKA"), ("SE","SWE"), ("CH","CHE"), ("SR","SUR"), ("SJ","SJM"), ("SZ","SWZ"), ("SY","SYR"),' .
    '("TJ","TJK"), ("TW","TWN"), ("TZ","TZA"), ("TD","TCD"), ("TF","ATF"), ("TH","THA"), ("TL","TLS"), ("TG","TGO"),' .
    '("TK","TKL"), ("TO","TON"), ("TT","TTO"), ("TN","TUN"), ("TM","TKM"), ("TC","TCA"), ("TR","TUR"), ("TV","TUV"),' .
    '("UA","UKR"), ("UY","URY"), ("VU","VUT"), ("VE","VEN"), ("VN","VNM"), ("WF","WLF"), ("YE","YEM"), ("ZM","ZMB"),' .
    '("ZW","ZWE");';


$sql[] = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'oney_phone` (' .
    '`iso_code` VARCHAR(2),' .
    '`prefix` VARCHAR(10),' .
    '`size` INT(11),' .
    '`mobile_or_fixe` INT(1) DEFAULT 3,' .
    'PRIMARY KEY (`iso_code`, `mobile_or_fixe`))';

// mobile OR fixe : 1 = fixe, 2 = mobile, 3 = les deux
$sql[] = 'INSERT IGNORE INTO `' . _DB_PREFIX_ . 'oney_phone` VALUES ("FR","+33", 9, 3), ("BE", "+32", 8, 1),' .
    '("BE", "+32", 9, 2), ("ES", "+34", 9, 3), ("PT", "+351", 9, 3), ("IT", "+39", 10, 3)';

$sql[] = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'oney_cache` (' .
    '`id` VARCHAR(50) PRIMARY KEY,' .
    '`reponse` TEXT NOT NULL,' .
    '`date_upd` DATETIME' .
    ')';

foreach ($sql as $query) {
    if (Db::getInstance()->execute($query) == false) {
        return false;
    }
}
