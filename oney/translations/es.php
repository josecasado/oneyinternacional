<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{oney}prestashop>oney_706f4ef9796fddea8861071bbea540d5'] = '3x 4x Oney';
$_MODULE['<{oney}prestashop>oney_8acbbb25b0177de7627a36b3e741a702'] = 'La primera solución de pago en 3 o 4 veces con tarjeta bancaria.';
$_MODULE['<{oney}prestashop>oney_43524d160f1d7ab5b0597f6f104757bf'] = '¿Estás seguro de que deseas desinstalar Oney 3x 4x?';
$_MODULE['<{oney}prestashop>oney_92d4773a9f32fc3333cb8238471cf763'] = 'Debe activar la extensión cURL en su servidor para instalar este módulo.';
$_MODULE['<{oney}prestashop>oney_db8284e533c1e5704597d157445dcbca'] = 'Nuevo socio';
$_MODULE['<{oney}prestashop>oney_a658d9ec19c688eb3e2024b8db3175af'] = 'Se produjo un error al enviar el correo electrónico.';
$_MODULE['<{oney}prestashop>oney_0d888180845ad2c0c560e10b791ffff9'] = 'Recogida de mercancías en la tienda.';
$_MODULE['<{oney}prestashop>oney_b8b4291965c435b57f9a340de6721416'] = 'Colección en un punto de terceros (como ups, alveol, etc.)';
$_MODULE['<{oney}prestashop>oney_b6e8dca613c4c55be121e600fc03e2d9'] = 'Recogida en un aeropuerto, estación de tren o agencia de viajes.';
$_MODULE['<{oney}prestashop>oney_0222407a0a4cecef0aa3b4771833ad22'] = 'Operador (La Poste, Colissimo, UPS, DHL ... o cualquier otro operador privado)';
$_MODULE['<{oney}prestashop>oney_fc61296ce6e3fdff96fe281928dc5071'] = 'Emisión de tickets electrónicos, descargas, etc.';
$_MODULE['<{oney}prestashop>oney_b144fa061545497bebee8c414efc99a9'] = 'Exprimir';
$_MODULE['<{oney}prestashop>oney_eb6d8ae6f20283755b339c0dc273988b'] = 'Estándar';
$_MODULE['<{oney}prestashop>oney_502996d9790340c5fd7b86a5b93b1c9f'] = 'Prioridad';
$_MODULE['<{oney}prestashop>oney_0d756793f55d2173b7c91165d1c1f96d'] = 'Menos de o igual a 1 hora';
$_MODULE['<{oney}prestashop>oney_36f81ba0085b380377081cddf6703666'] = 'Mas de una hora';
$_MODULE['<{oney}prestashop>oney_43f6615bbb2c40a5306ff804094420b1'] = 'Inmediato';
$_MODULE['<{oney}prestashop>oney_3203f8ab03935d687eb2dc0ecf489f68'] = '24/24 7/7';
$_MODULE['<{oney}prestashop>oney_896890bfb700eac98300d639ca970f2b'] = 'Comerciante';
$_MODULE['<{oney}prestashop>oney_7b6f3c95aa286cec44fe908d826d42c1'] = 'Punto de retransmisión de terceros';
$_MODULE['<{oney}prestashop>oney_d1e1aba6effbba2e025c2c38c6bc1fe8'] = 'Aeropuerto, estación de tren, agencia de viajes';
$_MODULE['<{oney}prestashop>oney_1fbc7e5f1b92c7ec072397b59a0bb5da'] = 'Dirección de facturación';
$_MODULE['<{oney}prestashop>oney_af0f5bdc5be121b9307687aeeae38c17'] = 'Dirección de entrega';
$_MODULE['<{oney}prestashop>oney_99be1f517480848977dbd180c3029220'] = 'Vía electrónica (boleto, descarga)';
$_MODULE['<{oney}prestashop>oney_87a3d06716261ed864c6329e5fad7262'] = 'Comida y bebida';
$_MODULE['<{oney}prestashop>oney_31f764a29f96ecde3bb62b9d9bffb1d1'] = 'Auto y moto';
$_MODULE['<{oney}prestashop>oney_4002ab96264ef2ab40134e02ec369a6b'] = 'Cultura y entretenimiento.';
$_MODULE['<{oney}prestashop>oney_ca5f6d15b151c54a4cbf6f231c540f39'] = 'Casa y jardin';
$_MODULE['<{oney}prestashop>oney_2042a77b2239f45cb13da5db44e079e8'] = 'Electrodomésticos';
$_MODULE['<{oney}prestashop>oney_0eaa86ff7a8132499ac20fe0812c9123'] = 'Múltiples subastas y compras';
$_MODULE['<{oney}prestashop>oney_e1bc3b4e930537de4707bb928c712a0c'] = 'Flores y regalos';
$_MODULE['<{oney}prestashop>oney_f717e5d9aa8b45e3da81fe00d7d8e448'] = 'Computadoras y software';
$_MODULE['<{oney}prestashop>oney_2c0a2fa68bd358738766a1e7cb3aa917'] = 'Salud y belleza';
$_MODULE['<{oney}prestashop>oney_582637e2e2b2322884a867fa9ecd54ed'] = 'Servicios personales';
$_MODULE['<{oney}prestashop>oney_02bff7ce3f37d5238c217f00d3e19bc4'] = 'Servicios profesionales';
$_MODULE['<{oney}prestashop>oney_96faa3e6c45bb5a07bcc0bcd3be37654'] = 'Deporte';
$_MODULE['<{oney}prestashop>oney_2e22a11f2216b183de96c2f8d5c2b0f3'] = 'Ropa y accesorios';
$_MODULE['<{oney}prestashop>oney_65be4d5d1eed4e214aa6073d511f46a2'] = 'Viajes y turismo';
$_MODULE['<{oney}prestashop>oney_e4148efa15737caef497bbe700443b5e'] = 'Equipo de alta fidelidad, foto y video';
$_MODULE['<{oney}prestashop>oney_2c3c20fee5fd681e7ca35bf41b775479'] = 'Telefono y comunicacion';
$_MODULE['<{oney}prestashop>oney_f54653c089e69821b73ca309041d8fa6'] = 'Exitoso: la conexión es funcional.';
$_MODULE['<{oney}prestashop>oney_b5fb456a339a303646bc4eae4fb53cf7'] = 'Falló - La conexión falló';
$_MODULE['<{oney}prestashop>oney_6ef443f85e16ff3ee6c9b8092da68c8d'] = 'Éxito: lista de ofertas importadas';
$_MODULE['<{oney}prestashop>oney_dd0cddc7ff8c0702c535799f2d57f87f'] = 'Falló - ';
$_MODULE['<{oney}prestashop>oney_13b020e99e2ce882d9354e42fa1385f9'] = 'Sin identificación';
$_MODULE['<{oney}prestashop>oney_cdd3b58223be6b2847cada60a456a6d8'] = 'Si desea pagar con Oney, debe ingresar al menos un número de teléfono';
$_MODULE['<{oney}prestashop>pepswebservice_5491133fc2026e187360e4499fa752cc'] = 'Por favor contacte al servicio técnico';
$_MODULE['<{oney}prestashop>simulation-product_f9943419a1c78ab34d297e9320fbfe92'] = 'Oney';
$_MODULE['<{oney}prestashop>simulation-product_020abddf5a694cca1e4efa5f60a53e65'] = 'Pago aplazado[1][/1] [2]con tu tarjeta bancaria[/2]';
$_MODULE['<{oney}prestashop>simulation-product_97feee60eca788083075fb46e5e60714'] = 'Pago aplazado [1]con tu tarjeta bancaria[/1]';
$_MODULE['<{oney}prestashop>simulation-product_13f7434fb6d2449be1adf1165a192fc2'] = 'Cantidad a financiar';
$_MODULE['<{oney}prestashop>simulation-product_d9aa407c08439172a4f780f91152572c'] = 'Ver más';
$_MODULE['<{oney}prestashop>banner-col_b4fe334d1b7cdcbc01db8426803ebaff'] = 'Paga tu compra online en';
$_MODULE['<{oney}prestashop>banner-col_9dd4e461268c8034f5c8564e155c67a6'] = 'x';
$_MODULE['<{oney}prestashop>banner-col_455c523398a0f7b89bdac7de3550538b'] = 'sin gastos';
$_MODULE['<{oney}prestashop>banner-col_ffb9f405d2f78bf6bd95f2ee073dfff4'] = '¡3x 4x Oney es la solución de pago sencilla, rápida y segura para tus compras online!';
$_MODULE['<{oney}prestashop>banner-col_f9943419a1c78ab34d297e9320fbfe92'] = 'Oney';
$_MODULE['<{oney}prestashop>banner-home_6bf40725c073e7d98169b5177bd5c16e'] = 'Simple [1]y rápido[/1]';
$_MODULE['<{oney}prestashop>banner-home_b4fe334d1b7cdcbc01db8426803ebaff'] = 'Paga tu compra online en';
$_MODULE['<{oney}prestashop>banner-home_9dd4e461268c8034f5c8564e155c67a6'] = 'x';
$_MODULE['<{oney}prestashop>banner-home_ffb9f405d2f78bf6bd95f2ee073dfff4'] = '¡3x 4x Oney es la solución de pago sencilla, rápida y segura para tus compras online!';
$_MODULE['<{oney}prestashop>banner-home_455c523398a0f7b89bdac7de3550538b'] = 'sin gastos';
$_MODULE['<{oney}prestashop>banner-home_f9943419a1c78ab34d297e9320fbfe92'] = 'Oney';
$_MODULE['<{oney}prestashop>pedagogique-oney_f9943419a1c78ab34d297e9320fbfe92'] = 'Oney';
$_MODULE['<{oney}prestashop>pedagogique-oney_9c11d9cceee221af89f9e0e291e00a08'] = 'Pago aplazado [1]con tu tarjeta bancaria[/1]';
$_MODULE['<{oney}prestashop>pedagogique-oney_97feee60eca788083075fb46e5e60714'] = 'Pago aplazado [1]con tu tarjeta bancaria[/1]';
$_MODULE['<{oney}prestashop>pedagogique-oney_c4ca4238a0b923820dcc509a6f75849b'] = '1';
$_MODULE['<{oney}prestashop>pedagogique-oney_c81e728d9d4c2f636f067f89cc14862c'] = '2';
$_MODULE['<{oney}prestashop>pedagogique-oney_eccbc87e4b5ce2fe28308fd9f2a7baf3'] = '3';
$_MODULE['<{oney}prestashop>pedagogique-oney_961f2247a2070bedff9f9cd8d64e2650'] = 'Elige';
$_MODULE['<{oney}prestashop>pedagogique-oney_9dd4e461268c8034f5c8564e155c67a6'] = 'x';
$_MODULE['<{oney}prestashop>pedagogique-oney_be5d5d37542d75f93a87094459f76678'] = 'o';
$_MODULE['<{oney}prestashop>pedagogique-oney_76170e50b8bc93240e1c8da1bf7e300b'] = 'sin gastos';
$_MODULE['<{oney}prestashop>pedagogique-oney_86d93e659de61eb1c50db42028288461'] = 'Selecciona el número de cuotas';
$_MODULE['<{oney}prestashop>pedagogique-oney_583eb722b77d6c7791d1811f6a42450c'] = 'y';
$_MODULE['<{oney}prestashop>pedagogique-oney_651dd10790c5ce8e1a77c1379f7fc92a'] = 'Confirme su compra y seleccione 3x4xOney para el pago.';
$_MODULE['<{oney}prestashop>pedagogique-oney_6708b322a7860e438b8b48c6f0110ca9'] = 'Simple y seguro';
$_MODULE['<{oney}prestashop>pedagogique-oney_5b4e10947b9987fb90fb76155ab5c74e'] = 'Completa el [1]formulario[/1]';
$_MODULE['<{oney}prestashop>pedagogique-oney_b9d419c33f1f0638ceea323e34067135'] = 'Rápido y sencillo, solo con tu DNI';
$_MODULE['<{oney}prestashop>pedagogique-oney_a333d6c6aff9a472405833cc3cc5d584'] = '¡Listo!';
$_MODULE['<{oney}prestashop>pedagogique-oney_8cb93c57519377640b821bfd6923c227'] = 'Recibirás una respuesta inmediata';
$_MODULE['<{oney}prestashop>simulation-cart_2d8649324e0f7715798206eb177b4a2f'] = 'Link_no_credit_intermediary';
$_MODULE['<{oney}prestashop>simulation-cart_b0818786fe9d776a3dbad5cc33bb1a05'] = '3x 4x oney';
$_MODULE['<{oney}prestashop>simulation-cart_7192cae31bce41e5fba6e4b05b030a94'] = 'Pago aplazado con [1]tu tarjeta bancaria[/1]';
$_MODULE['<{oney}prestashop>simulation-cart_f9943419a1c78ab34d297e9320fbfe92'] = 'Oney';
$_MODULE['<{oney}prestashop>simulation-cart_020abddf5a694cca1e4efa5f60a53e65'] = 'Pago aplazado[1][/1] [2]con tu tarjeta bancaria[/2]';
$_MODULE['<{oney}prestashop>simulation-cart_97feee60eca788083075fb46e5e60714'] = 'Pago aplazado [1]con tu tarjeta bancaria[/1]';
$_MODULE['<{oney}prestashop>simulation-cart_13f7434fb6d2449be1adf1165a192fc2'] = 'Cantidad a financiar';
$_MODULE['<{oney}prestashop>simulation-cart_d9aa407c08439172a4f780f91152572c'] = 'Mas info';
$_MODULE['<{oney}prestashop>simulation-cart-ajax_9dd4e461268c8034f5c8564e155c67a6'] = 'x';
$_MODULE['<{oney}prestashop>simulation-cart-ajax_76170e50b8bc93240e1c8da1bf7e300b'] = 'sin gastos';
$_MODULE['<{oney}prestashop>after-price_2d8649324e0f7715798206eb177b4a2f'] = 'Link_no_credit_intermediary';
$_MODULE['<{oney}prestashop>after-price_b0818786fe9d776a3dbad5cc33bb1a05'] = '3x 4x oney';
$_MODULE['<{oney}prestashop>after-price_587ce2e176526a0f9ef877ad70464fc7'] = 'o paga en';
$_MODULE['<{oney}prestashop>after-price_9dd4e461268c8034f5c8564e155c67a6'] = 'x';
$_MODULE['<{oney}prestashop>after-price_e81c4e4f2b7b93b481e13a8553c2ae1b'] = 'o';
$_MODULE['<{oney}prestashop>after-price_fef8880b4d54a92116e4ff49526c095c'] = 'con tarjeta de credito';
$_MODULE['<{oney}prestashop>after-price_15e27d5f24edb9b9e8cf697a788869bf'] = 'sin gastos';
$_MODULE['<{oney}prestashop>after-price_f2b798f672d4b42c0359ced11d4f10cd'] = 'cuotas';
$_MODULE['<{oney}prestashop>after-price_b4fe334d1b7cdcbc01db8426803ebaff'] = 'Pagar en';
$_MODULE['<{oney}prestashop>after-price_76170e50b8bc93240e1c8da1bf7e300b'] = 'sin gastos';
$_MODULE['<{oney}prestashop>payment_444d4d1d26f8595be7d86bbf07787f8f'] = 'Paga con mi módulo de pago';
$_MODULE['<{oney}prestashop>payment_76170e50b8bc93240e1c8da1bf7e300b'] = 'sin gastos';
$_MODULE['<{oney}prestashop>payment_3de0ccb95481c32d121e03dbeb00a8af'] = 'cuotas de';
$_MODULE['<{oney}prestashop>payment_af754e40798a216a425e6280164eaaba'] = '- 1a pago:';
$_MODULE['<{oney}prestashop>payment_26b17225b626fb9238849fd60eabdf60'] = '+';
$_MODULE['<{oney}prestashop>payment_336d5ebc5436534e61d16e63ddfca327'] = '-';
$_MODULE['<{oney}prestashop>payment_b8a397ccf1506aca07ed8fc9718db526'] = 'a pago:';
$_MODULE['<{oney}prestashop>payment_4fcb844723818b7e526b6808d1cf5de4'] = 'Primer pago de';
$_MODULE['<{oney}prestashop>payment_addec426932e71323700afa1911f8f1c'] = 'mas';
$_MODULE['<{oney}prestashop>payment_14f76d80477fa132254fd9bd3f605943'] = 'pagos de';
$_MODULE['<{oney}prestashop>payment_a8486baf43daf545d22e361bac945e81'] = 'Total :';
$_MODULE['<{oney}prestashop>payment_54d3b260d7e0e3377ff04b75bf564982'] = 'Más';
$_MODULE['<{oney}prestashop>payment_dc4773a26b397e215e26cd720080deab'] = 'de gastos de gestión';
$_MODULE['<{oney}prestashop>payment_7847383427bef23520c777f7667066f2'] = 'TIN';
$_MODULE['<{oney}prestashop>payment_56fca6aa5cf475cf2b81c8debc5824da'] = 'TAE:';
$_MODULE['<{oney}prestashop>simulation-product-ajax_e81c4e4f2b7b93b481e13a8553c2ae1b'] = 'o';
$_MODULE['<{oney}prestashop>simulation-product-ajax_9dd4e461268c8034f5c8564e155c67a6'] = 'x';
$_MODULE['<{oney}prestashop>shopping-simulation_1c481aa99d081c32182011a758f73d33'] = '%s';
$_MODULE['<{oney}prestashop>shopping-simulation_3de0ccb95481c32d121e03dbeb00a8af'] = 'cuotas de';
$_MODULE['<{oney}prestashop>shopping-simulation_76170e50b8bc93240e1c8da1bf7e300b'] = 'sin gastos';
$_MODULE['<{oney}prestashop>simulation_76170e50b8bc93240e1c8da1bf7e300b'] = 'sin gastos';
$_MODULE['<{oney}prestashop>simulation_f2ecd8ca225e754e0afeb8a3846bd398'] = 'Total adeudado :';
$_MODULE['<{oney}prestashop>simulation_945ff3ce9cb1771d663e2d3f9d13fdba'] = 'TAE :';
$_MODULE['<{oney}prestashop>simulation_0bcef9c45bd8a48eda1b26eb0c61c869'] = '%';
$_MODULE['<{oney}prestashop>simulation_3023db88ac4767bf88f98e6ad2bb7c31'] = 'Contribución';
$_MODULE['<{oney}prestashop>simulation_26b17225b626fb9238849fd60eabdf60'] = '+';
$_MODULE['<{oney}prestashop>simulation_9dd4e461268c8034f5c8564e155c67a6'] = 'x';
$_MODULE['<{oney}prestashop>simulation_776ab0cab944a105a75ded3068095c03'] = '0,0%';
$_MODULE['<{oney}prestashop>simulation_1113fe1448f5ab6a9d190d990abcf376'] = 'TAN :';
$_MODULE['<{oney}prestashop>simulation_a450ba0a6c1dc91f44b7eb3e971d1739'] = '0,00%';
$_MODULE['<{oney}prestashop>simulation_2d6bd4b47ee66bd6d1b0a1b1655824ac'] = 'MTIC:';
$_MODULE['<{oney}prestashop>simulation_30b6a73fcfc4ca8f105ab7552a9624a2'] = 'Interés:';
$_MODULE['<{oney}prestashop>simulation_023e6a7a9e9ee4b0822c37f9ca154484'] = 'Primer pago';
$_MODULE['<{oney}prestashop>simulation_41f00c8dfec079d0555fdadeb2123412'] = 'Más %1d pagos';
$_MODULE['<{oney}prestashop>payment_intro_76170e50b8bc93240e1c8da1bf7e300b'] = 'sin gastos';
$_MODULE['<{oney}prestashop>payment_intro_3de0ccb95481c32d121e03dbeb00a8af'] = 'cuotas de';
$_MODULE['<{oney}prestashop>payment_intro_4fcb844723818b7e526b6808d1cf5de4'] = 'Primer pago de';
$_MODULE['<{oney}prestashop>payment_intro_addec426932e71323700afa1911f8f1c'] = 'mas';
$_MODULE['<{oney}prestashop>payment_intro_14f76d80477fa132254fd9bd3f605943'] = 'pagos de';
$_MODULE['<{oney}prestashop>payment_intro_a8486baf43daf545d22e361bac945e81'] = 'Total :';
$_MODULE['<{oney}prestashop>payment_intro_54d3b260d7e0e3377ff04b75bf564982'] = 'Más';
$_MODULE['<{oney}prestashop>payment_intro_dc4773a26b397e215e26cd720080deab'] = 'de gastos de gestión';
$_MODULE['<{oney}prestashop>payment_intro_7847383427bef23520c777f7667066f2'] = 'TIN';
$_MODULE['<{oney}prestashop>payment_intro_56fca6aa5cf475cf2b81c8debc5824da'] = 'TAE:';
$_MODULE['<{oney}prestashop>payment_intro_af754e40798a216a425e6280164eaaba'] = '- 1a pago:';
$_MODULE['<{oney}prestashop>payment_intro_26b17225b626fb9238849fd60eabdf60'] = '+';
$_MODULE['<{oney}prestashop>payment_intro_336d5ebc5436534e61d16e63ddfca327'] = '-';
$_MODULE['<{oney}prestashop>payment_intro_b8a397ccf1506aca07ed8fc9718db526'] = 'a pago :';
$_MODULE['<{oney}prestashop>confirmation_fb077ecba55e5552916bde26d8b9e794'] = 'Confirmación de pedido';
$_MODULE['<{oney}prestashop>confirmation_d835f2af3f37d958c3fa39ab2bf3dc78'] = 'Su pedido ha sido registrado con éxito.';
$_MODULE['<{oney}prestashop>confirmation_b2f40690858b404ed10e62bdf422c704'] = 'Cantidad';
$_MODULE['<{oney}prestashop>confirmation_63d5049791d9d79d86e9a108b0a999ca'] = 'Referencia';
$_MODULE['<{oney}prestashop>confirmation_d02bbc3cb147c272b0445ac5ca7d1a36'] = 'Estado del pedido';
$_MODULE['<{oney}prestashop>confirmation_19c419a8a4f1cd621853376a930a2e24'] = 'Se ha enviado un correo electrónico con esta información.';
$_MODULE['<{oney}prestashop>confirmation_ca7e41a658753c87973936d7ce2429a8'] = 'Si tiene alguna pregunta, comentario o inquietud, no dude en ponerse en contacto con nuestro';
$_MODULE['<{oney}prestashop>confirmation_cd430c2eb4b87fb3b49320bd21af074e'] = 'equipo de expertos en servicio al cliente.';
$_MODULE['<{oney}prestashop>confirmation_1db17dac39310aa8e60ccb169c074f01'] = 'Su pedido no ha sido aceptado.';
$_MODULE['<{oney}prestashop>confirmation_caa4088f1d295cf8ce8e358eb975ab32'] = 'Por favor renueve su solicitud';
$_MODULE['<{oney}prestashop>simulation-product_375fefff8d81edea69264fb30f4eebe3'] = 'Pago aplazado[1][/1] [2]con tu tarjeta bancaria[/2]';
$_MODULE['<{oney}prestashop>simulation-product_4497ae0a16346af1cec52e44c347e2be'] = 'Pago aplazado[1]con tu tarjeta bancaria[/1]';
$_MODULE['<{oney}prestashop>simulation-product_a6ff5c4ac632638902e5147f82ff1ab9'] = 'Cantidad a financiar';
$_MODULE['<{oney}prestashop>simulation-product_a642e95109a4c0aa8dfff75d7bed3110'] = 'Cantidad de pagos mensuales';
$_MODULE['<{oney}prestashop>simulation-product_8a535b5860057eb832a24e92e8367b13'] = 'Oferta de financiación con aportación obligatoria, reservada para particulares y válida para cualquier compra desde %1d€ a %2d€. Sujeto a aceptación por parte de Oney Bank. Tiene 14 días para renunciar a su crédito. Oney Bank - SA con un capital de € 51,286,585 - 34 Avenue de Flandre 59 170 Croix - [1]546 380 197 RCS Lille Métropole[/1] - n ° Orias 07 023 261 www.orias.fr';
$_MODULE['<{oney}prestashop>banner-col_3a1411345f13193fd725724d6a967c47'] = 'Paga tu compra online en';
$_MODULE['<{oney}prestashop>banner-col_36138a616c7386982d7042d750dcf052'] = '¡3x 4x Oney es la solución de pago sencilla, rápida y segura para tus compras online!';
$_MODULE['<{oney}prestashop>banner-home_108a6c764032cccea7c55e83a6625cef'] = 'Simple [1]y rápido[/1]';
$_MODULE['<{oney}prestashop>banner-home_3a1411345f13193fd725724d6a967c47'] = 'Paga tu compra online en';
$_MODULE['<{oney}prestashop>banner-home_36138a616c7386982d7042d750dcf052'] = '¡3x 4x Oney es la solución de pago sencilla, rápida y segura para tus compras online!';
$_MODULE['<{oney}prestashop>pedagogique-oney_61726cea9f5c095ccc1934836bc7cdec'] = 'Pago aplazado [1]con tu tarjeta bancaria[/1]';
$_MODULE['<{oney}prestashop>pedagogique-oney_4497ae0a16346af1cec52e44c347e2be'] = 'Pago aplazado [1]con tu tarjeta bancaria[/1]';
$_MODULE['<{oney}prestashop>pedagogique-oney_03342417bfba7a92b62437a7fb283273'] = 'Elige';
$_MODULE['<{oney}prestashop>pedagogique-oney_886bf5b014b7ec172b95291a14c92694'] = 'Cuando selecciona su método de pago.';
$_MODULE['<{oney}prestashop>pedagogique-oney_01c66f9dd5bec239f1be1927f2546269'] = 'Completa el [1]formulario[/1]';
$_MODULE['<{oney}prestashop>pedagogique-oney_5221919d15abd722b5194b2d63ff9c50'] = 'Rápido y sencillo, solo con tu DNI';
$_MODULE['<{oney}prestashop>pedagogique-oney_c207707123555ba2d55892efdcca7fb1'] = '¡Listo!';
$_MODULE['<{oney}prestashop>pedagogique-oney_810befa9b33a82287a57235e36d99c29'] = 'Recibirás una respuesta inmediata';
$_MODULE['<{oney}prestashop>pedagogique-oney_a7e2aa46dbb5114dead6213326391e6a'] = 'Válido para cualquier compra de %1d€ a %2d€ en %3dx.';
$_MODULE['<{oney}prestashop>pedagogique-oney_8a535b5860057eb832a24e92e8367b13'] = 'Oferta de financiación con aportación obligatoria, reservada para particulares y válida para cualquier compra desde %1d€ a %2d€. Sujeto a aceptación por parte de Oney Bank. Tiene 14 días para renunciar a su crédito. Oney Bank - SA con un capital de € 51,286,585 - 34 Avenue de Flandre 59 170 Croix - [1]546 380 197 RCS Lille Métropole[/1] - n ° Orias 07 023 261 www.orias.fr';
$_MODULE['<{oney}prestashop>simulation-cart-ajax_455c523398a0f7b89bdac7de3550538b'] = 'sin gastos';
$_MODULE['<{oney}prestashop>after-price_2cc53209deae5e80c27ffdc3e55e51e6'] = 'o paga en';
$_MODULE['<{oney}prestashop>after-price_583eb722b77d6c7791d1811f6a42450c'] = 'o';
$_MODULE['<{oney}prestashop>after-price_455c523398a0f7b89bdac7de3550538b'] = 'sin gastos';
$_MODULE['<{oney}prestashop>payment_455c523398a0f7b89bdac7de3550538b'] = 'sin gastos';
$_MODULE['<{oney}prestashop>payment_d281e37ff2495c544cae2bd2e5a12da0'] = 'Contribución:';
$_MODULE['<{oney}prestashop>payment_b5641a636ccf6b4fed159d312be34c14'] = 'Costo de financiamiento:';
$_MODULE['<{oney}prestashop>payment_ea82fc8d8f320d771fe2d4072039e7bf'] = '-';
$_MODULE['<{oney}prestashop>payment_5c6229d2a94fcd80bd5dc31a0bfbe161'] = 'e mensual:';
$_MODULE['<{oney}prestashop>payment_ff1492b32fcd8e3f572384d08bdfd788'] = 'e mensual:';
$_MODULE['<{oney}prestashop>shopping-simulation_d281e37ff2495c544cae2bd2e5a12da0'] = 'Contribución:';
$_MODULE['<{oney}prestashop>shopping-simulation_b95a31596dfc67b3ac03099611f50652'] = '+ %s pagos mensuales:';
$_MODULE['<{oney}prestashop>shopping-simulation_b5641a636ccf6b4fed159d312be34c14'] = 'Costo de financiamiento:';
$_MODULE['<{oney}prestashop>simulation_d281e37ff2495c544cae2bd2e5a12da0'] = 'Contribución:';
$_MODULE['<{oney}prestashop>simulation_082324f32317350e91dbe9a7f3fd9770'] = 'pagos mensuales de:';
$_MODULE['<{oney}prestashop>simulation_b5641a636ccf6b4fed159d312be34c14'] = 'Costo de financiamiento:';
$_MODULE['<{oney}prestashop>simulation_e3e60b9e637251d648f93d246cbc3111'] = 'TAEG :';
$_MODULE['<{oney}prestashop>simulation_455c523398a0f7b89bdac7de3550538b'] = 'sin gastos';
$_MODULE['<{oney}prestashop>shopping-simulation_5b186428ed5d9cc40595941ef562fc0a'] = 'Contribución de';
$_MODULE['<{oney}prestashop>shopping-simulation_26b17225b626fb9238849fd60eabdf60'] = '+';
$_MODULE['<{oney}prestashop>shopping-simulation_9dd4e461268c8034f5c8564e155c67a6'] = 'x';
$_MODULE['<{oney}prestashop>payment_intro_455c523398a0f7b89bdac7de3550538b'] = 'sin gastos';
$_MODULE['<{oney}prestashop>payment_intro_d281e37ff2495c544cae2bd2e5a12da0'] = 'Contribución:';
$_MODULE['<{oney}prestashop>payment_intro_b5641a636ccf6b4fed159d312be34c14'] = 'Costo de financiamiento:';
$_MODULE['<{oney}prestashop>payment_intro_ea82fc8d8f320d771fe2d4072039e7bf'] = '-';
$_MODULE['<{oney}prestashop>payment_intro_5c6229d2a94fcd80bd5dc31a0bfbe161'] = 'e mensual:';
$_MODULE['<{oney}prestashop>payment_intro_ff1492b32fcd8e3f572384d08bdfd788'] = 'e mensual:';
$_MODULE['<{oney}prestashop>confirmation_c61a3fd25720de7f60925b74135dd235'] = 'Su orden ha sido registrada';
$_MODULE['<{oney}prestashop>confirmation_9eebd70648bffd4a97c400fa97198410'] = 'Su pedido no fue aceptado';
$_MODULE['<{oney}prestashop>error_a25c753ee3e4be15ec0daa5a40deb7b8'] = 'Se ha producido un error';
$_MODULE['<{oney}prestashop>confirmation_261101b62bc7c45a29cc42c3e841617b'] = 'Su orden ha sido registrada';
$_MODULE['<{oney}prestashop>confirmation_df8da1b6558716f435b0455b5f1be676'] = 'Su orden esta completa';
$_MODULE['<{oney}prestashop>confirmation_2192e9bfaf670ad431dcd2d4ad5ba9b7'] = 'Su pedido en %s no fue aceptado';
$_MODULE['<{oney}prestashop>admin_63e424aea9bd2798e7f9f18fda37f6e7'] = 'Solución rápida de usar';
$_MODULE['<{oney}prestashop>admin_7a60601c80272561b1dc5df25ffd627e'] = 'Garantía contra fraudes con tarjetas de crédito y clientes impagos';
$_MODULE['<{oney}prestashop>admin_624d31c3911b032c3ecc60fd9b6ccd0f'] = 'Financiación del 100% del precio de compra en 48 horas.';
$_MODULE['<{oney}prestashop>admin_2497ab88ac37597622a4cb2d2b08ac65'] = 'Adquisición de nuevos clientes.';
$_MODULE['<{oney}prestashop>admin_33491efa57a820f08f9384648b77d1b2'] = '3x 4x oney, la primera solución de pago en 3 o 4 veces con tarjeta de crédito';
$_MODULE['<{oney}prestashop>admin_305853d3fe4b88c068290760837ac2be'] = 'Gracias por su interés en el 3x 4x Oney';
$_MODULE['<{oney}prestashop>admin_72bc97a81c52f81217abbf3807a6842b'] = 'Su solicitud ha sido tomada en cuenta, pronto recibirá un correo electrónico a la dirección que tiene para finalizar el procedimiento de activación de su cuenta.';
$_MODULE['<{oney}prestashop>admin_8b2458b37ef6567c04d624d8f25c36a4'] = 'Este módulo debe poder actualizar los estados de los pedidos.';
$_MODULE['<{oney}prestashop>admin_bf67a04fa1ecf99a17813fc1d2291d62'] = 'Para hacer esto, simplemente active una tarea programada';
$_MODULE['<{oney}prestashop>admin_cca27d43f0ca850d309b14b527239cca'] = 'Para configurar esta tarea programada, debe acceder a su interfaz de alojamiento o ponerse en contacto con su anfitrión.';
$_MODULE['<{oney}prestashop>admin_b1f3a9eca6688c3da33d1c7685904620'] = 'Definir la tarea que realiza la actualización cada 2 horas.';
$_MODULE['<{oney}prestashop>admin_0ad13398f72098a34cd5b48cf71c33a0'] = 'La URL para llamar es';
$_MODULE['<{oney}prestashop>admin_f1206f9fadc5ce41694f69129aecac26'] = 'Configuración';
$_MODULE['<{oney}prestashop>admin_c10a87def8b36c1f9cb5057a2845fa5d'] = 'Selecciona mis ofertas comerciales';
$_MODULE['<{oney}prestashop>onboarding_723a89813a16f49b80e29fd750dd0b32'] = 'Agradecemos tu interés. Teniendo en cuenta los criterios indicados, lamentablemente no eres elegible para el 3x 4x Oney en este momento.';
$_MODULE['<{oney}prestashop>onboarding_bc3e215b4f591b6897aa473474ae23f5'] = 'No dude en repetir su solicitud si su situación cambiara';
$_MODULE['<{oney}prestashop>onboarding_f9943419a1c78ab34d297e9320fbfe92'] = 'Oney';
$_MODULE['<{oney}prestashop>onboarding_b4ade3b561567e0fceb5c082c8ec02dd'] = '¿Quieres saber más sobre nuestras soluciones de financiación y pago?';
$_MODULE['<{oney}prestashop>onboarding_84a05dd3d3225386aea6c2aaa5540874'] = 'La solución de pago múltiple requiere la firma de un contrato de asociación entre Oney y el comerciante. Para verificar su elegibilidad y ser contactado por Oney.';
$_MODULE['<{oney}prestashop>onboarding_8d0b74e966f6f4b006a996a712f0ffcb'] = 'Haga clic aquí';
$_MODULE['<{oney}prestashop>onboarding_d3f49ff7e6e57d0a1a3671214fc1681a'] = 'Configuración';
$_MODULE['<{oney}prestashop>configure_f1206f9fadc5ce41694f69129aecac26'] = 'Configuración';
$_MODULE['<{oney}prestashop>configure_93cba07454f06a4a960172bbd6e2a435'] = 'Sí';
$_MODULE['<{oney}prestashop>configure_bafd7322c6e97d25b6299b5d6fe8920b'] = 'No';
$_MODULE['<{oney}prestashop>configure_fae3058dd304ff0397f117ad623cfc5c'] = 'Modo de operación';
$_MODULE['<{oney}prestashop>configure_756d97bb256b8580d4d71ee0c547804e'] = 'Producción';
$_MODULE['<{oney}prestashop>configure_0cbc6611f5540bd0809a388dc95a615b'] = 'Test';
$_MODULE['<{oney}prestashop>configure_ea8afbc21dc379e326642638febdfb4a'] = 'GUID del comerciante';
$_MODULE['<{oney}prestashop>configure_b6e86527d44c841f1adca6c467ff29ab'] = 'El identificador de su sitio, disponible en la oficina administrativa de su tienda.';
$_MODULE['<{oney}prestashop>configure_fa7818673b6b65d99e17122b1ca307cc'] = 'PSP GUID';
$_MODULE['<{oney}prestashop>configure_38692ad4780cc093ec6e1cf6012f980c'] = 'Clave de pago';
$_MODULE['<{oney}prestashop>configure_328088d06fa98b294c135aa2bb227814'] = 'Clave de marketing';
$_MODULE['<{oney}prestashop>configure_38df8308a0f6a712f94de034d4b97d8d'] = 'Secreto de Oney';
$_MODULE['<{oney}prestashop>configure_2c0fe3cb1d8cecbc28fb3fe0a170065c'] = 'Intermediario de crédito';
$_MODULE['<{oney}prestashop>configure_68b79f132fa23bd0ec92e6ee136ab078'] = 'Si es un intermediario de crédito, marque la casilla';
$_MODULE['<{oney}prestashop>configure_de8137af019e0071065deee6f574ed8d'] = 'Confirmación automática';
$_MODULE['<{oney}prestashop>configure_644ecc1903b625b328d6a4530970b4c7'] = 'Al recibir una opinión favorable de Oney, la aceptación automática de la financiación y la realización del pedido en \"Pago aceptado\".';
$_MODULE['<{oney}prestashop>configure_7b6aff5116a0ca2e719b16c30ca662d6'] = 'Información necesaria para el análisis antifraude.';
$_MODULE['<{oney}prestashop>configure_3adbdb3ac060038aa0e6e6c138ef9873'] = 'Categoría';
$_MODULE['<{oney}prestashop>configure_c9cc8cce247e49bae79f15173ce97354'] = 'Guardar';
$_MODULE['<{oney}prestashop>opc_e26f16ccfe045395266af9e2d52b1a23'] = 'LISTA DE OFERTAS COMERCIALES';
$_MODULE['<{oney}prestashop>opc_b24ce0cd392a5b0b8dedc66c25213594'] = 'gratis';
$_MODULE['<{oney}prestashop>opc_032557a8588f5cd271b004804386eb86'] = 'Después de ingresar su ID de tienda, haga clic en \"Recuperar ofertas\".';
$_MODULE['<{oney}prestashop>opc_c2915b44c8ea4ad7984a2829b4d76409'] = 'Recuperando ofertas';
$_MODULE['<{oney}prestashop>opc_c9cc8cce247e49bae79f15173ce97354'] = 'Guardar';
