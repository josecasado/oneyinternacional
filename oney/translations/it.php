<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{oney}prestashop>oney_706f4ef9796fddea8861071bbea540d5'] = '3x 4x Oney';
$_MODULE['<{oney}prestashop>oney_8acbbb25b0177de7627a36b3e741a702'] = 'La soluzione di pagamento  in 3 o 4 rate con Carta di Credito';
$_MODULE['<{oney}prestashop>oney_43524d160f1d7ab5b0597f6f104757bf'] = 'Sei sicuro di voler disinstallare la soluzione Oney 3x 4x ?';
$_MODULE['<{oney}prestashop>oney_92d4773a9f32fc3333cb8238471cf763'] = 'Devi abilitare l\'estensione cURL sul tuo server per poter installare questo modulo';
$_MODULE['<{oney}prestashop>oney_2e6c2d96aaacc8ef06d0ea12237cfc2e'] = 'Tutti i campi sono necessari';
$_MODULE['<{oney}prestashop>oney_db8284e533c1e5704597d157445dcbca'] = 'Nuovo Partner';
$_MODULE['<{oney}prestashop>oney_a658d9ec19c688eb3e2024b8db3175af'] = 'Si è verificato un errore durante l\'invio dell\'email';
$_MODULE['<{oney}prestashop>oney_0d888180845ad2c0c560e10b791ffff9'] = 'Ritiro della merce in negozio';
$_MODULE['<{oney}prestashop>oney_b8b4291965c435b57f9a340de6721416'] = 'Ritiro della merce in un punto di ritiro di una terza parte (come ups o alveol, ecc…)';
$_MODULE['<{oney}prestashop>oney_b6e8dca613c4c55be121e600fc03e2d9'] = 'Ritiro della merce in un aeroporto, stazione ferroviaria o agenzia di viaggi';
$_MODULE['<{oney}prestashop>oney_0222407a0a4cecef0aa3b4771833ad22'] = 'Corriere (Poste Italiane, BRT, UPS, DHL...o altro)';
$_MODULE['<{oney}prestashop>oney_fc61296ce6e3fdff96fe281928dc5071'] = 'Emissione di biglietto elettronico, download ecc…';
$_MODULE['<{oney}prestashop>oney_b144fa061545497bebee8c414efc99a9'] = 'Express';
$_MODULE['<{oney}prestashop>oney_eb6d8ae6f20283755b339c0dc273988b'] = 'Standard';
$_MODULE['<{oney}prestashop>oney_502996d9790340c5fd7b86a5b93b1c9f'] = 'Priority';
$_MODULE['<{oney}prestashop>oney_0d756793f55d2173b7c91165d1c1f96d'] = 'Inferiore o uguale ad un ora';
$_MODULE['<{oney}prestashop>oney_36f81ba0085b380377081cddf6703666'] = 'Superiore ad un ora';
$_MODULE['<{oney}prestashop>oney_43f6615bbb2c40a5306ff804094420b1'] = 'Immediato';
$_MODULE['<{oney}prestashop>oney_3203f8ab03935d687eb2dc0ecf489f68'] = '24/24 7/7';
$_MODULE['<{oney}prestashop>oney_896890bfb700eac98300d639ca970f2b'] = 'Commerciante';
$_MODULE['<{oney}prestashop>oney_7b6f3c95aa286cec44fe908d826d42c1'] = 'Punto di ritiro di una terza parte';
$_MODULE['<{oney}prestashop>oney_d1e1aba6effbba2e025c2c38c6bc1fe8'] = 'Aeroporto, stazione ferroviaria, agenzia di viaggi';
$_MODULE['<{oney}prestashop>oney_1fbc7e5f1b92c7ec072397b59a0bb5da'] = 'Indirizzo di fatturazione';
$_MODULE['<{oney}prestashop>oney_af0f5bdc5be121b9307687aeeae38c17'] = 'Indirizzo di spedizione';
$_MODULE['<{oney}prestashop>oney_99be1f517480848977dbd180c3029220'] = 'Modalità elettronica (biglietto elettronico, download)';
$_MODULE['<{oney}prestashop>oney_87a3d06716261ed864c6329e5fad7262'] = 'Cibo e Alcoolici';
$_MODULE['<{oney}prestashop>oney_31f764a29f96ecde3bb62b9d9bffb1d1'] = 'Automobili e motociclette';
$_MODULE['<{oney}prestashop>oney_4002ab96264ef2ab40134e02ec369a6b'] = 'Cultura e intrattenimento';
$_MODULE['<{oney}prestashop>oney_ca5f6d15b151c54a4cbf6f231c540f39'] = 'Casa e Giardino';
$_MODULE['<{oney}prestashop>oney_2042a77b2239f45cb13da5db44e079e8'] = 'Apparecchiature per la casa';
$_MODULE['<{oney}prestashop>oney_0eaa86ff7a8132499ac20fe0812c9123'] = 'Offerte e multi acquisto';
$_MODULE['<{oney}prestashop>oney_e1bc3b4e930537de4707bb928c712a0c'] = 'Fiori e regali';
$_MODULE['<{oney}prestashop>oney_f717e5d9aa8b45e3da81fe00d7d8e448'] = 'Computers e software';
$_MODULE['<{oney}prestashop>oney_2c0a2fa68bd358738766a1e7cb3aa917'] = 'Salute e bellezza';
$_MODULE['<{oney}prestashop>oney_582637e2e2b2322884a867fa9ecd54ed'] = 'Servizi personali';
$_MODULE['<{oney}prestashop>oney_02bff7ce3f37d5238c217f00d3e19bc4'] = 'Servizi professionali';
$_MODULE['<{oney}prestashop>oney_96faa3e6c45bb5a07bcc0bcd3be37654'] = 'Sport';
$_MODULE['<{oney}prestashop>oney_2e22a11f2216b183de96c2f8d5c2b0f3'] = 'Vestiario e accessori';
$_MODULE['<{oney}prestashop>oney_65be4d5d1eed4e214aa6073d511f46a2'] = 'Viaggi e turismo';
$_MODULE['<{oney}prestashop>oney_e4148efa15737caef497bbe700443b5e'] = 'Hifi, photo e video';
$_MODULE['<{oney}prestashop>oney_2c3c20fee5fd681e7ca35bf41b775479'] = 'Telefonia e comunicazioni';
$_MODULE['<{oney}prestashop>oney_f54653c089e69821b73ca309041d8fa6'] = 'Successo - la connessione é avvenuta correttamente';
$_MODULE['<{oney}prestashop>oney_b5fb456a339a303646bc4eae4fb53cf7'] = 'Errore - la connessione é fallita';
$_MODULE['<{oney}prestashop>oney_6ef443f85e16ff3ee6c9b8092da68c8d'] = 'Successo - Lista degli UCIs importata';
$_MODULE['<{oney}prestashop>oney_dd0cddc7ff8c0702c535799f2d57f87f'] = 'Errore -';
$_MODULE['<{oney}prestashop>oney_13b020e99e2ce882d9354e42fa1385f9'] = 'Nessun ID inserito';
$_MODULE['<{oney}prestashop>oney_cdd3b58223be6b2847cada60a456a6d8'] = 'Per poter richiedere  Oney 3x 4x è necessario un numero di telefono cellulare ';
$_MODULE['<{oney}prestashop>pepswebservice_5491133fc2026e187360e4499fa752cc'] = 'Contattate il servizio tecnico';
$_MODULE['<{oney}prestashop>simulation-product_f9943419a1c78ab34d297e9320fbfe92'] = 'Oney';
$_MODULE['<{oney}prestashop>simulation-product_020abddf5a694cca1e4efa5f60a53e65'] = 'Pagamento [1]con carta di credito[/1] [2]nessuna commissione[/2]';
$_MODULE['<{oney}prestashop>simulation-product_97feee60eca788083075fb46e5e60714'] = 'Pagamento in 3 o 4 rate [1]con carta di credito[/1]';
$_MODULE['<{oney}prestashop>simulation-product_13f7434fb6d2449be1adf1165a192fc2'] = 'Importo da finanziare';
$_MODULE['<{oney}prestashop>simulation-product_d9aa407c08439172a4f780f91152572c'] = 'scopri di più';
$_MODULE['<{oney}prestashop>banner-col_b4fe334d1b7cdcbc01db8426803ebaff'] = 'Paghi in';
$_MODULE['<{oney}prestashop>banner-col_9dd4e461268c8034f5c8564e155c67a6'] = 'x';
$_MODULE['<{oney}prestashop>banner-col_455c523398a0f7b89bdac7de3550538b'] = 'nessuna commissione';
$_MODULE['<{oney}prestashop>banner-col_ffb9f405d2f78bf6bd95f2ee073dfff4'] = 'Con [1]carta di credito[/1]';
$_MODULE['<{oney}prestashop>banner-col_f9943419a1c78ab34d297e9320fbfe92'] = 'Oney';
$_MODULE['<{oney}prestashop>legalnoticeitaly_81dbd2e476e434ac5ebe1bd62fc38947'] = 'Per %1dx Oney l’importo finanziabile di %2d€ a %3d€.';
$_MODULE['<{oney}prestashop>banner-home_6bf40725c073e7d98169b5177bd5c16e'] = 'Semplice [1]& veloce[/1]';
$_MODULE['<{oney}prestashop>banner-home_b4fe334d1b7cdcbc01db8426803ebaff'] = 'Paghi in';
$_MODULE['<{oney}prestashop>banner-home_9dd4e461268c8034f5c8564e155c67a6'] = 'x';
$_MODULE['<{oney}prestashop>banner-home_ffb9f405d2f78bf6bd95f2ee073dfff4'] = 'Con carta [1]di credito[/1]';
$_MODULE['<{oney}prestashop>banner-home_455c523398a0f7b89bdac7de3550538b'] = 'nessuna commissione';
$_MODULE['<{oney}prestashop>banner-home_f9943419a1c78ab34d297e9320fbfe92'] = 'Oney';
$_MODULE['<{oney}prestashop>pedagogique-oney_f9943419a1c78ab34d297e9320fbfe92'] = 'Oney';
$_MODULE['<{oney}prestashop>pedagogique-oney_9c11d9cceee221af89f9e0e291e00a08'] = 'Pagamento in 3 o 4 rate [1]con carta di credito[/1]';
$_MODULE['<{oney}prestashop>pedagogique-oney_97feee60eca788083075fb46e5e60714'] = 'Pagamento in 3 o 4 rate [1]con carta di credito[/1]';
$_MODULE['<{oney}prestashop>pedagogique-oney_c4ca4238a0b923820dcc509a6f75849b'] = '1';
$_MODULE['<{oney}prestashop>pedagogique-oney_c81e728d9d4c2f636f067f89cc14862c'] = '2';
$_MODULE['<{oney}prestashop>pedagogique-oney_eccbc87e4b5ce2fe28308fd9f2a7baf3'] = '3';
$_MODULE['<{oney}prestashop>pedagogique-oney_961f2247a2070bedff9f9cd8d64e2650'] = 'Scegli';
$_MODULE['<{oney}prestashop>pedagogique-oney_9dd4e461268c8034f5c8564e155c67a6'] = 'x';
$_MODULE['<{oney}prestashop>pedagogique-oney_76170e50b8bc93240e1c8da1bf7e300b'] = 'nessuna commissione';
$_MODULE['<{oney}prestashop>pedagogique-oney_583eb722b77d6c7791d1811f6a42450c'] = 'o';
$_MODULE['<{oney}prestashop>pedagogique-oney_651dd10790c5ce8e1a77c1379f7fc92a'] = 'Conferma il tuo acquisto e seleziona 3x4xOney per il pagamento.';
$_MODULE['<{oney}prestashop>pedagogique-oney_6708b322a7860e438b8b48c6f0110ca9'] = 'Semplice e sicuro';
$_MODULE['<{oney}prestashop>pedagogique-oney_5b4e10947b9987fb90fb76155ab5c74e'] = 'Ti chiediamo [1]solo qualche informazione[/1]';
$_MODULE['<{oney}prestashop>pedagogique-oney_b9d419c33f1f0638ceea323e34067135'] = 'Completa la tua richiesta in modo semplice e con pochi click.';
$_MODULE['<{oney}prestashop>pedagogique-oney_a333d6c6aff9a472405833cc3cc5d584'] = 'Ti rispondiamo subito';
$_MODULE['<{oney}prestashop>pedagogique-oney_8cb93c57519377640b821bfd6923c227'] = 'Ti daremo una risposta senza attese.';
$_MODULE['<{oney}prestashop>simulation-cart_2d8649324e0f7715798206eb177b4a2f'] = 'Link_no_credit_intermediary';
$_MODULE['<{oney}prestashop>simulation-cart_b0818786fe9d776a3dbad5cc33bb1a05'] = '3x 4x oney';
$_MODULE['<{oney}prestashop>simulation-cart_f9943419a1c78ab34d297e9320fbfe92'] = 'Oney';
$_MODULE['<{oney}prestashop>simulation-cart_020abddf5a694cca1e4efa5f60a53e65'] = 'Pague com [1]sem taxas extras[/1] [2]cartão de débito ou crédito[/2]';
$_MODULE['<{oney}prestashop>simulation-cart_97feee60eca788083075fb46e5e60714'] = 'Pague com [1]cartão de débito ou crédito[/1]';
$_MODULE['<{oney}prestashop>simulation-cart_13f7434fb6d2449be1adf1165a192fc2'] = 'Montante a financiar';
$_MODULE['<{oney}prestashop>simulation-cart_d9aa407c08439172a4f780f91152572c'] = 'Me diga mais';
$_MODULE['<{oney}prestashop>simulation-cart-ajax_9dd4e461268c8034f5c8564e155c67a6'] = 'x';
$_MODULE['<{oney}prestashop>simulation-cart-ajax_76170e50b8bc93240e1c8da1bf7e300b'] = 'nessuna commissione';
$_MODULE['<{oney}prestashop>after-price_2d8649324e0f7715798206eb177b4a2f'] = 'Link_no_credit_intermediary';
$_MODULE['<{oney}prestashop>after-price_b0818786fe9d776a3dbad5cc33bb1a05'] = '3x 4x oney';
$_MODULE['<{oney}prestashop>after-price_587ce2e176526a0f9ef877ad70464fc7'] = 'o paga in';
$_MODULE['<{oney}prestashop>after-price_9dd4e461268c8034f5c8564e155c67a6'] = 'x';
$_MODULE['<{oney}prestashop>after-price_e81c4e4f2b7b93b481e13a8553c2ae1b'] = 'o';
$_MODULE['<{oney}prestashop>after-price_fef8880b4d54a92116e4ff49526c095c'] = 'con carta di credito ';
$_MODULE['<{oney}prestashop>after-price_15e27d5f24edb9b9e8cf697a788869bf'] = 'nessuna commissione';
$_MODULE['<{oney}prestashop>after-price_b4fe334d1b7cdcbc01db8426803ebaff'] = 'Paghi in';
$_MODULE['<{oney}prestashop>after-price_76170e50b8bc93240e1c8da1bf7e300b'] = 'nessuna commissione';
$_MODULE['<{oney}prestashop>payment_444d4d1d26f8595be7d86bbf07787f8f'] = 'Pago con il mio modulo di pagamento';
$_MODULE['<{oney}prestashop>payment_76170e50b8bc93240e1c8da1bf7e300b'] = 'nessuna commissione ';
$_MODULE['<{oney}prestashop>payment_3de0ccb95481c32d121e03dbeb00a8af'] = 'rate da';
$_MODULE['<{oney}prestashop>payment_3d95dc518d6ed987a6cc52e8e787fcfb'] = 'più una commissione di %1d € applicata sulla prima rata';
$_MODULE['<{oney}prestashop>payment_f2c46814085b143fcc7aa70d24996346'] = 'più una commissione di %1s applicata sulla prima rata';
$_MODULE['<{oney}prestashop>payment_3023db88ac4767bf88f98e6ad2bb7c31'] = '1° rata :';
$_MODULE['<{oney}prestashop>payment_26b17225b626fb9238849fd60eabdf60'] = '+';
$_MODULE['<{oney}prestashop>payment_9dd4e461268c8034f5c8564e155c67a6'] = 'x';
$_MODULE['<{oney}prestashop>payment_45d73cf640c5e554499b16dc81e68b8c'] = 'Commissione applicata : ';
$_MODULE['<{oney}prestashop>payment_af754e40798a216a425e6280164eaaba'] = '- 1° rata :';
$_MODULE['<{oney}prestashop>payment_336d5ebc5436534e61d16e63ddfca327'] = '-';
$_MODULE['<{oney}prestashop>payment_b8a397ccf1506aca07ed8fc9718db526'] = '° mese :';
$_MODULE['<{oney}prestashop>simulation-product-ajax_e81c4e4f2b7b93b481e13a8553c2ae1b'] = 'oppure';
$_MODULE['<{oney}prestashop>simulation-product-ajax_9dd4e461268c8034f5c8564e155c67a6'] = 'x';
$_MODULE['<{oney}prestashop>shopping-simulation_1c481aa99d081c32182011a758f73d33'] = '%s';
$_MODULE['<{oney}prestashop>shopping-simulation_3de0ccb95481c32d121e03dbeb00a8af'] = 'rate da';
$_MODULE['<{oney}prestashop>shopping-simulation_3d95dc518d6ed987a6cc52e8e787fcfb'] = 'più una commissione di %1d € applicata sulla prima rata';
$_MODULE['<{oney}prestashop>shopping-simulation_f2c46814085b143fcc7aa70d24996346'] = 'più una commissione di %1s applicata sulla prima rata';
$_MODULE['<{oney}prestashop>shopping-simulation_76170e50b8bc93240e1c8da1bf7e300b'] = 'nessuna commissione';
$_MODULE['<{oney}prestashop>payment_error_cdd3b58223be6b2847cada60a456a6d8'] = 'Per poter richiedere  Oney 3x 4x è necessario un numero di telefono cellulare ';
$_MODULE['<{oney}prestashop>simulation_3d95dc518d6ed987a6cc52e8e787fcfb'] = 'più una commissione di %1d € applicata sulla prima rata';
$_MODULE['<{oney}prestashop>simulation_f2c46814085b143fcc7aa70d24996346'] = 'più una commissione di %1s applicata sulla prima rata';
$_MODULE['<{oney}prestashop>simulation_76170e50b8bc93240e1c8da1bf7e300b'] = 'nessuna commissione';
$_MODULE['<{oney}prestashop>simulation_f2ecd8ca225e754e0afeb8a3846bd398'] = 'Totale :';
$_MODULE['<{oney}prestashop>simulation_945ff3ce9cb1771d663e2d3f9d13fdba'] = 'TAEG';
$_MODULE['<{oney}prestashop>simulation_0bcef9c45bd8a48eda1b26eb0c61c869'] = '%';
$_MODULE['<{oney}prestashop>simulation_3023db88ac4767bf88f98e6ad2bb7c31'] = '1° rata :';
$_MODULE['<{oney}prestashop>simulation_26b17225b626fb9238849fd60eabdf60'] = '+';
$_MODULE['<{oney}prestashop>simulation_9dd4e461268c8034f5c8564e155c67a6'] = 'x';
$_MODULE['<{oney}prestashop>simulation_776ab0cab944a105a75ded3068095c03'] = '0,0%';
$_MODULE['<{oney}prestashop>simulation_1113fe1448f5ab6a9d190d990abcf376'] = 'TAN :';
$_MODULE['<{oney}prestashop>simulation_a450ba0a6c1dc91f44b7eb3e971d1739'] = '0,00%';
$_MODULE['<{oney}prestashop>simulation_2d6bd4b47ee66bd6d1b0a1b1655824ac'] = 'MTIC :';
$_MODULE['<{oney}prestashop>simulation_30b6a73fcfc4ca8f105ab7552a9624a2'] = 'Interessi :';
$_MODULE['<{oney}prestashop>payment_intro_76170e50b8bc93240e1c8da1bf7e300b'] = 'nessuna commissione :';
$_MODULE['<{oney}prestashop>payment_intro_3de0ccb95481c32d121e03dbeb00a8af'] = 'rate da';
$_MODULE['<{oney}prestashop>payment_intro_3d95dc518d6ed987a6cc52e8e787fcfb'] = 'più una commissione di %1d € applicata sulla prima rata';
$_MODULE['<{oney}prestashop>payment_intro_f2c46814085b143fcc7aa70d24996346'] = 'più una commissione di %1s applicata sulla prima rata';
$_MODULE['<{oney}prestashop>payment_intro_af754e40798a216a425e6280164eaaba'] = '- 1° rata:';
$_MODULE['<{oney}prestashop>payment_intro_26b17225b626fb9238849fd60eabdf60'] = '+';
$_MODULE['<{oney}prestashop>payment_intro_336d5ebc5436534e61d16e63ddfca327'] = '-';
$_MODULE['<{oney}prestashop>payment_intro_b8a397ccf1506aca07ed8fc9718db526'] = '° rata:';
$_MODULE['<{oney}prestashop>confirmation_fb077ecba55e5552916bde26d8b9e794'] = 'Conferma dell\'ordine';
$_MODULE['<{oney}prestashop>confirmation_d835f2af3f37d958c3fa39ab2bf3dc78'] = 'La tua richiesta è stata registrata.';
$_MODULE['<{oney}prestashop>confirmation_b2f40690858b404ed10e62bdf422c704'] = 'Importo';
$_MODULE['<{oney}prestashop>confirmation_63d5049791d9d79d86e9a108b0a999ca'] = 'Referenza';
$_MODULE['<{oney}prestashop>confirmation_d02bbc3cb147c272b0445ac5ca7d1a36'] = 'Stato dell\'ordine';
$_MODULE['<{oney}prestashop>confirmation_19c419a8a4f1cd621853376a930a2e24'] = 'Una email con questa informazione è stata inviata';
$_MODULE['<{oney}prestashop>confirmation_ca7e41a658753c87973936d7ce2429a8'] = 'In caso di domande, commenti o dubbi ti preghiamo di contattare il nostro';
$_MODULE['<{oney}prestashop>confirmation_cd430c2eb4b87fb3b49320bd21af074e'] = 'team di supporto al cliente.';
$_MODULE['<{oney}prestashop>confirmation_1db17dac39310aa8e60ccb169c074f01'] = 'La tua richiesta non è stata accettata.';
$_MODULE['<{oney}prestashop>confirmation_caa4088f1d295cf8ce8e358eb975ab32'] = 'Ti preghiamo di ritentare l\'ordine';
$_MODULE['<{oney}prestashop>simulation-product_375fefff8d81edea69264fb30f4eebe3'] = 'Pagamento [1]con carta di credito[/1] [2]nessuna commissione[/2]';
$_MODULE['<{oney}prestashop>simulation-product_4497ae0a16346af1cec52e44c347e2be'] = 'Pagamento in 3 o 4 rate [1]con carta di credito[/1]';
$_MODULE['<{oney}prestashop>simulation-product_a6ff5c4ac632638902e5147f82ff1ab9'] = 'Importo da finanziare';
$_MODULE['<{oney}prestashop>simulation-product_a642e95109a4c0aa8dfff75d7bed3110'] = 'Numero di pagamenti mensili';
$_MODULE['<{oney}prestashop>simulation-product_8a535b5860057eb832a24e92e8367b13'] = ' ';
$_MODULE['<{oney}prestashop>banner-col_3a1411345f13193fd725724d6a967c47'] = 'Pagi in';
$_MODULE['<{oney}prestashop>banner-col_36138a616c7386982d7042d750dcf052'] = 'Con [1]carta di credito[/1]';
$_MODULE['<{oney}prestashop>banner-home_108a6c764032cccea7c55e83a6625cef'] = 'Semplice [1]& veloce[/1]';
$_MODULE['<{oney}prestashop>banner-home_3a1411345f13193fd725724d6a967c47'] = 'Paghi in';
$_MODULE['<{oney}prestashop>banner-home_36138a616c7386982d7042d750dcf052'] = 'Con carta [1]di credito[/1]';
$_MODULE['<{oney}prestashop>pedagogique-oney_61726cea9f5c095ccc1934836bc7cdec'] = 'Pagamento in 3 o 4 rate [1]con carta di credito[/1]';
$_MODULE['<{oney}prestashop>pedagogique-oney_4497ae0a16346af1cec52e44c347e2be'] = 'Pagamento in 3 o 4 rate [1]con carta di credito[/1]';
$_MODULE['<{oney}prestashop>pedagogique-oney_03342417bfba7a92b62437a7fb283273'] = 'Scegli';
$_MODULE['<{oney}prestashop>pedagogique-oney_886bf5b014b7ec172b95291a14c92694'] = 'Quando selezioni il metodo di pagamento.';
$_MODULE['<{oney}prestashop>pedagogique-oney_01c66f9dd5bec239f1be1927f2546269'] = 'Dicci [1]tutto[/1]';
$_MODULE['<{oney}prestashop>pedagogique-oney_5221919d15abd722b5194b2d63ff9c50'] = 'Facile e veloce, completa il modulo, senza fornire alcun documento.';
$_MODULE['<{oney}prestashop>pedagogique-oney_c207707123555ba2d55892efdcca7fb1'] = 'E questo è tutto!';
$_MODULE['<{oney}prestashop>pedagogique-oney_810befa9b33a82287a57235e36d99c29'] = 'Ti daremo una risposta senza attese.';
$_MODULE['<{oney}prestashop>pedagogique-oney_a7e2aa46dbb5114dead6213326391e6a'] = 'Válido para qualquer compra de %1d € a %2d € em %3dx.';
$_MODULE['<{oney}prestashop>pedagogique-oney_8a535b5860057eb832a24e92e8367b13'] = 'Offerta di finanziamento con contributo obbligatorio, riservata ai privati ​​e valida per qualsiasi acquisto da %1d€ a %2d€. Previa accettazione da parte di Oney Bank. Hai 14 giorni per cancellare il tuo credito. Oney Bank - SA con capitale di 51 286 585 € - 40 Avenue of Flanders 59 170 Cross - [1]546 380 197 RCS Lille Métropole[/1] - Orias n. 07 023 261 www.orias.fr';
$_MODULE['<{oney}prestashop>simulation-cart-ajax_455c523398a0f7b89bdac7de3550538b'] = 'nessuna commissione';
$_MODULE['<{oney}prestashop>after-price_2cc53209deae5e80c27ffdc3e55e51e6'] = 'o paga in';
$_MODULE['<{oney}prestashop>after-price_583eb722b77d6c7791d1811f6a42450c'] = 'o';
$_MODULE['<{oney}prestashop>after-price_455c523398a0f7b89bdac7de3550538b'] = ' nessuna commissione';
$_MODULE['<{oney}prestashop>payment_455c523398a0f7b89bdac7de3550538b'] = 'nessuna commissione';
$_MODULE['<{oney}prestashop>payment_d281e37ff2495c544cae2bd2e5a12da0'] = 'Prima rata:';
$_MODULE['<{oney}prestashop>payment_b5641a636ccf6b4fed159d312be34c14'] = 'Di cui costo di finanziamento:';
$_MODULE['<{oney}prestashop>payment_ea82fc8d8f320d771fe2d4072039e7bf'] = '-';
$_MODULE['<{oney}prestashop>payment_5c6229d2a94fcd80bd5dc31a0bfbe161'] = '° rata:';
$_MODULE['<{oney}prestashop>payment_ff1492b32fcd8e3f572384d08bdfd788'] = '° rata:';
$_MODULE['<{oney}prestashop>shopping-simulation_d281e37ff2495c544cae2bd2e5a12da0'] = '1° rata : ';
$_MODULE['<{oney}prestashop>shopping-simulation_b95a31596dfc67b3ac03099611f50652'] = '+%s pagamenti mensili:';
$_MODULE['<{oney}prestashop>shopping-simulation_b5641a636ccf6b4fed159d312be34c14'] = 'Comprensiva di commissione pari a:';
$_MODULE['<{oney}prestashop>simulation_d281e37ff2495c544cae2bd2e5a12da0'] = 'Prima rata:';
$_MODULE['<{oney}prestashop>simulation_082324f32317350e91dbe9a7f3fd9770'] = 'pagamenti mensili di:';
$_MODULE['<{oney}prestashop>simulation_b5641a636ccf6b4fed159d312be34c14'] = 'Di cui costo di finanziamento:';
$_MODULE['<{oney}prestashop>simulation_e3e60b9e637251d648f93d246cbc3111'] = 'TAEG :';
$_MODULE['<{oney}prestashop>simulation_455c523398a0f7b89bdac7de3550538b'] = 'nessuna commissione';
$_MODULE['<{oney}prestashop>shopping-simulation_5b186428ed5d9cc40595941ef562fc0a'] = 'Contributo di';
$_MODULE['<{oney}prestashop>shopping-simulation_26b17225b626fb9238849fd60eabdf60'] = '+';
$_MODULE['<{oney}prestashop>shopping-simulation_9dd4e461268c8034f5c8564e155c67a6'] = 'x';
$_MODULE['<{oney}prestashop>payment_intro_455c523398a0f7b89bdac7de3550538b'] = 'nessuna commissione ';
$_MODULE['<{oney}prestashop>payment_intro_d281e37ff2495c544cae2bd2e5a12da0'] = '1° rata :';
$_MODULE['<{oney}prestashop>payment_intro_b5641a636ccf6b4fed159d312be34c14'] = 'Comprensiva di commissione pari a:';
$_MODULE['<{oney}prestashop>payment_intro_ea82fc8d8f320d771fe2d4072039e7bf'] = '-';
$_MODULE['<{oney}prestashop>payment_intro_5c6229d2a94fcd80bd5dc31a0bfbe161'] = '° rata: ';
$_MODULE['<{oney}prestashop>payment_intro_ff1492b32fcd8e3f572384d08bdfd788'] = '° rata: ';
$_MODULE['<{oney}prestashop>confirmation_c61a3fd25720de7f60925b74135dd235'] = 'Il tuo ordine è stato registrato correttamente';
$_MODULE['<{oney}prestashop>confirmation_9eebd70648bffd4a97c400fa97198410'] = 'Il vostro ordine su non é stato accettato';
$_MODULE['<{oney}prestashop>error_a25c753ee3e4be15ec0daa5a40deb7b8'] = 'Si è verificato un errore';
$_MODULE['<{oney}prestashop>confirmation_261101b62bc7c45a29cc42c3e841617b'] = 'Il tuo ordine é stato registrato correttamente';
$_MODULE['<{oney}prestashop>confirmation_df8da1b6558716f435b0455b5f1be676'] = 'Il tuo ordine é completo';
$_MODULE['<{oney}prestashop>confirmation_2192e9bfaf670ad431dcd2d4ad5ba9b7'] = 'Il vostro ordine su %s non è stato accettato.';
$_MODULE['<{oney}prestashop>admin_63e424aea9bd2798e7f9f18fda37f6e7'] = 'Soluzione di pagamento facile e veloce';
$_MODULE['<{oney}prestashop>admin_7a60601c80272561b1dc5df25ffd627e'] = 'Protezione al 100%  contro frodi sulla carta di credito e insoluti ';
$_MODULE['<{oney}prestashop>admin_624d31c3911b032c3ecc60fd9b6ccd0f'] = 'Liquidazione del carrello entro 48 ore dalla conclusione dell\'ordine';
$_MODULE['<{oney}prestashop>admin_2497ab88ac37597622a4cb2d2b08ac65'] = 'Una soluzione per acquisire nuovi clienti';
$_MODULE['<{oney}prestashop>admin_33491efa57a820f08f9384648b77d1b2'] = ' 3x4x Oney soluzione di pagamento in 3 o 4 rate con carta di credito';
$_MODULE['<{oney}prestashop>admin_305853d3fe4b88c068290760837ac2be'] = 'Grazie per aver scelto 3x4x Oney ';
$_MODULE['<{oney}prestashop>admin_72bc97a81c52f81217abbf3807a6842b'] = 'La sua richiesta è stata presa in carico. riceverai una email all\'indirizzo che hai indicato per finalizzare l\'attivazione del tuo account';
$_MODULE['<{oney}prestashop>admin_8b2458b37ef6567c04d624d8f25c36a4'] = 'Questo modulo deve poter aggiornare lo stato dell’ordine:';
$_MODULE['<{oney}prestashop>admin_bf67a04fa1ecf99a17813fc1d2291d62'] = 'A questo scopo è necessario configurare un’attività programmata.';
$_MODULE['<{oney}prestashop>admin_cca27d43f0ca850d309b14b527239cca'] = 'Per configurare questa attività devi accedere alla tua interfaccia di hosting o contattare il tuo hosting provider.';
$_MODULE['<{oney}prestashop>admin_b1f3a9eca6688c3da33d1c7685904620'] = 'Configura l’attività per eseguire un aggiornamento ogni 2 ore.';
$_MODULE['<{oney}prestashop>admin_0ad13398f72098a34cd5b48cf71c33a0'] = 'L\'URL da chiamare è';
$_MODULE['<{oney}prestashop>admin_f1206f9fadc5ce41694f69129aecac26'] = 'Configura';
$_MODULE['<{oney}prestashop>admin_c10a87def8b36c1f9cb5057a2845fa5d'] = 'Selezionare la mia Business Transaction';
$_MODULE['<{oney}prestashop>onboarding_723a89813a16f49b80e29fd750dd0b32'] = 'Siamo spiacenti  di comunicarti  che la tua richiesta  di  3x 4x Oney non è stata accettata';
$_MODULE['<{oney}prestashop>onboarding_bc3e215b4f591b6897aa473474ae23f5'] = 'Ti invitiamo a ripetere il tuo acquisto utilizzando un\'altra modalità di pagamento';
$_MODULE['<{oney}prestashop>onboarding_f9943419a1c78ab34d297e9320fbfe92'] = 'Oney';
$_MODULE['<{oney}prestashop>onboarding_b4ade3b561567e0fceb5c082c8ec02dd'] = 'Desideri ricevere maggiori informazioni sulla nostra soluzione di pagamento 3x4x Oney?';
$_MODULE['<{oney}prestashop>onboarding_84a05dd3d3225386aea6c2aaa5540874'] = 'La soluzione multi-pagamento richiede la firma di un contratto di partnership tra Oney e il commerciante. Per verificare la tue idoneità e essere contattato da Oney';
$_MODULE['<{oney}prestashop>onboarding_8d0b74e966f6f4b006a996a712f0ffcb'] = 'Clicca qui';
$_MODULE['<{oney}prestashop>onboarding_d3f49ff7e6e57d0a1a3671214fc1681a'] = 'Configurare';
$_MODULE['<{oney}prestashop>onboarding_305853d3fe4b88c068290760837ac2be'] = 'Grazie per l\'interesse dimostrato per il 3X 4X Oney.';
$_MODULE['<{oney}prestashop>onboarding_c2cc7082a89c1ad6631a2f66af5f00c0'] = 'Collegamento';
$_MODULE['<{oney}prestashop>onboarding_94d704a1bb6c42b0775abde9e7394ba3'] = 'Modulo di contatto';
$_MODULE['<{oney}prestashop>onboarding_93cba07454f06a4a960172bbd6e2a435'] = 'Sì';
$_MODULE['<{oney}prestashop>onboarding_bafd7322c6e97d25b6299b5d6fe8920b'] = 'No';
$_MODULE['<{oney}prestashop>onboarding_eb15cdc27c891a8980cac0ebb9991958'] = '* Campi richiesti';
$_MODULE['<{oney}prestashop>onboarding_94966d90747b97d1f0f206c98a8b1ac3'] = 'Spedire';
$_MODULE['<{oney}prestashop>configure_f1206f9fadc5ce41694f69129aecac26'] = 'Configura';
$_MODULE['<{oney}prestashop>configure_d45db514d9389d60dc5c229eaf7edbdf'] = 'Tema';
$_MODULE['<{oney}prestashop>configure_f87a604e5366b28ba26e14e20c067de3'] = 'Verde';
$_MODULE['<{oney}prestashop>configure_2575a9ed0d99379086ce1883cc116ca9'] = 'Grigio';
$_MODULE['<{oney}prestashop>configure_fae3058dd304ff0397f117ad623cfc5c'] = 'Modalità di funzionamento';
$_MODULE['<{oney}prestashop>configure_756d97bb256b8580d4d71ee0c547804e'] = 'Produzione';
$_MODULE['<{oney}prestashop>configure_0cbc6611f5540bd0809a388dc95a615b'] = 'Test';
$_MODULE['<{oney}prestashop>configure_ea8afbc21dc379e326642638febdfb4a'] = 'Merchant GUID';
$_MODULE['<{oney}prestashop>configure_b6e86527d44c841f1adca6c467ff29ab'] = 'Identificativo del tuo sito come indicato nel tuo shop di back office';
$_MODULE['<{oney}prestashop>configure_fa7818673b6b65d99e17122b1ca307cc'] = 'PSP GUID';
$_MODULE['<{oney}prestashop>configure_38692ad4780cc093ec6e1cf6012f980c'] = 'Payment key';
$_MODULE['<{oney}prestashop>configure_328088d06fa98b294c135aa2bb227814'] = 'Marketing Key';
$_MODULE['<{oney}prestashop>configure_38df8308a0f6a712f94de034d4b97d8d'] = 'Oney Secret';
$_MODULE['<{oney}prestashop>configure_2c0fe3cb1d8cecbc28fb3fe0a170065c'] = 'Credito itermediario';
$_MODULE['<{oney}prestashop>configure_93cba07454f06a4a960172bbd6e2a435'] = 'Si';
$_MODULE['<{oney}prestashop>configure_bafd7322c6e97d25b6299b5d6fe8920b'] = 'No';
$_MODULE['<{oney}prestashop>configure_68b79f132fa23bd0ec92e6ee136ab078'] = 'Se sei un credito intermediario, spunta la casella';
$_MODULE['<{oney}prestashop>configure_de8137af019e0071065deee6f574ed8d'] = 'Conferma automatica';
$_MODULE['<{oney}prestashop>configure_644ecc1903b625b328d6a4530970b4c7'] = 'Su conferma di Oney, accettazione automatica del finanziamento e cambio stato in \"Pagamento accettato\"';
$_MODULE['<{oney}prestashop>configure_7b6aff5116a0ca2e719b16c30ca662d6'] = 'Informazioni necessarie per analisi anti frode';
$_MODULE['<{oney}prestashop>configure_3adbdb3ac060038aa0e6e6c138ef9873'] = 'Categoria';
$_MODULE['<{oney}prestashop>configure_c9cc8cce247e49bae79f15173ce97354'] = 'Salva';
$_MODULE['<{oney}prestashop>opc_e26f16ccfe045395266af9e2d52b1a23'] = 'Lista delle Business Transactions';
$_MODULE['<{oney}prestashop>opc_b24ce0cd392a5b0b8dedc66c25213594'] = 'Gratis';
$_MODULE['<{oney}prestashop>opc_032557a8588f5cd271b004804386eb86'] = 'Una volta inserito l\'ID del tuo shop, ti pregiamo di cliccare su \"Recupero delle business transactions\"';
$_MODULE['<{oney}prestashop>opc_c2915b44c8ea4ad7984a2829b4d76409'] = 'Recupero delle business transactions';
$_MODULE['<{oney}prestashop>opc_c9cc8cce247e49bae79f15173ce97354'] = 'Salva';
