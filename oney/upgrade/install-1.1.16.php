<?php
/**
 * 3x 4x Oney Module version 1.1.21 for PrestaShop 1.6 and PrestaShop 1.7. Support contact : prestashop@itroom.fr
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/afl-3.0.php
 *
 * @author    ITroom (http://itroom.fr/)
 * @copyright ITroom
 * @license   https://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * @category  payment
 * @package   oney
 */

if (!defined('_PS_VERSION_')) {
    exit;
}

function upgrade_module_1_1_16()
{
    $sSQL = array();
    $sSQL[] = 'INSERT INTO `' . _DB_PREFIX_ . 'oney_phone` (`iso_code`, `prefix`, `size`, `mobile_or_fixe`) VALUES 
        ("GP", "+590", 9, 3),("MQ", "+596", 9, 3),("YT", "+262", 9, 3),("NC", "+687", 9, 3),("PF", "+689", 9, 3),
        ("RE", "+262", 9, 3),("TF", "", 9, 3),("GF", "+592", 9, 3),("WF", "+681", 9, 3),("BL", "", 9, 3),
        ("MF", "", 9, 3),("PM", "+508", 9, 3)
    ';

    foreach ($sSQL as $query) {
        if (Db::getInstance()->execute($query) == false) {
            return false;
        }
    }

    return true;
}
