<?php
/**
 * 3x 4x Oney Module version 1.1.21 for PrestaShop 1.6 and PrestaShop 1.7. Support contact : prestashop@itroom.fr
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/afl-3.0.php
 *
 * @author    ITroom (http://itroom.fr/)
 * @copyright ITroom
 * @license   https://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * @category  payment
 * @package   oney
 */

if (!defined('_PS_VERSION_')) {
    exit;
}

function upgrade_module_1_1_7($module)
{
    $sSQL = array();
    $sSQL[] = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'oney_phone` ('.
        '`iso_code` VARCHAR(2),'.
        '`prefix` VARCHAR(10),'.
        '`size` INT(11),'.
        '`mobile_or_fixe` INT(1) DEFAULT 3,'.
        'PRIMARY KEY (`iso_code`, `mobile_or_fixe`))';

    // mobile OR fixe : 1 = fixe, 2 = mobile, 3 = les deux
    $sSQL[] = 'INSERT IGNORE INTO `' . _DB_PREFIX_ . 'oney_phone` VALUES ("FR","+33", 9, 3), ("BE", "+32", 8, 1),' .
        '("BE", "+32", 9, 2), ("ES", "+34", 9, 3), ("PT", "+351", 9, 3), ("IT", "+39", 10, 3)';

    foreach ($sSQL as $query) {
        if (Db::getInstance()->execute($query) == false) {
            return false;
        }
    }

    return true;
}
