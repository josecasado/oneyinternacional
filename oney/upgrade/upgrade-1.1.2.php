<?php
/**
 * 3x 4x Oney Module version 1.1.21 for PrestaShop 1.6 and PrestaShop 1.7. Support contact : prestashop@itroom.fr
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/afl-3.0.php
 *
 * @author    ITroom (http://itroom.fr/)
 * @copyright ITroom
 * @license   https://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * @category  payment
 * @package   oney
 */

if (!defined('_PS_VERSION_')) {
    exit;
}

function upgrade_module_1_1_2()
{
    $aSQL = array();
    $aSQL[] = 'DROP TABLE `'. _DB_PREFIX_ .'facilypay_opc`';

    foreach ($aSQL as $query) {
        if (Db::getInstance()->execute($query) == false) {
            return false;
        }
    }

    include('../sql/install.php');

    return true;
}
