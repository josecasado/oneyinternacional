/*
* 3x 4x Oney Module version 1.1.21 for PrestaShop 1.6. Support contact : prestashop@itroom.fr
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* https://opensource.org/licenses/afl-3.0.php
*
* @author    ITroom (http://itroom.fr/)
* @copyright ITroom
* @license   https://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
* @category  payment
* @package   oney
*/
$(document).ready(function () {

    showQuestion();

    $('#module_form input[type=radio]').change(function () {
        showQuestion();
    });

    function showQuestion(){
        $('#oney_form #module_form .form-radio').each(function () {
            if ($('input[value=1]', this).is(':checked')) {
                $(this).next('.form-radio').slideDown();
                $('.oney_return .alert-danger').slideUp();
            } else if ($('input[value=0]', this).is(':checked')) {
                $(this).next('.form-radio').slideUp();
                $(this).next('.form-radio').find('input').prop('checked', false);
                $('.oney_return .alert-danger').slideDown();
            } else {
                $(this).next('.form-radio').slideUp();
                $(this).next('.form-radio').find('input').prop('checked', false);
            }
        });

        /*if ($('#oney-question_3 input[value=1]').is(':checked')) {
            $('.oney-information_sheet').slideDown();
        } else {
            $('.oney-information_sheet').slideUp();
        }*/
    }

    if($('.prestashop-switch input[name="FACILIPAY_INTERMEDIAIRE_CREDIT[PT]"]#FACILIPAY_INTERMEDIAIRE_CREDIT_on').is(':checked')) {
        $('.solo-credit').show();
    }
    $('.prestashop-switch input[name="FACILIPAY_INTERMEDIAIRE_CREDIT[PT]"]').change(function() {
        if (this.value == 'credit') {
            $('.solo-credit').show();
        }
        else if (this.value == 'no_credit') {
            $('.solo-credit').hide();
        }
    });

});