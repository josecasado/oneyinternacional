/*
* 3x 4x Oney Module version 1.1.21 for PrestaShop 1.6. Support contact : prestashop@itroom.fr
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* https://opensource.org/licenses/afl-3.0.php
*
* @author    ITroom (http://itroom.fr/)
* @copyright ITroom
* @license   https://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
* @category  payment
* @package   oney
*/

$(document).ready(function(){
	if($('#checkout-payment-step.-current').length > 0) {
		$('.simulation_oney_shopping_info.simulation_oney_17').addClass('hidden');
	}
	$('body').on('click', '.checkout-step .step-title', function(){
		if($('#checkout-payment-step.-current').length > 0) {
			$('.simulation_oney_shopping_info.simulation_oney_17').addClass('hidden');
		}else {
			$('.simulation_oney_shopping_info.simulation_oney_17').removeClass('hidden');
		}
	});

	if ($('body#product').length > 0 && ($('.info_simulation .codeOPC').length > 0 || $('#simulation_oney_product input.codeOPC').length>0)) {
		getSimulation();
	}

	if ($('body#product').length < 1 && $('.info_simulation').length > 0) {
		getSimulationCart();
	}

  	if ($('.oney_after_price.after_oney').length > 0) {
		prestashop.on('updatedProduct', function (event) {
			let new_price = $('.current-price > span').attr('content');
			$('#priceSimulation').attr('value', new_price);
			getSimulation();
		});
	} else {
		$('body').on('change', '#buy_block .attribute_select', function(){
			reloadSimulation();
		});
		$('body').on('click', '#buy_block .color_pick', function(e){
			reloadSimulation();
		});
		$('body').on('click', '#buy_block .attribute_radio', function(e){
			reloadSimulation();
		});
	}

	if ($('.display li').length > 0) {
		$('.display').on('click', 'li', function(){
			$('#product_list li').each(function(){
				var _this = $(this);
				var oney = _this.find('.oney_after_price').length;
				var htmlOney = _this.find('.oney_after_price');
				if(oney > 0 && oney < 2) {
					_this.find('.content_price').append(htmlOney);
				}
			});
		});
	}

	if ($('#cart-subtotal-products').length > 0) {
		prestashop.on('updatedCart', function (event) {
			var nb_info_simulation = $('.info_simulation').length;
			if(nb_info_simulation > 1) {
				$('.info_simulation').first().remove();
			} 
			var nb_simulation_oney = $('.simulation_oney_shopping_info').length;
			if(nb_simulation_oney > 1) {
				$('.simulation_oney_shopping_info').first().remove();
				$('.simulation_oney_financement_solo').first().remove();
			} 
			
			var price = $('#cart-subtotal-products .value').text().replace('.', '').replace(',', '.').replace(/\s/g, '');
			$('#priceSimulation').val(parseFloat(price));	
			$('.simulation_oney_shopping_info').html('');
			getSimulationCart();
		});
	}

	if ($('#cart_summary').length > 0) {
		$('#cart_summary').on('click', '.cart_quantity a, .cart_delete a', function(){
			$('.simulation_oney_shopping_info').slideUp('fast', function(){
				setTimeout(function(){
					getSimulationCart();
				},1200);
			});
		});
		$('.cart_quantity_input').change(function(){
			$('.simulation_oney_shopping_info').slideUp('fast', function(){
				setTimeout(function(){
					getSimulationCart();
				},1200);
			});
		});
	}

	if ($('#simulation_oney_product').length > 0) {
		$('#simulation_oney_product').appendTo($('.oney_after_price'));
  	}

  	$(document).on('click', '.interrogation_bulle:not(.interrogation_product), .btn_pedagogique.tellmemore', function(){
		$('.pedagogique_oney').fadeIn();
	});
	$(document).on('click', '.close_pedagogique', function(){
		$('.pedagogique_oney').fadeOut();
	});
	$(document).on('click','.interrogation_bulle.interrogation_product', function(){
		$('#simulation_oney_product').fadeToggle();
	});
	$(document).on('click','.close_simulation', function(){
		$('#simulation_oney_product').fadeOut();
	});

	// if ($('#cart-subtotal-products').length > 0) {
	// 	prestashop.on('updatedDeliveryForm', function () {
	// 		getSimulationCart();
	// 	});
	// }
	var temp = 1;
	setTimeout(function(){
		$('.simulation_oney_shopping_info.espagne .simulation_oney_financement').each(function(){
			$(this).attr('data-position', temp);
			temp += 1;
		});
	},2000);
	$(document).on('click', '.simulation_oney_shopping_info.espagne .interrogation_bulle.interrogation_product', function(e){
		let instalment = $(this).closest('.simulation_oney_financement').data('instalment');
		let position = $(this).closest('.simulation_oney_financement').data('position');
		$('.simulation_oney_shopping_info.espagne .interrogation_bulle.interrogation_product').toggleClass('open_simulation');
		if($(this).hasClass('open_simulation')){
			$('#simulation_oney_product').removeClass('is-1 is-2 is-3 is-4 is-5');
			$('#simulation_oney_product').addClass('is-' + position);
			$('#simulation_oney_product.simulation_oney_cart.espagne .simulation_oney_content .simulation_oney_financement').hide();
			$('#simulation_oney_product.simulation_oney_cart.espagne .simulation_oney_content .simulation_oney_financement.instalment-' + instalment).show();
		}
	});

	if ($('#cart-subtotal-products').length > 0) {
		prestashop.on('updatedDeliveryForm', function () {
			getSimulationCart();
		});
	}

});

function getSimulation()
{
  	if ($('.oney_after_price').length > 0) {
		ajaxSimulation();
	} else {
		var price = $('#our_price_display').text().replace(',', '.').replace(/\s/g, '');
		$('#priceSimulation').val(parseFloat(price));
		$('#buy_block .choice_instalments a').trigger('click');
		ajaxSimulation();
	}
}

function getSimulationCart()
{
	ajaxSimulation();
}

function reloadSimulation()
{
	$('.simulation_oney_shopping_info').slideUp('fast', function(){
		setTimeout(function(){
			$('.simulation_oney_shopping_info').html('');
			getSimulation();
		},100);
	});
	$('#simulation_oney_product').fadeOut('fast', function(){
		setTimeout(function(){
			$('.simulation_oney_content').html('');
			getSimulation();
		},100);
	});
}

function ajaxSimulation() 
{
	var price = $('#priceSimulation').val();
	var urlSimulation = $('#URLSimulation').val();
	var inProductJs = 0;

	if(typeof inProduct != 'undefined' && inProduct) {
		inProductJs = 1;
	}

	// UPDATE code OPC
	$.ajax({
		method: "POST",
		url: urlSimulation,
		data: { updeCodeOpc: true, price: price, ajax: true, inProduct: inProductJs }
	}).success(function(data){
		$('.info_simulation .codeOPC').remove();
		var obj = JSON.parse(data);
		var i = 1;
		if(obj.length == 0) {
			$('.simulation_oney_financement_solo').hide();
		}else {
			$('.simulation_oney_financement_solo').show();
		}
		for (var property in obj) {
			$('.info_simulation').append('<input type="hidden" data-position="codeOPC-' + i + '" class="codeOPC" value="' + obj[property] + '"/>');
			i += 1;
		}
		// UPDATE display OPC
		var opcList=[];
		$('#simulation_oney_product input.codeOPC, .info_simulation input.codeOPC').each(function() {
			var code = $(this).val();
			var price = $('#priceSimulation').val();
			var urlSimulation = $('#URLSimulation').val();
			var inProductJs = 0;

			if(typeof inProduct != 'undefined' && inProduct) {
				inProductJs = 1;
			}

			opcList.push({
				"code":code,
				"price":price,
				"urlSimulation":urlSimulation,
				"inProductJs":inProductJs
			});

			// $.ajax({
			// 	method: "POST",
			// 	url: urlSimulation,
			// 	data: { price: price, code: code, ajax: true, inProduct: inProductJs }
			// }).success(function(data){
			// 	if(data != '') {
			// 		var obj = JSON.parse(data);
			// 		if (obj.mensualite == 4) {
			// 			$('.simulation_oney_content').append(obj.simulation_info);
			// 			$('.simulation_oney_shopping_info').append(obj.shopping_info);
			// 		} else {
			// 			$('.simulation_oney_content').prepend(obj.simulation_info);
			// 			$('.simulation_oney_shopping_info').prepend(obj.shopping_info);
			// 		}
			// 	}

			// }).complete(function(){
			// 	setTimeout(function(){
			// 		var arrayPrice = [];

			// 		$('.simulation_oney_financement').each(function(){
			// 			var priceValue = $(this).attr('data-price');
			// 			arrayPrice.push(priceValue);
			// 		});
			// 	},1000);

			// 	$('.simulation_oney_shopping_info').slideDown('fast');
			// 	$('#simulation_oney_product').appendTo($('.oney_after_price'));
			// });
		});
		var requests = opcList.map(function(opc){
			return $.ajax({
			method: "POST",
			url: opc.urlSimulation,
			data: { price: opc.price, code: opc.code, ajax: true, inProduct: opc.inProductJs }
		})
	});

	$.when.apply(this, requests).then(function() {
		var obj = '';
		$('#simulation_oney_product').appendTo($('.oney_after_price'));
		if(Array.isArray(arguments[0])){
			$.each(arguments, function(k,data){
				obj = JSON.parse(data[0]);
				fillDOM(obj);
			});
		}
		else{
			obj = JSON.parse(arguments[0]);
			fillDOM(obj);
		}
		$('.simulation_oney_shopping_info').slideDown('fast');
	});
	});
}

function fillDOM(obj){
	$('.simulation_oney_content').append(obj.simulation_info);
	$('.simulation_oney_shopping_info:not(.simulation_cart_solo)').append(obj.shopping_info);
}