{*
 * 3x 4x Oney Module version 1.1.21 for PrestaShop 1.6. Support contact : prestashop@itroom.fr
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/afl-3.0.php
 *
 * @author    ITroom (http://itroom.fr/)
 * @copyright ITroom
 * @license   https://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * @category  payment
 * @package   oney
 *}

{if !isset($tab_active)}
	{assign var="tab_active" value="1"}
{/if}
{if !isset($langue_active)}
	{assign var="langue_active" value=1}
{/if}

<div id="FacilyPay_module">
	<div>
		<div class="row">
			<div class="step-1 col-xs-12 col-sm-6 col-md-3">
				<img src="{$module_dir|escape:'htmlall':'UTF-8'}views/img/step1.png" title=""/>
				<p>{l s='Ready to use solution quick to set up' mod='oney'}</p>
			</div>
			<div class="step-2 col-xs-12 col-sm-6 col-md-3">
				<img src="{$module_dir|escape:'htmlall':'UTF-8'}views/img/step2.png" title=""/>
				<p>{l s='Guarantee against credit card fraud and unpaid customers' mod='oney'}</p>
			</div>
			<div class="step-3 col-xs-12 col-sm-6 col-md-3">
				<img src="{$module_dir|escape:'htmlall':'UTF-8'}views/img/step3.png" title=""/>
				<p>{l s='100% financing of the purchase price within 48 hours' mod='oney'}</p>
			</div>
			<div class="step-4 col-xs-12 col-sm-6 col-md-3">
				<img src="{$module_dir|escape:'htmlall':'UTF-8'}views/img/step4.png" title=""/>
				<p>{l s='Acquisition of new clients' mod='oney'}</p>
			</div>
		</div>
		<div class="titleFacilyPay">
			<h1>{l s='Facily Pay, The first payment solution in 3 or 4 times by credit card' mod='oney'}</h1>
		</div>
	</div>

	{if isset($smarty.get.sendMail)}
		<div class="alert alert-success">
			{l s='Thank you for your interest in the 3X 4X Oney.' mod='oney'}<br/>
			{l s='Your request has been taken into account, you will soon receive an email to the address you have in order to finalize the activation procedure for your account.' mod='oney'}
		</div>
		<div class="alert alert-success">
			{Oney::getConfigByCountry("phrase_succes", $iso_code|escape:'htmlall':'UTF-8')}
		</div>
	{/if}
	<div class="alert alert-warning">
		<p><strong>{l s='This module must be able to update the order statuses' mod='oney'}</strong></p>
		<p>{l s='To do this, simply activate a scheduled task.' mod='oney'}</p>
		<p>{l s='To set up this scheduled task, you must access your hosting interface or contact your host.' mod='oney'}</p>
		<p>{l s='Set the task to run the update every 2h' mod='oney'}.</p>
		<p>{l s='The URL to call is' mod='oney'} {$shop_url|escape:'htmlall':'UTF-8'}/module/oney/orderStateUpdate?token={$token|escape:'htmlall':'UTF-8'}</p>
	</div>
	{if isset($error) && $error}
		{include file="./tab/messages.tpl" id="main" text=$error class='error'}
	{/if}
	{if isset($success) && $success}
		{include file="./tab/messages.tpl" id="main" text=$success class='success'}
	{/if}
	<!-- Nav tabs -->
	<ul class="nav nav-tabs" role="tablist">
		<li {if $tab_active == 1}class="active"{/if}>
			<a href="#template_1" role="tab" data-toggle="tab">{l s='Configure' mod='oney'}</a>
		</li>
		<li {if $tab_active == 2}class="active"{/if}>
			<a href="#template_2" role="tab" data-toggle="tab">{l s='Select my OPC' mod='oney'}</a>
		</li>
	</ul>
	<!-- Tab panes -->
	<div class="tab-content">
		<div class="tab-pane {if $tab_active == 1}active{/if}" id="template_1">
			{include file='./tab/configure.tpl'}
		</div>
		<div class="tab-pane {if $tab_active == 2}active{/if}" id="template_2">
			{include file='./tab/OPC.tpl'}
		</div>
	</div>
</div>
