{*
 * 3x 4x Oney Module version 1.1.21 for PrestaShop 1.6. Support contact : prestashop@itroom.fr
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/afl-3.0.php
 *
 * @author    ITroom (http://itroom.fr/)
 * @copyright ITroom
 * @license   https://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * @category  payment
 * @package   oney
 *}
{assign var="iso_code" value=Context::getContext()->language->iso_code|upper}
{if Oney::getConfigByCountry("formulaire_onboarding", $iso_code) == false}
	<div id="oney_form">
		<div class="oney_return">
			<div class="alert alert-danger">
				{l s='Thank you for your interest. In view of the criteria given, you are not at the moment not eligible for the 3x 4X Oney.' mod='oney'}
				<br/>
				{l s='Do not hesitate to repeat your request if your situation changes.' mod='oney'}
			</div>
		</div>

		<form id="module_form" class="defaultForm form-horizontal" method="post">
			<div class="panel">
				<div class="tab-content">
					<div class="tab-pane active">
						<img class="logo_back" src="{$module_dir|escape:'htmlall':'UTF-8'}views/img/logo_3-4-fois.png" title="{l s='Oney' mod='oney'}" />
						<h3>{l s='Would you like to know more about our financing and payment solutions?' mod='oney'}</h3>
						<p>{l s='La solution de paiement en plusieurs fois nécessite la signature d’un contrat de partenariat entre Oney et le commerçant. Pour vérifier votre éligibilité et être contacté par Oney.' mod='oney'} <a href="https://www.oney-ecommerce.com/contactez-nous/?utm_source=prestashop&utm_medium=email&utm_campaign=ps_juin_19" target="_blank">{l s='Cliquez-ici' mod='oney'}</a></p>

						{*if isset($error) && $error}
							{include file="./tab/messages.tpl" id="main" text=$error class='error'}
						{/if}
						{if isset($success) && $success}
							{include file="./tab/messages.tpl" id="main" text=$success class='success'}
						{/if*}

						<a href="{$adminUrl|escape:'htmlall':'UTF-8'}" class="btn btn-default pull-right">
							<i class="icon-key"></i> {l s='Configuring' mod='oney'}
						</a>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
		</form>
	</div>
{else}
	<div id="oney_form">

		<div class="oney_return">
			<div class="alert alert-success">
				{l s='Thank you for your interest in the 3X 4X Oney.' mod='oney'}<br/>
				{l s='Your request has been taken into account, you will soon receive an email to the address you have in order to finalize the activation procedure for your account.' mod='oney
				'}
			</div>

			<div class="alert alert-danger">
				{l s='Thank you for your interest. In view of the criteria given, you are not at the moment not eligible for the 3x 4X Oney.' mod='oney'}
				<br/>
				{l s='Do not hesitate to repeat your request if your situation changes.' mod='oney'}
			</div>
		</div>

		<form id="module_form" class="defaultForm form-horizontal" method="post">

			<div class="panel">

				<div class="tab-content">
					<div class="tab-pane active">
						{Oney::getConfigByCountry("phrase_accroche", $iso_code|escape:'htmlall':'UTF-8')}

						{*if isset($error) && $error}
							{include file="./tab/messages.tpl" id="main" text=$error class='error'}
						{/if}
						{if isset($success) && $success}
							{include file="./tab/messages.tpl" id="main" text=$success class='success'}
						{/if*}

						<a href="{$adminUrl|escape:'htmlall':'UTF-8'}" class="btn btn-default pull-right">
							<i class="icon-key"></i> {l s='Connection' mod='oney'}
						</a>

						<div class="oney-information_sheet clearfix">
							<hr/>

							<h2>{l s='Information sheet' mod='oney'}</h2>



							{foreach from=Oney::getConfigByCountry("champ_onboarding", $iso_code|escape:'htmlall':'UTF-8') item=champs key=key}
								<div class="form-group clearfix {$champs->col|escape:'htmlall':'UTF-8'}">
									<label class="control-label">{$champs->name|escape:'htmlall':'UTF-8'}{if $champs->required == true}*{/if}</label>
									<div>
										{if $champs->type == "textarea"}
											<textarea id="{$key|escape:'htmlall':'UTF-8'}" name="fiche[{$key|escape:'htmlall':'UTF-8'}]" class="form-control">{if isset($smarty.post.fiche.$key)}{$smarty.post.fiche.$key|escape:'htmlall':'UTF-8'}{/if}</textarea>
										{elseif $champs->type == "switch"}
											<span class="switch prestashop-switch fixed-width-lg">
												<input type="radio"
													name="fiche[{$key|escape:'htmlall':'UTF-8'}]"
													id="{$key|escape:'htmlall':'UTF-8'}_on" value="Sim"
												/>
												<label class="t" for="{$key|escape:'htmlall':'UTF-8'}_on">{l s='Yes' mod='oney'}</label>
												<input type="radio"
													name="fiche[{$key|escape:'htmlall':'UTF-8'}]"
													id="{$key|escape:'htmlall':'UTF-8'}_off" value="Não"
													checked="checked" 
												/>
												<label class="t" for="{$key|escape:'htmlall':'UTF-8'}_off">{l s='No' mod='oney'}</label>
												<a class="slide-button btn"></a>
                                  			</span>
										{else}
											<input type="{$champs->type|escape:'htmlall':'UTF-8'}" name="fiche[{$key|escape:'htmlall':'UTF-8'}]" id="{$key|escape:'htmlall':'UTF-8'}" {if $champs->required == true}required {/if}{if isset($smarty.post.fiche)}value="{$smarty.post.fiche.$key|escape:'htmlall':'UTF-8'}{/if}>
										{/if}
									</div>
								</div>
							{/foreach}
						</div>
						<div>
							<p>{l s='* Required fields' mod='oney'}</p>
						</div>
						<div class="panel-footer">
							<button type="submit" value="1" id="module_form_submit_btn" name="submitOneyOnboarding"
									class="btn btn-default pull-right">
								<i class="process-icon-save"></i> {l s='Send' mod='oney'}
							</button>
						</div>
					</div>
		</form>
	</div>
{/if}