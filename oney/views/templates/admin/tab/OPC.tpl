{*
 * 3x 4x Oney Module version 1.1.21 for PrestaShop 1.6. Support contact : prestashop@itroom.fr
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/afl-3.0.php
 *
 * @author    ITroom (http://itroom.fr/)
 * @copyright ITroom
 * @license   https://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * @category  payment
 * @package   oney
 *}

<form id="module_form" class="defaultForm form-horizontal" action="{$form_uri|escape:'htmlall':'UTF-8'}" method="post">
    <div class="panel" id="fieldset_0">
        <div class="panel-heading">
            <i class="icon-list"></i> {l s='LIST OF OPCs' mod='oney'}
        </div>

        <ul class="nav nav-tabs nav-langue" role="tablist">{strip}
                {foreach item=country key=k from=$countries name=countries}
                    {if $smarty.foreach.countries.total > 1}
                        <li {if $smarty.foreach.countries.first}class="active"{/if}>
                            <a href="#template_opc_{$country.iso_code|escape:'htmlall':'UTF-8'}" role="tab"
                               data-toggle="tab">{$country.name|escape:'htmlall':'UTF-8'}</a>
                        </li>
                    {/if}
                {/foreach}
            {/strip}</ul>

        <div class="tab-content">
            {foreach item=country key=k from=$countries name=countries}
                <div class="tab-pane {if $smarty.foreach.countries.first}active{/if}"
                     id="template_opc_{$country.iso_code|escape:'htmlall':'UTF-8'}">
                    <div class="form-wrapper">
                        {if !empty($FACILIPAY_LISTE_OPC_{$country.iso_code|escape:'htmlall':'UTF-8'})}
                            {foreach item=OPC from=$FACILIPAY_LISTE_OPC_{$country.iso_code|escape:'htmlall':'UTF-8'}}
                                <div class="form-group list_paiement_facilyPay">
                                    <div class="labelFacilyPay">
                                        <input type="checkbox" class=""
                                               name="FACILYPAY_OPC_{$country.iso_code|escape:'htmlall':'UTF-8'}[]"
                                               id="{$OPC.business_transaction_code|escape:'htmlall':'UTF-8'}"
                                               value="{$OPC.business_transaction_code|escape:'htmlall':'UTF-8'}"
                                               {if $OPC.selected}checked{/if}/>
                                        <div class="logo_paiement">
                                            {assign var=nbr_opc value=($OPC.maximum_number_of_instalments + 1)}
                                            {if $country.iso_code != 'FR'}
                                                {$nbr_opc = $OPC.maximum_number_of_instalments}
                                            {/if}
                                            {assign var="logoOney" value="{$smarty.server.DOCUMENT_ROOT}/modules/oney/views/img/logo_oney-financement_{$nbr_opc}_0_BO.png"}
                                            {if file_exists($logoOney)}
                                                <img src="{$module_dir|escape:'htmlall':'UTF-8'}views/img/logo_oney-financement_{$nbr_opc|escape:'htmlall':'UTF-8'}_0_BO.png">
                                            {else}
                                                <img src="{$module_dir|escape:'htmlall':'UTF-8'}views/img/logo_default.jpg">
                                            {/if}
                                        </div>
                                    </div>
                                    <label class="control-label"
                                           for="{$OPC.business_transaction_code|escape:'htmlall':'UTF-8'}">
                                        {if !empty($OPC.customer_label)}
                                            {$OPC.customer_label|escape:'htmlall':'UTF-8'}
                                        {else}
                                            {$OPC.long_label|escape:'htmlall':'UTF-8'}
                                        {/if}
                                        <span class="help-block">
                                            {if $OPC.free_business_transaction}{l s='Free' mod='oney'} |{/if} {$OPC.business_transaction_code|escape:'htmlall':'UTF-8'} | {$OPC.minimum_selling_price|escape:'htmlall':'UTF-8'} >> {$OPC.maximum_selling_price|escape:'htmlall':'UTF-8'}
									</span>
                                    </label>
                                </div>
                            {/foreach}
                        {else}
                            <div class="message alert alert-danger">{l s='After entering your shop ID, please click on "Recovery of OPCs"' mod='oney'}</div>
                        {/if}

                        <div class="panel-footer">
                            <button type="submit" value="{$country.iso_code|escape:'htmlall':'UTF-8'}"
                                    id="module_form_submit_get_btn" name="submitFacilypayModuleGetOPC"
                                    class="btn btn-default pull-right">
                                <i class="process-icon-upload"></i> {l s='Recovery of OPCs' mod='oney'}
                            </button>
                            <button type="submit" value="{$country.iso_code|escape:'htmlall':'UTF-8'}"
                                    id="module_form_submit_save_btn" name="submitFacilypayModuleSaveOPC"
                                    class="btn btn-default pull-right">
                                <i class="process-icon-save"></i> {l s='Save' mod='oney'}
                            </button>
                        </div>
                    </div>
                </div>
            {/foreach}
        </div>
    </div>
</form>
