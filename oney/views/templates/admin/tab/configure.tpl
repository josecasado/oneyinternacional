{*
 * 3x 4x Oney Module version 1.1.21 for PrestaShop 1.6. Support contact : prestashop@itroom.fr
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/afl-3.0.php
 *
 * @author    ITroom (http://itroom.fr/)
 * @copyright ITroom
 * @license   https://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * @category  payment
 * @package   oney
 *}

<form id="module_form" class="defaultForm form-horizontal" action="{$form_uri|escape:'htmlall':'UTF-8'}"
      method="post" enctype="multipart/form-data">
  <div class="panel" id="fieldset_0">

    <div class="panel-heading">
      <i class="icon-cogs"></i> {l s='Configure' mod='oney'}
    </div>

    <div class="form-wrapper">
      <div class="form-group">
        <label class="control-label  col-lg-3">{l s='Rétrocompatibilité thème' mod='oney'}</label>
        <div class="col-lg-6">
        <span class="switch prestashop-switch fixed-width-lg facilypay_theme">
          <input type="radio" name="FACILYPAY_RETROTHEME" id="FACILYPAY_RETROTHEME_on" value="1"
                 {if $FACILYPAY_RETROTHEME == 1}checked="checked" {/if} />
          <label class="t" for="FACILYPAY_RETROTHEME_on">{l s='Yes' mod='oney'}</label>
          <input type="radio" name="FACILYPAY_RETROTHEME" id="FACILYPAY_RETROTHEME_off" value="0"
                 {if $FACILYPAY_RETROTHEME == 0}checked="checked" {/if} />
          <label class="t" for="FACILYPAY_RETROTHEME_off">{l s='No' mod='oney'}</label>
          <a class="slide-button btn"></a>
        </span>
        </div>
      </div>
    </div>

    <div class="form-wrapper">
      <div class="form-group">
        <label class="control-label  col-lg-3">{l s='Mode de fonctionnement' mod='oney'}</label>
        <div class="col-lg-6">
					<span class="switch prestashop-switch fixed-width-lg">
						<input type="radio" name="FACILYPAY_MODE" id="FACILYPAY_MODE_on" value="1"
                               {if $FACILYPAY_MODE == 1}checked="checked" {/if} />
						<label class="t" for="FACILYPAY_MODE_on">{l s='Production' mod='oney'}</label>
						<input type="radio" name="FACILYPAY_MODE" id="FACILYPAY_MODE_off" value="0"
                               {if $FACILYPAY_MODE == 0}checked="checked" {/if} />
						<label class="t" for="FACILYPAY_MODE_off">{l s='Test' mod='oney'}</label>
						<a class="slide-button btn"></a>
					</span>
                </div>
            </div>
        </div>

    <ul class="nav nav-tabs nav-langue" role="tablist">{strip}
        {foreach item=country key=k from=$countries name=countries}
          {if $smarty.foreach.countries.total > 1}
            <li {if $smarty.foreach.countries.first}class="active"{/if}>
              <a href="#template_{$country.iso_code|escape:'htmlall':'UTF-8'}" role="tab"
                 data-toggle="tab">{$country.name|escape:'htmlall':'UTF-8'}</a>
            </li>
          {/if}
        {/foreach}
      {/strip}</ul>

    <div class="tab-content">
      {foreach item=country key=k from=$countries name=countries}
        <div class="tab-pane {if $smarty.foreach.countries.first}active{/if}"
             id="template_{$country.iso_code|escape:'htmlall':'UTF-8'}">
          <div class="form-wrapper">
            <div class="form-group">
              <label class="control-label col-lg-3">{l s='Merchant GUID' mod='oney'}</label>
              <div class="col-lg-6">
                <input type="text"
                       name="FACILYPAY_MERCHANT_GUID[{$country.iso_code|escape:'htmlall':'UTF-8'}]"
                       id="FACILYPAY_ACCOUNT_IDENTIFIANT"
                       value="{if !empty($FACILYPAY_MERCHANT_GUID_{$country.iso_code|escape:'htmlall':'UTF-8'})}{$FACILYPAY_MERCHANT_GUID_{$country.iso_code|escape:'htmlall':'UTF-8'}}{/if}">
                <p class="help-block">
                  {l s='The identifier of your site, available in the back office of your shop' mod='oney'}
                </p>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-lg-3">{l s='PSP GUID' mod='oney'}</label>
              <div class="col-lg-6">
                <input type="text"
                       name="FACILYPAY_PSP_GUID[{$country.iso_code|escape:'htmlall':'UTF-8'}]"
                       id="FACILYPAY_PSP_GUID_{$country.iso_code|escape:'htmlall':'UTF-8'}"
                       value="{if !empty($FACILYPAY_PSP_GUID_{$country.iso_code|escape:'htmlall':'UTF-8'})}{$FACILYPAY_PSP_GUID_{$country.iso_code|escape:'htmlall':'UTF-8'}}{/if}">
                <p class="help-block">
                  {l s='PSP GUID' mod='oney'}
                </p>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-lg-3">{l s='Payment key' mod='oney'}</label>
              <div class="col-lg-6">
                <input type="text"
                       name="FACILYPAY_API_KEY_PAYMENT[{$country.iso_code|escape:'htmlall':'UTF-8'}]"
                       id="FACILYPAY_API_KEY_PAYMENT"
                       value="{if !empty($FACILYPAY_API_KEY_PAYMENT_{$country.iso_code|escape:'htmlall':'UTF-8'})}{$FACILYPAY_API_KEY_PAYMENT_{$country.iso_code|escape:'htmlall':'UTF-8'}}{/if}">
                <p class="help-block">{l s='Payment key' mod='oney'}</p>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-lg-3">{l s='Marketing key' mod='oney'}</label>
              <div class="col-lg-6">
                <input type="text"
                       name="FACILIPAY_API_KEY_MARKETING[{$country.iso_code|escape:'htmlall':'UTF-8'}]"
                       id="FACILIPAY_API_KEY_MARKETING"
                       value="{if !empty($FACILIPAY_API_KEY_MARKETING_{$country.iso_code|escape:'htmlall':'UTF-8'})}{$FACILIPAY_API_KEY_MARKETING_{$country.iso_code|escape:'htmlall':'UTF-8'}}{/if}">
                <p class="help-block">{l s='Marketing key' mod='oney'}</p>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-lg-3">{l s='Oney secret' mod='oney'}</label>
              <div class="col-lg-6">
                <input type="text"
                       name="FACILIPAY_ONEY_SECRET[{$country.iso_code|escape:'htmlall':'UTF-8'}]"
                       id="FACILIPAY_ONEY_SECRET_{$country.iso_code|escape:'htmlall':'UTF-8'}"
                       value="{if !empty($FACILIPAY_ONEY_SECRET_{$country.iso_code|escape:'htmlall':'UTF-8'})}{$FACILIPAY_ONEY_SECRET_{$country.iso_code|escape:'htmlall':'UTF-8'}}{/if}">
                <p class="help-block">{l s='Oney secret' mod='oney'}</p>
              </div>
            </div>
            {* UNIQUEMENT POUR LE PORTUGAL *}
            {if $country.iso_code == 'PT'}
              <div class="form-group">
                <label class="control-label col-lg-3">{l s='Credit intermediary' mod='oney'} ?</label>
                <div class="col-lg-6">
									                <span class="switch prestashop-switch fixed-width-lg">
                                      <input type="radio"
                                              name="FACILIPAY_INTERMEDIAIRE_CREDIT[{$country.iso_code|escape:'htmlall':'UTF-8'}]"
                                              id="FACILIPAY_INTERMEDIAIRE_CREDIT_on" value="credit"
                                              {if $FACILIPAY_INTERMEDIAIRE_CREDIT_{$country.iso_code} == 'credit'}checked="checked" {/if} />
                                      <label class="t" for="FACILIPAY_INTERMEDIAIRE_CREDIT_on">{l s='Yes' mod='oney'}</label>
                                      <input type="radio"
                                              name="FACILIPAY_INTERMEDIAIRE_CREDIT[{$country.iso_code|escape:'htmlall':'UTF-8'}]"
                                              id="FACILIPAY_INTERMEDIAIRE_CREDIT_off" value="no_credit"
                                              {if $FACILIPAY_INTERMEDIAIRE_CREDIT_{$country.iso_code} == 'no_credit'}checked="checked" {/if} />
                                      <label class="t" for="FACILIPAY_INTERMEDIAIRE_CREDIT_off">{l s='No' mod='oney'}</label>
                                      <a class="slide-button btn"></a>
                                  </span>
                  <p class="help-block">{l s='If your are credit intermediary, tick the box' mod='oney'}</p>
                </div>
              </div>
              <div class="form-group solo-credit">
                <label class="control-label col-lg-3">{l s='Company legal designation' mod='oney'}</label>
                <div class="col-lg-6">
                  <input type="text"
                         name="FACILIPAY_MERCHANT_NAME[{$country.iso_code|escape:'htmlall':'UTF-8'}]"
                         id="FACILIPAY_MERCHANT_NAME_{$country.iso_code|escape:'htmlall':'UTF-8'}"
                         value="{if !empty($FACILIPAY_MERCHANT_NAME_{$country.iso_code|escape:'htmlall':'UTF-8'})}{$FACILIPAY_MERCHANT_NAME_{$country.iso_code|escape:'htmlall':'UTF-8'}}{/if}">
                  <p class="help-block">{l s='Merchant name' mod='oney'}</p>
                </div>
              </div>
              <div class="form-group solo-credit">
                <label class="control-label col-lg-3">{l s='Exclusivity regime' mod='oney'} ?</label>
                <div class="col-lg-6">
                  <div class="radio">
                    <label class="t"
                           for="FACILIPAY_INTERMEDIATION_TYPE_linked">{l s='Em regime de exclusividade' mod='oney'}
                      <input type="radio"
                             name="FACILIPAY_INTERMEDIATION_TYPE[{$country.iso_code|escape:'htmlall':'UTF-8'}]"
                             id="FACILIPAY_INTERMEDIATION_TYPE_linked" value="linked"
                             {if $FACILIPAY_INTERMEDIATION_TYPE_{$country.iso_code} == 'linked'}checked="checked" {/if} />
                    </label>
                  </div>
                  <div class="radio">
                    <label class="t" for="FACILIPAY_INTERMEDIATION_TYPE_not_linked">{l s='Sem exclusividade' mod='oney'}
                      <input type="radio"
                             name="FACILIPAY_INTERMEDIATION_TYPE[{$country.iso_code|escape:'htmlall':'UTF-8'}]"
                             id="FACILIPAY_INTERMEDIATION_TYPE_not_linked" value="not_linked"
                             {if $FACILIPAY_INTERMEDIATION_TYPE_{$country.iso_code} == 'not_linked'}checked="checked" {/if} />
                    </label>
                  </div>
                </div>
              </div>
              <div class="form-group file">
                <label class="control-label col-lg-3">{l s='Legal document' mod='oney'}</label>
                <div class="col-lg-6">
                  <input type="file"
                         name="FACILIPAY_LEGAL_DOCUMENT[{$country.iso_code|escape:'htmlall':'UTF-8'}]"
                         id="FACILIPAY_LEGAL_DOCUMENT_{$country.iso_code|escape:'htmlall':'UTF-8'}"
                  >
                  <p class="help-block">
                    {assign var="file{$country.iso_code}" value={Oney::getUploadedFile($country.iso_code)|escape:'htmlall':'UTF-8'}}
                    {if $file{$country.iso_code}}
                      <a href="{$file{$country.iso_code}|escape:'htmlall':'UTF-8'}" target="_blank">{l s='Legal document' mod='oney'}</a>
                      <a href="{$adminUrl|escape:'htmlall':'UTF-8'}&deleteUploadFile={$country.iso_code|escape:'htmlall':'UTF-8'}" class="btn"><i class="icon-trash"></i></a></p>
                    {/if}
                </div>
              </div>
            {/if}
            <div class="row">
              <button type="submit" value="{$country.iso_code|escape:'htmlall':'UTF-8'}"
                      id="module_form_submit_btn" name="submitFacilypayModuleTest"
                      class="btn btn-default pull-right">
                <i class="icon-check-square-o"></i> {l s='Test' mod='oney'}
              </button>
            </div>
          </div>
        </div>
      {/foreach}
    </div>

        <hr/>

        <div class="form-wrapper">
            <div class="form-group">
                <div class="row">
                    <label class="control-label col-lg-3">{l s='Confirmation automatique' mod='oney'}</label>
                    <div class="col-lg-6">
                        <span class="switch prestashop-switch fixed-width-lg">
                            <input type="radio" name="FACILYPAY_ACTION_CONFIRM" id="FACILYPAY_ACTION_CONFIRM_on" value="1"
                                              {if $FACILYPAY_ACTION_CONFIRM == 1}checked="checked" {/if} />
                            <label class="t" for="FACILYPAY_ACTION_CONFIRM_on">{l s='Yes' mod='oney'}</label>
                            <input type="radio" name="FACILYPAY_ACTION_CONFIRM" id="FACILYPAY_ACTION_CONFIRM_off" value="0"
                                              {if $FACILYPAY_ACTION_CONFIRM == 0}checked="checked" {/if} />
                            <label class="t" for="FACILYPAY_ACTION_CONFIRM_off">{l s='No' mod='oney'}</label>
                            <a class="slide-button btn"></a>
                        </span>
                        <p class="help-block">
                            {l s='Upon receipt of a favorable opinion from Oney, automatic acceptance of the ficancing and switch of the order to "Payment accepted".' mod='oney'}
                        </p>
                    </div>
                </div>
            </div>
        </div>

        <hr/>

        <div class="form-wrapper">
            <h2>
                {l s='Information needed for anti-fraud analysis' mod='oney'}
            </h2>
            <div class="form-group">
                <div class="row">
                    <label class="control-label col-lg-3">{l s='Category' mod='oney'}</label>
                    <div class="col-lg-6">
                        <select name="FACILYPAY_CATEGORY">
                            {foreach item=category from=$categories key="key"}
                                <option value="{$key|escape:'htmlall':'UTF-8'}"
                                        {if $selected_categorie == $key}selected{/if}>{$category|escape:'htmlall':'UTF-8'}</option>
                            {/foreach}
                        </select>
                    </div>
                </div>
            </div>
        </div>

        <hr/>

        <div class="form-wrapper">
            <div class="form-group">
                {foreach item=carrier from=$carriers name=carrier}
                    <div class="row select_block">
                        <label class="control-label col-lg-2">{$carrier.name|escape:'htmlall':'UTF-8'}</label>
                        <div class="col-lg-2">
                            <select name="oney_carrier[{$carrier.id_carrier|escape:'htmlall':'UTF-8'}][carrier]">
                                {foreach item=association_carrier from=$association_carriers key="key"}
                                    <option value="{$key|escape:'htmlall':'UTF-8'}"
                                            {if isset($selected_association[$carrier.id_carrier]) && $selected_association[$carrier.id_carrier]['delivery_mode'] == $key }selected{/if}>{$association_carrier|escape:'htmlall':'UTF-8'}</option>
                                {/foreach}
                            </select>
                        </div>
                        <div class="col-lg-2">
                            <select name="oney_carrier[{$carrier.id_carrier|escape:'htmlall':'UTF-8'}][priority]">
                                {foreach item=priority from=$priorities key="key"}
                                    <option value="{$key|escape:'htmlall':'UTF-8'}"
                                            {if isset($selected_association[$carrier.id_carrier]) && $selected_association[$carrier.id_carrier]['priority'] == $key }selected{/if}>{$priority|escape:'htmlall':'UTF-8'}</option>
                                {/foreach}
                            </select>
                        </div>
                        <div class="col-lg-2">
                            <select name="oney_carrier[{$carrier.id_carrier|escape:'htmlall':'UTF-8'}][priority_code]">
                                {foreach item=priority_code from=$priorities_code key="key"}
                                    <option value="{$key|escape:'htmlall':'UTF-8'}"
                                            {if isset($selected_association[$carrier.id_carrier]) && $selected_association[$carrier.id_carrier]['priority_delivery_code'] == $key }selected{/if}>{$priority_code|escape:'htmlall':'UTF-8'}</option>
                                {/foreach}
                            </select>
                        </div>
                        <div class="col-lg-2">
                            <select name="oney_carrier[{$carrier.id_carrier|escape:'htmlall':'UTF-8'}][address_type]">
                                {foreach item=address_type from=$addresses_type key="key"}
                                    <option value="{$key|escape:'htmlall':'UTF-8'}"
                                            {if isset($selected_association[$carrier.id_carrier]) && $selected_association[$carrier.id_carrier]['address_type'] == $key }selected{/if}>{$address_type|escape:'htmlall':'UTF-8'}</option>
                                {/foreach}
                            </select>
                        </div>
                        <div class="col-lg-2">
                            <select name="oney_carrier[{$carrier.id_carrier|escape:'htmlall':'UTF-8'}][store]">
                                {foreach from=$stores item=store}
                                    <option value="{$store.id_store|escape:'htmlall':'UTF-8'}"
                                            {if isset($selected_association[$carrier.id_carrier]) && $selected_association[$carrier.id_carrier]['id_store'] == $store.id_store }selected{/if}>
                                        {$store.name|escape:'htmlall':'UTF-8'}
                                    </option>
                                {/foreach}
                            </select>
                        </div>
                    </div>
                {/foreach}
            </div>
        </div>

        <div class="panel-footer">
            <button type="submit" value="1" id="module_form_submit_btn" name="submitFacilypayModuleIdentification"
                    class="btn btn-default pull-right">
                <i class="process-icon-save"></i> {l s='Save' mod='oney'}
            </button>
        </div>
    </div>
</form>
