{*
 * 3x 4x Oney Module version 1.1.21 for PrestaShop 1.6. Support contact : prestashop@itroom.fr
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/afl-3.0.php
 *
 * @author    ITroom (http://itroom.fr/)
 * @copyright ITroom
 * @license   https://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * @category  payment
 * @package   oney
 *}

{capture name=path}{l s='Order confirmation' mod='oney'}{/capture}

{if (isset($status) == true) && ($status == 'ok')}
	<p class="alert alert-success">{l s='Votre commande a bien été enregistrée.' mod='oney' sprintf=$shop_name mod='oney'}</p>
	<div class="box order-confirmation">
		<h3 class="page-subheading">{l s='Votre commande est complète.' sprintf=$shop_name mod='oney'}</h3>
		<p>
			- {l s='Amount' mod='oney'} : <span class="price"><strong>{convertPrice price=$total|escape:'htmlall':'UTF-8'}</strong></span>
			<br/>- {l s='Reference' mod='oney'} : <span class="reference"><strong>{$reference|escape:'html':'UTF-8'}</strong></span>
			<br/>- {l s='Order state' mod='oney'} : <span class="reference"><strong>{$statut|escape:'html':'UTF-8'}</strong></span>
			<br/>{l s='An email has been sent with this information.' mod='oney'}
			<br/>{l s='If you have questions, comments or concerns, please contact our' mod='oney'} <a href="{$link->getPageLink('contact', true)|escape:'html':'UTF-8'}">{l s='expert customer support team.' mod='oney'}</a>
		</p>
	</div>
{else}
	<p class="alert alert-danger">{l s='Votre commande sur %s n\'a pas été acceptée.' mod='oney' sprintf=$shop_name mod='oney'}</p>
	<div class="box order-confirmation">
		<h3 class="page-subheading">{l s='Votre commande sur n\'a pas été acceptée.' sprintf=$shop_name mod='oney'}</h3>
		<p>
			- {l s='Reference' mod='oney'} <span class="reference"> <strong>{$reference|escape:'html':'UTF-8'}</strong></span>
			<br/>{l s='Please, try to order again.' mod='oney'}
			<br/>{l s='If you have questions, comments or concerns, please contact our' mod='oney'} <a href="{$link->getPageLink('contact', true)|escape:'html':'UTF-8'}">{l s='expert customer support team.' mod='oney'}</a>
		</p>
	</div>
{/if}
<hr/>
