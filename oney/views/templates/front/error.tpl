{*
 * 3x 4x Oney Module version 1.1.21 for PrestaShop 1.6. Support contact : prestashop@itroom.fr
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/afl-3.0.php
 *
 * @author    ITroom (http://itroom.fr/)
 * @copyright ITroom
 * @license   https://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * @category  payment
 * @package   oney
 *}

{if version_compare($smarty.const._PS_VERSION_, '1.7', '<')}
  <div>
    <h3>{l s='An error occurred' mod='oney'}:</h3>
    <ul class="alert alert-danger">
      {foreach from=$errors item='error'}
        <li>{$error|escape:'htmlall':'UTF-8'}.</li>
      {/foreach}
    </ul>
  </div>
{else}
  {extends file="page.tpl"}

  {block name="content" append}
    <div>
      <h3>{l s='An error occurred' mod='oney'}:</h3>
      <ul class="alert alert-danger">
        {foreach from=$errors item='error'}
        <li>{$error|escape:'htmlall':'UTF-8'}.</li>
        {/foreach}
      </ul>
    </div>
  {/block}
{/if}