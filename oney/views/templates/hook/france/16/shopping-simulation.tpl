{*
 * 3x 4x Oney Module version 1.1.21 for PrestaShop 1.6. Support contact : prestashop@itroom.fr
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/afl-3.0.php
 *
 * @author    ITroom (http://itroom.fr/)
 * @copyright ITroom
 * @license   https://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * @category  payment
 * @package   oney
 *}

<div class="simulation_oney_price">
  {if $simulation->total_cost > 0}
    <span class="simulation_oney_info_price with_tax">
      <span class="simulation_oney_apport">{l s='Apport :' mod='oney'} {convertPrice price=$simulation->down_payment_amount}</span>
      <span class="simulation_oney_mensualite">{l s='+ %s mensualités :' sprintf=$simulation->instalments|@count mod='oney'}
        {convertPrice price=$simulation->price_instalments}
      </span>
      <span class="interrogation_bulle"><img src="{$module_dir|escape:'htmlall':'UTF-8'}views/img/interrogation.png"></span>
    </span>
    <span class="prix_financement">{l s='Dont coût du financement :' mod='oney'} {convertPrice price=$simulation->total_cost}</span>
  {else}
    <span class="simulation_oney_info_price">
      <span class="simulation_oney_montant">
        <span>{convertPrice price=$simulation->price_instalments}</span>
      </span>
      <span class="interrogation_bulle"><img src="{$module_dir|escape:'htmlall':'UTF-8'}views/img/interrogation.png"></span>
    </span>
  {/if}
</div>