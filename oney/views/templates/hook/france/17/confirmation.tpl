{*
 * 3x 4x Oney Module version 1.1.21 for PrestaShop 1.6. Support contact : prestashop@itroom.fr
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/afl-3.0.php
 *
 * @author    ITroom (http://itroom.fr/)
 * @copyright ITroom
 * @license   https://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * @category  payment
 * @package   oney
 *}

{block name='content' append}
	{capture name=path}{l s='Order confirmation' mod='oney'}{/capture}

	{if (isset($status) == true) && ($status == 'ok')}
		<p>{l s='Votre commande à bien été enregistrée.' sprintf=$shop_name mod='oney'}</p>

		<dl>
		    <dt>{l s='Amount' mod='oney'}</dt>
		    <dd><strong>{$total|escape:'htmlall':'UTF-8'}</strong></dd>
		    <dt>{l s='Reference' mod='oney'}</dt>
		    <dd><strong>{$reference|escape:'html':'UTF-8'}</strong></dd>
		    <dt>{l s='Order state' mod='oney'}</dt>
		    <dd><strong>{$statut|escape:'html':'UTF-8'}</strong></dd>
		</dl>

		<p>{l s='An email has been sent with this information.' mod='oney'}</p>
		<p>{l s='If you have questions, comments or concerns, please contact our' mod='oney'} <a href="{$link->getPageLink('contact', true)|escape:'html':'UTF-8'}">{l s='expert customer support team.' mod='oney'}</a></p>

	{else}
		<p>{l s='Votre commande sur n\'a pas été acceptée.' sprintf=$shop_name mod='oney'}</p>
		<p>
			- {l s='Reference' mod='oney'} <span
					class="reference"> <strong>{$reference|escape:'html':'UTF-8'}</strong></span>
			<br/><br/>{l s='Please, try to order again.' mod='oney'}
			<br/><br/>{l s='If you have questions, comments or concerns, please contact our' mod='oney'} <a
					href="{$link->getPageLink('contact', true)|escape:'html':'UTF-8'}">{l s='expert customer support team.' mod='oney'}</a>
		</p>
	{/if}
	<hr/>
{/block}