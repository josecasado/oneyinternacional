{*
 * 3x 4x Oney Module version 1.1.21 for PrestaShop 1.6. Support contact : prestashop@itroom.fr
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/afl-3.0.php
 *
 * @author    ITroom (http://itroom.fr/)
 * @copyright ITroom
 * @license   https://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * @category  payment
 * @package   oney
 *}

{if isset($methodPayment[$key]['simulation']) && is_object($methodPayment[$key]['simulation'])}
	<div class="simulation_oney-payment clearfix">
		<div class="simulation_oney-img_payment">
			<img src="/modules/oney/views/img/logo_payment_{$methodPayment[$key]['minimum_number_of_instalments'] + 1|escape:'htmlall':'UTF-8'}.png" alt="">
			{if isset($methodPayment[$key]['free_business_transaction']) && $methodPayment[$key]['free_business_transaction'] == 1}
				<span class="simulation_oney-paiement_sf">{l s='sans frais' mod='oney'}</span>
			{/if}
		</div>
		<div class="simulation_oney-info_payment">
			<p class="simulation_oney-apport_payment">{l s='Apport :' mod='oney'} {Tools::displayPrice($methodPayment[$key]['simulation']->down_payment_amount)|escape:'htmlall':'UTF-8'}</p>
			<p class="simulation_oney-taeg_payment">{l s='Dont coût du financement :' mod='oney'} {Tools::displayPrice($methodPayment[$key]['simulation']->total_cost)|escape:'htmlall':'UTF-8'}</p>
			{foreach item=instalment from=$methodPayment[$key]['simulation']->instalments key=k}
				{strip}
					<p class="simulation_oney-instalments_payment">
						{if $k == 0}
							{l s='- ' mod='oney'} {$k+1|escape:'htmlall':'UTF-8'}{l s='ère mensualité : ' mod='oney'}
						{else}
							{l s='- ' mod='oney'} {$k+1|escape:'htmlall':'UTF-8'}{l s='éme mensualité : ' mod='oney'}
						{/if}
						{Tools::displayPrice($instalment->instalment_amount)|escape:'htmlall':'UTF-8'}
					</p>
				{/strip}
			{/foreach}
		</div>
	</div>
{/if}
