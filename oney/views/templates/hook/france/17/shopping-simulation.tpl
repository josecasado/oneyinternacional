{*
 * 3x 4x Oney Module version 1.1.21 for PrestaShop 1.6. Support contact : prestashop@itroom.fr
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/afl-3.0.php
 *
 * @author    ITroom (http://itroom.fr/)
 * @copyright ITroom
 * @license   https://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * @category  payment
 * @package   oney
 *}

{if !isset($simulation->total_cost)}
  {assign var=total_cost_oney value='0'}
{else}
  {assign var=total_cost_oney value=$simulation->total_cost}
{/if}

<div class="simulation_oney_price{if isset($simulation->total_cost) && $simulation->total_cost > 0} payant{/if}">
  {if isset($simulation->total_cost) && $simulation->total_cost > 0}
    <span class="simulation_oney_info_price">
      <span class="simulation_oney_apport">{l s='Apport de' mod='oney'} {Tools::displayPrice($simulation->down_payment_amount)|escape:'htmlall':'UTF-8'}</span>
      <span class="simulation_oney_mensualite">{l s='+' mod='oney'} {$simulation->instalments|@count|escape:'htmlall':'UTF-8'} {l s='x' mod='oney'}
          {Tools::displayPrice($priceInstalments)|escape:'htmlall':'UTF-8'}
      </span>
      <span class="interrogation_bulle"><img src="{$module_dir|escape:'htmlall':'UTF-8'}views/img/interrogation.png"></span>
      <span class="cout_financement">{l s='Dont coût du financement :' mod='oney'} {Tools::displayPrice($simulation->total_cost)|escape:'htmlall':'UTF-8'}</span>
    </span>
  {else}
    <span class="simulation_oney_info_price">
      <span class="simulation_oney_montant">
        <span>{Tools::displayPrice($priceInstalments)|escape:'htmlall':'UTF-8'}</span>
      </span>
      <span class="interrogation_bulle"><img src="{$module_dir|escape:'htmlall':'UTF-8'}views/img/interrogation.png"></span>
    </span>
  {/if}
</div>