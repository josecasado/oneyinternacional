{*
 * 3x 4x Oney Module version 1.1.21 for PrestaShop 1.6. Support contact : prestashop@itroom.fr
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/afl-3.0.php
 *
 * @author    ITroom (http://itroom.fr/)
 * @copyright ITroom
 * @license   https://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * @category  payment
 * @package   oney
 *}

{if !isset($simulation->total_cost)}
  {assign var=total_cost_oney value='0'}
{else}
  {assign var=total_cost_oney value=$simulation->total_cost}
{/if}

<div class="simulation_oney_info" data-financement="{if isset($simulation->total_cost)}{$simulation->total_cost|escape:'htmlall':'UTF-8'}{else}0{/if}">
  {if isset($simulation->total_cost) && $simulation->total_cost > 0}
    <span class="simulation_oney_nb"><span class="bulle"><span>{$simulation->instalments|@count + 1|escape:'htmlall':'UTF-8'}</span><span>{l s='x' mod='oney'}</span></span></span>
    <div class="simulation_details">
      <span class="simulation_oney_apport">{l s='Apport :' mod='oney'}
        {Tools::displayPrice($simulation->down_payment_amount)|escape:'htmlall':'UTF-8'}</span>
      <span class="simulation_oney_mensualite">{l s='+' mod='oney'}
        {$simulation->instalments|@count|escape:'htmlall':'UTF-8'} {l s='mensualités de :' mod='oney'}
        <span>{Tools::displayPrice($priceInstalments)|escape:'htmlall':'UTF-8'}</span>
      </span>
      <span>
        {l s='Dont coût du financement :' mod='oney'} {Tools::displayPrice($simulation->total_cost)|escape:'htmlall':'UTF-8'}
        {l s='TAEG :' mod='oney'} {$simulation->nominal_annual_percentage_rate|escape:'htmlall':'UTF-8'|replace:'.':','}{l s='%' mod='oney'}
      </span>
    </div>
  {else}
    <span class="simulation_oney_nb"><span class="bulle"><span>{$simulation->instalments|@count + 1|escape:'htmlall':'UTF-8'}</span><span>{l s='x' mod='oney'}</span></span></span>
    <div class="simulation_details">
      <span class="simulation_oney_montant">
        {foreach item=instalment from=$simulation->instalments}{strip}
          {Tools::displayPrice($instalment->instalment_amount)|escape:'htmlall':'UTF-8'}
          {break}
        {/strip}{/foreach}
        <span>{l s='sans frais' mod='oney'}</span>
      </span>
      <span class="simulation_oney_apport">{l s='Apport :' mod='oney'} {Tools::displayPrice($simulation->down_payment_amount)|escape:'htmlall':'UTF-8'}</span>
      <span class="simulation_oney_mensualite">{l s='+' mod='oney'} {$simulation->instalments|@count|escape:'htmlall':'UTF-8'} {l s='mensualités de :' mod='oney'}
          {Tools::displayPrice($priceInstalments)|escape:'htmlall':'UTF-8'}
      </span>
      <span>
        {l s='Dont coût du financement :' mod='oney'} {Tools::displayPrice($simulation->total_cost)|escape:'htmlall':'UTF-8'}
        {l s='TAEG :' mod='oney'} {$simulation->nominal_annual_percentage_rate|escape:'htmlall':'UTF-8'|replace:'.':','}{l s='%' mod='oney'}
      </span>
    </div>
  {/if}
</div>