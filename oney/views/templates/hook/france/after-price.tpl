{*
 * 3x 4x Oney Module version 1.1.21 for PrestaShop 1.6. Support contact : prestashop@itroom.fr
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/afl-3.0.php
 *
 * @author    ITroom (http://itroom.fr/)
 * @copyright ITroom
 * @license   https://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * @category  payment
 * @package   oney
 *}

{if version_compare($smarty.const._PS_VERSION_, '1.7', '<')}
  {if $opcs|@count > 0}
    {if $hookName != 'unit_price'}
      <span class="oney_after_price oney_16">{strip}
        <span>{l s='ou payez en' mod='oney'}</span>
        <span class="after_price_content">
          <span class="bulle"><span>3</span><span>{l s='x' mod='oney'}</span></span>
          <span>{l s='ou' mod='oney'}</span>
          <span class="bulle"><span>4</span><span>{l s='x' mod='oney'}</span></span>
          {if isset($free_transaction) && $free_transaction == 1}
            <span class="sans_frais">{l s='sans frais' mod='oney'}</span>
          {/if}
         <span class="interrogation_bulle interrogation_product"><img src="{$module_dir|escape:'htmlall':'UTF-8'}views/img/interrogation.png"></span>
        </span>
      {/strip}</span>
    {/if}
  {/if}
{else}
  {if $opcs|@count > 0}
    <span class="oney_after_price{if version_compare($smarty.const._PS_VERSION_, '1.7', '>=')} after_oney{/if}">{strip}
      <span>{l s='ou payez en' mod='oney'}</span>
      <span class="after_price_content">
        <span class="bulle"><span>3</span><span>{l s='x' mod='oney'}</span></span>
        <span>{l s='ou' mod='oney'}</span>
        <span class="bulle"><span>4</span><span>{l s='x' mod='oney'}</span></span>
        {if isset($free_transaction) && $free_transaction == 1}
          <span class="sans_frais">{l s='sans frais' mod='oney'}</span>
        {/if}
        <span class="interrogation_bulle interrogation_product"><img src="{$module_dir|escape:'htmlall':'UTF-8'}views/img/interrogation.png"></span>
      </span>
    {/strip}</span>
  {/if}
{/if}