{*
 * 3x 4x Oney Module version 1.1.21 for PrestaShop 1.6. Support contact : prestashop@itroom.fr
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/afl-3.0.php
 *
 * @author    ITroom (http://itroom.fr/)
 * @copyright ITroom
 * @license   https://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * @category  payment
 * @package   oney
 *}

<div class="banner_oney banner_col">
    <div class="banner_body">
        <div class="banner_body_pay">
            <p class="payment">{l s='Payez en' mod='oney'}</p>
            <span class="bulle"><span>3</span><span>{l s='x' mod='oney'}</span></span>
            <span class="bulle"><span>4</span><span>{l s='x' mod='oney'}</span></span>
            <p class="sans_frais">{if isset($free_transaction) && $free_transaction == 1}{l s='sans frais' mod='oney'}{/if}</p>
        </div>
        <div class="banner_body_cart">
            <p>{l s='Par [1]Carte[/1] [1]bancaire[/1]' mod='oney' tags=['<span>']}</p>
        </div>
        <div class="banner_body_signature">
            <img src="{$module_dir|escape:'htmlall':'UTF-8'}views/img/logo_oney_banner.png" alt="{l s='Oney' mod='oney'}">
            <span class="interrogation_bulle"><img src="{$module_dir|escape:'htmlall':'UTF-8'}views/img/interrogation.png"></span>
        </div>
    </div>
</div>