{*
 * 3x 4x Oney Module version 1.1.21 for PrestaShop 1.6. Support contact : prestashop@itroom.fr
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/afl-3.0.php
 *
 * @author    ITroom (http://itroom.fr/)
 * @copyright ITroom
 * @license   https://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * @category  payment
 * @package   oney
 *}

<div class="row">
	<div class="col-xs-12">
		{foreach from=$methodPayment item=payment}
			<p class="payment_module" id="facilypay_payment_button">
				<a href="{$payment.url|escape:'htmlall':'UTF-8'}" title="{l s='Pay with my payment module' mod='oney'}">{strip}
					{if $payment.maximum_number_of_instalments == 2}
						<span class="logo_payement">
							<img src="{$module_dir|escape:'htmlall':'UTF-8'}views/img/logo_oney-financement_3{if isset($payment.free_business_transaction) && $payment.free_business_transaction == 1}_1{/if}_payement.png">
							{if isset($payment.free_business_transaction) && $payment.free_business_transaction == 1}
								<span class="simulation_oney-paiement_sf">{l s='sans frais' mod='oney'}</span>
							{/if}
						</span>
					{elseif $payment.maximum_number_of_instalments == 3}
						<span class="logo_payement">
							<img src="{$module_dir|escape:'htmlall':'UTF-8'}views/img/logo_oney-financement_4{if isset($payment.free_business_transaction) && $payment.free_business_transaction == 1}_1{/if}_payement.png">
							{if isset($payment.free_business_transaction) && $payment.free_business_transaction == 1}
								<span class="simulation_oney-paiement_sf">{l s='sans frais' mod='oney'}</span>
							{/if}
						</span>
					{else}
						<img src="{$module_dir|escape:'htmlall':'UTF-8'}views/img/logo_default.jpg">
					{/if}

					{$payment.customer_label|escape:'htmlall':'UTF-8'}

          <span class="simulation_oney-info_payment">
            <span class="simulation_oney-apport_payment">{l s='Apport :' mod='oney'} {convertPrice price=$payment.simulation->down_payment_amount}</span>
            <span class="simulation_oney-taeg_payment">{l s='Dont coût du financement :' mod='oney'} {convertPrice price=$payment.simulation->total_cost}</span>
            {foreach item=instalment from=$payment.simulation->instalments key=k}
              {strip}
                <span class="simulation_oney-instalments_payment">
                  {if $k == 0}
                    {l s='- ' mod='oney'} {$k+1|escape:'htmlall':'UTF-8'}{l s='ère mensualité : ' mod='oney'}
                  {else}
                    {l s='- ' mod='oney'} {$k+1|escape:'htmlall':'UTF-8'}{l s='éme mensualité : ' mod='oney'}
                  {/if}
                  {convertPrice price=$instalment->instalment_amount}
                </span>
              {/strip}
            {/foreach}
          </span> 
				{/strip}</a>
			</p>
		{/foreach}
	</div>
</div>
