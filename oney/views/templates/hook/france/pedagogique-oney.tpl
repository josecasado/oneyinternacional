{*
 * 3x 4x Oney Module version 1.1.21 for PrestaShop 1.6. Support contact : prestashop@itroom.fr
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/afl-3.0.php
 *
 * @author    ITroom (http://itroom.fr/)
 * @copyright ITroom
 * @license   https://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * @category  payment
 * @package   oney
 *}

<div class="pedagogique_oney">
  <div class="overlay_oney">
    <div class="header_pedagogique">
      <img src="{$module_dir|escape:'htmlall':'UTF-8'}views/img/logo_oney.png" alt="{l s='Oney' mod='oney'}">
      <div class="title_block" >
        {if isset($free_transaction) && $free_transaction == 1}
          <span>{l s='Paiement sans frais [1]par carte bancaire[/1]' mod='oney' tags=['<span>']}</span>
        {else}
          <span>{l s='Paiement [1]par carte bancaire[/1]' mod='oney' tags=['<span>']}</span>
        {/if}
      </div>
      {if version_compare($smarty.const._PS_VERSION_, '1.7', '<')}
        <div class="close_pedagogique"></div>
      {else}
        <div class="close_pedagogique oney_icons">
          <i class="material-icons">&#xe5cd;</i>
        </div>
      {/if}
    </div>

    <div class="content_pedagogique">
      <div class="step_nbr_pedagogique">
        <span class="nbr_pedagogique">{l s='1' mod='oney'}</span>
        <span class="nbr_pedagogique">{l s='2' mod='oney'}</span>
        <span class="nbr_pedagogique">{l s='3' mod='oney'}</span>
      </div>

      <div class="step_pedagogique">
        <div class="bloc_pedagogique">
          <div class="content_align">
            <p class="title_pedagogique">{l s='Choisissez' mod='oney'}</p>
            <div class="align-block">
              <div class="choix_pedagogique">
                <span class="bulle"><span>3</span><span>{l s='x' mod='oney'}</span></span>
                <span>{l s='ou' mod='oney'}</span>
                <span class="bulle"><span>4</span><span>{l s='x' mod='oney'}</span></span>
              </div>
              <p>{l s='Lorsque vous sélectionnez votre mode de paiement.' mod='oney'}</p>
              <img src="{$module_dir|escape:'htmlall':'UTF-8'}views/img/logo_cart_oney.png" class="logos_cart_oney" alt="">
            </div>
          </div>
        </div>
        <div class="bloc_pedagogique">
          <div class="content_align">
            <p class="title_pedagogique">{l s='Dites nous [1]tout[/1]' tags=['<span>'] mod='oney'}</p>
            <div class="align-block"><p>{l s='Facile et rapide, complétez le formulaire, sans fournir aucun document.' mod='oney'}</p></div>
          </div>
        </div>
        <div class="bloc_pedagogique">
          <div class="content_align">
            <p class="title_pedagogique">{l s='Et voilà !' mod='oney'}</p>
            <div class="align-block"><p>{l s='Vous avez une réponse immédiate.' mod='oney'}</p></div>
          </div>
        </div>
      </div>

      <div class="footer_pedagogique">
        {if isset($legalNotice) && $legalNotice}
          {if isset($legalNotice->text) && $legalNotice->text}
            <p>
              {foreach from=$listopc item=opc key=k}
                {assign var=fois value=$k+1}
                {l s='Valable pour tout achat de %1d€ à %2d€ en %3dx.' sprintf=[$opc.minimum|escape:'htmlall':'UTF-8', $opc.maximum|escape:'htmlall':'UTF-8', $fois|escape:'htmlall':'UTF-8'] mod='oney'}
              {/foreach}
              {if isset($free_transaction) && $free_transaction == 1}
                {l s='Exemple en 3 fois pour un achat de 150€, apport de 50€, puis 2 mensualités de 50€. Crédit sur 2 mois au TAEG fixe de 0%. Coût du financement 0€. Exemple en 4 fois pour un achat de 400€, apport de 100€, puis 3 mensualités de 100€. Crédit sur 3 mois au TAEG fixe de 0%. Coût du financement 0€.' mod='oney'}
              {else}
                {l s='Exemple en 3 fois pour un achat de 150€, apport de 52,18€, puis 2 mensualités de 50€. Crédit sur 2 mois au TAEG fixe de 19,26%. Coût du financement 2,18€ dans la limite de 15€ maximum. Exemple en 4 fois pour un achat de 400€, apport de 108,80€, puis 3 mensualités de 100€. Crédit sur 3 mois au TAEG fixe de 19,31%. Coût du financement 8,80€, dans la limite de 30€ maximum.' mod='oney'}
              {/if}
              {$legalNotice->text|escape:'htmlall':'UTF-8'}
            </p>
          {else}
            <p>{l s='Offre de financement avec apport obligatoire, réservée aux particuliers et valable pour tout achat de %1d€ à %2d€. Sous réserve d\'acceptaion par Oney Bank. Vous disposez d\'un délai de 14 jours pour renoncer à votre crédit. Oney Bank - SA au capital de 50 741 215€ - 40 Avenue de Flandre 59 170 Croix - [1]546 380 197 RCS Lille Métropole[/1] - n° Orias 07 023 261 www.orias.fr' tags=['<strong>']  sprintf=[$minimum_selling_price|escape:'htmlall':'UTF-8', $maximum_selling_price|escape:'htmlall':'UTF-8'] mod='oney'}</p>
          {/if}
        {else}
          <p>{l s='Offre de financement avec apport obligatoire, réservée aux particuliers et valable pour tout achat de %1d€ à %2d€. Sous réserve d\'acceptaion par Oney Bank. Vous disposez d\'un délai de 14 jours pour renoncer à votre crédit. Oney Bank - SA au capital de 50 741 215€ - 40 Avenue de Flandre 59 170 Croix - [1]546 380 197 RCS Lille Métropole[/1] - n° Orias 07 023 261 www.orias.fr' tags=['<strong>']  sprintf=[$minimum_selling_price|escape:'htmlall':'UTF-8', $maximum_selling_price|escape:'htmlall':'UTF-8'] mod='oney'}</p>
        {/if}
      </div>
    </div>
  </div>
  <div class="align_oney"></div>
</div>
