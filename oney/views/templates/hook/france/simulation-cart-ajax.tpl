{*
 * 3x 4x Oney Module version 1.1.21 for PrestaShop 1.6. Support contact : prestashop@itroom.fr
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/afl-3.0.php
 *
 * @author    ITroom (http://itroom.fr/)
 * @copyright ITroom
 * @license   https://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * @category  payment
 * @package   oney
 *}


{if isset($simulation) && is_object($simulation)}
  {if !isset($simulation->total_cost)}
    {assign var=total_cost_oney value='0'}
  {else}
    {assign var=total_cost_oney value=$simulation->total_cost}
  {/if}
	<div class="simulation_oney_financement clearfix{if version_compare($smarty.const._PS_VERSION_, '1.7', '<')} financement_fr_16{/if}" data-price="{$priceInstalments|escape:'htmlall':'UTF-8'}">
    {if $priceInstalments|escape:'htmlall':'UTF-8' > 0}
      <div class="simulation_oney_img">
        <span class="bulle"><span>{$simulation->instalments|@count + 1|escape:'htmlall':'UTF-8'}</span><span>{l s='x' mod='oney'}</span></span>
        {if isset($total_cost_oney) && $total_cost_oney == 0}
          <span class="simulation_oney_paiement_sf">{l s='sans frais' mod='oney'}</span>
        {/if}
      </div>
      {if version_compare($smarty.const._PS_VERSION_, '1.7', '<')}
        {include file="./16/shopping-simulation.tpl"}
      {else}
        {include file="./17/shopping-simulation.tpl"}
      {/if}
    {/if}
	</div>
{/if}