{*
 * 3x 4x Oney Module version 1.1.21 for PrestaShop 1.6. Support contact : prestashop@itroom.fr
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/afl-3.0.php
 *
 * @author    ITroom (http://itroom.fr/)
 * @copyright ITroom
 * @license   https://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * @category  payment
 * @package   oney
 *}

<div class="info_simulation">
	<input type="hidden" id="priceSimulation" value="{$priceProduct|escape:'htmlall':'UTF-8'}" />
	<input type="hidden" id="URLSimulation" value="{$base_uri|escape:'htmlall':'UTF-8'}modules/oney/ajax/simulation.php?token=5C95E717753C656DB7C76C9747EBF" />
	{foreach item=OPC key=k from=$listOPC}
		<input type="hidden" id="codeOPC-{$k|escape:'htmlall':'UTF-8'}" class="codeOPC" value="{$OPC.business_transaction_code|escape:'htmlall':'UTF-8'}"/>
	{/foreach}
</div>
<div class="simulation_oney_shopping_info{if version_compare($smarty.const._PS_VERSION_, '1.7', '>=')} simulation_oney_17{/if}"></div>