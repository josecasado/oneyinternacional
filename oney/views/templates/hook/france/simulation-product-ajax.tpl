{*
 * 3x 4x Oney Module version 1.1.21 for PrestaShop 1.6. Support contact : prestashop@itroom.fr
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/afl-3.0.php
 *
 * @author    ITroom (http://itroom.fr/)
 * @copyright ITroom
 * @license   https://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * @category  payment
 * @package   oney
 *}


{if isset($simulation) && is_object($simulation)}
	{if !isset($simulation->total_cost)}
		{assign var=total_cost_oney value='0'}
	{else}
		{assign var=total_cost_oney value=$simulation->total_cost}
	{/if}
	<div class="simulation_oney_more"></div>
	<div class="simulation_oney_financement clearfix">
		{if version_compare($smarty.const._PS_VERSION_, '1.7', '<')}
			{include file="./16/simulation.tpl"}
		{else}
			{include file="./17/simulation.tpl"}
		{/if}
	</div>
{/if}
