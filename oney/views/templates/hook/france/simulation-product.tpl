{*
 * 3x 4x Oney Module version 1.1.21 for PrestaShop 1.6. Support contact : prestashop@itroom.fr
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/afl-3.0.php
 *
 * @author    ITroom (http://itroom.fr/)
 * @copyright ITroom
 * @license   https://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * @category  payment
 * @package   oney
 *}

{if isset($simulation) && is_object($simulation)}
	<div id="simulation_oney_product"{if version_compare($smarty.const._PS_VERSION_, '1.7', '>=')} class="simulation_oney_right"{/if}>
		<div class="simulation_oney_header">
		{if version_compare($smarty.const._PS_VERSION_, '1.7', '<')}
			<div class="close_simulation close_16"></div>
		{else}
			<div class="close_simulation oney_icons">
			<i class="material-icons">&#xe5cd;</i>
			</div>
		{/if}
			<img src="{$module_dir|escape:'htmlall':'UTF-8'}views/img/logo_oney.png" alt="{l s='Oney' mod='oney'}">
			<div class="title_block">
				{if isset($free_transaction) && $free_transaction == 1}
					<span>{l s='Paiement [1]sans frais[/1] [2]par carte bancaire[/2]' mod='oney' tags=['<span class="sf">', '<span>']}</span>
				{else}
					<span>{l s='Paiement [1]par carte bancaire[/1]' mod='oney' tags=['<span>']}</span>
				{/if}
			</div>
		</div>
		<div class="simulation_oney_bouton">
			<p class="amount_simulation">{l s='Montant à financer' mod='oney'}
				{$simulation->payment_amount|escape:'htmlall':'UTF-8'}
			</p>
			<p class="monthly_simulation">{l s='Nombre de mensualités' mod='oney'}</p>
			<input type="hidden" id="priceSimulation" value="{$priceProduct|escape:'htmlall':'UTF-8'}"/>
			<input type="hidden" id="URLSimulation" value="{$base_uri|escape:'htmlall':'UTF-8'}modules/oney/ajax/simulation.php?token=5C95E717753C656DB7C76C9747EBF"/>
			{foreach item=OPC key=k from=$listOPC}
				<input type="hidden" id="codeOPC-{$k|escape:'htmlall':'UTF-8'}" class="codeOPC" value="{$OPC.business_transaction_code|escape:'htmlall':'UTF-8'}"/>
			{/foreach}

			{if !empty($listOPC[0].example)}
				<div>
					<p>{$listOPC[0].example|escape:'htmlall':'UTF-8'}<p>
				</div>
			{/if}
		</div>

		<div class="simulation_oney_content"></div>

		<div class="simulation_oney_mentions_legales">
		{if isset($legalNotice) && $legalNotice}
          {if isset($legalNotice->text) && $legalNotice->text}
            <p>{$legalNotice->text|escape:'htmlall':'UTF-8'}</p>
          {else}
            <p>{l s='Offre de financement avec apport obligatoire, réservée aux particuliers et valable pour tout achat de %1d€ à %2d€. Sous réserve d\'acceptaion par Oney Bank. Vous disposez d\'un délai de 14 jours pour renoncer à votre crédit. Oney Bank - SA au capital de 50 741 215€ - 40 Avenue de Flandre 59 170 Croix - [1]546 380 197 RCS Lille Métropole[/1] - n° Orias 07 023 261 www.orias.fr' tags=['<strong>']  sprintf=[$minimum_selling_price|escape:'htmlall':'UTF-8', $maximum_selling_price|escape:'htmlall':'UTF-8'] mod='oney'}</p>
          {/if}
        {else}
          <p>{l s='Offre de financement avec apport obligatoire, réservée aux particuliers et valable pour tout achat de %1d€ à %2d€. Sous réserve d\'acceptaion par Oney Bank. Vous disposez d\'un délai de 14 jours pour renoncer à votre crédit. Oney Bank - SA au capital de 50 741 215€ - 40 Avenue de Flandre 59 170 Croix - [1]546 380 197 RCS Lille Métropole[/1] - n° Orias 07 023 261 www.orias.fr' tags=['<strong>']  sprintf=[$minimum_selling_price|escape:'htmlall':'UTF-8', $maximum_selling_price|escape:'htmlall':'UTF-8'] mod='oney'}</p>
        {/if}
		</div>
	</div>
{/if}