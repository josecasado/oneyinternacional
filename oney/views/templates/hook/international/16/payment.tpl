{*
 * 3x 4x Oney Module version 1.1.21 for PrestaShop 1.6. Support contact : prestashop@itroom.fr
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/afl-3.0.php
 *
 * @author    ITroom (http://itroom.fr/)
 * @copyright ITroom
 * @license   https://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * @category  payment
 * @package   oney
 *}

<div class="row">
	<div class="col-xs-12">
		{foreach from=$methodPayment item=payment}
      {if !isset($payment.simulation->total_cost)}
        {assign var=total_cost_oney value='0'}
      {else}
        {assign var=total_cost_oney value=$payment.simulation->total_cost}
      {/if}
			<p class="payment_module international" id="facilypay_payment_button">
        {if $iso_code_oney == 'IT'}
          <span class="btn_pedagogique">
            <span class="interrogation_bulle"><img src="{$module_dir|escape:'htmlall':'UTF-8'}views/img/interrogation.png"></span>
          </span>
        {/if}
				<a href="{$payment.url|escape:'htmlall':'UTF-8'}" title="{l s='Pay with my payment module' mod='oney'}">{strip}

					{if $payment.simulation->instalments|@count == 2 || $payment.simulation->instalments|@count == 3}

            {if Configuration::get('FACILIPAY_INTERMEDIAIRE_CREDIT_'|cat:$iso_code_oney, null) === 'no_credit'}

              <span class="logo_payement">
                <img src="{$module_dir|escape:'htmlall':'UTF-8'}views/img/logo_oney-financement_{$payment.simulation->instalments|@count + 1}_payement.png">
              </span>

            {else}

              <span class="logo_payement{if isset($payment.free_business_transaction) && $payment.free_business_transaction == 1} logo_payement_free{/if}">
                {if isset($payment.free_business_transaction) && $payment.free_business_transaction == 1}
                  <img src="{$module_dir|escape:'htmlall':'UTF-8'}views/img/logo_oney_international-financement_{$payment.simulation->instalments|@count + 1}_payement.png">
                  <span class="simulation_oney-paiement_sf">{l s='no extra fees' mod='oney'}</span>
                {else}
                  <img src="{$module_dir|escape:'htmlall':'UTF-8'}views/img/logo_oney-financement_{$payment.simulation->instalments|@count + 1}_payement.png">
                {/if}
              </span>

            {/if}

					{else}
						<img src="{$module_dir|escape:'htmlall':'UTF-8'}views/img/logo_default.jpg">
					{/if}

					{$payment.customer_label|escape:'htmlall':'UTF-8'}
          {if Configuration::get('FACILIPAY_INTERMEDIAIRE_CREDIT_'|cat:$iso_code_oney, null) !== 'no_credit' && isset($payment.simulation->instalments)}
            <span class="simulation_oney-info_payment">

              {if $iso_code_oney == 'IT'}
                <span class="simulation_oney-apport_payment">{$payment.simulation->instalments|@count + 1|escape:'htmlall':'UTF-8'} {l s='installments from' mod='oney'} {convertPrice price=$payment.simulation->price_instalments}
                </span>

                {if isset($payment.simulation->administration_fee_amount) && $payment.simulation->administration_fee_amount > 0}
                  <span class="simulation_oney-taeg_payment">
                    {assign var=zero value='.'|explode:$payment.simulation->administration_fee_amount}
                    {assign var=commission_price value=Tools::displayPrice($payment.simulation->administration_fee_amount)}
                    {if $zero[1] == null}
                      {l s='plus a commission of %1d € applied to the first installment' sprintf=[$commission_price|escape:'htmlall':'UTF-8'] mod='oney'}
                    {else}
                      {l s='plus a commission of %1s applied to the first installment' sprintf=[$commission_price|escape:'htmlall':'UTF-8'|replace:'&euro;':'€'] mod='oney'}
                    {/if}
                  </span>
                {/if}
             {/if}

              <span class="simulation_oney-payment_instalments">
                {if $iso_code_oney != 'ES'}
                  <span class="simulation_oney-instalments_payment">
                    <span>{l s='- 1rst month : ' mod='oney'}</span>
                    {if $iso_code_oney == 'PT'}
                      <span class="price_oney">{Tools::displayPrice($methodPayment[$key]['simulation']->down_payment_amount)|escape:'htmlall':'UTF-8'}</span>
                    {else}
                      <span class="price_oney">{Tools::displayPrice($methodPayment[$key]['simulation']->down_payment_amount - $total_cost_oney)|escape:'htmlall':'UTF-8'} {l s='+' mod='oney'} {Tools::displayPrice($total_cost_oney)|escape:'htmlall':'UTF-8'}</span>
                    {/if}
                  </span>
                  {foreach item=instalment from=$payment.simulation->instalments key=k}
                    {strip}
                      <span class="simulation_oney-instalments_payment">
                        <span>{l s='-' mod='oney'} {$k+2|escape:'htmlall':'UTF-8'}{l s='nd month : ' mod='oney'}</span>
                        <span class="price_oney">{convertPrice price=$instalment->instalment_amount}</span>
                      </span>
                    {/strip}
                  {/foreach}
                {else}
                  <span class="simulation_oney-instalments_payment">
                    {l s='First payment of' mod='oney'} {convertPrice price=$payment.simulation->down_payment_amount} 
                    {l s='more' mod='oney'} {$payment.simulation->instalments|@count|escape:'htmlall':'UTF-8'} {l s='payments of' mod='oney'} {convertPrice price=$payment.simulation->price_instalments}
                  <span>
                  <span class="simulation_oney-instalments_payment">
                    {l s='Total owed:' mod='oney'} {convertPrice price=$total_cost_oney} ({l s='Including' mod='oney'} prix gestion {l s='management fees' mod='oney'}) {l s='Tin' mod='oney'} tin {l s='TAE:' mod='oney'} tae
                  <span>
                {/if}
              </span>
            </span>
          {/if}
				{/strip}</a>
			</p>
		{/foreach}
	</div>
</div>