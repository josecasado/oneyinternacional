{*
 * 3x 4x Oney Module version 1.1.21 for PrestaShop 1.6. Support contact : prestashop@itroom.fr
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/afl-3.0.php
 *
 * @author    ITroom (http://itroom.fr/)
 * @copyright ITroom
 * @license   https://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * @category  payment
 * @package   oney
 *}

{if !isset($simulation->total_cost)}
  {assign var=total_cost_oney value='0'}
{else}
  {assign var=total_cost_oney value=$simulation->total_cost}
{/if}


{if $iso_code_oney == 'IT'}
  <div class="simulation_oney_price">
    <span class="container_simulation_oney_price">
      <span class="simulation_oney_info_price">
        <span class="simulation_oney_montant">
          <span>{l s='%s' sprintf=$simulation->instalments|@count + 1 mod='oney'} {l s='installments from' mod='oney'} {convertPrice price=$priceInstalments}
          </span>
        </span>
      </span>
      {if isset($simulation->administration_fee_amount) && $simulation->administration_fee_amount > 0}
        <span class="financing_cost_oney">
          {assign var=zero value='.'|explode:$simulation->administration_fee_amount}
          {assign var=commission_price value=Tools::displayPrice($simulation->administration_fee_amount)}
          {if $zero[1] == null}
            {l s='plus a commission of %1d € applied to the first installment' sprintf=[$commission_price|escape:'htmlall':'UTF-8'] mod='oney'}
          {else}
            {l s='plus a commission of %1s applied to the first installment' sprintf=[$commission_price|escape:'htmlall':'UTF-8'|replace:'&euro;':'€'] mod='oney'}
          {/if}
        </span>
      {else}
        <span class="financing_cost_oney">
          {l s='no extra fees' mod='oney'}
        </span>
      {/if}
      {if isset($simulation->effective_annual_percentage_rate)}
        <span class="financing_cost_oney">{l s='TAEG :' mod='oney'} {$simulation->effective_annual_percentage_rate|escape:'htmlall':'UTF-8'}{l s='%' mod='oney'}</span>
      {/if}

      <span class="infos_financement">
        <span class="interrogation_bulle interrogation_product"><img src="{$module_dir|escape:'htmlall':'UTF-8'}views/img/interrogation.png"></span>
      </span>
      <span class="btn_pedagogique">
        <span class="interrogation_bulle"><img src="{$module_dir|escape:'htmlall':'UTF-8'}views/img/interrogation.png"></span>
      </span>
    </span>
  </div>
{else}
  <div class="simulation_oney_price">
    <span class="container_simulation_oney_price">
      {if isset($simulation->total_cost) && $simulation->total_cost > 0}
        <span class="simulation_oney_info_price">
          <span class="simulation_oney_mensualite">
              {convertPrice price=$simulation->price_instalments}
          </span>
        </span>
      {else}
        <span class="simulation_oney_info_price">
          <span class="simulation_oney_montant">
            <span>{convertPrice price=$simulation->price_instalments}</span>
          </span>
        </span>
      {/if}
      <span class="infos_financement">
        <span class="interrogation_bulle interrogation_product"><img src="{$module_dir|escape:'htmlall':'UTF-8'}views/img/interrogation.png"></span>
      </span>
      <span class="btn_pedagogique">
        <span class="interrogation_bulle"><img src="{$module_dir|escape:'htmlall':'UTF-8'}views/img/interrogation.png"></span>
      </span>
    </span>
  </div>
{/if}