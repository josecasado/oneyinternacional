{*
 * 3x 4x Oney Module version 1.1.21 for PrestaShop 1.6. Support contact : prestashop@itroom.fr
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/afl-3.0.php
 *
 * @author    ITroom (http://itroom.fr/)
 * @copyright ITroom
 * @license   https://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * @category  payment
 * @package   oney
 *}

{if isset($methodPayment[$key]['simulation']) && is_object($methodPayment[$key]['simulation'])}
  {if !isset($methodPayment[$key]['simulation']->total_cost)}
    {assign var=total_cost_oney value='0'}
  {else}
    {assign var=total_cost_oney value=$methodPayment[$key]['simulation']->total_cost}
  {/if}

    <div class="simulation_oney-payment international clearfix{if $iso_code_oney == 'PT'} portugal{/if}">
		<div class="simulation_oney-img_payment">
			<img src="/modules/oney/views/img/logo_payment_{$methodPayment[$key]['simulation']->instalments|@count + 1|escape:'htmlall':'UTF-8'}.png" alt="">
			{if isset($methodPayment[$key]['free_business_transaction']) && $methodPayment[$key]['free_business_transaction'] == 1 && Configuration::get('FACILIPAY_INTERMEDIAIRE_CREDIT_'|cat:$iso_code_oney, null) !== 'no_credit' && $iso_code_oney != 'ES'}
				<span class="simulation_oney-paiement_sf">{l s='no extra fees' mod='oney'}</span>
			{/if}
		</div>

    {if Configuration::get('FACILIPAY_INTERMEDIAIRE_CREDIT_'|cat:$iso_code_oney, null) !== 'no_credit' && isset($methodPayment[$key]['simulation']->instalments)}

      {if $iso_code_oney == 'IT'}
        <div class="info_simulation_oney-info_payment">
          <p class="simulation_oney-apport_payment">{$methodPayment[$key]['simulation']->instalments|@count|escape:'htmlall':'UTF-8' + 1} {l s='installments from' mod='oney'} {Tools::displayPrice($methodPayment[$key]['simulation']->price_instalments)|escape:'htmlall':'UTF-8'}</p>
          {if isset($methodPayment[$key]['simulation']->administration_fee_amount) && $methodPayment[$key]['simulation']->administration_fee_amount > 0}
            <p class="simulation_oney-taeg_payment">
              {assign var=zero value='.'|explode:$methodPayment[$key]['simulation']->administration_fee_amount}
              {if $zero[1] == null}
                {l s='plus a commission of %1d € applied to the first installment' sprintf=[Tools::displayPrice($methodPayment[$key]['simulation']->administration_fee_amount)|escape:'htmlall':'UTF-8'] mod='oney'}
              {else}
                {l s='plus a commission of %1s applied to the first installment' sprintf=[Tools::displayPrice($methodPayment[$key]['simulation']->administration_fee_amount)|escape:'htmlall':'UTF-8'] mod='oney'}
              {/if}
            </p>
          {/if}
          {if isset($methodPayment[$key]['simulation']->effective_annual_percentage_rate)}
            <p class="simulation_oney-taeg_payment">{l s='TAEG :' mod='oney'} {$methodPayment[$key]['simulation']->effective_annual_percentage_rate|escape:'htmlall':'UTF-8'}{l s='%' mod='oney'}</p>
          {/if}
          <span class="btn-3_4_fois_oney oney_icons">
            <span class="interrogation_bulle"><img src="{$module_dir|escape:'htmlall':'UTF-8'}views/img/interrogation.png"></span>
          </span>
        </div>
      {elseif $iso_code_oney != 'PT'}
        <div class="info_simulation_oney-info_payment">
          <p class="simulation_oney-apport_payment">
            {l s='First payment of' mod='oney'} {Tools::displayPrice($methodPayment[$key]['simulation']->down_payment_amount)|escape:'htmlall':'UTF-8'}
            {l s='more' mod='oney'} {$methodPayment[$key]['simulation']->instalments|@count|escape:'htmlall':'UTF-8'} {l s='payments of' mod='oney'} 
            {Tools::displayPrice($methodPayment[$key]['simulation']->price_instalments)|escape:'htmlall':'UTF-8'}
          </p>
          <p class="simulation_oney-taeg_payment{if $iso_code_oney == 'ES'} espagne{/if}">
            {l s='Total owed:' mod='oney'} {Tools::displayPrice($methodPayment[$key]['simulation']->payment_amount|escape:'htmlall':'UTF-8')} ({l s='Including' mod='oney'} {Tools::displayPrice($methodPayment[$key]['simulation']->total_cost|escape:'htmlall':'UTF-8')} {l s='management fees' mod='oney'}) 
            {l s='Tin' mod='oney'} {$methodPayment[$key]['simulation']->nominal_annual_percentage_rate|escape:'htmlall':'UTF-8'}% 
            <b class="tae">{l s='TAE:' mod='oney'} {$methodPayment[$key]['simulation']->effective_annual_percentage_rate|replace:'E+':''|escape:'htmlall':'UTF-8'}% </b>
          </p>
        </div>
      {/if}
      {if $iso_code_oney != 'ES'}
      <div class="simulation_oney-info_payment">
        <p class="simulation_oney-instalments_payment">
          <span>{l s='- 1rst month : ' mod='oney'}</span>
          {if $iso_code_oney == 'PT'}
            <span class="price_oney">{Tools::displayPrice($methodPayment[$key]['simulation']->down_payment_amount)|escape:'htmlall':'UTF-8'}</span>
          {else}
            <span class="price_oney">{Tools::displayPrice($methodPayment[$key]['simulation']->down_payment_amount - $total_cost_oney)|escape:'htmlall':'UTF-8'} {l s='+' mod='oney'} {Tools::displayPrice($total_cost_oney)|escape:'htmlall':'UTF-8'}</span>
          {/if}
        </p>
        {foreach item=instalment from=$methodPayment[$key]['simulation']->instalments key=k}
          {strip}
            <p class="simulation_oney-instalments_payment">
              <span>{l s='-' mod='oney'} {$k+2|escape:'htmlall':'UTF-8'}{l s='nd month : ' mod='oney'} </span>
              <span class="price_oney">{Tools::displayPrice($instalment->instalment_amount)|escape:'htmlall':'UTF-8'}</span>
            </p>
          {/strip}
        {/foreach}
      </div>
      {/if}
    {/if}
	</div>
{/if}
