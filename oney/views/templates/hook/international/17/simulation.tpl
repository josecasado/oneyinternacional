{*
 * 3x 4x Oney Module version 1.1.21 for PrestaShop 1.6. Support contact : prestashop@itroom.fr
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/afl-3.0.php
 *
 * @author    ITroom (http://itroom.fr/)
 * @copyright ITroom
 * @license   https://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * @category  payment
 * @package   oney
 *}

{if !isset($simulation->total_cost)}
    {assign var=total_cost_oney value='0'}
{else}
    {assign var=total_cost_oney value=$simulation->total_cost}
{/if}
{if !isset($simulation->effective_annual_percentage_rate)}
    {assign var=effective_annual_percentage_rate value='0'}
{else}
    {assign var=effective_annual_percentage_rate value=$simulation->effective_annual_percentage_rate}
{/if}

<div class="simulation_oney_info{if $iso_code_oney == 'ES'} espagne{/if}" data-financement="{if isset($simulation->total_cost)}{$simulation->total_cost|escape:'htmlall':'UTF-8'}{else}0{/if}">

    {if $iso_code_oney == 'IT'}


          <span class="simulation_oney_montant">
            <span>{Tools::displayPrice($priceInstalments)|escape:'htmlall':'UTF-8'}</span>
          </span>
          {if isset($simulation->administration_fee_amount) && $simulation->administration_fee_amount > 0}
            <span class="simulation_oney_mensualite">
              {assign var=zero value='.'|explode:$simulation->administration_fee_amount}
              {if $zero[1] == null}
                {l s='plus a commission of %1d € applied to the first installment' sprintf=[Tools::displayPrice($simulation->administration_fee_amount)|escape:'htmlall':'UTF-8'] mod='oney'}
              {else}
                {l s='plus a commission of %1s applied to the first installment' sprintf=[Tools::displayPrice($simulation->administration_fee_amount)|escape:'htmlall':'UTF-8'] mod='oney'}
              {/if}
            </span>
          {else}
            <span class="simulation_oney_mensualite">
              {l s='no extra fees' mod='oney'}
            </span>
          {/if}

          <span class="total_simulation_oney">{l s='Total :' mod='oney'} <span>{Tools::displayPrice($simulation->payment_amount + $simulation->total_cost)|escape:'htmlall':'UTF-8'}</span></span>
            
          <span>{l s='TAE :' mod='oney'} {$simulation->effective_annual_percentage_rate|escape:'htmlall':'UTF-8'}{l s='%' mod='oney'}</span>



    {else}
      {if isset($simulation->total_cost) && $simulation->total_cost > 0}
        {if $iso_code_oney == 'PT'}
          <span class="simulation_oney_montant">{l s='Contribution' mod='oney'}
            <span>{Tools::displayPrice($simulation->down_payment_amount)|escape:'htmlall':'UTF-8'}</span>
          </span>
          <span class="simulation_oney_mensualite">{l s='+' mod='oney'} {$simulation->instalments|@count|escape:'htmlall':'UTF-8'}{l s='x' mod='oney'}
              {Tools::displayPrice($priceInstalments)|escape:'htmlall':'UTF-8'}
          </span>
          <span class="total_simulation_oney">{l s='Total :' mod='oney'} <span>{Tools::displayPrice($simulation->payment_amount + $simulation->total_cost)|escape:'htmlall':'UTF-8'}</span></span>
          {if $iso_code_oney == 'PT'}
              <span><span class="taeg"><b>{l s='TAE :' mod='oney'} {l s='0,0%' mod='oney'}</b></span><span>{l s='TAN :' mod='oney'} {l s='0,00%' mod='oney'}</span></span>
              <span>{l s='MTIC:' mod='oney'} {Tools::displayPrice($simulation->payment_amount + $simulation->total_cost)|escape:'htmlall':'UTF-8'}</span>
          {else}
              <span>{l s='TAE :' mod='oney'} {$simulation->effective_annual_percentage_rate|escape:'htmlall':'UTF-8'}{l s='%' mod='oney'}</span>
          {/if}
          <span class="total_interest_oney">{l s='Interest :' mod='oney'} <span>{Tools::displayPrice($simulation->total_cost)|escape:'htmlall':'UTF-8'}</span></span>
        {else}
          <span class="simulation_oney_montant">
            <span class="first">{l s='First payment' mod='oney'}</span>
            <span>{Tools::displayPrice($simulation->down_payment_amount)|escape:'htmlall':'UTF-8'}</span>
            <span class="other">{l s='Plus %1d payments' sprintf=[$simulation->instalments|escape:'htmlall':'UTF-8'|count] mod='oney'}</span>
            <span>{Tools::displayPrice($priceInstalments)|escape:'htmlall':'UTF-8'}</span>
          </span>
          <span class="total_simulation_oney">{l s='Total :' mod='oney'} <span>{Tools::displayPrice($simulation->payment_amount + $simulation->total_cost)|escape:'htmlall':'UTF-8'}</span></span>
          <span class="tae">{l s='TAE :' mod='oney'} {$simulation->effective_annual_percentage_rate|replace:'E+':''|escape:'htmlall':'UTF-8'}{l s='%' mod='oney'}</span>
          <span class="tin">{l s='TIN :' mod='oney'} {$simulation->nominal_annual_percentage_rate|escape:'htmlall':'UTF-8'}{l s='%' mod='oney'}</span>
        {/if}
      {else}
        <span class="simulation_oney_montant">
          {if $iso_code_oney == 'ES'}
            <span class="first">{l s='First payment' mod='oney'}</span>
            <span>{Tools::displayPrice($priceInstalments)|escape:'htmlall':'UTF-8'}</span>
            <span class="other">{l s='Plus %1d payments' sprintf=[$simulation->instalments|escape:'htmlall':'UTF-8'|count] mod='oney'}</span>
            <span>{Tools::displayPrice($priceInstalments)|escape:'htmlall':'UTF-8'}</span>
          {else}
            <span> {Tools::displayPrice($priceInstalments)|escape:'htmlall':'UTF-8'}</span>
          {/if}
        </span>
        <span class="total_simulation_oney">{l s='Total :' mod='oney'} <span>{Tools::displayPrice($simulation->payment_amount + $total_cost_oney)|escape:'htmlall':'UTF-8'}</span></span>
        {if $iso_code_oney == 'PT'}
            <span><span class="taeg"><b>{l s='TAE :' mod='oney'} {l s='0,0%' mod='oney'}</b></span><span class="tan">{l s='TAN :' mod='oney'} {l s='0,00%' mod='oney'}</span></span>
            <span>{l s='MTIC:' mod='oney'} {Tools::displayPrice($simulation->payment_amount + $total_cost_oney)|escape:'htmlall':'UTF-8'}</span>
            <span class="total_interest_oney">{l s='Interest :' mod='oney'} <span>{Tools::displayPrice($total_cost_oney)|escape:'htmlall':'UTF-8'}</span></span>
        {else}
            <span class="tae">{l s='TAE :' mod='oney'} {$effective_annual_percentage_rate|escape:'htmlall':'UTF-8'}{l s='%' mod='oney'}</span>
            <span class="tin">{l s='TIN :' mod='oney'} {$effective_annual_percentage_rate|escape:'htmlall':'UTF-8'}{l s='%' mod='oney'}</span>
        {/if}
      {/if}
    {/if}




</div>