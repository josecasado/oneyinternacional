{*
 * 3x 4x Oney Module version 1.1.21 for PrestaShop 1.6. Support contact : prestashop@itroom.fr
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/afl-3.0.php
 *
 * @author    ITroom (http://itroom.fr/)
 * @copyright ITroom
 * @license   https://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * @category  payment
 * @package   oney
 *}

{if Configuration::get('FACILIPAY_INTERMEDIAIRE_CREDIT_'|cat:$iso_code_oney, null) === 'no_credit'}
  {if $opcs|@count > 0}
    {if version_compare($smarty.const._PS_VERSION_, '1.7', '>=')}
      <div class="no_credit_oney">
        <a href="{l s='Link_no_credit_intermediary' mod='oney'}" target="_blanc">
          <span class="after_price-3_4_fois_oney international">{strip}
            <span class="after_price_content-3_4_fois_oney">
              <img src="{$module_dir|escape:'htmlall':'UTF-8'}views/img/logo_3-4-fois.png" alt="{l s='3x 4x oney' mod='oney'}">
            </span>
          {/strip}</span>
        </a>
      </div>
    {else}
      {if $hookName != 'unit_price'}
        <div class="no_credit_oney">
          <a href="{l s='Link_no_credit_intermediary' mod='oney'}" target="_blanc">
            <span class="after_price-3_4_fois_oney international">{strip}
              <span class="after_price_content-3_4_fois_oney">
                <img src="{$module_dir|escape:'htmlall':'UTF-8'}views/img/logo_3-4-fois.png" alt="{l s='3x 4x oney' mod='oney'}">
              </span>
            {/strip}</span>
          </a>
        </div>
      {/if}
    {/if}
  {/if}

{else}
  {if $opcs|@count > 0}
    {if isset($inProduct) && $inProduct}
      {if $hookName != 'unit_price'}
        <div class="info_simulation">
          <input type="hidden" id="priceSimulation" value="{$priceProduct|escape:'htmlall':'UTF-8'}" />
          <input type="hidden" id="URLSimulation" value="{$base_uri|escape:'htmlall':'UTF-8'}modules/oney/ajax/simulation.php?token=5C95E717753C656DB7C76C9747EBF" />
          {foreach item=OPC key=k from=$opcs}
            <input type="hidden" id="codeOPC-{$k|escape:'htmlall':'UTF-8'}" class="codeOPC" value="{$OPC.business_transaction_code|escape:'htmlall':'UTF-8'}"/>
          {/foreach}
        </div>
        <span class="oney_after_price{if version_compare($smarty.const._PS_VERSION_, '1.7', '>=')} after_oney{/if}{if $iso_code_oney == 'IT' && version_compare($smarty.const._PS_VERSION_, '1.7', '<')} italy_16{/if}">{strip}
          <span>{l s='or pay in' mod='oney'}</span>
          <span class="after_price_content">
            {if $iso_code_oney != 'ES'}
              <span class="bulle"><span>3</span><span>{l s='x' mod='oney'}</span></span>
              <span>{l s='or' mod='oney'}</span>
              <span class="bulle"><span>4</span><span>{l s='x' mod='oney'}</span></span>
              {if $iso_code_oney == 'IT'}
                <span>{l s='con carta di credito' mod='oney'}</span>
              {/if}
              {if isset($free_transaction) && $free_transaction == 1 && $iso_code_oney != 'PT'}
                <span class="sans_frais">{l s='no charge' mod='oney'}</span>
              {/if}
            {else}
              {foreach item=OPC key=k from=$opcs}
                <span class="bulle"><span>{$OPC.minimum_number_of_instalments|escape:'htmlall':'UTF-8'}</span><span>{l s='x' mod='oney'}</span></span>
              {/foreach}
              <span>{l s='times' mod='oney'}</span>
            {/if}
            <span class="interrogation_bulle interrogation_product"><img src="{$module_dir|escape:'htmlall':'UTF-8'}views/img/interrogation.png"></span>
          </span>
        {/strip}</span>
      {/if}
    {else}
      {if version_compare($smarty.const._PS_VERSION_, '1.7', '<')}
        {if $hookName != 'unit_price'}
          <span class="after_price-3_4_fois_oney international">{strip}
            <span>{l s='Pay in' mod='oney'}</span>
            <span class="after_price_content-3_4_fois_oney">
              <img src="{$module_dir|escape:'htmlall':'UTF-8'}views/img/logo_3-4-fois.png" alt="{l s='3x 4x oney' mod='oney'}">
              <span class="infos_financement-3_4_fois_oney"></span>
              {if isset($free_transaction) && $free_transaction == 1}
                <span class="paiement_sf-3_4_fois_oney">{l s='no extra fees' mod='oney'}</span>
              {/if}
            </span>
          {/strip}</span>
        {/if}
      {else}
        <span class="after_price-3_4_fois_oney international">{strip}
          <span>{l s='Pay in' mod='oney'}</span>
          <span class="after_price_content-3_4_fois_oney">
            <img src="{$module_dir|escape:'htmlall':'UTF-8'}views/img/logo_3-4-fois.png" alt="{l s='3x 4x oney' mod='oney'}">
            <span class="infos_financement-3_4_fois_oney oney_icons">
              <i class="material-icons">&#xe887;</i>
            </span>
            {if isset($free_transaction) && $free_transaction == 1}
              <span class="paiement_sf-3_4_fois_oney">{l s='no extra fees' mod='oney'}</span>
            {/if}
          </span>
        {/strip}</span>
      {/if}
    {/if}
  {/if}
{/if}