{*
 * 3x 4x Oney Module version 1.1.21 for PrestaShop 1.6. Support contact : prestashop@itroom.fr
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/afl-3.0.php
 *
 * @author    ITroom (http://itroom.fr/)
 * @copyright ITroom
 * @license   https://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * @category  payment
 * @package   oney
 *}

<div class="banner_oney banner_col international country_{$iso_code_oney|escape:'htmlall':'UTF-8'}">
    <div class="banner_body">
        <div class="banner_body_pay{if $iso_code_oney == 'IT'} italy{/if}">
            <p class="payment">{l s='Pay in' mod='oney'}</p>
            <span class="bulle"><span>3</span><span>{l s='x' mod='oney'}</span></span>
            <span class="bulle"><span>4</span><span>{l s='x' mod='oney'}</span></span>
            {if $iso_code_oney == 'ES'}
                <span class="bulle"><span>6</span><span>{l s='x' mod='oney'}</span></span>
                <span class="bulle"><span>10</span><span>{l s='x' mod='oney'}</span></span>
                <span class="bulle"><span>12</span><span>{l s='x' mod='oney'}</span></span>
            {/if}
            <p class="sans_frais">{if isset($free_transaction) && $free_transaction == 1}{l s='sans frais' mod='oney'}{/if}</p>
        </div>
        <div class="banner_body_cart">
            <p>{l s='By [1]Credit[/1] [1]card[/1]' mod='oney' tags=['<span>']}</p>
        </div>
        <div class="banner_body_signature">
            <img src="{$module_dir|escape:'htmlall':'UTF-8'}views/img/logo_oney_banner.png" alt="{l s='Oney' mod='oney'}">
            <span class="interrogation_bulle"><img src="{$module_dir|escape:'htmlall':'UTF-8'}views/img/interrogation.png"></span>
        </div>
    </div>
    {if $iso_code_oney == 'PT'}
        <div class="banner_mentions">
            <p>
                <span class="taeg_pedago">{l s='TAEG de 0,0' mod='oney'}%</span> 
                {l s='calculada com base na TAN 0,00%2s. Crédito Pessoal para montantes de financiamento entre %3d€ a %4d€, pagos em 3 ou 4 prestações com cartão de débito ou de crédito das redes Visa ou Mastercard, sem juros ou outros encargos.' sprintf=['%', $minimum_selling_price|escape:'htmlall':'UTF-8', $maximum_selling_price|escape:'htmlall':'UTF-8'] mod='oney'}
                <br>
                {l s='Adesão ao crédito sujeito à aprovação. Informe-se junto do Oney Bank – Sucursal em Portugal em oney.pt.' mod='oney'}
                {if Configuration::get('FACILIPAY_INTERMEDIAIRE_CREDIT_'|cat:$iso_code_oney, null) !== 'no_credit'}
                    {if Configuration::get('FACILIPAY_INTERMEDIATION_TYPE_PT') == "linked"}
                        {assign var=exclusivity value="em regime de exclusividade"}
                    {else}
                        {assign var=exclusivity value="sem exclusividade"}
                    {/if}
                    <br>
                    {l s='O/A %1s atua como intermediário de crédito a título acessório %2s.' sprintf=[Configuration::get('FACILIPAY_MERCHANT_NAME_PT'), $exclusivity] mod='oney'}
                    <br>
                    {l s='Informação referente à prestação de serviços de intermediação de crédito disponível no rodapé desta página.' mod='oney'}
                {/if}
            </p>
        </div>
    {/if}
</div>