{*
 * 3x 4x Oney Module version 1.1.21 for PrestaShop 1.6. Support contact : prestashop@itroom.fr
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/afl-3.0.php
 *
 * @author    ITroom (http://itroom.fr/)
 * @copyright ITroom
 * @license   https://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * @category  payment
 * @package   oney
 *}

<div class="legal_notice_italy">
  <p class="title_legal_notice">{l s='Condizioni necessarie per richiedere 3X 4X Oney:' mod='oney'}</p>
  <ul>
    <li>{l s='Soluzione di finanziamento riservata a persone fisiche maggiorenni e residenti in Italia;' mod='oney'}</li>
    <li>{l s='utilizzare una carta di credito o debito, Visa o MasterCard, emessa da un intermediario bancario / finanziario italiano, ad eccezione di tutte le carte e di credito virtuali, prepagate e ricaricabili;' mod='oney'}</li>
    <li>{l s='La carta di credito deve avere una validità residua superiore ad almeno due mesi rispetto alla durata massima del finanziamento 3x 4x Oney prescelto;' mod='oney'}</li>
    {foreach from=$listopc item=opc key=k}
      <li>{l s='Per %1dx Oney l’importo finanziabile di €%2d a €%3d.' sprintf=[$k|escape:'htmlall':'UTF-8', $opc.minimum|escape:'htmlall':'UTF-8', $opc.maximum|escape:'htmlall':'UTF-8'] mod='oney'}</li>
    {/foreach}
  </ul>

  <div class="logo_legal_notice_italy">
    <img src="{$base_uri|escape:'htmlall':'UTF-8'}modules/oney/views/img/logo_oney-financement_3_payement.png" alt="{l s='3xOney' mod='oney'}">
  </div>

  <p>{l s='È una soluzione di finanziamento rateale il cui importo iniziale, pari alla somma della prima rata e della commissione stabilita da Oney Bank SA (Oney) per l’utilizzo di 3xOney, è addebitato ovvero pagato mediante gli strumenti di pagamento espressamente accettati da Oney a seguito del buon esito dell’ordine d’acquisto ovvero della prenotazione dei beni e/o servizi eseguiti, mentre il pagamento dell’importo residuo verrà addebitato sulla carta di credito in due rate di pari importo ovvero pagato mediante gli strumenti di pagamento espressamente accettati da Oney, nei due mesi successivi alla data di esecuzione dell’acquisto effettuato ovvero della prenotazione dei beni e/o servizi eseguiti con 3xOney. Le commissioni previste da Oney per l’utilizzo del servizio sono indicate nei paragrafi successivi.' mod='oney'}<p>
  <p>{l s='Esempio: per un ordine di acquisto di € 300 effettuato in data 21/04/2019 con 3xOney, il cliente pagherà a seguito del buon esito dell’ordine di acquisto del bene un importo iniziale pari a €104 (di cui €100 come prima rata e €4 di commissione), e due rate da €100 ciascuna, addebitate il 21/05/2019 e il 21/06/2019, per una durata complessiva di 2 mesi.' mod='oney'}</p>

  <div class="logo_legal_notice_italy">
    <img src="{$base_uri|escape:'htmlall':'UTF-8'}modules/oney/views/img/logo_oney-financement_4_payement.png" alt="{l s='4xOney' mod='oney'}">
  </div>

  <p>{l s='È una soluzione di finanziamento rateale il cui importo iniziale, pari alla somma della prima rata e della commissione stabilita da Oney per l’utilizzo di 4xOney, è addebitato ovvero pagato mediante gli strumenti di pagamento espressamente accettati da Oney, a seguito del buon esito dell’ordine d’acquisto ovvero della prenotazione dei beni e/o servizi eseguiti, mentre il pagamento dell’importo residuo verrà addebitato sulla carta di credito in tre rate di pari importo ovvero pagato mediante gli strumenti di pagamento espressamente accettati da Oney, nei tre mesi successivi alla data di esecuzione dell’acquisto effettuato ovvero della prenotazione dei beni e/o servizi eseguiti con 4xOney. Le commissioni previste da Oney per l’utilizzo del servizio sono indicate nei paragrafi successivi.' mod='oney'}</p>
  <p>{l s='Esempio: per un ordine di acquisto di €300 effettuato in data 21/04/2019 con 4xOney, il cliente pagherà a seguito del buon esito dell’ordine di acquisto del bene un importo iniziale di €81 (di cui €75 come prima rata e €6 di commissione) e tre rate da €75 ciascuna, addebitate il 21/05/2019, il 21/06/2019 e il 21/07/2019 per una durata complessiva di 3 mesi.' mod='oney'}</p>


  <p class="title_legal-notice">{l s='Commissioni applicate al cliente' mod='oney'}</p>
  <p>{l s='In caso di utilizzo di 3x Oney il cliente è tenuto a corrispondere a Oney esclusivamente una commissione di gestione, il cui importo varia a seconda dell’importo dell’acquisto effettuato dallo stesso cliente e della durata.' mod='oney'}</p>
  <p>{l s='Le commissioni applicate da Oney in caso di 3x sono suddivise nei seguenti scaglioni: €1,20 per importo compreso tra €100 a €149,99; €2 per importo compreso tra €150 a €249,99; €3,30 per importo compreso tra €250 a €299,99; €4 per importo compreso tra €300 a €449,99; €6 per un importo compreso tra €450 e €649,99; €8,50 per un importo compreso tra €650 e €799,99; €10,50 per un importo compreso tra €800 e €1.199,99; €15 per un importo compreso tra €1.200 e €2.500.' mod='oney'}</p>
  <p>{l s='Le commissioni applicate da Oney in caso di 4x sono suddivise nei seguenti scaglioni: €1,80 per importo compreso tra €100 a €149,99; €3 per importo compreso tra €150 a €249,99; €5 per importo compreso tra €250 a €299,99; €6 per importo compreso tra €300 a €449,99; €9 per un importo compreso tra €450 e €649,99; €13 per un importo compreso tra €650 e €799,99; €16 per un importo compreso tra €800 e €1.199,99; €20 per un importo compreso tra €1.200 e €2.500.' mod='oney'}</p>

  <p>{l s='In caso di acquisto contestuale di più beni, l’importo della commissione applicata sarà quello corrispondente allo scaglione commissionale nel quale ricade la somma degli acquistati effettuati.' mod='oney'}</p>
  <p>{l s='Sulla base degli accordi commerciali conclusi tra il sito di vendita e Oney, 3x 4x Oney potrà essere offerto alla clientela senza l’applicazione delle commissioni sopra indicate.' mod='oney'}</p>
  <p>{l s='[1]Costi applicati in caso di inadempimento:[/1] il cliente dovrà corrispondere a Oney un importo di €6, ogniqualvolta una delle rate di 3x4xOney rimanga impagata per più di 7 giorni dalla data di addebito del pagamento della stessa, fatto salvo il caso in cui tale costo sia già stato applicato da Oney per una precedente rata 3x4xOney che risulti attualmente impagata.' tags=['<strong>'] mod='oney'}</p>

  <p>{l s='Qualora il mancato pagamento di una rata 3x4xOney si protragga per più di 30 giorni dalla data di addebito del pagamento del relativo importo, il cliente sarà, inoltre, tenuto a corrispondere a Oney una somma pari al 15,5% degli importi complessivamente scaduti e impagati, a cui si aggiungeranno quelli relativi alle rate successive nel caso in cui anche queste ultime risultino scadute e impagate.' mod='oney'}</p>

  <p>{l s='3x 4x Oney è una dilazione di pagamento che non ricade nell’ambito di applicazione del credito ai consumatori di cui al Titolo VI, Capo II TUB, offerta da Oney Bank SA., capitale € 50 741 215 - 40 Avenue de Flandre 59 170 Croix - RCS Lille, Metropole - 546 380 197 - n° Orias 07 023 261, che opera in Italia in regime di libera prestazione di servizi.' mod='oney'}</p>

  <p>{l s='L’utilizzo di 3x4xOney è riservato alle persone fisiche maggiorenni, per importi finanziabili compresi tra un minimo di €%d1 e un massimo di €%d2 con rimborsi rateali fino ad un massimo di 90 giorni ed è soggetto alla preventiva approvazione di Oney Bank SA. Oney Bank SA. prevede l’applicazione di una commissione fissa per l’utilizzo di 3x4xOney.' sprintf=[$minimum_selling_price|escape:'htmlall':'UTF-8', $maximum_selling_price|escape:'htmlall':'UTF-8'] mod='oney'}

  <p>{l s='Il cliente ha 14 giorni di tempo dalla data di conclusione di ciascun finanziamento per rinunciarvi e esercitare il diritto di recesso, senza spese e costi. Documenti informativi disponibili su [1]www.oney.it[/1]' tags=['<a href="https://www.oney.it/" target="_blank">'] mod='oney'}</p>
</div>