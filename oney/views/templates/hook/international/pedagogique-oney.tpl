{*
 * 3x 4x Oney Module version 1.1.20 for PrestaShop 1.6. Support contact : prestashop@itroom.fr
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/afl-3.0.php
 *
 * @author    ITroom (http://itroom.fr/)
 * @copyright ITroom
 * @license   https://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * @category  payment
 * @package   oney
 *}

<div class="pedagogique_oney international{if isset($payment_pedagogic) && $payment_pedagogic} payment_pedagogic{/if}{if $iso_code_oney == 'IT'} italy{/if}">
  <div class="overlay_oney">
    <div class="header_pedagogique">
      <img src="{$module_dir|escape:'htmlall':'UTF-8'}views/img/logo_oney.png" alt="{l s='Oney' mod='oney'}">
      <div class="title_block" >
        {if isset($free_transaction) && $free_transaction == 1 && $iso_code_oney != 'PT'}
          <span>{l s='No extra fees [1]by credit card[/1]' mod='oney' tags=['<span>']}</span>
        {else}
          <span>{l s='Payment [1]by credit card[/1]' mod='oney' tags=['<span>']}</span>
        {/if}
      </div>
      {if version_compare($smarty.const._PS_VERSION_, '1.7', '<')}
        <div class="close_pedagogique"></div>
      {else}
        <div class="close_pedagogique oney_icons">
          <i class="material-icons">&#xe5cd;</i>
        </div>
      {/if}
    </div>
    <div class="content_pedagogique">
      <div class="step_nbr_pedagogique">
        <span class="nbr_pedagogique">{l s='1' mod='oney'}</span>
        <span class="nbr_pedagogique">{l s='2' mod='oney'}</span>
        <span class="nbr_pedagogique">{l s='3' mod='oney'}</span>
      </div>
      <div class="step_pedagogique">
        {if $iso_code_oney != 'IT'}
          <div class="bloc_pedagogique no-italy">
            <div class="content_align">
              <p class="title_pedagogique">{l s='Choose' mod='oney'}</p>
              {if $iso_code_oney == 'PT'}
                <img src="{$module_dir|escape:'htmlall':'UTF-8'}views/img/logo_3x_4x_sem_custos.png" alt="{l s='Oney' mod='oney'}">
              {else}
                <div class="choix_pedagogique{if $iso_code_oney == 'ES'} espagne{/if}">
                  <span class="bulle"><span>3</span><span>{l s='x' mod='oney'}</span></span>
                  <span class="bulle"><span>4</span><span>{l s='x' mod='oney'}</span></span>
                  {if $iso_code_oney == 'ES'}
                    <span class="bulle"><span>6</span><span>{l s='x' mod='oney'}</span></span>
                    <span class="bulle"><span>10</span><span>{l s='x' mod='oney'}</span></span>
                    {l s='and' mod='oney'}
                    <span class="bulle"><span>12</span><span>{l s='x' mod='oney'}</span></span>
                  {/if}
                </div>
                {if isset($free_transaction) && $free_transaction == 1 && $iso_code_oney != 'ES'}<p class="sans_frais">{l s='no extra fees' mod='oney'}</p>{/if}
                {if $iso_code_oney != 'PT'}<p>{l s='select the number of fees' mod='oney'}</p>{/if}
              {/if}
            </div><span class="align_oney"></span>
          </div>
        {else}
          <div class="bloc_pedagogique">
            <div class="content_align">
              <p class="title_pedagogique">
                <img src="{$module_dir|escape:'htmlall':'UTF-8'}views/img/1.png">
                <span>{l s='Choose' mod='oney'}</span>
              </p>
              <div class="align-block">
                <div class="choix_pedagogique">
                  <span class="bulle"><span>3</span><span>{l s='x' mod='oney'}</span></span>
                  <span>{l s='ou' mod='oney'}</span>
                  <span class="bulle"><span>4</span><span>{l s='x' mod='oney'}</span></span>
                </div>
                <p>{l s='Confirm your purchase and select 3x4xOney for payment' mod='oney'}</p>
                <p class="cadenas_text"><img src="{$module_dir|escape:'htmlall':'UTF-8'}views/img/cadenas.png"><span>{l s='Simple and safe' mod='oney'}</span></p>
              </div>
            </div><span class="align_oney"></span>
          </div>
        {/if}
        <div class="bloc_pedagogique">
          <div class="content_align">
            <p class="title_pedagogique">
              {if $iso_code_oney == 'IT'}<img src="{$module_dir|escape:'htmlall':'UTF-8'}views/img/2.png">{/if}
              <span>{l s='Tell us [1]a bit more[/1]' tags=['<span>'] mod='oney'}</span>
            </p>
            <div class="align-block"><p>{l s='Enjoy end quick, fill in the form, without any document' mod='oney'}</p></div>
          </div><span class="align_oney"></span>
        </div>
        <div class="bloc_pedagogique">
          <div class="content_align">
            <p class="title_pedagogique">
              {if $iso_code_oney == 'IT'}<img src="{$module_dir|escape:'htmlall':'UTF-8'}views/img/3.png">{/if}
              <span>{l s='That\'s it !' mod='oney'}</span>
            </p>
            <div class="align-block"><p>{l s='You got a direct answer !' mod='oney'}</p></div>
          </div><span class="align_oney"></span>
        </div>
      </div>
      <div class="footer_pedagogique">
        {if $iso_code_oney == 'IT'}
          {include file="./legalNoticeItaly.tpl"}
        {else}
          {if isset($legalNotice) && $legalNotice}
            {if isset($legalNotice->text) && $legalNotice->text}
              <p>
                {$legalNotice->text|escape:'htmlall':'UTF-8'}
              </p>
            {else}
              <p>
                {if $iso_code_oney == 'PT'}
                  <span class="taeg_pedago">{l s='TAEG de 0,0' mod='oney'}%</span>
                  {l s='calculada com base na TAN 0,00%2s. Crédito Pessoal para montantes de financiamento de [1]%3d€ a %4d€, pagos em 3 prestações ou de %5d€ a %6d€, pagos em 4 prestações, com cartão de débito ou de crédito das redes Visa® ou Mastercard®[/1], sem juros nem encargos.' tags=['<b>'] sprintf=['%', $listopc[3].minimum|escape:'htmlall':'UTF-8', $listopc[3].maximum|escape:'htmlall':'UTF-8', $listopc[4].minimum|escape:'htmlall':'UTF-8', $listopc[4].maximum|escape:'htmlall':'UTF-8'] mod='oney'}
                  <br>
                  {l s='Adesão ao crédito sujeito à aprovação. Informe-se junto do Oney Bank - Sucursal em Portugal em oney.pt.' mod='oney'}
                  {if Configuration::get('FACILIPAY_INTERMEDIAIRE_CREDIT_'|cat:$iso_code_oney, null) !== 'no_credit'}
                    {if Configuration::get('FACILIPAY_INTERMEDIATION_TYPE_PT') == "linked"}
                      {assign var=exclusivity value="em regime de exclusividade"}
                    {else}
                      {assign var=exclusivity value="sem exclusividade"}
                    {/if}
                    <br>
                    {l s='O/A %1s deste website atua como intermediário de crédito a título acessório %2s.' sprintf=[Configuration::get('FACILIPAY_MERCHANT_NAME_PT'), $exclusivity] mod='oney'}
                    <br>
                    {assign var="file{$iso_code_oney}" value={Oney::getUploadedFile($iso_code_oney)|escape:'htmlall':'UTF-8'}}
                    {if $file{$iso_code_oney}}
                        {l s='Informação referente à prestação de serviços de intermediação de crédito disponível ' mod='oney'} <a target="_blank" href="{$file{$iso_code_oney}|escape:'htmlall':'UTF-8'}">{l s='aqui' mod='oney'}</a>
                    {else}
                      {l s='Informação referente à prestação de serviços de intermediação de crédito disponível no rodapé desta página.' mod='oney'}
                    {/if}
                  {/if}
                {elseif $iso_code_oney == 'ES'}
                  {l s='Sujeto a la aprobación de Oney Servicios Financieros EFC, SAU. Entidad financiera registrada y supervisada por el Banco de España con número 8814.' mod='oney'}
                  <a href="https://www.oney.es/wp-content/uploads/2020/01/Info_legal_3x4x_Oney-1.pdf" target="_blank">
                    {l s='Más info' mod='oney'}
                  </a>
                {/if}
              </p>
            {/if}
          {else}
            <p>
              {if $iso_code_oney == 'PT'}
                <span class="taeg_pedago">{l s='TAEG de 0,0' mod='oney'}%</span>
                  {l s='calculada com base na TAN 0,00%2s. Crédito Pessoal para montantes de financiamento de [1]%3d€ a %4d€, pagos em 3 prestações ou de %5d€ a %6d€, pagos em 4 prestações, com cartão de débito ou de crédito das redes Visa® ou Mastercard®[/1], sem juros nem encargos.' tags=['<b>'] sprintf=['%', $listopc[3].minimum|escape:'htmlall':'UTF-8', $listopc[3].maximum|escape:'htmlall':'UTF-8', $listopc[4].minimum|escape:'htmlall':'UTF-8', $listopc[4].maximum|escape:'htmlall':'UTF-8'] mod='oney'}
                  <br>
                {l s='Adesão ao crédito sujeito à aprovação. Informe-se junto do Oney Bank - Sucursal em Portugal em oney.pt.' mod='oney'}
                {if Configuration::get('FACILIPAY_INTERMEDIAIRE_CREDIT_'|cat:$iso_code_oney, null) !== 'no_credit'}
                  {if Configuration::get('FACILIPAY_INTERMEDIATION_TYPE_PT') == "linked"}
                    {assign var=exclusivity value="em regime de exclusividade"}
                  {else}
                    {assign var=exclusivity value="sem exclusividade"}
                  {/if}
                  <br>
                  {l s='O/A %1s atua como intermediário de crédito a título acessório %2s.' sprintf=[Configuration::get('FACILIPAY_MERCHANT_NAME_PT'), $exclusivity] mod='oney'}
                  <br>
                  {assign var="file{$iso_code_oney}" value={Oney::getUploadedFile($iso_code_oney)|escape:'htmlall':'UTF-8'}}
                  {if $file{$iso_code_oney}}
                      {l s='Informação referente à prestação de serviços de intermediação de crédito disponível ' mod='oney'} <a target="_blank" href="{$file{$iso_code_oney}|escape:'htmlall':'UTF-8'}">{l s='aqui' mod='oney'}</a>
                  {else}
                    {l s='Informação referente à prestação de serviços de intermediação de crédito disponível no rodapé desta página.' mod='oney'}
                  {/if}
                {/if}
              {elseif $iso_code_oney == 'ES'}
                {l s='Sujeto a la aprobación de Oney Servicios Financieros EFC, SAU. Entidad financiera registrada y supervisada por el Banco de España con número 8814.' mod='oney'}
                <a href="https://www.oney.es/wp-content/uploads/2020/01/Info_legal_3x4x_Oney-1.pdf" target="_blank">
                  {l s='Más info' mod='oney'}
                </a>
              {/if}
            </p>
          {/if}
        {/if}
      </div>
    </div>
  </div><span class="align_oney"></span>
</div>
