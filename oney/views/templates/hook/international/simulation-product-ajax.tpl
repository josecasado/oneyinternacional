{*
 * 3x 4x Oney Module version 1.1.21 for PrestaShop 1.6. Support contact : prestashop@itroom.fr
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/afl-3.0.php
 *
 * @author    ITroom (http://itroom.fr/)
 * @copyright ITroom
 * @license   https://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * @category  payment
 * @package   oney
 *}

{if isset($simulation) && is_object($simulation)}
  {if !isset($simulation->total_cost)}
    {assign var=total_cost_oney value='0'}
  {else}
    {assign var=total_cost_oney value=$simulation->total_cost}
  {/if}
	<div class="simulation_oney_more{if $iso_code_oney == 'IT'} italy{/if}">{if $iso_code_oney == 'IT'}<span><span>{l s='or' mod='oney'}</span></span>{/if}</div>
	<div class="simulation_oney_financement international clearfix{if $iso_code_oney == 'IT'} simulation_oney_financement_ita{elseif $iso_code_oney == 'ES'} instalment-{$simulation->instalments|@count + 1|escape:'htmlall':'UTF-8'}{/if}">
		<div class="simulation_oney_img">
      <span class="bulle"><span>{$simulation->instalments|@count + 1|escape:'htmlall':'UTF-8'}</span><span>{l s='x' mod='oney'}</span></span>
		</div>
    {if version_compare($smarty.const._PS_VERSION_, '1.7', '<')}
		  {include file="./16/simulation.tpl"}
    {else}
		  {include file="./17/simulation.tpl" simulation=$simulation}
    {/if}
	</div>
{/if}
