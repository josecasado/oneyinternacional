{*
 * 3x 4x Oney Module version 1.1.21 for PrestaShop 1.6. Support contact : prestashop@itroom.fr
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/afl-3.0.php
 *
 * @author    ITroom (http://itroom.fr/)
 * @copyright ITroom
 * @license   https://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * @category  payment
 * @package   oney
 *}
{if isset($simulation) && is_object($simulation) && Configuration::get('FACILIPAY_INTERMEDIAIRE_CREDIT_'|cat:$iso_code_oney, null) !== 'no_credit'}
	<div id="simulation_oney_product" class="{if version_compare($smarty.const._PS_VERSION_, '1.7', '>=')}simulation_oney_right {/if}international{if $iso_code_oney == 'IT'} italy{elseif $iso_code_oney == 'ES'} espagne{/if}">
		<div class="simulation_oney_header">
      {if version_compare($smarty.const._PS_VERSION_, '1.7', '<')}
        <div class="close_simulation close_16"></div>
      {else}
        <div class="close_simulation oney_icons">
          <i class="material-icons">&#xe5cd;</i>
        </div>
      {/if}
      {if $iso_code_oney == 'PT'}
        <img src="{$module_dir|escape:'htmlall':'UTF-8'}views/img/logo_3-4-fois.png" alt="{l s='Oney' mod='oney'}">
        <span class="sem-custos">{l s='sem custos' mod='oney'}</span>
      {else}
			  <img src="{$module_dir|escape:'htmlall':'UTF-8'}views/img/logo_oney.png" alt="{l s='Oney' mod='oney'}">
      {/if}
			<div class="title_block">
				{if isset($free_transaction) && $free_transaction == 1 && $iso_code_oney != 'PT'}
					<span>{l s='Payment [1]no extra fees[/1] [2]by credit card[/2]' mod='oney' tags=['<span class="sf">', '<span class="simulation_oney-cart">']}</span>
				{else}
					<span>{l s='Payment [1]by credit card[/1]' mod='oney' tags=['<span class="simulation_oney-cart">']}</span>
				{/if}
			</div>
		</div>
    <div class="simulation_oney_bouton">
      <p class="amount_simulation">{l s='Amount to finance' mod='oney'}
        {* {$simulation->payment_amount|escape:'htmlall':'UTF-8'} *}
      </p>
      {if !empty($listOPC[0].example)}
      <div>
        <p>{$listOPC[0].example|escape:'htmlall':'UTF-8'}<p>
      </div>
      {/if}
    </div>
    <div class="simulation_oney_content"></div>
    <span class="btn_pedagogique tellmemore">{l s='Tell me more' mod='oney'}</span>
    {if $iso_code_oney == 'PT'}
      <div class="logo_oney_footer_simulation">
        <img src="{$module_dir|escape:'htmlall':'UTF-8'}views/img/logo_oney_banner.png" alt="{l s='Oney' mod='oney'}">
      </div>
    {/if}
	</div>
{/if}